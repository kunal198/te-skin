//
//  A3ViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSViewController.h"
#import "MOOCViewController.h"
#import "SelectedAnswers.h"
#import <QuartzCore/QuartzCore.h>
#import "AnswersViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "MoreViewController.h"
#import "ProfileViewController.h"

@class AppDelegate;

extern UIImageView *profileImage;
extern BOOL isSelected;



@interface A3ViewController : UIViewController<UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSMutableArray *colletionArray;
    NSArray *mediaTypes;
    int i ;
    UIImage *thumbnail;
    
    UIButton *rightArrowBtn;
    UIButton *leftArrowBtn;
    AppDelegate *appDelegate;
    NSMutableArray *headerArray;
    UIImage *profile;
    UIButton *btn;
    int index;
    
    }
@property (weak, nonatomic) IBOutlet UIButton *ssGraybtn;
@property (weak, nonatomic) IBOutlet UIButton *moocGrayBtn;
@property (strong,nonatomic) NSString *selectedAskPro;
@property (strong,nonatomic) NSString *text;


- (IBAction)btnOrangeA3:(id)sender;

- (IBAction)btnGrayMOOC:(id)sender;

- (IBAction)btnGraySS:(id)sender;

- (IBAction)backToA3:(id)sender;

- (IBAction)plusBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *A3HighlightedView;

@property (weak, nonatomic) IBOutlet UIImageView *navigationImage;

@property (weak, nonatomic) IBOutlet UIScrollView *a3ScrollView;

@property (weak, nonatomic) IBOutlet UIView *bottomBtnBg;
- (IBAction)shareBtn:(id)sender;

- (IBAction)askBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *A3ImageOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *SSOutletImage;

@property (weak, nonatomic) IBOutlet UIImageView *MOOCImageOutlet;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *thirdBtn;
@property (weak, nonatomic) IBOutlet UIButton *fourthBtn;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (nonatomic, assign) BOOL isFromProfile;
@property (weak, nonatomic) IBOutlet UILabel *selectedAskProLbl;

-(void)aws;

@end
