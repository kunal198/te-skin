//
//  A3ViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "A3ViewController.h"
#import "AppDelegate.h"
#import "TalentEdgeHeader.h"
#import <AWSCore/AWSCore.h>
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <AWSSQS/AWSSQS.h>
#import <AWSSNS/AWSSNS.h>
#import <AWSCognito/AWSCognito.h>


UIImageView *profileImage;// = nil;
BOOL isSelected = nil;
@interface A3ViewController ()
{
    BOOL isDisableLeftArrow;
    int j;
//    AWSStaticCredentialsProvider *credentialsProvider;
//    AWSServiceConfiguration *configuration;
//    AWSS3 *transferManager;
    
}
@end


@implementation A3ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    index = 0;
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.selectedAskProLbl.text = self.selectedAskPro;
    
    self.ssGraybtn.hidden = YES;
    self.moocGrayBtn.hidden = YES;
    
    
    j = 0;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        self.firstBtn.frame = CGRectMake(198,23,50, 50);
        self.secondBtn.frame = CGRectMake(270,25,100,100);
        self.thirdBtn.frame = CGRectMake(400, 25, 100, 100);
        self.fourthBtn.frame = CGRectMake(522,23,50, 50);
        
        _A3ImageOutlet.frame = CGRectMake(_A3ImageOutlet.frame.origin.x, _A3ImageOutlet.frame.origin.y, _A3ImageOutlet.frame.size.width, 50);
        _MOOCImageOutlet.frame = CGRectMake(_MOOCImageOutlet.frame.origin.x, _MOOCImageOutlet.frame.origin.y, _MOOCImageOutlet.frame.size.width, 50);
        
        _SSOutletImage.frame = CGRectMake(_SSOutletImage.frame.origin.x, _SSOutletImage.frame.origin.y, _SSOutletImage.frame.size.width, 50);
        
    }
    isDisableLeftArrow = true;
    
    if (_isFromProfile)
    {
        _navigationImage.image = [UIImage imageNamed:@"user_icon1.png.png"];
    }
    
    self.a3ScrollView.delegate = self;
    
    if (self.view.frame.size.height == 480 && self.view.frame.size.width == 320)
    {
        self.firstBtn.frame = CGRectMake(70, 8, 30, 28);
        self.secondBtn.frame = CGRectMake(105,12,51,48);
        self.thirdBtn.frame = CGRectMake(164, 12, 51, 48);
        self.fourthBtn.frame = CGRectMake(220, 8, 30, 28);
    }
    
    
    

//   if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"guide"] == NO)
//    {
//        [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(pushMethod:) userInfo:nil repeats:NO];
//    
//        self.A3HighlightedView.hidden = NO;
//    
//    }
    
    colletionArray = [NSMutableArray new];
    [colletionArray addObjectsFromArray:[appDelegate GlobalCollectionProfile]];
    
    [self FillArray];
    [self addContent];
    
    _bottomBtnBg.frame = CGRectMake(_bottomBtnBg.frame.origin.x, _bottomBtnBg.frame.origin.y, _bottomBtnBg.frame.size.width, _bottomBtnBg.frame.size.height/2.7);
}


-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
    NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

-(void)pushMethod:(NSTimer *)timer
{
    
    MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
    [self.navigationController pushViewController:mooc animated:YES];
    
    
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"pop"] == NO)
//    {
//        NSLog(@"success");
//    }
//    else
//    {
//        self.A3HighlightedView.hidden = YES;
//    }
//
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)PlayVideo:(NSString*)videoName
{
    NSArray *dummyArray = [videoName componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:streamURL];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
        [videoPlayerView.moviePlayer play];
        
        [self generateThumbImage:streamURL];
    }
}

-(void)HeaderArrayCollection
{
    headerArray = [[NSMutableArray alloc]init];
    [headerArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"bhaduri.png",@"headerImage",
                            @"ABHIJIT BHADURI",@"headerName",
                            @"the Chief Learning Officer of Wipro Ltd",@"headerAddress",
                            nil]];
    [headerArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"bhaduri.png",@"headerImage",
                            @"ABHIJIT BHADURI",@"headerName",
                            @"the Chief Learning Officer of Wipro Ltd",@"headerAddress",
                            nil]];
}

-(void)FillArray
{
    colletionArray = [[NSMutableArray alloc]init];
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"ABHIJIT BHADURI",@"FooterTitle",
                               @"The Chief Learning Officer of Wipro Ltd",@"FooterDescription",
                               @"RohitBhayana.mp4",@"VideoLink",
                               
                               @"bhaduri.png",@"ProfileImage",
                               @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd. Has been part of companies like Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. Abhijit writes regularly for the Economic Times, People Matters and blogs for Times of India.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"PRABIR JHA",@"FooterTitle",
                               @"Global Chief People Officer of Cipla",@"FooterDescription",
                               @"MaheshGupta.mp4",@"VideoLink",
                               @"Prabir.png",@"ProfileImage",@"Prabir Jha, an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. Prabir brings with him an impeccable academic and professional record.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"Dr AQUIL BUSRAI",@"FooterTitle",
                               @"Chief Executive Officer at aquil busrai consulting",@"FooterDescription",
                               @"red_vid.mp4",@"VideoLink",
                               @"Aquil.png",@"ProfileImage",@"Dr Aquil Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. Currently runs his own consulting firm.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"SRIPADA CHANDRASEKHAR ",@"FooterTitle",
                               @"President and global head of HR at Dr. Reddy’s Laboratoires",@"FooterDescription",
                               @"s.mp4",@"VideoLink",
                               @"Sripada.png",@"ProfileImage",@"Dr. Sripada Chandrarsekhar is the global head of HR at Dr. Reddy’s Laboratoires, Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"VIKRAM BECTOR",@"FooterTitle",
                               @"Chief Talent Officer at Reliance Industries",@"FooterDescription",
                               @"tbi.mp4",@"VideoLink",
                               @"Vikram.png",@"ProfileImage",@"Vikram Bector - Chief Talent Officer at Reliance Industries Limited. Over two decades of experience with some leading Indian conglomerates.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"VIVEK PARANJPE",@"FooterTitle",
                               @"Management consultant & Executive",@"FooterDescription",
                               @"tbi.mp4",@"VideoLink",
                               @"Vivek Paranjpe.jpg",@"ProfileImage",@"Vivek has more than 40 years of work experience both in India and abroad. He has worked in various HR roles with Reliance Industries, Hewlett Packard, Hindustan Lever and few companies like ICIM, Hotel Corporation of India and Johnson & Johnson",@"info",nil]];
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"M R PARKER",@"FooterTitle",
                               @"Independent Director and Chief Mentor Quess corporation",@"FooterDescription",
                               @"tbi.mp4",@"VideoLink",
                               @"M. Parker.jpg",@"ProfileImage",@"Marcel has 38 years of experience in the Corporate world in some of India’s and the World’s best organizations-Voltas, ITC, Coromandel Fertilizers, The East India Hotels, Xerox, The Royal Dutch/Shell Group, SAP India and the Raymond Group.",@"info",nil]];
//    
//    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                               @"Problems in HR Industry",@"HeaderTitle",
//                               @"3",@"Answers",
//                               @"MANUFACTURING",@"FooterTitle",
//                               @"Work in Manufacturing Department",@"FooterDescription",
//                               @"MarutiSuzuki.mp4",@"VideoLink",
//                               @"user1.png",@"ProfileImage",nil]];
//    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                               @"Don't hire the best",@"HeaderTitle",
//                               @"2",@"Answers",
//                               @"DRIVING PRODUCTS",@"FooterTitle",
//                               @"Chief Learning Officer, Wipro",@"FooterDescription",
//                               @"HRServices.mp4",@"VideoLink",
//                               
//                               @"user.png",@"ProfileImage",nil]];
//    
//    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                               @"Problems in HR Industry",@"HeaderTitle",
//                               @"3",@"Answers",
//                               @"MANUFACTURING",@"FooterTitle",
//                               @"Work in Manufacturing Department",@"FooterDescription",
//                               @"MarutiSuzuki.mp4",@"VideoLink",
//                               @"user1.png",@"ProfileImage",nil]];
//    
//    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                               @"Don't hire the best",@"HeaderTitle",
//                               @"2",@"Answers",
//                               @"DRIVING PRODUCTS",@"FooterTitle",
//                               @"Chief Learning Officer, Wipro",@"FooterDescription",
//                               @"HRServices.mp4",@"VideoLink",
//                               @"user.png",@"ProfileImage",nil]];
//    
//    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                               @"Problems in HR Industry",@"HeaderTitle",
//                               @"3",@"Answers",
//                               @"MANUFACTURING",@"FooterTitle",
//                               @"Work in Manufacturing Department",@"FooterDescription",
//                               @"MarutiSuzuki.mp4",@"VideoLink",
//                               @"user1.png",@"ProfileImage",nil]];
}

-(void)addContent
{
    
    
    int noOfItems = (int)colletionArray.count;
    
    [self.a3ScrollView setContentSize:(CGSizeMake(self.a3ScrollView.frame.size.width*(noOfItems), self.a3ScrollView.frame.size.height))];
    
    
    
    for(i = 0; i < noOfItems; i++)
    {
        
        UIView *mainViewWrapper = [[UIView alloc]initWithFrame:CGRectMake(i*self.a3ScrollView.frame.size.width, 0, self.a3ScrollView.frame.size.width, self.a3ScrollView.frame.size.height)];
        [mainViewWrapper setBackgroundColor:[UIColor clearColor]];
        
        [self.a3ScrollView addSubview:mainViewWrapper];
        
        UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, mainViewWrapper.frame.size.width - 20, mainViewWrapper.frame.size.height - 30)];
        [mainView setBackgroundColor:[UIColor clearColor]];
        mainView.layer.masksToBounds = true;
        mainView.layer.cornerRadius = 8;
        mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        mainView.layer.borderWidth = 0.8f;
        [mainViewWrapper addSubview:mainView];
        
        float headerFooterHeight = mainView.frame.size.height/4;
        
        NSLog(@"headerFooterHeight %f",headerFooterHeight);
        
        float width = mainView.frame.size.width;
        float height = mainView.frame.size.height - 150;
        
        NSLog(@"%f",height);
        
        float x = 0;      // here
        float y = 0;      // here
        float space = 10;
        
        
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, headerFooterHeight)];
        [headerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
        [mainView addSubview:headerView];
        
        float lblHeight = (headerView.frame.size.height - space*3)/3 ;
        
        
        UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(x, mainView.frame.size.height - headerFooterHeight, width, headerFooterHeight)];
        [footerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
        [mainView addSubview:footerView];
        
        profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(space,space/4,headerView.frame.size.height - (space*0.7),headerView.frame.size.height - (space*0.7))];
        [profileImage setBackgroundColor:[UIColor clearColor]];
        //[self.itemImageButton setImage:[UIImage imageNamed:stretchImage] forState:UIControlStateNormal];
        UIImage *profileImageName = [UIImage imageNamed:[[colletionArray objectAtIndex:i] valueForKey:@"ProfileImage"]];
        profileImage.image = profileImageName;
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2;
        profileImage.layer.borderWidth = 2.0;
        profileImage.layer.masksToBounds = YES;

        profileImage.layer.borderColor = [[UIColor whiteColor]CGColor];
        profileImage.contentMode = UIViewContentModeScaleAspectFit;
        
        
        profile = profileImage.image;
        [headerView addSubview:profileImage];
        
        
        
        
        
        ///profile image Button///
        
        
        UIButton *profileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [profileBtn addTarget:self action:@selector(profileBtnClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        profileBtn.backgroundColor = [UIColor clearColor];
        profileBtn.tag = i+777;
        //[profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //profileBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        profileBtn.frame = CGRectMake(space,space,headerView.frame.size.height - (space*2),headerView.frame.size.height - (space*2));
        [headerView addSubview:profileBtn];
        
        
        //--more button -------//
//        UIButton *moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [moreBtn addTarget:self action:@selector(moreClicked:)
//          forControlEvents:UIControlEventTouchUpInside];
//        [moreBtn setTitle:@"More" forState:UIControlStateNormal];
//        moreBtn.backgroundColor = [UIColor clearColor];
//        moreBtn.tag = i;
//        [moreBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
//        {
//            moreBtn.titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
//        }
//        else
//        {
//            moreBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
//        }
//
//        moreBtn.frame = CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*1.5)),space,footerView.frame.size.height - (lblHeight+(space*1.5)),lblHeight);
//        [headerView addSubview:moreBtn];
        
        UIImageView *voteImage = [[UIImageView alloc] initWithFrame:CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5)),lblHeight,footerView.frame.size.height - footerView.frame.size.height - (lblHeight+(space*1.5)),footerView.frame.size.height - (lblHeight+(space*2.5)))];
        
        UIImage *voteImageIcon = [UIImage imageNamed:@"tlt_upvotes1.png"];
        voteImage.image = voteImageIcon;
        [voteImage setBackgroundColor:[UIColor clearColor]];
        [headerView addSubview:voteImage];
        
        
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), space, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight + (space/2))];
        [titleLbl setTextColor:[UIColor darkGrayColor]];
        [titleLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"FooterTitle"]];
        [titleLbl setBackgroundColor:[UIColor clearColor]];
        //[titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
        [headerView addSubview:titleLbl];
        
        
        UILabel *userNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), lblHeight+(space*1.5), width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
        userNameLbl.numberOfLines = 2;
        [userNameLbl setTextColor:[UIColor blackColor]];
       // [userNameLbl setTextColor:[UIColor colorWithRed:157.0f/255.0f green:157.0f/255.0f blue:157.0f/255.0f alpha:1]];
        
        [userNameLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"FooterDescription"]];
        [userNameLbl setBackgroundColor:[UIColor clearColor]];
        //[userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
        [headerView addSubview:userNameLbl];
        
        
        
//        UILabel *headerLbl = [[UILabel alloc]init];
//        [headerLbl setTextColor:[UIColor darkGrayColor]];
//        // [answerLbl setBackgroundColor:[UIColor clearColor]];
//        [headerLbl setFont:[UIFont fontWithName: @"Helvetica Neue" size: 20.0f]];
//        [headerLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"HeaderTitle"]];
//        CGSize answerFrame = [headerLbl sizeThatFits:headerLbl.bounds.size];
//        headerLbl.frame = CGRectMake((headerView.frame.origin.x) + (space), (footerView.frame.size.height - answerFrame.height)/2 - (space), answerFrame.width, answerFrame.height);
//        [footerView addSubview:headerLbl];
        
//        UILabel *answerLbl = [[UILabel alloc]init];
//        [answerLbl setTextColor:[UIColor colorWithRed:157.0f/255.0f green:157.0f/255.0f blue:157.0f/255.0f alpha:1]];
//        // [answerLbl setBackgroundColor:[UIColor clearColor]];
//        [answerLbl setFont:[UIFont fontWithName: @"Helvetica Neue" size: 14.0f]];
//        [answerLbl setText:[NSString stringWithFormat:@"%@ ANSWERS",[[colletionArray objectAtIndex:i] valueForKey:@"Answers"]]];
//        answerLbl.frame = CGRectMake((headerView.frame.origin.x) + (space),(headerLbl.frame.origin.y + headerLbl.frame.size.height + space/2), answerFrame.width, answerFrame.height);
//        [footerView addSubview:answerLbl];
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            
            [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 20.0f]];
            [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 18.0f]];
           // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 20.0f]];
        }
        else
        {
            [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
            [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
           // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 14.0f]];
        }

        
        UIButton *but = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        //[but setFrame:answerLbl.frame];
        but.tag = i +333;
        [but setExclusiveTouch:YES];
        [footerView addSubview:but];
        
        float combineHeightOfFooterHeader = mainView.frame.size.height - (headerView.frame.size.height + footerView.frame.size.height);
        
        UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(x, headerView.frame.size.height, width, combineHeightOfFooterHeader)];
        [centerView setBackgroundColor:[UIColor clearColor]];
        [mainView addSubview:centerView];
        
        NSLog(@" main view %f and %f",centerView.frame.size.width, centerView.frame.size.height);
        
        UIImageView *centerImage = [[UIImageView alloc]initWithFrame:CGRectMake(x, 0 , width, combineHeightOfFooterHeader)];
        [centerImage setBackgroundColor:[UIColor clearColor]];
        
        NSArray *dummyArray = [[[colletionArray objectAtIndex:i] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
        
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        UIImage *staticImage = [self generateThumbImage:streamURL];
      //  centerImage.image = staticImage;
        
        centerImage.image = [UIImage imageNamed:[[colletionArray objectAtIndex:i] valueForKey:@"ProfileImage"]];
        [centerImage setContentMode: UIViewContentModeScaleAspectFit];
        [centerImage setBackgroundColor:[UIColor blackColor]];
        [centerView addSubview:centerImage];
        
        NSLog(@"voteImage.frame.size.width is %f %f",voteImage.frame.size.width,centerView.frame.size.height);
        
        ///////Header button //////
        
        UIButton *headerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [headerBtn addTarget:self action:@selector(profileBtnClicked:)
            forControlEvents:UIControlEventTouchUpInside];
        headerBtn.backgroundColor = [UIColor clearColor];
        headerBtn.tag = i+777;
        
        //[profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //profileBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        headerBtn.frame = CGRectMake(x, y, width- voteImage.frame.size.width, headerFooterHeight);
        [headerView addSubview:headerBtn];
        

        
        
       UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, centerView.frame.size.width, centerView.frame.size.height)];
       //   UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x + centerView.frame.size.width / 8 + (space/2), 0 , width - ((centerView.frame.size.width / 8 + (space/2))*2), combineHeightOfFooterHeader)];
        [playBtn setBackgroundColor:[UIColor clearColor]];
        //  UIImage *playImage = [UIImage imageNamed:@"play.png"];
        playBtn.tag = i+999;
        // [playBtn setBackgroundImage:playImage forState:UIControlStateNormal];
        //[playBtn setBackgroundColor:[UIColor redColor]];
        [playBtn addTarget:self action:@selector(playClicked:) forControlEvents:UIControlEventTouchUpInside];
        [centerView addSubview:playBtn];
        
        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playbutton.png"]];
        
        img.frame = CGRectMake((centerView.frame.size.width - (centerView.frame.size.width/6)) / 2, (centerView.frame.size.height - (centerView.frame.size.width/6)) / 2, centerView.frame.size.width / 6, centerView.frame.size.width / 6);
        
        [centerView addSubview:img];
        
        UITextView *infoText = [[UITextView alloc]init];
        [infoText setText:[[colletionArray objectAtIndex:i] valueForKey:@"info"]];
        infoText.frame = CGRectMake(0, 0 ,footerView.frame.size.width , footerView.frame.size.height );
        
        [infoText setFont:[UIFont fontWithName:@"Helvetica-Light" size:9]];
        //infoText.backgroundColor = [UIColor redColor];
        
        [footerView addSubview:infoText];
        
        
        infoText.scrollEnabled = NO;
        infoText.selectable = NO;
        
        
        //        UILabel *headerLbl = [[UILabel alloc]init];
        //        [headerLbl setTextColor:[UIColor darkGrayColor]];
        //        // [answerLbl setBackgroundColor:[UIColor clearColor]];
        //        [headerLbl setFont:[UIFont fontWithName: @"Helvetica Neue" size: 20.0f]];
        //        [headerLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"HeaderTitle"]];
        //        CGSize answerFrame = [headerLbl sizeThatFits:headerLbl.bounds.size];
        //        headerLbl.frame = CGRectMake((headerView.frame.origin.x) + (space), (footerView.frame.size.height - answerFrame.height)/2 - (space), answerFrame.width, answerFrame.height);
        //        [footerView addSubview:headerLbl];
        
//        rightArrowBtn = [[UIButton alloc]initWithFrame:CGRectMake(centerView.frame.size.width-((space/2)+centerView.frame.size.width / 8), (centerView.frame.size.height - (centerView.frame.size.width/8)) / 2,centerView.frame.size.width / 8, centerView.frame.size.width / 8)];
//        [rightArrowBtn setBackgroundColor:[UIColor clearColor]];
//        rightArrowBtn.tag = i+555;
//        [rightArrowBtn setBackgroundImage:[UIImage imageNamed:@"arrowright.png"] forState:UIControlStateNormal];
//        
//     //   [rightArrowBtn addTarget:self action:@selector(rightArrowClicked:) forControlEvents:UIControlEventTouchUpInside];
//        
//        [centerView addSubview:rightArrowBtn];
//        
//        leftArrowBtn = [[UIButton alloc]initWithFrame:CGRectMake((space/2), (centerView.frame.size.height - (centerView.frame.size.width/8)) / 2,centerView.frame.size.width / 8, centerView.frame.size.width / 8)];
//        [leftArrowBtn setBackgroundImage:[UIImage imageNamed:@"arrowleft.png"] forState:UIControlStateNormal];
//        [leftArrowBtn setBackgroundColor:[UIColor clearColor]];
//        leftArrowBtn.tag = i+1111;
//        
//        if (i == 0)
//        {
//            leftArrowBtn.enabled =false;
//        }
//        NSLog(@"i+1111 is %d",i+1111);
        
     //   [leftArrowBtn addTarget:self action:@selector(leftArrowClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [centerView addSubview:leftArrowBtn];
    }
}

-(void)profileBtnClicked:(UIButton*)sender
{
    NSLog(@"profileBtnClicked");
    
//    NSString *videoPath = @"https://s3-us-west-2.amazonaws.com/talentedge/HRServices.mp4";
//    
//    NSLog(@"videoPath is %@",videoPath);
//    
//    NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
//    
//    NSLog(@"streamURL is %@",streamURL);
//    
//    MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:videoPath]];
//    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
//    [videoPlayerView.moviePlayer play];
//
    
    ProfileViewController *profiles = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    profiles.passImage = [[colletionArray objectAtIndex:(int)[sender tag]-777]valueForKey:@"FooterTitle"];
    profiles.indexValue = (int)[sender tag]-777;
    [self presentViewController:profiles animated:YES completion:nil];
}


//
//-(void)leftArrowClicked:(UIButton*)sender
//{
//    if (_a3ScrollView.contentOffset.x > 0)
//    {
//        leftArrowBtn.enabled=true;
//        [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x-self.a3ScrollView.frame.size.width, 0))animated:NO];
//    }
//    else
//    {
//        [_a3ScrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
//        // leftArrowBtn.enabled=false;
//        UIButton *buttonWithTag1 = (UIButton *)[self.a3ScrollView viewWithTag:[sender tag]];
//        buttonWithTag1.enabled = false;
//        
//    }
//    NSLog(@"left clicked");
//    
//}
//
//-(void)rightArrowClicked:(UIButton*)sender
//{
//    if (_a3ScrollView.contentOffset.x+self.a3ScrollView.frame.size.width == _a3ScrollView.contentSize.width)
//    {
//        //          [_a3ScrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
//        rightArrowBtn.enabled =false;
//        
//        //        UIButton *buttonWithTag1 = (UIButton *)[self.a3ScrollView viewWithTag:[sender tag]];
//        //        buttonWithTag1.hidden = YES;
//    }
//    else
//    {
//        rightArrowBtn.enabled=true;
//        //        int noOfItems = (int)colletionArray.count;
//        //        if (_a3ScrollView.contentOffset.x >= noOfItems*_a3ScrollView.frame.size.width)
//        //        {
//        //            [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x + _a3ScrollView.frame.size.width , 0))animated:NO];
//        //        }
//        [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x + _a3ScrollView.frame.size.width , 0))animated:NO];
//        //        rightArrowBtn.hidden=false;
//    }
//}

-(void) buttonClicked:(UIButton*)sender
{
    NSLog(@"more is %li",(long)sender.tag-333);
    NSLog(@"more.name is %@",[[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"FooterTitle"]);
    
    MoreViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreViewController"];
    more.name = [[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"FooterTitle"];
    more.address = [[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"FooterDescription"];
    
    
    NSLog(@"dat is %@",[[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"ProfileImage"]);
    
    more.moreImage = [[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"ProfileImage"];
    [self presentViewController:more animated:YES completion:nil];
}

-(void)playClicked:(UIButton*)sender
{
    NSLog(@"Play clicked");
//
//    NSArray *dummyArray = [[[colletionArray objectAtIndex:(int)[sender tag]-999] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
//    
//    if(dummyArray.count != 0)
//    {
//        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
//        
//        NSLog(@"videoPath is %@",videoPath);
//        
//        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
//        
//        NSLog(@"streamURL is %@",streamURL);
//        
//        MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:streamURL];
//        [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
//        [videoPlayerView.moviePlayer play];
//    }
    
}

-(void)moreClicked:(UIButton*)sender
{
    NSLog(@"more");
    MoreViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreViewController"];
    //more.setImage = [UIImage imageNamed:[[colletionArray objectAtIndex:sender.tag] valueForKey:@"ProfileImage"]];
    more.name = [[colletionArray objectAtIndex:sender.tag] valueForKey:@"FooterTitle"];
    more.address = [[colletionArray objectAtIndex:sender.tag] valueForKey:@"FooterDescription"];
    [self presentViewController:more animated:YES completion:nil];
}


- (IBAction)btnOrangeA3:(id)sender
{
    
}

- (IBAction)btnGrayMOOC:(id)sender
{
//    MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
//    [self.navigationController pushViewController:mooc animated:YES];
}


- (IBAction)btnGraySS:(id)sender
{
    // SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
    //  [self.navigationController popToViewController:ss animated:YES];
    
 //   [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    NSLog(@"j is %i",j);
    if (scrollView == _a3ScrollView)
    {
        
        if (_a3ScrollView.contentOffset.x > 0)
        {
            if (_a3ScrollView.contentOffset.x+self.a3ScrollView.frame.size.width == _a3ScrollView.contentSize.width)
            {
                rightArrowBtn.enabled=false;
            }
            else
            {
                rightArrowBtn.enabled=true;
            }
            leftArrowBtn.enabled=true;
        }
        else
        {
            leftArrowBtn.enabled=false;
        }
        
        //        if (_a3ScrollView.contentOffset.x == 0)
        //        {
        //            self.leftArrow.hidden = NO;
        //            self.leftArrow.enabled = NO;
        //        }
        //        else
        //        {
        //            self.leftArrow.hidden = NO;
        //            self.leftArrow.enabled = YES;
        //        }
        //        int noOfItems = (int)colletionArray.count;
        //        if (scrollView.contentOffset.x >= noOfItems*_a3ScrollView.frame.size.width)
        //        {
        //          [scrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
        //        }
    }
    
    CGFloat pageWidth = scrollView.frame.size.width; // you need to have a iVar with getter for scrollView
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    index = (int)page;
    
    NSLog(@"index is %d",index);
}

- (IBAction)shareBtn:(id)sender
{
    NSString *fileName = [NSString stringWithFormat:@"FirstVideo"];
    NSURL *fileURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:fileName]];
    
    NSLog(@"fileURL is %@",fileURL);
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
//    NSString *documentPath = [paths objectAtIndex:0];
//    NSURL *url = [NSURL fileURLWithPath:documentPath];
    
    
    AWSS3TransferManagerDownloadRequest *uploadRequest = [AWSS3TransferManagerDownloadRequest new];
    uploadRequest.bucket = @"talentedge";
    uploadRequest.key = @"RohitBhayana.mp4";
    uploadRequest.downloadingFileURL = fileURL;
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
     [[transferManager download:uploadRequest] continueWithBlock:^id _Nullable(AWSTask * _Nonnull task)
    {
        if (task.error)
        {
            NSLog(@"task.error is %@",task.error);
            
        }
        if (task.result)
        {
             NSLog(@"task.result is %@",task.result);
        }
         
         return nil;
     }];

    
    
    
    
    
//    NSLog(@"ShareBtn pressed fro sharing on social media");
//    
//    NSString *messages = @"Talent Edge Video";
//    
//    NSArray * shareItems = @[messages];
//    
//    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
//    
//    [self presentViewController:avc animated:YES completion:nil];

}


- (IBAction)askBtn:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        picker.videoMaximumDuration = 30.0f;
        
        mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        
        picker.mediaTypes = mediaTypes;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    else
    {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"No Camera in this Device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}
//- (IBAction)rightArrow:(id)sender
//{
//    NSLog(@"right clicked");
//
//    int noOfItems = (int)colletionArray.count;
//    if (_a3ScrollView.contentOffset.x >= noOfItems*_a3ScrollView.frame.size.width)
//    {
//        [_a3ScrollView setContentOffset:CGPointMake(_a3ScrollView.contentOffset.x + self.a3ScrollView.frame.size.width, 0)animated:NO];
//    }
//    [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x + self.a3ScrollView.frame.size.width, 0))animated:NO];
//}

//- (IBAction)leftArrow:(id)sender
//{
//    NSLog(@"left clicked");
//
//    if (_a3ScrollView.contentOffset.x == 0)
//    {
//        [_a3ScrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
////        if (isDisableLeftArrow == NO)
////        {
//            self.leftArrow.hidden = NO;
//            self.leftArrow.enabled = NO;
////        }
////        else
////        {
////            self.leftArrow.hidden = NO;
////            self.leftArrow.enabled = YES;
////        }
//
//    }
//    else
//    {
//            [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x - self.a3ScrollView.frame.size.width, 0))animated:NO];
//        self.leftArrow.hidden = NO;
//        self.leftArrow.enabled = YES;
//    }
//}
- (IBAction)backToA3:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)plusBtn:(id)sender
{
    
   NSLog(@"sender: %@", sender);
    MoreViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreViewController"];
    //more.setImage = [UIImage imageNamed:[[colletionArray objectAtIndex:sender.tag] valueForKey:@"ProfileImage"]];
    more.name = [[colletionArray objectAtIndex:index] valueForKey:@"FooterTitle"];
    more.address = [[colletionArray objectAtIndex:index] valueForKey:@"FooterDescription"];
    more.moreImage = [[colletionArray objectAtIndex:index] valueForKey:@"ProfileImage"];
    [self presentViewController:more animated:YES completion:nil];
}

-(void)aws
{
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"RohitBhayana" ofType:@"mp4"];
    
    NSLog(@"videoPath is %@",videoPath);
    
    NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
    
    NSLog(@"streamURL is %@",streamURL);
 
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = @"talentedge";
    uploadRequest.key = @"RohitBhayana.mp4";
    uploadRequest.body = streamURL;
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id _Nullable(AWSTask * _Nonnull task)
     {
         if (task.error)
         {
             NSLog(@"error is %@",task.error);
         }
         if (task.result)
         {
             AWSS3TransferManagerUploadOutput *uploadOutput = task.result;
             
             
             NSLog(@"uploadOutput is %@",uploadOutput);
             // The file uploaded successfully.
         }
         
         return nil;
     }];

}


@end
