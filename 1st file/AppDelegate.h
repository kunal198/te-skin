//
//  AppDelegate.h
//  Target Edge
//
//  Created by Mrinal Khullar on 11/19/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginVC.h"
#import "navViewController.h"
#import "ViewController.h"
#import "A3ViewController.h"
#import "SSViewController.h"
#import <AWSCore/AWSCore.h>

#import <AWSS3/AWSS3.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <AWSSQS/AWSSQS.h>
#import <AWSSNS/AWSSNS.h>
#import <AWSCognito/AWSCognito.h>

extern NSMutableArray *CollectionOfProfileImages;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate>
{
    navViewController *_objOfNav;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) UINavigationController *navigationController;
@property (nonatomic,strong) UIStoryboard *storyboard;

-(NSMutableArray*)GlobalCollectionProfile;



@end

