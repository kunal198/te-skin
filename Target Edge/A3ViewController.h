//
//  A3ViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSViewController.h"
#import "MOOCViewController.h"
#import "SelectedAnswers.h"
#import <QuartzCore/QuartzCore.h>
#import "AnswersViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "MoreViewController.h"
#import "ProfileViewController.h"
#import "ELCImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AWSS3/AWSS3.h>
#import "Constants.h"
#define AskProAPI @"http://mobileapi.talentedge.hub1.co/api/Advisor/Advisors_Listing"
#define AskQuestionAPI @"http://mobileapi.talentedge.hub1.co/api/Advisor/AskQuestion"
#define UpvoteAPI @"http://mobileapi.talentedge.hub1.co/api/Activity/AddUpVote"

#import <CoreMedia/CoreMedia.h>
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "Reachability.h"
#import "MyQuestionsA3.h"
#import "LoginVC.h"


@class AppDelegate;

extern UIImageView *profileImage;
extern BOOL isSelected;
extern NSString *DescriptionAbout;
extern NSString *advisorID;
extern NSString *Creater_User_ID;
extern NSString *No_Questions;
extern NSString *No_Followers;
extern BOOL isA3Advisor;
extern NSString *IsFollowed;
extern NSString *LinkedIn_ID;

@interface A3ViewController : UIViewController<UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate,MBProgressHUDDelegate>
{
    NSMutableArray *colletionArray;
    NSArray *mediaTypes;
    int i ;
    NSString *fileName;
    NSString *filePath;
  
    NSString *fileNameImage;
    
    UIButton *rightArrowBtn;
    UIButton *leftArrowBtn;
    AppDelegate *appDelegate;
    NSMutableArray *headerArray;
    UIImage *profile;
    UIButton *btn;
    int index;
    NSURL *chosenMovie;
    NSURL *fileURL;
    NSData *movieData;
    NSArray *saveVideo;
    NSArray *arr;
    NSString *keyStr;
    NSMutableArray *_objOfAskPro;
    NSMutableData *AskProData;
    NSMutableArray *AskProArray;
    NSMutableArray *askQuestionArray;
    MBProgressHUD *HUD;
    NSMutableArray *PlusQuestionObj;
    NSString *AdvisorImg;
    double seconds;
    NSString *secDisplay;
    NSString *strForAlert;
    NSString *combine;
    NSString *strA3Alert;
    NSString *outputName;
    UIImage *imgUrl;
    NSString *filePathsImage;
    NSDate *dateForPictureName;
    NSURL *finalUrl;
    NSString *workSpacePath;
    NSURL *urlfile;
    NSString *newName;
    NSString *answrVideoLinkA3;
    MPMoviePlayerController *playBtnLinkA3;
    BOOL isplayAginA3;
    NSMutableArray *video_thumbnail;
    NSMutableArray *UpvoteObj;
    NSString *AnswerID;
    UILabel *lblUpvote;
    NSString *No_UpvotesCount;
    NSString *strUpvotes;
    NSString *upvote;
    BOOL isUpvoteTrue;
    int UpvoteIndex;
    NSString *strForUpvote;
    NSString *varIndex;
    BOOL isDoneA3;
    BOOL isNotRepeatAskQuestionAPI;
    
}
@property (weak, nonatomic) IBOutlet UIButton *ssGraybtn;
@property (weak, nonatomic) IBOutlet UIButton *moocGrayBtn;
@property (strong,nonatomic) NSString *selectedAskPro;
@property (strong,nonatomic) NSString *text;
-(void)saveImageOfVideo;
-(void)getImageFromDirectory;

- (IBAction)btnOrangeA3:(id)sender;

- (IBAction)btnGrayMOOC:(id)sender;

- (IBAction)btnGraySS:(id)sender;

- (IBAction)backToA3:(id)sender;

- (IBAction)plusBtn:(id)sender;

-(void)FetchProgressValue;

@property (weak, nonatomic) IBOutlet UIView *A3HighlightedView;

@property (weak, nonatomic) IBOutlet UIImageView *navigationImage;

@property (weak, nonatomic) IBOutlet UIScrollView *a3ScrollView;

@property (weak, nonatomic) IBOutlet UIView *bottomBtnBg;
- (IBAction)shareBtn:(id)sender;

- (IBAction)askBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *A3ImageOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *SSOutletImage;

@property (weak, nonatomic) IBOutlet UIImageView *MOOCImageOutlet;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *thirdBtn;
@property (weak, nonatomic) IBOutlet UIButton *fourthBtn;
@property (nonatomic, assign) BOOL isFromProfile;
@property (weak, nonatomic) IBOutlet UILabel *selectedAskProLbl;
@property(strong,nonatomic) NSString *GiveVideoName;
/////////////////////

@property (weak, nonatomic) IBOutlet UIView *loaderViewA3;

@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@property(nonatomic) float progressFloatValue;

- (NSURL*)grabFileURL:(NSString *)fileName;

-(void)AskPro;
-(void)AskQuestion;
-(void)addContents;
-(void)PlusQuestionListing;
-(void)UpvoteListing;

-(void)UnupvoteListing;

@property (weak, nonatomic) IBOutlet UIButton *outletOfUpvoteBtn;
- (IBAction)upvoteBtn:(id)sender;

@end
