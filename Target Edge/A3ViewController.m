//
//  A3ViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "A3ViewController.h"
#import "AppDelegate.h"
#import "UploadViewController.h"
#import "VideoDiscussionsHeader.h"


UIImageView *profileImage;// = nil;
BOOL isSelected;
NSString *DescriptionAbout;
NSString *advisorID;
NSString *Creater_User_ID;
NSString *No_Questions;
NSString *No_Followers;
BOOL isA3Advisor;
NSString *IsFollowed;
NSString *LinkedIn_ID;

@interface A3ViewController ()
{
    BOOL isDisableLeftArrow;
    int j;
    NSString *FirstName;
    NSString *LastName;
    UIImage *thumbnail;
    NSMutableArray *pushObj;
    NSMutableArray *notif;
    BOOL isForLoopTrue;
}

-(void)pushMethod;

@end


@implementation A3ViewController
@synthesize progressBar;

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    for(UIView *subview in _a3ScrollView.subviews)
    {
        [subview removeFromSuperview];
    }
//    [self AskPro];
 
    // Do any additional setup after loading the view.
    thumbnail = [[UIImage alloc]init];
    
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FetchProgressValue)  name:@"PROGRESSVALUE" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AlertBox)  name:@"ALERTBOX" object:nil];
    
    index = 0;
    
//    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.selectedAskProLbl.text = self.selectedAskPro;
    
    self.ssGraybtn.hidden = YES;
    self.moocGrayBtn.hidden = YES;
    
    j = 0;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        self.firstBtn.frame = CGRectMake(198,23,50, 50);
        self.secondBtn.frame = CGRectMake(270,25,100,100);
        self.thirdBtn.frame = CGRectMake(400, 25, 100, 100);
        self.fourthBtn.frame = CGRectMake(522,23,50, 50);
        
        _A3ImageOutlet.frame = CGRectMake(_A3ImageOutlet.frame.origin.x, _A3ImageOutlet.frame.origin.y, _A3ImageOutlet.frame.size.width, 50);
        _MOOCImageOutlet.frame = CGRectMake(_MOOCImageOutlet.frame.origin.x, _MOOCImageOutlet.frame.origin.y, _MOOCImageOutlet.frame.size.width, 50);
        
        _SSOutletImage.frame = CGRectMake(_SSOutletImage.frame.origin.x, _SSOutletImage.frame.origin.y, _SSOutletImage.frame.size.width, 50);
    }
    isDisableLeftArrow = true;
    
    if (_isFromProfile)
    {
        _navigationImage.image = [UIImage imageNamed:@"user_icon1.png.png"];
    }
    
    self.a3ScrollView.delegate = self;
    
    if (self.view.frame.size.height == 480 && self.view.frame.size.width == 320)
    {
        self.firstBtn.frame = CGRectMake(70, 8, 30, 28);
        self.secondBtn.frame = CGRectMake(105,12,51,48);
        self.thirdBtn.frame = CGRectMake(164, 12, 51, 48);
        self.fourthBtn.frame = CGRectMake(220, 8, 30, 28);
    }
    
    colletionArray = [NSMutableArray new];
    [colletionArray addObjectsFromArray:[appDelegate GlobalCollectionProfile]];
    [self FillArray];
    [self HeaderArrayCollection];
    [self addContent];
    
//    [self AskPro];
    
    _bottomBtnBg.frame = CGRectMake(_bottomBtnBg.frame.origin.x, _bottomBtnBg.frame.origin.y, _bottomBtnBg.frame.size.width, _bottomBtnBg.frame.size.height/2.7);
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
//    if (internetStatus != NotReachable)
//    {
//        NSLog(@"Valid Internet Connection");
//        [self AskPro];
//
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Network Error" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
//    }
}

- (void) receiveNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"TestNotification"])
        NSLog (@"Successfully received the test notification!");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *checkAlert = [[NSString alloc] init];
    checkAlert = [defaults stringForKey:@"ReceivedNotifications"];
    NSLog(@"Alert Message: %@", checkAlert);
    if (notif == nil)
    {
        notif = [NSMutableArray arrayWithObjects:checkAlert, nil];
    }
    else
    {
        [notif addObject:checkAlert];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
//    for(UIView *subview in _a3ScrollView.subviews)
//    {
//        [subview removeFromSuperview];
//    }
//    [self AskPro];
      [playBtnLinkA3.view removeFromSuperview];
    [playBtnLinkA3 stop];
    
}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
//    NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC

    return thumbnail;
}

-(void)pushMethod:(NSTimer *)timer
{
    MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
    [self.navigationController pushViewController:mooc animated:YES];
}

-(void)AskPro
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    isA3Advisor = true;
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
//    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
//    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffA3) onTarget:self withObject:nil animated:YES];
    
         dataObj = [[NSMutableArray alloc]init];
    
        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
        NSLog(@"savedValue is %@",savedValue);
    
        NSString *post = [NSString stringWithFormat:@"AdvisorID=%@",@""];
        NSLog(@"post is %@",post);
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:AskProAPI]];
        [request setHTTPMethod:@"POST"];
        [request setValue:savedValue forHTTPHeaderField:@"Token"];
        [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
//        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
        
        if (data)
        {
            id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//            NSLog(@"result AskPro is %@",result);
            
            BOOL success = [[result objectForKey:@"result"] boolValue];
            
            NSString *errorAlert = [result objectForKey:@"message"];
            NSLog(@"errorAlert is %@",errorAlert);
            
            NSDictionary *data1 = [result valueForKey:@"Object"];
//            NSLog(@"data is %@",data1);
            
            
            NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
            
            if(items.count>1)
            {
                strA3Alert = [items objectAtIndex:1];
            }
            if (success)
            {
                NSLog(@"AskPro loaded successfully");
                
                success = [[result valueForKey:@"result"]boolValue];
                
                dataObj = [[data1 valueForKey:@"details"] mutableCopy];
                
                NSLog(@"dataObj is %@",dataObj);
                
//                FirstName = [[dataObj objectAtIndex:i] valueForKey:@"FirstName"];
//                
//                LastName = [[dataObj objectAtIndex:i] valueForKey:@"LastName"];
                
                upvote = [[[dataObj objectAtIndex:index] valueForKey:@"Question_Answer"] valueForKey:@"IsUpvoted"];
                
                isUpvoteTrue = [upvote boolValue];
                
                
                if ([[NSString stringWithFormat:@"%@", [[[dataObj objectAtIndex:index] valueForKey:@"Question_Answer"]valueForKey:@"IsUpvoted"] ] isEqualToString:@"1"] )
                {
                    [self.fourthBtn setSelected:YES];
                }
                else
                {
                    [self.fourthBtn setSelected:NO];
                }
                
////                [[[dataObj objectAtIndex:0] valueForKey:@"Question_Answer"] valueForKey:@"video_thumbnail"];
//                
//                NSString *str = [[data1 valueForKey:@"Question_Answer"] valueForKey:@"details"];
//                NSLog(@"video_thumbnail in AskPro Method is %@",str);
//                
//                
////                NSString *objStr = [[dataObj objectAtIndex:i] valueForKey:@"Answer_Video_Link"];
////                
////                NSString *ID = [[dataObj objectAtIndex:index] valueForKey:@"AdvisorID"];
//                
//                No_UpvotesCount = [[dataObj objectAtIndex:i] valueForKey:@"No_Up_Votes"];
//                
//                No_Questions = [[dataObj objectAtIndex:index] valueForKey:@"No_Questions"];
//                
////                NSLog(@"ID and answerVideoLink is %@ %@ %@",ID,objStr,No_Questions);
//                
//                No_Followers = [[dataObj objectAtIndex:index] valueForKey:@"No_Followers"];
//                
//                NSLog(@"FirstName & LastName is %@ %@",FirstName,LastName);
                
                DescriptionAbout = [dataObj valueForKey:@"Description"];
                
                
//                if ([[NSString stringWithFormat:@"%@", [[[dataObj objectAtIndex:0] valueForKey:@"Question_Answer"]valueForKey:@"IsUpvoted"] ] isEqualToString:@"1"] )
//                {
//                    [self.fourthBtn setSelected:YES];
//                    //[self.fourthBtn setImage:[UIImage imageNamed:@"tlt_upvote.png"] forState:UIControlStateNormal];
//                }
//                else
//                {
//                    [self.fourthBtn setSelected:NO];
//                    
//                    // [self.fourthBtn setImage:[UIImage imageNamed:@"tlt_upvote copy.png"] forState:UIControlStateNormal];
//                }
                [self addContent];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strA3Alert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
                HUD.hidden = YES;
            }
        }
        else if (error)
            
            NSLog(@"%@",error);
        });
    }];
}

-(void)AskQuestion
{
    isNotRepeatAskQuestionAPI = true;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffA3) onTarget:self withObject:nil animated:YES];

    askQuestionArray = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    advisorID = [[dataObj objectAtIndex:index] valueForKey:@"AdvisorID"];
    
    NSLog(@"advisorID is %@",advisorID);
    
//    NSString *whiteSpace = [keyStr stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
    
    NSString *questionLink = [NSString stringWithFormat:@"https://s3-us-west-1.amazonaws.com/talentedge1/%@",outputName];
    
//    NSString *heplStr = [NSString stringWithFormat:@"%@png",newName];
    
    NSString *imageLink = [NSString stringWithFormat:@"https://s3-us-west-1.amazonaws.com/talentedge1/%@png",newName];
    
    NSLog(@"imageLink  and questionLink is %@ %@",imageLink,questionLink);
    
    NSString *post;
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
         post = [NSString stringWithFormat:@"Creator_User_ID=%@&Advisor_User_ID=%@&Question_Video_Link=%@&Question_Video_Title=%@&playing_time=%@&CategoryName=%@&Video_thumbnail=%@",LinkedIn_ID,advisorID,questionLink,keyStr,secDisplay,_selectedAskPro,imageLink];
    }
    else
    {
        post = [NSString stringWithFormat:@"Creator_User_ID=%@&Advisor_User_ID=%@&Question_Video_Link=%@&Question_Video_Title=%@&playing_time=%@&CategoryName=%@&Video_thumbnail=%@",Creater_User_ID,advisorID,questionLink,keyStr,secDisplay,_selectedAskPro,imageLink];
    }
    
      NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:AskQuestionAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result AskPro is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
               
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 if(data1 == (id)[NSNull null])
                 {
                     NSLog(@"invalid playing time");
                 }
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 strForAlert = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 
                 if(items.count>1)
                 {
                     strForAlert = [items objectAtIndex:1];
//                     strForAlert = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 }
                 if (success)
                 {
                     
                     NSLog(@"AskPro loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     askQuestionArray = [data1 valueForKey:@"QuestionID"];
                     
                     NSLog(@"askQuestionArray is %@",askQuestionArray);
                     
                     HUD.hidden = true;
                     
//                     [self pushMethod];
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong. Please upload video again with suitable title." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
//                     UIAlertView *alerts = [[UIAlertView alloc]initWithTitle:@"Alert" message:[result objectForKey:@"message"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                     [alerts show];
                     
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);

        });
     }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)PlayVideo:(NSString*)videoName
{
    isNotRepeatAskQuestionAPI = false;
    NSArray *dummyArray = [videoName componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
//        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
//        NSLog(@"streamURL is %@",streamURL);
        
        MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:streamURL];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
        [videoPlayerView.moviePlayer play];
        
        [self generateThumbImage:streamURL];
    }
}

-(void)HeaderArrayCollection
{
    headerArray = [[NSMutableArray alloc]init];
    [headerArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"bhaduri.png",@"headerImage",
                            @"ABHIJIT BHADURI",@"headerName",
                            @"the Chief Learning Officer of Wipro Ltd",@"headerAddress",
                            nil]];
    [headerArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"bhaduri.png",@"headerImage",
                            @"ABHIJIT BHADURI",@"headerName",
                            @"the Chief Learning Officer of Wipro Ltd",@"headerAddress",
                            nil]];
}

#pragma mark - Fill Array

-(void)FillArray
{
    colletionArray = [[NSMutableArray alloc]init];
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"4",@"No_Up_Votes",
                               @"ABHIJIT BHADURI",@"FooterTitle",
                               @"The Chief Learning Officer of Wipro Ltd",@"FooterDescription",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               
                               @"advisor1.jpeg",@"ProfileImage",
                               @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd. Has been part of companies like Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. Abhijit writes regularly for the Economic Times, People Matters and blogs for Times of India.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"2",@"No_Up_Votes",
                               @"PRABIR JHA",@"FooterTitle",
                               @"Global Chief People Officer of Cipla",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"advisor2.jpeg",@"ProfileImage",
                               @"Prabir Jha, an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. Prabir brings with him an impeccable academic and professional record.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"15",@"No_Up_Votes",
                               @"Dr AQUIL BUSRAI",@"FooterTitle",
                               @"Chief Executive Officer at aquil busrai consulting",@"FooterDescription",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"advisor3.jpeg",@"ProfileImage",@"Dr Aquil Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. Currently runs his own consulting firm.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"11",@"No_Up_Votes",
                               @"SRIPADA CHANDRASEKHAR ",@"FooterTitle",
                               @"President and global head of HR at Dr. Reddy’s Laboratoires",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"advisor4.jpeg",@"ProfileImage",@"Dr. Sripada Chandrarsekhar is the global head of HR at Dr. Reddy’s Laboratoires, Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"8",@"No_Up_Votes",
                               @"VIKRAM BECTOR",@"FooterTitle",
                               @"Chief Talent Officer at Reliance Industries",@"FooterDescription",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"advisor5.jpeg",@"ProfileImage",@"Vikram Bector - Chief Talent Officer at Reliance Industries Limited. Over two decades of experience with some leading Indian conglomerates.",@"info",nil]];
    
}

#pragma mark - Add Content

-(void)addContent
{
//    int noOfItems = (int)dataObj.count;
    int noOfItems = (int)colletionArray.count;
    
    [self.a3ScrollView setContentSize:(CGSizeMake(self.a3ScrollView.frame.size.width*(noOfItems), self.a3ScrollView.frame.size.height))];
    
    for(i = 0; i < noOfItems; i++)
    {
        UIView *mainViewWrapper = [[UIView alloc]initWithFrame:CGRectMake(i*self.a3ScrollView.frame.size.width, 0, self.a3ScrollView.frame.size.width, self.a3ScrollView.frame.size.height)];
        [mainViewWrapper setBackgroundColor:[UIColor clearColor]];
        
        [self.a3ScrollView addSubview:mainViewWrapper];
        
        UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, mainViewWrapper.frame.size.width - 20, mainViewWrapper.frame.size.height - 30)];
        [mainView setBackgroundColor:[UIColor clearColor]];
        mainView.layer.masksToBounds = true;
        mainView.layer.cornerRadius = 8;
        mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        mainView.layer.borderWidth = 0.8f;
        [mainViewWrapper addSubview:mainView];
        
        float headerFooterHeight = mainView.frame.size.height/4;
        
//        NSLog(@"headerFooterHeight %f",headerFooterHeight);
        
        float width = mainView.frame.size.width;
        float height = mainView.frame.size.height - 150;
        
//        NSLog(@"%f",height);
        
        float x = 0;      // here
        float y = 0;      // here
        float space = 10;
        
        
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, headerFooterHeight)];
        [headerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
        [mainView addSubview:headerView];
        
        float lblHeight = (headerView.frame.size.height - space*3)/3 ;
        
        
        UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(x, mainView.frame.size.height - headerFooterHeight, width, headerFooterHeight)];
        [footerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
        [mainView addSubview:footerView];
        
        profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(space,space/4,headerView.frame.size.height - (space*0.7),headerView.frame.size.height - (space*0.7))];
        [profileImage setBackgroundColor:[UIColor clearColor]];
        //[self.itemImageButton setImage:[UIImage imageNamed:stretchImage] forState:UIControlStateNormal];
        
        //        UIImage *profileImageName = [UIImage imageNamed:[[dataObj objectAtIndex:i] valueForKey:@"Profile_Pic_Small"]];
        //        UIImage *profileImageName = [UIImage imageNamed:@"NoUsername.png"];
        //        profileImage.image = profileImageName;
        
        AdvisorImg = [[colletionArray objectAtIndex:i] valueForKey:@"ProfileImage"];
        
//        NSURL *url = [NSURL URLWithString:AdvisorImg];
//        
//        NSLog(@"url for AdvisorImg is %@",url);
       
//        [profileImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"NoUsername.png"]];
        
        profileImage.image = [UIImage imageNamed:AdvisorImg];
       
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2;
        profileImage.layer.borderWidth = 2.0;
        profileImage.layer.masksToBounds = YES;
        
        profileImage.layer.borderColor = [[UIColor whiteColor]CGColor];
        profileImage.contentMode = UIViewContentModeScaleAspectFit;
        profileImage.backgroundColor = [UIColor clearColor];
        
        profile = profileImage.image;
        [headerView addSubview:profileImage];
        
        ///profile image Button///
        
        UIButton *profileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [profileBtn addTarget:self action:@selector(profileBtnClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        profileBtn.backgroundColor = [UIColor clearColor];
        profileBtn.tag = i+777;
        //[profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //profileBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        profileBtn.frame = CGRectMake(space,space,headerView.frame.size.width/2,headerView.frame.size.height);
        [headerView addSubview:profileBtn];
        
        UIImageView *voteImage = [[UIImageView alloc] initWithFrame:CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5)),lblHeight,footerView.frame.size.height - footerView.frame.size.height - (lblHeight+(space*1.35)),footerView.frame.size.height - (lblHeight+(space*2.5)))];
        
        lblUpvote = [[UILabel alloc] initWithFrame:CGRectMake(voteImage.frame.origin.x + 9,voteImage.frame.size.height - (space*2.4),50,30)];
        strUpvotes = [NSString stringWithFormat:@"%@",[[colletionArray objectAtIndex:i] valueForKey:@"No_Up_Votes"]];
        [lblUpvote setText:strUpvotes];
//        [lblUpvote setText:@"4"];
        [lblUpvote setFont:[UIFont fontWithName: @"Helvetica-Light" size: 10.0f]];
        [lblUpvote setTextColor:[UIColor whiteColor]];
        [lblUpvote setBackgroundColor:[UIColor colorWithRed:78.0/255 green:172.0/255 blue:235.0/255 alpha:1.0]];
        lblUpvote.tag = i + 876;
        
//        lblUpvote.text = @"11113";
        lblUpvote.textAlignment = NSTextAlignmentCenter;
        CGSize lblsize = [lblUpvote.text sizeWithFont:lblUpvote.font constrainedToSize:CGSizeMake(MAXFLOAT,lblUpvote.frame.size.height) lineBreakMode:NSLineBreakByWordWrapping];
        if (lblsize.width > 30)
        {
        lblUpvote.frame = CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5))-lblsize.width+10, voteImage.frame.size.height - (space*2.4)+5, lblsize.width, 30);
        lblUpvote.layer.cornerRadius = 10;
        lblUpvote.layer.masksToBounds = YES;
        }
        else
        {
            lblUpvote.frame = CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5))-25, voteImage.frame.size.height - (space*2.4)+8, 30, 30);
            lblUpvote.layer.cornerRadius = 15;
            lblUpvote.layer.masksToBounds = YES;

        }
        [headerView addSubview:lblUpvote];
        
        UIImage *voteImageIcon = [UIImage imageNamed:@"uploo-1.png"];
        voteImage.image = voteImageIcon;
        [voteImage setBackgroundColor:[UIColor clearColor]];
//        [headerView addSubview:voteImage];

//        [[UIApplication sharedApplication].keyWindow bringSubviewToFront:voteImage];
        
         AnswerID = [[[colletionArray objectAtIndex:i] valueForKey:@"Question_Answer"] valueForKey:@"AnswerID"];
        NSLog(@"list is %@",AnswerID);
//
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), space/2, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight + (space/2))];
        [titleLbl setTextColor:[UIColor darkGrayColor]];
        
//        combine = [NSString stringWithFormat:@"%@ %@",FirstName,LastName];
        [titleLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"FooterTitle"]];
        [titleLbl setBackgroundColor:[UIColor clearColor]];
        //[titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
        [headerView addSubview:titleLbl];
        
        UILabel *DesignationLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), lblHeight + space/1.4, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
        [DesignationLbl setTextColor:[UIColor blackColor]];
        [DesignationLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"HeaderTitle"]];
        [DesignationLbl setBackgroundColor:[UIColor clearColor]];
        [DesignationLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
        [headerView addSubview:DesignationLbl];
        
        UILabel *userNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), lblHeight+(space*2.3), width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
        userNameLbl.numberOfLines = 2;
        [userNameLbl setTextColor:[UIColor blackColor]];
       // [userNameLbl setTextColor:[UIColor colorWithRed:157.0f/255.0f green:157.0f/255.0f blue:157.0f/255.0f alpha:1]];
        
        [userNameLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"Company"]];
        [userNameLbl setBackgroundColor:[UIColor clearColor]];
        //[userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
        [headerView addSubview:userNameLbl];

       ////////////////Added Label//////////////////////
        
        UILabel *headerLbl = [[UILabel alloc]init];
        [headerLbl setTextColor:[UIColor darkGrayColor]];
        headerLbl.numberOfLines = 3;
         [headerLbl setBackgroundColor:[UIColor clearColor]];
        [headerLbl setFont:[UIFont fontWithName: @"Helvetica Neue" size: 12.0f]];
        [headerLbl setText:[[colletionArray objectAtIndex:i] valueForKey:@"HeaderTitle"]];
        
        CGSize answerFrame = [headerLbl sizeThatFits:headerLbl.bounds.size];
        
        NSLog(@"%f %f",answerFrame.width,answerFrame.height);
        NSLog(@"%f %f",footerView.frame.size.width,footerView.frame.size.height);
        headerLbl.frame = CGRectMake((footerView.frame.origin.x) + (space/2), (footerView.frame.size.height - answerFrame.height)/2 - (space*2.7), footerView.frame.size.width -(space/2),footerView.frame.size.height);
        [footerView addSubview:headerLbl];
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            
            [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 20.0f]];
            [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 18.0f]];
           // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 20.0f]];
        }
        else
        {
            [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
            [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
           // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 14.0f]];
        }

        
        UIButton *but = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        //[but setFrame:answerLbl.frame];
        but.tag = i +333;
        [but setExclusiveTouch:YES];
        [footerView addSubview:but];
        
        float combineHeightOfFooterHeader = mainView.frame.size.height - (headerView.frame.size.height + footerView.frame.size.height);
        
        UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(x, headerView.frame.size.height, width, combineHeightOfFooterHeader)];
        [centerView setBackgroundColor:[UIColor clearColor]];
        [mainView addSubview:centerView];

        NSLog(@" main view %f and %f",centerView.frame.size.width, centerView.frame.size.height);

        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.frame = CGRectMake(centerView.frame.origin.x, centerView.frame.origin.y, centerView.frame.size.width, centerView.frame.size.height);
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        
       UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, centerView.frame.size.width, centerView.frame.size.height)];
       //   UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x + centerView.frame.size.width / 8 + (space/2), 0 , width - ((centerView.frame.size.width / 8 + (space/2))*2), combineHeightOfFooterHeader)];
        [playBtn setBackgroundColor:[UIColor clearColor]];//DefaultBackground-1
      
         playBtn.tag = i+999;
//        [playBtn setBackgroundImage:imgView.image forState:UIControlStateNormal];
        [playBtn setBackgroundColor:[UIColor clearColor]];
        [playBtn addTarget:self action:@selector(playClickedA3:) forControlEvents:UIControlEventTouchUpInside];
        [centerView addSubview:playBtn];
        
        UIImageView *centerImage = [[UIImageView alloc]initWithFrame:CGRectMake(x, 0 , width, combineHeightOfFooterHeader)];
        [centerImage setBackgroundColor:[UIColor clearColor]];
        
        /////////////////////
        NSArray *dummyArray = [[[colletionArray objectAtIndex:i] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
        
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        UIImage *staticImage = [self generateThumbImage:streamURL];
        centerImage.image = staticImage;
        
        [centerImage setContentMode: UIViewContentModeScaleAspectFit];
        [centerImage setBackgroundColor:[UIColor blackColor]];
        [centerView addSubview:centerImage];
        
        ////////////////////////

        UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playbutton.png"]];
        img.frame = CGRectMake((centerView.frame.size.width - (centerView.frame.size.width/6)) / 2, (centerView.frame.size.height - (centerView.frame.size.width/6)) / 2, centerView.frame.size.width / 6, centerView.frame.size.width / 6);
        
        [centerView addSubview:img];

          HUD.hidden = YES;
    }
}
-(void)profileBtnClicked:(UIButton*)sender
{
    isNotRepeatAskQuestionAPI = false;
    isFollowBtnVisible = @"1";
    
     isA3Advisor = true;
    
    NSLog(@"profileBtnClicked");
    
    NSLog(@"sender.tag is %ld",(long)sender.tag);
    
    ProfileViewController *profiles = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    profiles.passImage = [[colletionArray objectAtIndex:[sender tag]- 777] valueForKey:@"ProfileImage"];
    profiles.info = [[colletionArray objectAtIndex:[sender tag]- 777] valueForKey:@"info"];
    profiles.advisorFirstName = [[dataObj objectAtIndex:[sender tag]- 777] valueForKey:@"FirstName"];
    profiles.advisorLastName = [[dataObj objectAtIndex:[sender tag]- 777] valueForKey:@"LastName"];
    

    profiles.indexValue = (int)[sender tag]-777;
////    profiles.indexValue = i;
    profiles.GetAdvisorID = [[dataObj objectAtIndex:index] valueForKey:@"AdvisorID"];
    profiles.follower = [[dataObj objectAtIndex:index] valueForKey:@"IsFollowed"];
    profiles.tranferIndex = index;
    profiles.NO_data = [dataObj objectAtIndex:index];
    NSLog(@" profiles.GetAdvisorID  is %@ and %@", profiles.GetAdvisorID,profiles.NO_data);
    
       [self presentViewController:profiles animated:YES completion:nil];
}

//
//-(void)leftArrowClicked:(UIButton*)sender
//{
//    if (_a3ScrollView.contentOffset.x > 0)
//    {
//        leftArrowBtn.enabled=true;
//        [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x-self.a3ScrollView.frame.size.width, 0))animated:NO];
//    }
//    else
//    {
//        [_a3ScrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
//        // leftArrowBtn.enabled=false;
//        UIButton *buttonWithTag1 = (UIButton *)[self.a3ScrollView viewWithTag:[sender tag]];
//        buttonWithTag1.enabled = false;
//        
//    }
//    NSLog(@"left clicked");
//    
//}
//
//-(void)rightArrowClicked:(UIButton*)sender
//{
//    if (_a3ScrollView.contentOffset.x+self.a3ScrollView.frame.size.width == _a3ScrollView.contentSize.width)
//    {
//        //          [_a3ScrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
//        rightArrowBtn.enabled =false;
//        
//        //        UIButton *buttonWithTag1 = (UIButton *)[self.a3ScrollView viewWithTag:[sender tag]];
//        //        buttonWithTag1.hidden = YES;
//    }
//    else
//    {
//        rightArrowBtn.enabled=true;
//        //        int noOfItems = (int)colletionArray.count;
//        //        if (_a3ScrollView.contentOffset.x >= noOfItems*_a3ScrollView.frame.size.width)
//        //        {
//        //            [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x + _a3ScrollView.frame.size.width , 0))animated:NO];
//        //        }
//        [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x + _a3ScrollView.frame.size.width , 0))animated:NO];
//        //        rightArrowBtn.hidden=false;
//    }
//}

-(void) buttonClicked:(UIButton*)sender
{
    isNotRepeatAskQuestionAPI = false;
    NSLog(@"more is %li",(long)sender.tag-333);
    
    NSLog(@"more.name is %@",[[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"FooterTitle"]);
    
    MoreViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreViewController"];
    more.name = [[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"FooterTitle"];
    more.address = [[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"FooterDescription"];
    
    
    NSLog(@"dat is %@",[[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"ProfileImage"]);
    
    more.moreImage = [[colletionArray objectAtIndex:sender.tag-333] valueForKey:@"ProfileImage"];
    
    [self presentViewController:more animated:YES completion:nil];
}

-(void)playClickedA3:(UIButton*)sender
{
    isNotRepeatAskQuestionAPI = false;
    isDoneA3 = false;
    NSLog(@"Play clicked");
    
    NSArray *dummyArray = [[[colletionArray objectAtIndex:(int)[sender tag]-999] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        playBtnLinkA3 = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
        //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
        [self.view addSubview:playBtnLinkA3.view];
        playBtnLinkA3.fullscreen = YES;
        playBtnLinkA3.controlStyle = MPMovieControlStyleEmbedded;
        playBtnLinkA3.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        moviePlayer.view.layer.zPosition = 1;
        [playBtnLinkA3 prepareToPlay];
        [playBtnLinkA3 play];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteA3:) name:MPMoviePlayerPlaybackDidFinishNotification object:playBtnLinkA3];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickA3:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];

    }

}

- (void)aMoviePlaybackCompleteA3:(NSNotification*)notification
{
    
    isNotRepeatAskQuestionAPI = false;

    if (isplayAginA3 == false)
    {
        isDoneA3 = true;
        
        if (playBtnLinkA3.view)
        {
            [playBtnLinkA3.view removeFromSuperview];
            [playBtnLinkA3 stop];
        }
        
        if (!isDoneA3)
        {
            
            [playBtnLinkA3.view removeFromSuperview];
            [playBtnLinkA3 stop];
            
            NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice" ofType:@"mp4"];
            
            NSLog(@"videoPath is %@",videoPath);
            
            NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
            
            NSLog(@"streamURL is %@",streamURL);
            
            playBtnLinkA3 = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
            //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
            [self.view addSubview:playBtnLinkA3.view];
            playBtnLinkA3.fullscreen = YES;
            playBtnLinkA3.controlStyle = MPMovieControlStyleEmbedded;
            playBtnLinkA3.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            //        moviePlayer.view.layer.zPosition = 1;
            [playBtnLinkA3 prepareToPlay];
            [playBtnLinkA3 play];
            
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteA3:) name:MPMoviePlayerPlaybackDidFinishNotification object:playBtnLinkA3];
        
        isplayAginA3 = true;
    }
    else
    {
        NSLog(@"stop scucessfully");
        //isPlayBackCalled = NO;
        [playBtnLinkA3 stop];
        [playBtnLinkA3.view removeFromSuperview];
        isplayAginA3 = false;
    }
}


-(void)doneButtonClickA3:(NSNotification*)aNotification
{
    isNotRepeatAskQuestionAPI = false;
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        NSLog(@"done in if");
        [playBtnLinkA3.view removeFromSuperview];
        [playBtnLinkA3 stop];
    }
    else
    {
        NSLog(@"done in else");
        [playBtnLinkA3.view removeFromSuperview];
        [playBtnLinkA3 stop];
        isplayAginA3 = false;
        isDoneA3 = false;
        playBtnLinkA3 = nil;
    }
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
}

-(void)moreClicked:(UIButton*)sender
{
    isNotRepeatAskQuestionAPI = false;
    NSLog(@"more");
    MoreViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreViewController"];
//    //more.setImage = [UIImage imageNamed:[[colletionArray objectAtIndex:sender.tag] valueForKey:@"ProfileImage"]];
    more.setImage = [[colletionArray objectAtIndex:sender.tag] valueForKey:@"ProfileImage"];
    more.name = [[colletionArray objectAtIndex:sender.tag] valueForKey:@"FooterTitle"];
    more.address = [[colletionArray objectAtIndex:sender.tag] valueForKey:@"FooterDescription"];
        [self presentViewController:more animated:YES completion:nil];
}


- (IBAction)btnOrangeA3:(id)sender
{
    
}

- (IBAction)btnGrayMOOC:(id)sender
{
//    MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
//    [self.navigationController pushViewController:mooc animated:YES];
}


- (IBAction)btnGraySS:(id)sender
{
    // SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
    //  [self.navigationController popToViewController:ss animated:YES];
    
 //   [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    NSLog(@"j is %i",j);
    if (scrollView == _a3ScrollView)
    {
        if (_a3ScrollView.contentOffset.x > 0)
        {
            if (_a3ScrollView.contentOffset.x+self.a3ScrollView.frame.size.width == _a3ScrollView.contentSize.width)
            {
                rightArrowBtn.enabled=false;
            }
            else
            {
                rightArrowBtn.enabled=true;
            }
            leftArrowBtn.enabled=true;
        }
        else
        {
            leftArrowBtn.enabled=false;
        }
        
        //        if (_a3ScrollView.contentOffset.x == 0)
        //        {
        //            self.leftArrow.hidden = NO;
        //            self.leftArrow.enabled = NO;
        //        }
        //        else
        //        {
        //            self.leftArrow.hidden = NO;
        //            self.leftArrow.enabled = YES;
        //        }
        //        int noOfItems = (int)colletionArray.count;
        //        if (scrollView.contentOffset.x >= noOfItems*_a3ScrollView.frame.size.width)
        //        {
        //          [scrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
        //        }
    }
    
    CGFloat pageWidth = scrollView.frame.size.width; // you need to have a iVar with getter for scrollView
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    index = (int)page;
    
    NSLog(@"index is %d",index);
    
//    if ([varIndex isEqualToString:@"1"])
//    {
//        [self.fourthBtn setSelected:NO];
//    }
//    else
//    {
//        [self.fourthBtn setSelected:YES];
//    }
    
//    if (index>=0)
//    {
//        if ([[NSString stringWithFormat:@"%@", [[[dataObj objectAtIndex:index] valueForKey:@"Question_Answer"]valueForKey:@"IsUpvoted"] ] isEqualToString:@"1"] )
//        {
//            [self.fourthBtn setSelected:YES];
//            //[self.fourthBtn setImage:[UIImage imageNamed:@"tlt_upvote.png"] forState:UIControlStateNormal];
//        }
//        else
//        {
//            [self.fourthBtn setSelected:NO];
//
//            // [self.fourthBtn setImage:[UIImage imageNamed:@"tlt_upvote copy.png"] forState:UIControlStateNormal];
//        }
//    }
////    else
////    {
////        NSLog(@"sdjfgsdfgsd");
////    }
}

- (IBAction)shareBtn:(id)sender
{
    isNotRepeatAskQuestionAPI = false;
    MyQuestionsA3 *myQuestion = [self.storyboard instantiateViewControllerWithIdentifier:@"MyQuestionsA3"];
    [self presentViewController:myQuestion animated:YES completion:nil];
    
//        NSLog(@"ShareBtn pressed fro sharing on social media");
//    
//    NSString *messages = @"Got a customized career guidance from an expert I always wanted to connect with.  Download Talentedge App now to get yours!";
//    
//        NSArray * shareItems = @[messages];
//    
//        UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
//    
//        [self presentViewController:avc animated:YES completion:nil];
}

- (IBAction)askBtn:(id)sender
{
    isNotRepeatAskQuestionAPI = false;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        picker.videoMaximumDuration = 180.0f;
        
        mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        
        picker.mediaTypes = mediaTypes;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"No Camera in this Device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

//-(UIImage *)getThumbNail:(NSString*)stringPath
//{
//    NSURL *videoURL = [NSURL fileURLWithPath:stringPath];
//    
//    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
//    
//    UIImage *thumbnails = [player thumbnailImageAtTime:0.7 timeOption:MPMovieTimeOptionNearestKeyFrame];
//    
//    //Player autoplays audio on init
//
//    return thumbnails;
//}


-(void)saveVideo
{
    // grab our movie URL
  
    NSLog(@"info is %@",chosenMovie);
    
    // save it to the documents directory
    fileURL = [self grabFileURL:@"%@.mp4"];
    
    NSLog(@"fileURL is %@",fileURL);
    
    AVURLAsset *avUrl = [AVURLAsset assetWithURL:chosenMovie];
    CMTime time = [avUrl duration];
    seconds = ceil(time.value/time.timescale);
    
    NSLog(@"seconds is %f",seconds);
    
//    double newCurrentTime = objAudio.currentTime;
    int min = floor(seconds/60);
    int sec = trunc(seconds - min * 60);
    
    if (sec < 10)
    {
        secDisplay = [NSString stringWithFormat:@"%i:%i" ,min,sec];
        NSLog(@"secDisplay is %@",secDisplay);
    }
    else
    {
        secDisplay = [NSString stringWithFormat:@"%i:%i" ,min,sec];
        NSLog(@"secDisplay is %@",secDisplay);
    }
    
    movieData = [NSData dataWithContentsOfURL:chosenMovie];
    
    [movieData writeToURL:fileURL atomically:YES];
    
    // save it to the Camera Roll
    UISaveVideoAtPathToSavedPhotosAlbum([chosenMovie path], nil, nil, nil);
    
    saveVideo = [NSArray arrayWithObjects:[chosenMovie path], nil];
    
    NSLog(@"saveVideo is %@",saveVideo);
    
    // and dismiss the picker
    [self dismissViewControllerAnimated:YES completion:nil];
    
    progressBar.hidden = NO;
    self.loaderViewA3.hidden = NO;
    
//    NSString *whiteSpace = [keyStr stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
    UploadViewController * _obj_UploadViewController = [[UploadViewController alloc]init];
    [_obj_UploadViewController viewDidLoad];
    [_obj_UploadViewController CallAWS3ConnectionForAskQuestion:chosenMovie :outputName];
     [_obj_UploadViewController convertingDataIntoBitesForAskQuestion];
    
     [self saveImageOfVideo];
}

#pragma mark - UIImagePickerController delegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add question title" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
   [alert show];
    
     chosenMovie = [info objectForKey:UIImagePickerControllerMediaURL];
}

- (NSURL*)grabFileURL:(NSString *)fileNames
{
    fileNames = [NSString stringWithFormat:@"%f.mp4",[[NSDate date] timeIntervalSince1970]];
    //NSFileManager *fileManager = [NSFileManager defaultManager];
    
   // NSString *strVideoName = [_GiveVideoName stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
  // fileNames = [fileName stringByAppendingString:@".MOV"];
    
    NSLog(@"fileNames is %@",fileNames);
    
    outputName = fileNames;
    
    NSLog(@"outputName is %@",outputName);
    
  //  keyStr = [NSString stringWithFormat:@"%@.MOV",_GiveVideoName];
    
  //  NSLog(@"keystr is %@",keyStr);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *documentsDirectoryURL = [[fileManager URLsForDirectory: NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *fileURLs = [NSURL URLWithString:fileNames relativeToURL:documentsDirectoryURL];
    
    arr = [NSArray arrayWithObjects:[fileURLs path], nil];
    
    return fileURLs;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
//        if ([_GiveVideoName isEqualToString:@""])
//        {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The question video title field is required.Please upload video again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//            [alert show];
//
//        }
        _GiveVideoName = [alertView textFieldAtIndex:0].text;
        
        NSLog(@"_GiveVideoName is %@",_GiveVideoName);
        
        keyStr = _GiveVideoName;
        
        NSLog(@"keyStr is %@",keyStr);
        // name contains the entered value
        [self saveVideo];
    }
}

-(void)FetchProgressValue
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    progressBar.progress  = appDelegate.globle_progress_value;
}

-(void)saveImageOfVideo
{
    UIImageWriteToSavedPhotosAlbum(imgUrl, nil, nil, nil);

   // [self loadimage];
    
    UIImage*img = [self generateThumbImage:chosenMovie];
    
    NSData *imageData = UIImagePNGRepresentation(img);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",@"cached"]];
    NSLog(@"imagePath is %@",imagePath);
    
    NSLog((@"pre writing to file"));
    
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        NSLog((@"Failed to cache image data to disk"));
    }
    else
    {
        NSLog(@"the cachedImagedPath is %@",imagePath);
    }
    
    urlfile = [NSURL fileURLWithPath:imagePath];
    NSLog(@"urlfile is %@",urlfile);
   
    if ([outputName length] > 0)
    {
        newName = [outputName substringToIndex:[outputName length] - 3];
        NSLog(@"newName is %@",newName);
    }
    else
    {
        NSLog(@"//no characters to delete... attempting to do so will result in a crash");
    }
    
    UploadViewController * _obj_UploadViewController = [[UploadViewController alloc]init];
    [_obj_UploadViewController viewDidLoad];
    [_obj_UploadViewController CallAWS3ConnectionForImage:urlfile :[NSString stringWithFormat:@"%@png",newName]];
    [_obj_UploadViewController convertingDataIntoBitesForImage];
    
}


-(void)AlertBox
{
//    if (isNotRepeatAskQuestionAPI == false)
//    {
//        [self AskQuestion];
//    }
//    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Video has been successfully Uploaded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    self.loaderViewA3.hidden = YES;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


//- (IBAction)rightArrow:(id)sender
//{
//    NSLog(@"right clicked");
//
//    int noOfItems = (int)colletionArray.count;
//    if (_a3ScrollView.contentOffset.x >= noOfItems*_a3ScrollView.frame.size.width)
//    {
//        [_a3ScrollView setContentOffset:CGPointMake(_a3ScrollView.contentOffset.x + self.a3ScrollView.frame.size.width, 0)animated:NO];
//    }
//    [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x + self.a3ScrollView.frame.size.width, 0))animated:NO];
//}

//- (IBAction)leftArrow:(id)sender
//{
//    NSLog(@"left clicked");
//
//    if (_a3ScrollView.contentOffset.x == 0)
//    {
//        [_a3ScrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
////        if (isDisableLeftArrow == NO)
////        {
//            self.leftArrow.hidden = NO;
//            self.leftArrow.enabled = NO;
////        }
////        else
////        {
////            self.leftArrow.hidden = NO;
////            self.leftArrow.enabled = YES;
////        }
//
//    }
//    else
//    {
//            [_a3ScrollView setContentOffset:(CGPointMake(_a3ScrollView.contentOffset.x - self.a3ScrollView.frame.size.width, 0))animated:NO];
//        self.leftArrow.hidden = NO;
//        self.leftArrow.enabled = YES;
//    }
//}
- (IBAction)backToA3:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)plusBtn:(id)sender
{
    isNotRepeatAskQuestionAPI = false;
    NSLog(@"sender: %@", sender);
    
    MoreViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreViewController"];
//    //more.setImage = [UIImage imageNamed:[[colletionArray objectAtIndex:sender.tag] valueForKey:@"ProfileImage"]];
//    more.name = [[colletionArray objectAtIndex:index] valueForKey:@"FooterTitle"];
//    more.address = [[colletionArray objectAtIndex:index] valueForKey:@"FooterDescription"];
//    more.moreImage = [[colletionArray objectAtIndex:index] valueForKey:@"ProfileImage"];
    combine = [NSString stringWithFormat:@"%@ %@",FirstName,LastName];
    
    more.name = [[dataObj objectAtIndex:index] valueForKey:@"FirstName"];
        
    more.address = [[dataObj objectAtIndex:index] valueForKey:@"Location"];
    more.GetAdvisorIDMore = [[dataObj objectAtIndex:index] valueForKey:@"AdvisorID"];
    
    NSLog(@"more.name is %@ and more.address is %@ GetAdvisorIDMore is %@",more.name,more.address,more.GetAdvisorIDMore);
    
    more.setImage = [[colletionArray objectAtIndex:index] valueForKey:@"ProfileImage"];

    [self presentViewController:more animated:YES completion:nil];
}

- (void)doSomeFunkyStuffA3
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}

-(void)UpvoteListing
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];

    AnswerID = [[[dataObj objectAtIndex:index] valueForKey:@"Question_Answer"] valueForKey:@"AnswerID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffA3) onTarget:self withObject:nil animated:YES];
    
    UpvoteObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
    NSString *post;
    if (!(LinkedIn_ID == nil || [LinkedIn_ID isEqualToString:@""]))
    {
         post = [NSString stringWithFormat:@"Up_Vote_User_ID=%@&AnswerID=%@&Status=%@",LinkedIn_ID,AnswerID,@"1"];
    }
    else
    {
        post = [NSString stringWithFormat:@"Up_Vote_User_ID=%@&AnswerID=%@&Status=%@",Creater_User_ID,AnswerID,@"1"];
    }
    
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:UpvoteAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 strForUpvote = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 
                 if(items.count>1)
                 {
                     strForUpvote = [items objectAtIndex:1];
                     //                     strForAlert = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 }

                 if (success)
                 {
                     NSLog(@" UpvoteListing loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
//                     No_UpvotesCount = [[dataObj objectAtIndex:UpvoteIndex] valueForKey:@"No_Up_Votes"];
                     
//                     lblUpvote.text = @(lblUpvote.text.integerValue+1).stringValue;
                     
                     lblUpvote = (UILabel *)[self.view viewWithTag:UpvoteIndex]; // get the label with tag
                     lblUpvote.text = @(lblUpvote.text.integerValue+1).stringValue;
                     
                     [self.fourthBtn setSelected:YES];
                    // [self.fourthBtn setImage:[UIImage imageNamed:@"tlt_upvote.png"] forState:UIControlStateNormal];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strForUpvote delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     isUpvoteTrue = true;

                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strForUpvote delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)UnupvoteListing
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    AnswerID = [[[dataObj objectAtIndex:index] valueForKey:@"Question_Answer"] valueForKey:@"AnswerID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffA3) onTarget:self withObject:nil animated:YES];
    
    UpvoteObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
    NSString *post;
    if (!([LinkedIn_ID isEqualToString:@"" ]|| LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"Up_Vote_User_ID=%@&AnswerID=%@&Status=%@",LinkedIn_ID,AnswerID,@"0"];
    }
    else
    {
        post = [NSString stringWithFormat:@"Up_Vote_User_ID=%@&AnswerID=%@&Status=%@",Creater_User_ID,AnswerID,@"0"];
    }
    
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:UpvoteAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 strForUpvote = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 
                 if(items.count>1)
                 {
                     strForUpvote = [items objectAtIndex:1];
                     //                     strForAlert = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 }
                 
                 if (success)
                 {
                     NSLog(@" UpvoteListing loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
//                     NSMutableDictionary *newDic = [NSMutableDictionary new];
//                     newDic = [[dataObj objectAtIndex:index] mutableCopy];
////                     [[dataObj valueForKey:@"IsUpvoted"] insertObject:@"0" atIndex:index];
//                    
//                     
//                     [[[dataObj valueForKey:@"Question_Answer"]valueForKey:@"IsUpvoted"] insertObject:@"0" atIndex:index];
//                     
//                     NSLog(@"newDic in ununpvote is %@",newDic);
// 
                     lblUpvote = (UILabel *)[self.view viewWithTag:UpvoteIndex]; // get the label with tag
                     lblUpvote.text = @(lblUpvote.text.integerValue-1).stringValue;
                     [self.fourthBtn setSelected:NO];
                     //[self.fourthBtn setImage:[UIImage imageNamed:@"tlt_upvote copy.png"] forState:UIControlStateNormal];
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strForUpvote delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     isUpvoteTrue = false;
                     
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:strForUpvote delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                    
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

- (IBAction)upvoteBtn:(id)sender
{
    isNotRepeatAskQuestionAPI = false;
//    NSLog(@"index - 876 is %d",index + 876);
    
    UpvoteIndex = index + 876;
    
    if (isUpvoteTrue == false)
    {
        [self.fourthBtn setSelected:YES];
        isUpvoteTrue = true;
        lblUpvote = (UILabel *)[self.view viewWithTag:UpvoteIndex]; // get the label with tag
        lblUpvote.text = @(lblUpvote.text.integerValue+1).stringValue;
    }
    else
    {
      [self.fourthBtn setSelected:NO];
        isUpvoteTrue = false;
        lblUpvote = (UILabel *)[self.view viewWithTag:UpvoteIndex]; // get the label with tag
        lblUpvote.text = @(lblUpvote.text.integerValue-1).stringValue;
    }
}

@end
