//
//  AdvidorFollowerCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvidorFollowerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *followerAdvisorImageView;
@property (weak, nonatomic) IBOutlet UILabel *followerAdvisorTitle;
@property (weak, nonatomic) IBOutlet UILabel *followerAdvisorName;
@property (weak, nonatomic) IBOutlet UILabel *follwerAdvisorTime;
@property (weak, nonatomic) IBOutlet UIImageView *playBtnStaticImage;

@end
