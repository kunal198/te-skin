//
//  AdvisorAskPro.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "PlusViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#define AskProAPI @"http://mobileapi.talentedge.hub1.co/api/Advisor/Advisors_Listing"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AdvisorAskPro : UIViewController
{
    MBProgressHUD *HUD;
    NSMutableArray *AskProDataObj;
    NSString *strAskProAlert;
    NSString *FirstName;
    NSString *LastName;
    NSString *DescriptionAbout;
    int i;
    int j;
    int index;
    UIImageView *profileImage;
    NSString *AdvisorImg;
    UIImage *profiles;
    UIButton *rightArrowBtn;
    UIButton *leftArrowBtn;
    NSString *combine;
    MPMoviePlayerController *playAdvisorAskPro;
    BOOL isplayAdvisorVideo;
    NSString *answrAdvisorVideoLink;
    BOOL isAdvisorAskProDone;
    UIImage *thumbnail;
}
@property (weak, nonatomic) IBOutlet UIScrollView *AdvisorScrollView;
@property(strong,nonatomic) NSString *titleStr;
- (IBAction)askproBackBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

-(void)AskProAdvisor;
-(void)AdvisorData;
-(void)AskProForAdvisorFillArray;

-(void)addContentAdvisorListing;

- (IBAction)plusBtn:(id)sender;
@end
