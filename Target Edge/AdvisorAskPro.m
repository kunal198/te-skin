//
//  AdvisorAskPro.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "AdvisorAskPro.h"

@interface AdvisorAskPro ()

@end

@implementation AdvisorAskPro
@synthesize titleStr;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.titleLbl.text = titleStr;
    [self AskProForAdvisorFillArray];
    [self addContentAdvisorListing];
//    [self AskProAdvisor];
//    [self AdvisorData];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [playAdvisorAskPro.view removeFromSuperview];
    [playAdvisorAskPro stop];
}

- (void)doSomeFunkyStuff
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}

-(void)AskProAdvisor
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    
    AskProDataObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
            NSString *post = [NSString stringWithFormat:@"AdvisorID=%@",Creater_User_ID];
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:AskProAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result AskPro is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strAskProAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     NSLog(@"AskPro loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     AskProDataObj = [data1 valueForKey:@"details"];
                     
                     NSLog(@"dataObj is %@",AskProDataObj);
                     
                     FirstName = [[AskProDataObj objectAtIndex:i] valueForKey:@"FirstName"];
                     
                     LastName = [[AskProDataObj objectAtIndex:i] valueForKey:@"LastName"];
                     
                     NSString *ID = [[AskProDataObj objectAtIndex:index] valueForKey:@"AdvisorID"];
                     
                     
                     
                     NSLog(@"ID is %@",ID);
                     
                     NSLog(@"FirstName & LastName is %@ %@",FirstName,LastName);
                     
                     DescriptionAbout = [AskProDataObj valueForKey:@"Description"];
                     
                     //                NSLog(@"DescriptionAbout is %@",DescriptionAbout);
                     
                     [self addContentAdvisorListing];
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strAskProAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
                 
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

- (IBAction)plusBtn:(id)sender
{
    CGFloat pageWidth = self.AdvisorScrollView.frame.size.width; // you need to have a iVar with getter for scrollView
    float fractionalPage = self.AdvisorScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    index = (int)page;
    
    NSLog(@"index is %d",index);

    
    NSLog(@"sender: %@", sender);
    
    PlusViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"PlusViewController"];
        more.setImage = [[AskProDataObj objectAtIndex:index] valueForKey:@"ProfileImage"];
        more.name = [[AskProDataObj objectAtIndex:index] valueForKey:@"FooterTitle"];
        more.address = [[AskProDataObj objectAtIndex:index] valueForKey:@"FooterDescription"];
//        more.moreImage = [[AskProDataObj objectAtIndex:index] valueForKey:@"ProfileImage"];
    combine = [NSString stringWithFormat:@"%@ %@",more.setImage,LastName];
    
//    more.name = [[AskProDataObj objectAtIndex:index] valueForKey:@"FirstName"];
//    
//    more.address = [[AskProDataObj objectAtIndex:index] valueForKey:@"Location"];
//    more.GetAdvisorIDMore = [[AskProDataObj objectAtIndex:index] valueForKey:@"AdvisorID"];
//    
//    NSLog(@"more.name is %@ and more.address is %@ GetAdvisorIDMore is %@",more.name,more.address,more.GetAdvisorIDMore);
//    
//    more.setImage = [[AskProDataObj objectAtIndex:index] valueForKey:@"Profile_Pic_Small"];
    
    [self presentViewController:more animated:YES completion:nil];

}

#pragma mark - Generate Image From Videos

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
    //    NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

#pragma mark - Information 

-(void)AskProForAdvisorFillArray
{
    AskProDataObj = [[NSMutableArray alloc]init];
    [AskProDataObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"4",@"No_Up_Votes",
                               @"ABHIJIT BHADURI",@"FooterTitle",
                               @"The Chief Learning Officer of Wipro Ltd",@"FooterDescription",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               
                               @"advisor1.jpeg",@"ProfileImage",
                               @"Abhijit Bhaduri works as the Chief Learning Officer.",@"info",nil]];
}


#pragma mark - Add Content

-(void)addContentAdvisorListing
{
    {
        int noOfItems = (int)AskProDataObj.count;
        
        //    NSLog(@"noOfItems is %i",noOfItems);
        
        [self.AdvisorScrollView setContentSize:(CGSizeMake(self.AdvisorScrollView.frame.size.width*(noOfItems), self.AdvisorScrollView.frame.size.height))];
        
        for(i = 0; i < noOfItems; i++)
        {
            
            UIView *mainViewWrapper = [[UIView alloc]initWithFrame:CGRectMake(i*self.AdvisorScrollView.frame.size.width, 0, self.AdvisorScrollView.frame.size.width, self.AdvisorScrollView.frame.size.height)];
            [mainViewWrapper setBackgroundColor:[UIColor clearColor]];
            
            [self.AdvisorScrollView addSubview:mainViewWrapper];
            
            UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, mainViewWrapper.frame.size.width - 20, mainViewWrapper.frame.size.height - 30)];
            [mainView setBackgroundColor:[UIColor clearColor]];
            mainView.layer.masksToBounds = true;
            mainView.layer.cornerRadius = 8;
            mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            mainView.layer.borderWidth = 0.8f;
            [mainViewWrapper addSubview:mainView];
            
            float headerFooterHeight = mainView.frame.size.height/4;
            
            //        NSLog(@"headerFooterHeight %f",headerFooterHeight);
            
            float width = mainView.frame.size.width;
//            float height = mainView.frame.size.height - 150;
            
            //        NSLog(@"%f",height);
            
            float x = 0;      // here
            float y = 0;      // here
            float space = 10;
            
            
            UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, headerFooterHeight)];
            [headerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
            [mainView addSubview:headerView];
            
            float lblHeight = (headerView.frame.size.height - space*3)/3 ;
            
            
            UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(x, mainView.frame.size.height - headerFooterHeight, width, headerFooterHeight)];
            [footerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
            [mainView addSubview:footerView];
            
            profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(space,space/4,headerView.frame.size.height - (space*0.7),headerView.frame.size.height - (space*0.7))];
            [profileImage setBackgroundColor:[UIColor clearColor]];
            
            AdvisorImg = [[AskProDataObj objectAtIndex:i] valueForKey:@"ProfileImage"];
            
            NSURL *url = [NSURL URLWithString:AdvisorImg];
            
            NSLog(@"url is %@",url);
            
            [profileImage setImage:[UIImage imageNamed:AdvisorImg]];
            
            profileImage.layer.cornerRadius = profileImage.frame.size.width/2;
            profileImage.layer.borderWidth = 2.0;
            profileImage.layer.masksToBounds = YES;
            
            profileImage.layer.borderColor = [[UIColor whiteColor]CGColor];
            profileImage.contentMode = UIViewContentModeScaleAspectFit;
            profileImage.backgroundColor = [UIColor clearColor];
            
            profiles = profileImage.image;
            [headerView addSubview:profileImage];

            
            UIImageView *voteImage = [[UIImageView alloc] initWithFrame:CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5)),lblHeight,footerView.frame.size.height - footerView.frame.size.height - (lblHeight+(space*1.5)),footerView.frame.size.height - (lblHeight+(space*2.5)))];
            
            UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), space, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight + (space/2))];
            [titleLbl setTextColor:[UIColor darkGrayColor]];
            
            //        combine = [NSString stringWithFormat:@"%@ %@",FirstName,LastName];
            [titleLbl setText:[[AskProDataObj objectAtIndex:i] valueForKey:@"FooterTitle"]];
            [titleLbl setBackgroundColor:[UIColor clearColor]];
            //[titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
            [headerView addSubview:titleLbl];
            
            UILabel *DesignationLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), lblHeight + space/2, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
            [DesignationLbl setTextColor:[UIColor blackColor]];
            [DesignationLbl setText:[[AskProDataObj objectAtIndex:i] valueForKey:@"HeaderTitle"]];
            [DesignationLbl setBackgroundColor:[UIColor clearColor]];
            [DesignationLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
            [headerView addSubview:DesignationLbl];
            
            
            UILabel *userNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), lblHeight+(space*2), width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
            userNameLbl.numberOfLines = 2;
            [userNameLbl setTextColor:[UIColor blackColor]];
            // [userNameLbl setTextColor:[UIColor colorWithRed:157.0f/255.0f green:157.0f/255.0f blue:157.0f/255.0f alpha:1]];
            
//            [userNameLbl setText:[[AskProDataObj objectAtIndex:i] valueForKey:@"info"]];
//            [userNameLbl setBackgroundColor:[UIColor clearColor]];
//            //[userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
//            [headerView addSubview:userNameLbl];

            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 20.0f]];
                [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 18.0f]];
                // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 20.0f]];
            }
            else
            {
                [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
                [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
                // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 14.0f]];
            }
            
            
            UIButton *but = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//            [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            //[but setFrame:answerLbl.frame];
            but.tag = i +333;
            [but setExclusiveTouch:YES];
            [footerView addSubview:but];
            
            float combineHeightOfFooterHeader = mainView.frame.size.height - (headerView.frame.size.height + footerView.frame.size.height);
            
            UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(x, headerView.frame.size.height, width, combineHeightOfFooterHeader)];
            [centerView setBackgroundColor:[UIColor clearColor]];
            [mainView addSubview:centerView];
            
            NSLog(@" main view %f and %f",centerView.frame.size.width, centerView.frame.size.height);
            
            
            ////////////////Added Label//////////////////////
            
            UILabel *headerLbl = [[UILabel alloc]init];
            [headerLbl setTextColor:[UIColor darkGrayColor]];
            // [answerLbl setBackgroundColor:[UIColor clearColor]];
            [headerLbl setFont:[UIFont fontWithName: @"Helvetica Neue" size: 11.0f]];
            headerLbl.numberOfLines = 3;
            [headerLbl setText:[[AskProDataObj objectAtIndex:i] valueForKey:@"info"]];
            
            CGSize answerFrame = [headerLbl sizeThatFits:headerLbl.bounds.size];
            headerLbl.frame = CGRectMake((headerView.frame.origin.x) + (space), (footerView.frame.size.height - answerFrame.height)/2 - (space), answerFrame.width, answerFrame.height);
            [footerView addSubview:headerLbl];
            
            ////////////////Added Label//////////////////////
            
            
            ////////////////////PlayBtn///////////////////////
            
            UIImageView *imgView = [[UIImageView alloc]init];
            imgView.frame = CGRectMake(centerView.frame.origin.x, centerView.frame.origin.y, centerView.frame.size.width, centerView.frame.size.height);
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            
            UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, centerView.frame.size.width, centerView.frame.size.height)];
            //   UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x + centerView.frame.size.width / 8 + (space/2), 0 , width - ((centerView.frame.size.width / 8 + (space/2))*2), combineHeightOfFooterHeader)];
            [playBtn setBackgroundColor:[UIColor clearColor]];//DefaultBackground-1
            //          UIImage *playImae = [UIImage imageNamed:@"play.png"];
            
            NSString  *videoStr = [[[AskProDataObj objectAtIndex:i] valueForKey:@"Question_Answer"] valueForKey:@"video_thumbnail"];
            NSLog(@"videoStr is %@",videoStr);
            
            if(videoStr == (id)[NSNull null])
            {
                videoStr = @"";
            }
            
            NSURL *videothumbnialUrl = [NSURL URLWithString:videoStr];
            
            NSLog(@"videothumbnialUrl is %@",videothumbnialUrl);
            if (videothumbnialUrl == (id)[NSNull null])
            {
                [playBtn setBackgroundImage:[UIImage imageNamed:@"no-icon.png"] forState:UIControlStateNormal];
            }
            [playBtn sd_setImageWithURL:videothumbnialUrl forState:UIControlStateNormal];
            //        sd_setImageWithURL:(NSURL *)url forState:(UIControlState)state placeholderImage:(UIImage *)placeholder
            
            [playBtn  sd_setImageWithURL:videothumbnialUrl forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"no_image_icon.png"]];
            
            playBtn.tag = i+888;
            //        [playBtn setBackgroundImage:imgView.image forState:UIControlStateNormal];
            [playBtn setBackgroundColor:[UIColor clearColor]];
            [playBtn addTarget:self action:@selector(playClickedAvisorAskPro:) forControlEvents:UIControlEventTouchUpInside];
            [centerView addSubview:playBtn];
            
            UIImageView *centerImage = [[UIImageView alloc]initWithFrame:CGRectMake(x, 0 , width, combineHeightOfFooterHeader)];
            [centerImage setBackgroundColor:[UIColor clearColor]];

            /////////////////////
            NSArray *dummyArray = [[[AskProDataObj objectAtIndex:i] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
            
            NSLog(@"videoPath is %@",videoPath);
            NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
            UIImage *staticImage = [self generateThumbImage:streamURL];
            centerImage.image = staticImage;
            
            [centerImage setContentMode: UIViewContentModeScaleAspectFit];
            [centerImage setBackgroundColor:[UIColor blackColor]];
            [centerView addSubview:centerImage];
            
            ////////////////////////
            
            
            
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playbutton.png"]];
            
            img.frame = CGRectMake((centerView.frame.size.width - (centerView.frame.size.width/6)) / 2, (centerView.frame.size.height - (centerView.frame.size.width/6)) / 2, centerView.frame.size.width / 6, centerView.frame.size.width / 6);
            
            [centerView addSubview:img];
    
            HUD.hidden = YES;
        }
    }
}

-(void)playClickedAvisorAskPro:(UIButton*)sender
{
    isAdvisorAskProDone = false;
    isplayAdvisorVideo = false;
    
    NSLog(@"Play clicked");
    
    NSArray *dummyArray = [[[AskProDataObj objectAtIndex:(int)[sender tag]-888] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        playAdvisorAskPro = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
        //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
        [self.view addSubview:playAdvisorAskPro.view];
        playAdvisorAskPro.fullscreen = YES;
        playAdvisorAskPro.controlStyle = MPMovieControlStyleEmbedded;
        playAdvisorAskPro.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        moviePlayer.view.layer.zPosition = 1;
        [playAdvisorAskPro prepareToPlay];
        [playAdvisorAskPro play];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteAdvisorAskPro:) name:MPMoviePlayerPlaybackDidFinishNotification object:playAdvisorAskPro];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickAdvisorAskPro:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert!!" message:@"No video available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)aMoviePlaybackCompleteAdvisorAskPro:(NSNotification*)notification
{
    if (isplayAdvisorVideo == false)
    {
         isAdvisorAskProDone = true;
        
        NSLog(@"second video called");
     
        [playAdvisorAskPro.view removeFromSuperview];
        [playAdvisorAskPro stop];

            if (!isAdvisorAskProDone)
            {
                [playAdvisorAskPro.view removeFromSuperview];
                [playAdvisorAskPro stop];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice" ofType:@"mp4"];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                playAdvisorAskPro = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:playAdvisorAskPro.view];
                playAdvisorAskPro.fullscreen = YES;
                playAdvisorAskPro.controlStyle = MPMovieControlStyleEmbedded;
                playAdvisorAskPro.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [playAdvisorAskPro prepareToPlay];
                [playAdvisorAskPro play];
            }
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteAdvisorAskPro:) name:MPMoviePlayerPlaybackDidFinishNotification object:playAdvisorAskPro];
            
            isplayAdvisorVideo = true;
    }
    else
    {
        
        NSLog(@"stop successfully");
        isplayAdvisorVideo = false;
        
        [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             playAdvisorAskPro.view.alpha = 0;
                         }completion:^(BOOL finished)
         {
             [playAdvisorAskPro.view removeFromSuperview];
             [playAdvisorAskPro stop];
         }];
        
        
    }
    
}

-(void)doneButtonClickAdvisorAskPro:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        NSLog(@"done in if");
        [playAdvisorAskPro.view removeFromSuperview];
        [playAdvisorAskPro stop];
    }
    else
    {
        NSLog(@"done in else");
        [playAdvisorAskPro.view removeFromSuperview];
        [playAdvisorAskPro stop];
        isplayAdvisorVideo = false;
        playAdvisorAskPro = nil;
        isAdvisorAskProDone = false;
    }
    
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    NSLog(@"j is %i",j);
    if (scrollView == self.AdvisorScrollView)
    {
        if (self.AdvisorScrollView.contentOffset.x > 0)
        {
            if (self.AdvisorScrollView.contentOffset.x+self.self.AdvisorScrollView.frame.size.width == self.AdvisorScrollView.contentSize.width)
            {
                rightArrowBtn.enabled=false;
            }
            else
            {
                rightArrowBtn.enabled=true;
            }
            leftArrowBtn.enabled=true;
        }
        else
        {
            leftArrowBtn.enabled=false;
        }
        
        //        if (_a3ScrollView.contentOffset.x == 0)
        //        {
        //            self.leftArrow.hidden = NO;
        //            self.leftArrow.enabled = NO;
        //        }
        //        else
        //        {
        //            self.leftArrow.hidden = NO;
        //            self.leftArrow.enabled = YES;
        //        }
        //        int noOfItems = (int)colletionArray.count;
        //        if (scrollView.contentOffset.x >= noOfItems*_a3ScrollView.frame.size.width)
        //        {
        //          [scrollView setContentOffset:(CGPointMake(0, 0))animated:NO];
        //        }
    }
    
    CGFloat pageWidth = scrollView.frame.size.width; // you need to have a iVar with getter for scrollView
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    index = (int)page;
    
    NSLog(@"index is %d",index);
}

-(void)AdvisorData
{
    NSMutableDictionary *NO_dataAdvisor = [AskProDataObj objectAtIndex:index];
    NSLog(@"NO_dataAdvisor is %@",NO_dataAdvisor);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)askproBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
