//
//  AnswerCustomCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerCustomCell : UITableViewCell

///////////////////////profile Answer TableView////////////////
@property (weak, nonatomic) IBOutlet UIImageView *answerVideoImage;

@property (weak, nonatomic) IBOutlet UIImageView *answerPlayImage;

@property (weak, nonatomic) IBOutlet UILabel *headingLbl;

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@property (weak, nonatomic) IBOutlet UILabel *answerTimeLbl;



@end
