//
//  AnswerCustomCell.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "AnswerCustomCell.h"

@implementation AnswerCustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        ////////////////answer Tab////////////////////////
        
        _headingLbl.frame = CGRectMake(_answerVideoImage.frame.origin.x + _answerVideoImage.frame.size.width + 10, 10 ,_headingLbl.frame.size.width,  _headingLbl.frame.size.height);
        [_headingLbl setFont:[UIFont systemFontOfSize:18]];
        
        _answerVideoImage.frame = CGRectMake(15, _answerVideoImage.frame.origin.y ,130,_answerVideoImage.frame.size.height);
        
        _answerPlayImage.frame = CGRectMake(67, _answerPlayImage.frame.origin.y,_answerPlayImage.frame.size.height  , _answerPlayImage.frame.size.height);
        
        
        
        _nameLbl.frame = CGRectMake(_answerVideoImage.frame.origin.x + _answerVideoImage.frame.size.width + 10, 40 ,_nameLbl.frame.size.width, _headingLbl.frame.size.height);
        [_nameLbl setFont:[UIFont systemFontOfSize:20]];
        
        _answerTimeLbl.frame = CGRectMake(_nameLbl.frame.origin.x + _nameLbl.frame.size.width + 10, _answerTimeLbl.frame.origin.y ,_answerTimeLbl.frame.size.width, _answerTimeLbl.frame.size.height);
        [_answerTimeLbl setFont:[UIFont systemFontOfSize:20]];
        
    }
    else
    {
        NSLog(@"ELSE IN ANSWER CUSTOM CELL");
    }

    // Configure the view for the selected state
}

@end
