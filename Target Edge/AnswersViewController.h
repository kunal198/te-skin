//
//  AnswersViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/25/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>


extern NSArray *videoPlayArray;

@interface AnswersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UIImage *thumbnail;
    NSMutableArray *tableArray;
}
- (IBAction)a3Pop:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *answerTableView;
@property (weak, nonatomic) IBOutlet UILabel *A3Label;

@end
