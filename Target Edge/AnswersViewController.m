//
//  AnswersViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/25/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "AnswersViewController.h"
NSArray *videoPlayArray = nil;

@interface AnswersViewController ()

@end

@implementation AnswersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self FillArray];
    
     self.answerTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [tableArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.answerLbl.text = [[tableArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];
//    Cell.answerLbl.lineBreakMode = NSLineBreakByWordWrapping;
    Cell.answerLbl.numberOfLines = 2;
    
    Cell.authorName.text = [[tableArray objectAtIndex:indexPath.row] valueForKey:@"names"];
    Cell.timeLbl.text = [[tableArray objectAtIndex:indexPath.row] valueForKey:@"time"];
    
    /////
    NSArray *dummyArray = [[[tableArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
    
    NSLog(@"videoPath is %@",videoPath);
    
    NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
    
    NSLog(@"streamURL is %@",streamURL);

    
    Cell.answerImg.image = [self generateThumbImage:streamURL];
    
    ////
//    Cell.authorName.text = [authorArray objectAtIndex:indexPath.row];
//    Cell.timeLbl.text = [timeArray objectAtIndex:indexPath.row];
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    
    [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self PlayVideo:[videoPlayArray objectAtIndex:indexPath.row]];
    
}

-(void)PlayVideo:(NSString*)videoName
{
      
    NSArray *dummyArray = [videoName componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:streamURL];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
        [videoPlayerView.moviePlayer play];
    }
}

- (IBAction)a3Pop:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
    NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

-(void)FillArray
{
    tableArray = [[NSMutableArray alloc]init];
    [tableArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"VideoLink",
                               @"Martin",@"names",
                               @"25 m",@"time",
                               nil]];
    
    [tableArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"VideoLink",
                              @"Bob Cary",@"names",
                             @"20 m",@"time",
                              nil]];
    
}


@end
