//
//  AppDelegate.h
//  Target Edge
//
//  Created by Mrinal Khullar on 11/19/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginVC.h"
#import "navViewController.h"
#import "ViewController.h"
#import "A3ViewController.h"
#import "SSViewController.h"
#import "NewViewController.h"
#import <linkedin-sdk/LISDK.h>
#import <Pushbots/Pushbots.h>
#import "MOOCViewController.h"
#import "ProfileViewController.h"
#import "MyProfileViewController.h"
#import "AskProViewController.h"


extern NSMutableArray *CollectionOfProfileImages;
extern NSMutableArray *dataObj;
extern NSString * deviceTokenString;
extern BOOL isPushBool;
extern NSString *notificationMessage;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    navViewController *_objOfNav;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) UINavigationController *navigationController;
@property (nonatomic,strong) UIStoryboard *storyboard;
@property(assign) float globle_progress_value;


-(NSMutableArray*)GlobalCollectionProfile;
-(void)registerDeviceTokenOnPushBots;

@end

