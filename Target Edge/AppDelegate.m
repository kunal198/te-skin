//
//  AppDelegate.m
//  Target Edge
//
//  Created by Mrinal Khullar on 11/19/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "AppDelegate.h"
#import <AWSCore/AWSCore.h>
NSMutableArray *CollectionOfProfileImages = nil;
NSMutableArray *dataObj;
NSString * deviceTokenString;
BOOL isPushBool;
NSString *notificationMessage;

@interface AppDelegate ()
{
    NSString* notificationViewControllerIdentifer;
}


@end

@implementation AppDelegate
@synthesize globle_progress_value;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    globle_progress_value = 0.0;
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:CognitoRegionType identityPoolId:CognitoIdentityPoolId];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:DefaultServiceRegionType credentialsProvider:credentialsProvider];
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"loginAgainUser"])
    {
        _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        AskProViewController *AskProViewController = [_storyboard instantiateViewControllerWithIdentifier:@"AskProViewController"];
        UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:AskProViewController];
        self.window.rootViewController = navigationController;
    }
     if ([[NSUserDefaults standardUserDefaults] boolForKey:@"loginAgainAdvisor"])
    {
        _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        NewViewController *new = [_storyboard instantiateViewControllerWithIdentifier:@"NewViewController"];
        UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:new];
        self.window.rootViewController = navigationController;
    }
    
    NSLog(@"loginAgainLinkedIn%hhd",[[NSUserDefaults standardUserDefaults] boolForKey:@"loginAgainLinkedIn"]);
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"loginAgainLinkedIn"])
    {
        _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        AskProViewController *AskProViewController = [_storyboard instantiateViewControllerWithIdentifier:@"AskProViewController"];
        UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:AskProViewController];
        self.window.rootViewController = navigationController;
    }

    [self clearNotifications];

    // Override point for customization after application launch.
    return YES;
}

- (void) clearNotifications
{
    [[Pushbots sharedInstance] setBadgeCount:@"0"];
    [[Pushbots sharedInstance] resetBadgeCount];
    [[Pushbots sharedInstance] clearBadgeCount];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // This method will be called everytime you open the app
    // Register the deviceToken on Pushbots
//    [[Pushbots sharedInstance] registerOnPushbots:deviceToken];
    [[NSUserDefaults standardUserDefaults]setObject:deviceToken forKey:@"DeviceToken"];
    
    deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]stringByReplacingOccurrencesOfString: @">" withString: @""]stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"the generated device token string is : %@",deviceTokenString);
    
//    
//    NSString *pushToken = [deviceToken description];
//    pushToken = [pushToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//    [[NSUserDefaults standardUserDefaults] setObject:pushToken forKey:@"device_token_data"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void)registerDeviceTokenOnPushBots
{
     [[Pushbots sharedInstance] registerOnPushbots:[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"]];
    
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Notification Registration Error %@", [error userInfo]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //Handle notification when the user click it while app is running in background or foreground.
    
//    [[Pushbots sharedInstance] receivedPush:userInfo];
 
    NSLog(@"UserInfo: %@", userInfo);
    
    NSDictionary *dictionaryApp = [userInfo valueForKey:@"aps"];
    NSLog(@"dictionaryApp is %@",dictionaryApp);
    
    NSString *alert = [dictionaryApp valueForKey:@"alert"];
    NSLog(@"alert is %@",alert);

    notificationMessage = alert;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    if(application.applicationState == UIApplicationStateActive)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Push Notification" message:[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alertView show];
    }
    else
    {
//        NSLog(@"notificationMessage is %@",);
        
        NSLog(@"isPushNotificationForAdvisor is %hhd",isPushNotificationForAdvisor);
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"loginAgainUser"])
        {
            _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            SSViewController *ss = [_storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
            UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:ss];
            self.window.rootViewController = navigationController;
             isPushBool = true;
        }
        else
        {
            _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            NewViewController *new = [_storyboard instantiateViewControllerWithIdentifier:@"NewViewController"];
            UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:new];
            self.window.rootViewController = navigationController;
            isPushNotificationForAdvisor = true;
        }
//        if (isPushNotificationForAdvisor == true)
//        {
//            _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//            NewViewController *new = [_storyboard instantiateViewControllerWithIdentifier:@"NewViewController"];
//            UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:new];
//            self.window.rootViewController = navigationController;
//        }
//        else
//        {
//            _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//            SSViewController *ss = [_storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
//            UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:ss];
//            self.window.rootViewController = navigationController;
//            isPushBool = true;
//        }
    }
}

-(void) receivedPush:(NSDictionary *)userInfo
{
    //Try to get Notification from [didReceiveRemoteNotification] dictionary
    NSDictionary *pushNotification = [userInfo objectForKey:@"aps"];
    
    NSLog(@"pushNotification is %@",pushNotification);
    
    if(!pushNotification)
    {
        //Try as launchOptions dictionary
        userInfo = [userInfo objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        pushNotification = [userInfo objectForKey:@"aps"];
    }
    
    if (!pushNotification)
        return;
    
    //Get notification payload data [Custom fields]
    
    //For example: get viewControllerIdentifer for deep linking
    notificationViewControllerIdentifer = [userInfo objectForKey:@"notification_identifier"];
    
    //Set the default viewController Identifer
    if(!notificationViewControllerIdentifer)
        notificationViewControllerIdentifer = @"home";
    
    _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    SSViewController *ss = [_storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:ss];
    self.window.rootViewController = navigationController;
    return;
}

//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//    if([title isEqualToString:@"OK"])
//    {
//        [[Pushbots sharedInstance] OpenedNotification];
//    }
//}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([LISDKCallbackHandler shouldHandleUrl:url])
    {
        return [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self clearNotifications];
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(NSMutableArray*)GlobalCollectionProfile
{
    NSString * firstDesc = @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd (a $7.3bn company with 150,000 employees worldwide). Prior to this he worked for Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. He is on the Board of the prestigious program for Chief Learning Officers run by the University of Pennsylvania.\n\n He has been voted as one of the influencers on social media.\n\n Abhijit writes regularly for the Economic Times,People Matters and blogs for Ti mes of India. His articles have appeared in Wall Street Journal and Forbes. He has been a speaker at TEDx and most recently at the INK conference.\n\n Abhijit has written two books of fiction and a non-fiction book on how to hire for cultural fit. He appeared in a cameo appearance in Apna Aasmaan starring Irrfan, Shobana and Anupam Kher. He has acted in short plays in India, Malaysia and US and anchored a successful radio show on Bollywood music in US.\n\nTwitter: @AbhijitBhaduri\nWebsite: abhijitbhaduri.com";
    
    NSString * secDesc = @"Prabir Jha an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. 48 years old, Prabir brings with him an impeccable academic and professional record. After doing his Masters, Prabir was selected for the prestigious Civil Services and as a bureaucrat handled the entire gamut of HR & IR issues of the Indian Ordnance Factories. On his switch to the corporate world after almost 10 years in the government, he has worked in senior management positions for organizations like Thermax & TechMahindra and has been the CHRO at two NYSE-listed Indian majors Dr.Reddy’s and Tata Motors. He has handled all areas in HR, with special interest in Change Management, OD interventions, Global HR Strategy, Employer Branding and Leadership Capability Development.\n\n Prabir has consistently been featured in the \"List of Most Powerful HR Professionals in India\" and has a number of awards to his credit. Among others, he is the recipient of the National Institute of Personnel Management (NIPM) Medal, the Reckitt & Colman Award and the Citibank Leaders Award at XLRI, for the “highest level of academic performance, competence, originality, creativity, communication skills and leadership” as also the Director’s Medal in the Civil Services Foundation Course. He was conferred the Asia HRD Award - 2012 for “outstanding contribution to the field of Human Resource Development”.\n\n In addition, Prabir is an accomplished thought leader whose articles have been published in various publications. He is a sought-after speaker, executive coach and a visiting faculty at various business schools and top management training institutes." ;
    
    NSString * thirdDesc = @"Dr Aquil Busrai graduated in Commerce with a Distinction. He obtained his MBA from Xavier Labour Relations Institute where he was awarded the J M Kumarappa Gold Medal for ranking First.  He later passed LL. B and an Advanced Diploma in Training and Development.  A University rank holder, he was honored with the Bharucha Gold Medal for academic excellence. He was awarded Ph D in April 2012. He is a Certified Executive Coach.\n\n Dr Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. He worked with Shell Malaysia as Director Human Resources, Shell Malaysia and Managing Director of Shell People Services Asia Sdn Bhd. His last corporate assignment was with IBM India Limited as Executive Director Human Resources. He is currently CEO of aquil busrai consulting.\n\n Dr Busrai has been actively associated with Management movement in India having been the President of Delhi Management Association, Vice Chairman for All India Management Association’s Northern Region. He is on the National Council of CII, ISTD and ASSOCHAM and on Board of several education institutes and Corporates. He was awarded the “Pathfinder Award” by National HRD Network for being selected as “Outstanding Seasoned HR Professional”. Last year, he was adjudged amongst the “Most powerful HR Professional in India” at the Asia Pacific HRM Summit in Singapore. He was also bestowed “Lifetime Achievement Award” at Top Rankers Meet in Kuala Lumpur. At its National Convention in November 2012, National HRD Network honoured Dr Busrai with the “Life Time Achievement Award”. In March 2015 he was honoured with The Golden Globe Tigers Award in Kuala Lumpur for Leadership in HR. Dr Busrai is a visiting faculty at Berkeley EMP and UCLA PGPX, besides Indian Institute of Management. He is a Fellow of All India Management Association and Past National President of National HRD Network.\n\n Dr Busrai is a keen student of non-verbal communication and is authoring a book on ‘Body Language’.  He is an ardent wild life enthusiast and a serious wild life photographer.";
    
    NSString * fouthDesc = @"Dr. Sripada Chandrasekhar is the global head of HR at Dr. Reddy’s Laboratoires, a NYSE Listed leading Phrmaceuticals company headquartered in India which operates across 25 countries in the world.\n\n Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors - both in the early economy areas of steel and manufacturing as well as in the more recent domains of Telecom, IT Services and Consulting.\n\n Prior to joining Dr. Reddy’s, Chandra was with IBM, India as Vice President & Head of Human Resources for the India/South Asia region. At IBM, he was a key member of the India Leadership Team and a Director on the Board of IBM India.\n\n Chandra is well known for his expertise in Executive Coaching, Board Level governance, Leadership development, Strategic HR management, writing and public speaking. He teaches at premier business schools in India and abroad, in his spare time. \n\n In his personal capacity, he works actively with non-profit organizations and chapmions the cause of “girl child education“.\n\n Chandra has done his MBA at the Leeds Business School, United Kingdom and has a Ph.D in Organizational Behavior.";
    
    NSString * fifthDesc = @"Vikram Bector is the Chief Talent Officer at Reliance Industries Limited and Has over two decades of experience with some leading Indian conglomerates such as the Tata group (Tata Motors and Tata Steel), the Aditya Birla Group and multinational and multi-locational organizations such as Deloitte, Canon and Mahindra Satyam. \n\n Vikram is an executive coach certified by the International Coach Federation (ICF) and is widely regarded as a thought leader in Human Resources. Vikram was the Chief Talent Officer with Tata Motors and Tata Motors was awarded the Best Learning Organisation in Asia in 2012 under his leadership. Vikram is a certified MBTI facilitator and has served on the American Society for Training and Development’s (ASTD) program evaluation committee for the International Conference and Exposition for 2008 and 2009. He has contributed to the ASTD Management handbook which was published in 2012.\n\n When he is not busy coaching, Vikram enjoys spending time with his wife and two children. He loves trekking, yoga and reading inspiring biographies and is currently based out of Mumbai, India.";
    
    NSString * sixthDesc = @"Vivek Paranjpe - More than 40 years of work experience both in India and abroad.\n Extensive leadership experience, having lead the Indian as well as International teams of very senior leaders both in Indian as well as Multinational corporations.\n Experience in mergers, acquisition, consulting, business transformation etc.\n Current role: Management consultant & Executive coach 2203 till 2009 & 2014 onwards. \n Focused on Business Transformational assignments and Executive coaching to CXO level positions \nPast work experience: \n \n Reliance Industries Ltd (2009 to 2014) Group President HR. This is largest Indian Corporation in private sector.\n\nHewlett Packard: (1988 to 2003) \n\nLast role: Lead Integration of Compaq in to HP for Asia Pacific Region \n\nDid several roles in HP like: \nDirector HR operations, Asia Pacific Region, at Singapore\nDirector HR & Quality, South East Asia Region, at Singapore\nDirector Diversity & Work/Life, Asia Pacific Region, at Singapore\nVP HR, India\n\nBefore Hewlett Packard: (1975-1988)\n\nStarted the career with Hindustan Lever in 1975, and worked with few companies like: ICIM, Hotel Corporation of India and Johnson & Johnson.\n\nEducation:\nB. Sc. Honors, Fergusson College, Pune, 1973\nPostgraduate from XLRI Jamshedpur, 1973-75.\nRecipient of Abdul Bari Gold Medal\nCurrent professional engagements:\nIndependent Director, on the Board of Motilal Oswal Financial Services Limited since January 2011\nOn the board of Governors of ITM University\nOn the advisory Board of SHRM for South East Asia\nPast professional engagements:\nColumnist (5 years) for the monthly publication “People Matters\nMember HR committee of Board of Directors – Punjab National Bank.\nVice President of Delhi Management Association\nGoverning Body member of All India Management Association\nBoard: National HRD Network of India, & Honorary CEO for few years.\nChairman Manpower Committee: American Chamber of Commerce in Singapore\nMember: Many task forces of the Singapore Government, such as Domestic Workers advisory panel, Executive Union Legislation drafting committee etc.\nVisiting Professor: Off & on with management schools like, IMI, XLRI, SIBM etc.\nServed on the Syllabus Committee of the Symbiosis University.\nLead assessor: CII Awards in Business Excellence & HR Excellence\nAuthored two management books in the past.\n\n\nSome of the awards and recognitions received in the past:\nNHRDN’s President’s award for distinguished services and contribution to HR fraternity\nChitpavan Ratna Sanman – An award given by the community that he belongs to\nRecognized by SHRM in 2015 as amongst the top 25 influencers in Social media";
    
    NSString * seventhDesc = @"Marcel R Parker - Marcel has an Honours Degree in Economics from St.Stephen’s College Delhi and an MBA from the Indian Institute of Management,Ahmedabad[IIMA]coupled with 38 years in the Corporate world in some of India’s and the Worlds best organizations-Voltas,ITC,Coromandel Fertilizers,The East India Hotels,Xerox,The Royal Dutch/Shell Group, SAP  India and the Raymond Group.\nHe  Chaired the Board of IKYA Human Capital Solutions[now called Quess Corporation],India’s fastest growing   Business Solutions organization till 2013 and currently serves as an Independent Director and Chief Mentor for the Company .In just 7 years this organization, funded by PE originally  and a strategic investor, in the space of Executive Search,Recruitment, IT & General Staffing,Skill Development ,Professional Services and Managed Facilities has over  2000 employees across 42 offices in India, the UAE,Canada,USA,Malaysia  and The Philippines,with over 1,00,000 Associates and 1000 clients.\nMarcel works closely with advocacy causes in the HR space with the National HRD Network where he has served on the National Board as well as been the Regional President for the West.He was awarded the President’s Award for contribution to the profession too.He has  worked  with  the Employers Federation of India and the CII in helping to enhance  capability for  academics. Marcel was the pioneer in introducing structured Mentoring for students in a leading Business School  and is deeply committed to the cause of mentoring young HR professionals and rural youth.\nCoaching is a passion developed over the last 7  years and after getting himself certifed by the ICF he coaches Leadership Teams,Individuals and a variety of professionals  in MNC’s and leading Indian organizations.The satisfaction of enhancing individual’s performances through structured coaching is a driving force for him. He serves as a Subject Matter Expert on Coaching with  SHRM India and is a Member of the SHRM Advisory Board for India & South Asia’Marcel consults,teaches and serves on the Board of a few educational institutions apart from speaking  and teaching engagements at a variety of forums , mentoring the WoMentoring initiative of the NHRD and working with NextGen HR leaders to enhance capabilities.";
    
    NSString *firstAbout = @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd. Has been part of companies like Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. Abhijit writes regularly for the Economic Times, People Matters and blogs for Times of India.";
    
    CollectionOfProfileImages = [[NSMutableArray alloc]init];
    
    [CollectionOfProfileImages addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"ABHIJIT BHADURI",@"Name",firstDesc,@"Description",firstAbout,@"about",@"bhaduri.png",@"ProfileImages",@"the Chief Learning Officer of Wipro Ltd",@"headerAddress", nil]];
    
    [CollectionOfProfileImages addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"PRABIR JHA",@"Name",secDesc,@"Description",@"Prabir Jha, an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. Prabir brings with him an impeccable academic and professional record.",@"about",@"Prabir.png",@"ProfileImages",@"the Chief Learning Officer of Wipro Ltd",@"headerAddress", nil]];
    
    [CollectionOfProfileImages addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Dr AQUIL BUSRAI",@"Name",thirdDesc,@"Description",@"Dr Aquil Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. Currently runs his own consulting firm.",@"about",@"Aquil.png",@"ProfileImages",@"the Chief Learning Officer of Wipro Ltd",@"headerAddress", nil]];
    
    [CollectionOfProfileImages addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"SRIPADA CHANDRASEKHAR",@"Name",fouthDesc,@"Description",@"Dr. Sripada Chandrarsekhar is the global head of HR at Dr. Reddy’s Laboratoires, Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors.",@"about",@"Sripada.png",@"ProfileImages",@"the Chief Learning Officer of Wipro Ltd",@"headerAddress", nil]];
    [CollectionOfProfileImages addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"VIKRAM BECTOR",@"Name",fifthDesc,@"Description",@"Vikram Bector - Chief Talent Officer at Reliance Industries Limited. Over two decades of experience with some leading Indian conglomerates",@"about",@"Vikram.png",@"ProfileImages",@"the Chief Learning Officer of Wipro Ltd",@"headerAddress", nil]];
    [CollectionOfProfileImages addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"Vivek Paranjpe",@"Name",sixthDesc,@"Description",@"Vivek has more than 40 years of work experience both in India and abroad. He has worked in various HR roles with Reliance Industries, Hewlett Packard, Hindustan Lever and few companies like ICIM, Hotel Corporation of India and Johnson & Johnson ",@"about",@"Vivek Paranjpe.jpg",@"ProfileImages",@"the Chief Learning Officer of Wipro Ltd",@"headerAddress", nil]];
   [CollectionOfProfileImages addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"M R PARKER",@"Name",seventhDesc,@"Description",@"Marcel has 38 years of experience in the Corporate world in some of India’s and the World’s best organizations-Voltas, ITC, Coromandel Fertilizers, The East India Hotels, Xerox, The Royal Dutch/Shell Group, SAP India and the Raymond Group",@"about",@"M. Parker.jpg",@"ProfileImages",@"the Chief Learning Officer of Wipro Ltd",@"headerAddress", nil]];
    
    return CollectionOfProfileImages;


}



@end
