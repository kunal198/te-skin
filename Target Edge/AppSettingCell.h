//
//  AppSettingCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppSettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *settingLbl;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSWITCH;

@end
