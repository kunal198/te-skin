//
//  AppSettingController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppSettingCell.h"
#import "SettingViewController.h"
#import "PrivacyPolicy.h"
#import "TermsAndConditions.h"

extern BOOL isPrivacyPolicySelected;
@interface AppSettingController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *AppSettingArray;
}
@property (weak, nonatomic) IBOutlet UITableView *appSettingTableView;
- (IBAction)appBackBtn:(id)sender;

@end
