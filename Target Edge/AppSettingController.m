//
//  AppSettingController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "AppSettingController.h"

BOOL isPrivacyPolicySelected;
@interface AppSettingController ()

@end

@implementation AppSettingController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppSettingArray = [[NSMutableArray alloc]init];
//    [AppSettingArray addObject:@"Delete My Account"];
    //    [settingArray addObject:@"Discovery Preferences"];
//    [AppSettingArray addObject:@"Notification"];
    [AppSettingArray addObject:@"Privacy Policy"];
    [AppSettingArray addObject:@"Terms and Conditions"];
    
    self.appSettingTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [AppSettingArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AppSettingCell";
    
    AppSettingCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//    
//    if(indexPath.row == 0)
//    {
//        Cell.notificationSWITCH.hidden = false;
//        Cell.settingLbl.text = [AppSettingArray objectAtIndex:indexPath.row];
//    }
//    else
//    {
        Cell.settingLbl.text = [AppSettingArray objectAtIndex:indexPath.row];
        Cell.notificationSWITCH.hidden = true;
//    }
    
    
    

//    Cell.settingLbl.textAlignment = NSTextAlignmentCenter;
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        isPrivacyPolicySelected = true;
        PrivacyPolicy *privacy = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicy"];
//        privacy.titleFromAppSetting = @"Privacy Policy";
        [self.navigationController pushViewController:privacy animated:YES];
    }
    else if (indexPath.row == 1)
    {
        TermsAndConditions *terms = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditions"];
//        privacy.titleFromAppSetting = @"Terms and Conditions";
        [self.navigationController pushViewController:terms animated:YES];
    }
}


- (IBAction)appBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
