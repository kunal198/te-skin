//
//  AskProViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/8/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "A3ViewController.h"
#import "MOOCViewController.h"
#import "SSViewController.h"
#import "StartUpsViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#define AskProCategoryAPI @"http://mobileapi.talentedge.hub1.co/api/Advisor/Question_Category_Listing"

@interface AskProViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *askProArray;
    NSMutableArray *startItemList;
    NSMutableArray *CategoryList;
    MBProgressHUD *HUD;
    NSString *strAskProCategoryAlert;
    
}
@property (weak, nonatomic) IBOutlet UITableView *askProTableView;
@property (assign) BOOL fromApp;
- (IBAction)ssBtnOnAsk:(id)sender;
- (IBAction)moocGrayBtnOnAsk:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *askHightLightView;
- (IBAction)infoBtn:(id)sender;

@end
