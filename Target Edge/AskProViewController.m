//
//  AskProViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/8/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "AskProViewController.h"

@interface AskProViewController ()

@end

@implementation AskProViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = true;
    
    
//    [self CategoryListingForAskPro];
    
    askProArray = [[NSMutableArray alloc]initWithObjects:@"Career Advisory",@"Start up Advisory",@"Skills Advisory", nil];
    startItemList = [[NSMutableArray alloc]initWithObjects:@"Starup Advisory",@"Business Advisory", nil];
    
      self.askProTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    
    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"guide"] == NO)
    {
        [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(pushMethod:) userInfo:nil repeats:NO];
        
       self.askHightLightView.hidden = NO;
        
    }
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_fromApp == true)
    {
        MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
        mooc.moocPush = true;
        [self.navigationController pushViewController:mooc animated:NO];
        _fromApp = false;
    }
}
- (void)doSomeFunkyStuffAskProCategory
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}

-(void)CategoryListingForAskPro
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffAskProCategory) onTarget:self withObject:nil animated:YES];
    
    CategoryList = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:AskProCategoryAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strAskProCategoryAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     CategoryList = [data1 valueForKey:@"Categories"];
                     
                     NSLog(@"CategoryList is %@",CategoryList);
                     HUD.hidden = true;
                     
                     [self.askProTableView reloadData];
                     
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)pushMethod:(NSTimer *)timer
{
    MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
    [self.navigationController pushViewController:mooc animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
//    if (isPushBool == true)
//    {
//        MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
//        [self.navigationController pushViewController:mooc animated:YES];
//    }

    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"pop"] == NO)
    {
        NSLog(@"success");
    }
    else
    {
        self.askHightLightView.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [askProArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    Cell.askProLbl.text = [[CategoryList objectAtIndex:indexPath.row] valueForKey:@"CategoryName"];
   
    
//    Cell.askProLbl.textAlignment = NSTextAlignmentLeft;
    Cell.askProLbl.text = [askProArray objectAtIndex:indexPath.row];
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    //[Cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
 
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    A3ViewController *a3 = [self.storyboard instantiateViewControllerWithIdentifier:@"A3ViewController"];
    a3.selectedAskPro = [askProArray objectAtIndex:indexPath.row];
    [self presentViewController:a3 animated:YES completion:nil];
}

- (IBAction)ssBtnOnAsk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)moocGrayBtnOnAsk:(id)sender
{
    MOOCViewController *mooc = [self.storyboard instantiateViewControllerWithIdentifier:@"MOOCViewController"];
    [self.navigationController pushViewController:mooc animated:YES];
}

- (IBAction)infoBtn:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}
@end
