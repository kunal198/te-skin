//
//  ChangePasswordVC.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/24/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingViewController.h"
#import "MBProgressHUD.h"
#define ChangePasswordAPI @"http://mobileapi.talentedge.hub1.co/Api/Authenticate/ChangePassword"

@interface ChangePasswordVC : UIViewController<UITextFieldDelegate,UIAlertViewDelegate,MBProgressHUDDelegate,UIAlertViewDelegate>
{
    NSMutableArray *changePasswordData;
    MBProgressHUD *HUD;
    NSString *strChange;
}

@property (weak, nonatomic) IBOutlet UITextField *oldPassword;

@property (weak, nonatomic) IBOutlet UITextField *changePassword;

@property (weak, nonatomic) IBOutlet UIButton *submitChanges;


@property (weak, nonatomic) IBOutlet UIImageView *oldPasswordHLV;
@property (weak, nonatomic) IBOutlet UIImageView *changePasswordHLV;
/////////////
- (IBAction)backToSetting:(id)sender;
- (IBAction)submitBtnChanges:(id)sender;

-(void)ChangePasswordMethod;


@end
