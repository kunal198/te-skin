//
//  ChangePasswordVC.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/24/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC

- (void)viewDidLoad
{
    self.oldPassword.delegate = self;
    self.changePassword.delegate = self;
    
    [super viewDidLoad];
    
    [self placeholderColor];
    
//    NSUserDefaults *SaveEmail = [NSUserDefaults standardUserDefaults];
//    // getting an NSString
//    saveEmail = [SaveEmail stringForKey:@"saveEmail"];
//    NSLog(@"In change Password saveEmail is %@",saveEmail);
    // Do any additional setup after loading the view.
}

- (void)doSomeFunkyStuffChangePassword
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000);
    }
}

-(void)ChangePasswordMethod
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffChangePassword) onTarget:self withObject:nil animated:YES];
    
    changePasswordData = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
    NSString *post = [NSString stringWithFormat:@"Email=%@&RoleName=%@&OldPassword=%@&NewPassword=%@",saveEmail,@"User",_oldPassword.text,_changePassword.text];
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:ChangePasswordAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strChange = [items objectAtIndex:1];
                 }
                 

                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strChange delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strChange delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
         [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        NSLog(@"sfhsgcdshc");
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField == self.oldPassword)
    {
        self.oldPasswordHLV.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
    else
    {
        self.changePasswordHLV.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.oldPassword)
    {
          self.oldPasswordHLV.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
    else
    {
          self.changePasswordHLV.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
}
-(void)placeholderColor
{
    //if (UItextField)
    
//    [self.emailForChangePassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.oldPassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.changePassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    self.submitChanges.layer.borderWidth = 2.0f;
    //    _loginFooterOutlet.layer.borderColor= [[UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0]CGColor];
    //    _loginFooterOutlet.layer.borderWidth=2.0f;
    self.submitChanges.layer.borderColor= [[UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0]CGColor];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.oldPassword resignFirstResponder ];
    [self.changePassword resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}

- (IBAction)submitBtnChanges:(id)sender
{
    if ([self.oldPassword.text isEqualToString:@""] || [self.changePassword.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"All text fields are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        [self ChangePasswordMethod];
    }
    
     [self.view endEditing:YES];
}

- (IBAction)backToSetting:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}
@end
