//
//  ContactTalenty.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ContactTalenty : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *emailContactLbl;
@property (weak, nonatomic) IBOutlet UILabel *mobileNoContact;
- (IBAction)emailBtn:(id)sender;
- (IBAction)mobileNoBtn:(id)sender;

- (IBAction)backContact:(id)sender;
@end
