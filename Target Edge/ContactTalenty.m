//
//  ContactTalenty.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "ContactTalenty.h"

@interface ContactTalenty ()

@end

@implementation ContactTalenty

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.emailContactLbl.text = @"dummy@dummy.in";
    self.mobileNoContact.text = @"+91-123-3456781";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)emailBtn:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
        composeVC.mailComposeDelegate = self;
        
        // Configure the fields of the interface.
        [composeVC setToRecipients:@[@"enquiry.dtd@talentedge.in"]];
//        [composeVC setSubject:@"Hello!"];
//        [composeVC setMessageBody:@"Hello from California!" isHTML:NO];
        
        // Present the view controller modally.
        [self presentViewController:composeVC animated:YES completion:nil];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)mobileNoBtn:(id)sender
{
    NSString *phNo = @"+91-124-4901700";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)backContact:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
