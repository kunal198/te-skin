//
//  CustomCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *advisorAskProLbl;
//////////////////

@property (weak, nonatomic) IBOutlet UILabel *SSLbl;
@property (weak, nonatomic) IBOutlet UILabel *moocLbl;
@property (weak, nonatomic) IBOutlet UIImageView *imgOfSelectedMOOC;
@property (weak, nonatomic) IBOutlet UILabel *lblOfSelectedMOOC;
@property (weak, nonatomic) IBOutlet UIImageView *imgOfSettingCell;
@property (weak, nonatomic) IBOutlet UILabel *lblOfSettingCell;





/////////////////////////////////////

@property (weak, nonatomic) IBOutlet UIImageView *hrViewImage;

@property (weak, nonatomic) IBOutlet UILabel *hrViewName;
@property (weak, nonatomic) IBOutlet UILabel *viewUpvotes;
@property (weak, nonatomic) IBOutlet UILabel *viewTime;

@property (weak, nonatomic) IBOutlet UILabel *viewAddress;
@property (weak, nonatomic) IBOutlet UIImageView *videoImage;
@property (weak, nonatomic) IBOutlet UILabel *upvotes;

///////////////////////////////////
@property (weak, nonatomic) IBOutlet UILabel *answerLbl;

@property (weak, nonatomic) IBOutlet UIImageView *answerImg;

@property (weak, nonatomic) IBOutlet UILabel *videoName;

- (IBAction)videoPlayBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *authorName;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

////

@property (weak, nonatomic) IBOutlet UILabel *settingLbl;


///////////more view

@property (weak, nonatomic) IBOutlet UIImageView *moreViewPlayButtonOutlet;

@property (weak, nonatomic) IBOutlet UIImageView *moreCellImage;
@property (weak, nonatomic) IBOutlet UILabel *moreTitleName;
@property (weak, nonatomic) IBOutlet UILabel *moreAuthorName;
@property (weak, nonatomic) IBOutlet UILabel *moreTimeLbl;

////////////selected MOOC view

@property (weak, nonatomic) IBOutlet UIImageView *SelectedMoocImg;
@property (weak, nonatomic) IBOutlet UILabel *selectedMoocName;
@property (weak, nonatomic) IBOutlet UILabel *selectedMoocAuthor;
@property (weak, nonatomic) IBOutlet UIImageView *playButtonImg;

@property (weak, nonatomic) IBOutlet UILabel *selectedMoocTime;




//////Ask Pro Table//////
@property (weak, nonatomic) IBOutlet UILabel *askProLbl;
@property (weak, nonatomic) IBOutlet UIImageView *advImage;
@property (weak, nonatomic) IBOutlet UILabel *advTilte;

@property (weak, nonatomic) IBOutlet UILabel *advNAme;
@property (weak, nonatomic) IBOutlet UILabel *advTime;
@property (weak, nonatomic) IBOutlet UIButton *advReplyOutlet;

@end
