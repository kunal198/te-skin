//
//  CustomCell.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell
@synthesize SSLbl,moocLbl;
@synthesize imgOfSelectedMOOC,lblOfSelectedMOOC;
@synthesize imgOfSettingCell,lblOfSettingCell;
@synthesize hrViewImage,viewAddress,viewTime,viewUpvotes;


- (void)awakeFromNib
{
    // Initialization code
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

//    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
//    {
//        /////////////follower Tab////////////
//        
//        
//        _followerImageView.frame = CGRectMake(_followerImageView.frame.origin.x , _followerImageView.frame.origin.y, _followerImageView.frame.size.height, _followerImageView.frame.size.height);
//        _FollowerAddressLbl.frame = CGRectMake(_followerImageView.frame.origin.x + _followerImageView.frame.size.width + 10 , _FollowerAddressLbl.frame.origin.y, _FollowerAddressLbl.frame.size.width, _FollowerAddressLbl.frame.size.height);
//        _followerNameLbl.frame = CGRectMake(_followerImageView.frame.origin.x + _followerImageView.frame.size.width + 10 , _followerNameLbl.frame.origin.y, _followerNameLbl.frame.size.width, _followerNameLbl.frame.size.height);
//         [_FollowerAddressLbl setFont:[UIFont systemFontOfSize:18]];
//         [_followerNameLbl setFont:[UIFont systemFontOfSize:16]];
//        
//        
//        //////////////upvotes Tab/////////////////////
//        
//        
//        _upvotesImageView.frame = CGRectMake(_upvotesImageView.frame.origin.x , _upvotesImageView.frame.origin.y, _upvotesImageView.frame.size.height, _upvotesImageView.frame.size.height);
//        _upvotesAddressLbl.frame = CGRectMake(_upvotesImageView.frame.origin.x + _upvotesImageView.frame.size.width + 10 , _upvotesAddressLbl.frame.origin.y, _upvotesAddressLbl.frame.size.width, _upvotesAddressLbl.frame.size.height);
//        _upvotesNameLbl.frame = CGRectMake(_upvotesImageView.frame.origin.x + _upvotesImageView.frame.size.width + 10 , _upvotesNameLbl.frame.origin.y, _upvotesNameLbl.frame.size.width, _upvotesNameLbl.frame.size.height);
//        [_upvotesAddressLbl setFont:[UIFont systemFontOfSize:18]];
//        [_upvotesNameLbl setFont:[UIFont systemFontOfSize:16]];
//
//        ////////////////answer Tab////////////////////////
//        
//        headingLbl.frame = CGRectMake(answerVideoImage.frame.origin.x + answerVideoImage.frame.size.width + 10, 10 ,headingLbl.frame.size.width,  headingLbl.frame.size.height);
//        [headingLbl setFont:[UIFont systemFontOfSize:18]];
//        
//        answerVideoImage.frame = CGRectMake(15, answerVideoImage.frame.origin.y ,130,answerVideoImage.frame.size.height);
//        
//        _answerPlayImage.frame = CGRectMake(67, _answerPlayImage.frame.origin.y,_answerPlayImage.frame.size.height  , _answerPlayImage.frame.size.height);
//        
//        
//      
//        _nameLbl.frame = CGRectMake(answerVideoImage.frame.origin.x + answerVideoImage.frame.size.width + 10, 40 ,_nameLbl.frame.size.width, headingLbl.frame.size.height);
//        [_nameLbl setFont:[UIFont systemFontOfSize:20]];
//        
//        answerTimeLbl.frame = CGRectMake(_nameLbl.frame.origin.x + _nameLbl.frame.size.width + 10, answerTimeLbl.frame.origin.y ,answerTimeLbl.frame.size.width, answerTimeLbl.frame.size.height);
//        [answerTimeLbl setFont:[UIFont systemFontOfSize:20]];
//        
//        /////////////////////question Tab/////////////////////
//        
//        _questionImageView.frame = CGRectMake(15, _questionImageView.frame.origin.y ,130,_questionImageView.frame.size.height );
//
//        _questionPlayImage.frame = CGRectMake(67, _questionPlayImage.frame.origin.y,_questionPlayImage.frame.size.height  , _questionPlayImage.frame.size.height );
//        
//        _questionHeadinglabel.frame = CGRectMake(_questionImageView.frame.origin.x + _questionImageView.frame.size.width + 10, 10 ,_questionHeadinglabel.frame.size.width,  _questionHeadinglabel.frame.size.height);
//        [_questionHeadinglabel setFont:[UIFont systemFontOfSize:18]];
//        
//         _questionNameLbl.frame = CGRectMake(_questionImageView.frame.origin.x + _questionImageView.frame.size.width + 10,40,_questionNameLbl.frame.size.width, _questionHeadinglabel.frame.size.height);
//        [_questionNameLbl setFont:[UIFont systemFontOfSize:20]];
//        
//        
//        _questionTimeLbl.frame = CGRectMake(_questionNameLbl.frame.origin.x + _questionNameLbl.frame.size.width + 10, _questionNameLbl.frame.origin.y ,_questionTimeLbl.frame.size.width, _questionTimeLbl.frame.size.height);
//        [_questionTimeLbl setFont:[UIFont systemFontOfSize:20]];
//        
//        ////////////////////selected MOOC Category View ////////////////////
//        
//        _playButtonImg.frame = CGRectMake(135,30,30,30);
//        
////        _SelectedMoocImg.frame = CGRectMake(15, _questionImageView.frame.origin.y ,150,_questionImageView.frame.size.height );
//        
////        _questionPlayImage.frame = CGRectMake(67, _questionPlayImage.frame.origin.y,_questionPlayImage.frame.size.height  , _questionPlayImage.frame.size.height );
//        
//        _selectedMoocName.frame = CGRectMake(_selectedMoocName.frame.origin.x, 5 ,_selectedMoocName.frame.size.width,  _selectedMoocName.frame.size.height);
//        [_selectedMoocName setFont:[UIFont systemFontOfSize:20]];
//        
//        _selectedMoocAuthor.frame = CGRectMake(_selectedMoocAuthor.frame.origin.x,50,_selectedMoocAuthor.frame.size.width, _selectedMoocAuthor.frame.size.height);
//        [_selectedMoocAuthor setFont:[UIFont systemFontOfSize:18]];
//        
////        
//        _selectedMoocTime.frame = CGRectMake(_selectedMoocTime.frame.origin.x , _selectedMoocTime.frame.origin.y ,_selectedMoocTime.frame.size.width, _selectedMoocTime.frame.size.height);
//        [_selectedMoocTime setFont:[UIFont systemFontOfSize:18]];
//
//        ////////moreview////////
//        
//        _moreViewPlayButtonOutlet.frame = CGRectMake(135,30,30,30);
//        
//      // _moreViewPlayButtonOutlet.frame = CGRectMake(_moreViewPlayButtonOutlet.frame.origin.x,10 ,_moreViewPlayButtonOutlet.frame.size.width,  _moreViewPlayButtonOutlet.frame.size.height);
//        
//    }

    
    // Configure the view for the selected state
}

- (IBAction)videoPlayBtn:(id)sender
{
    
}
- (IBAction)advReplyBtn:(id)sender {
}
@end
