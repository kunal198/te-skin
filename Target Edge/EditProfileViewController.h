//
//  EditProfileViewController.h
//  Talent Edge
//
//  Created by brst on 12/19/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "A3ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define Update_ProfileAPI  @"http://mobileapi.talentedge.hub1.co/Api/profile/UpdateProfile"
#define User_Profile_API @"http://mobileapi.talentedge.hub1.co/Api/profile/User_Profile_Details"

@interface EditProfileViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,UITextFieldDelegate,MBProgressHUDDelegate,UITextViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    NSMutableArray *updateProfileData;
    MBProgressHUD *HUD;
    UIImage *image_updateProfilePic;
    NSData *imageDataOfImage;
    NSString *strEditAlert;
    NSMutableArray *UserDataAgain;
}

@property (weak, nonatomic) IBOutlet UITextField *lName;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *location;
@property (weak, nonatomic) IBOutlet UITextField *mobile;

@property (weak, nonatomic) IBOutlet UITextField *fName;
@property (strong, nonatomic) UIImage *blurImage;
@property (weak, nonatomic) IBOutlet UIImageView *blurProfileImage;
@property (weak, nonatomic) IBOutlet UIImageView *fNameHighlightedView;
@property (weak, nonatomic) IBOutlet UIImageView *mobileHighlightedView;
@property (weak, nonatomic) IBOutlet UIImageView *locationHighlightedView;
@property (weak, nonatomic) IBOutlet UIImageView *emailHighlightedView;
@property (weak, nonatomic) IBOutlet UIImageView *lNameHighlightedView;
@property (nonatomic) CGFloat _currentKeyboardHeight;
- (IBAction)backButton:(id)sender;

- (IBAction)changeProfileImage:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (nonatomic, retain) UIImagePickerController *imgPicker;
@property (weak, nonatomic) IBOutlet UITextView *about;
- (IBAction)saveBtn:(id)sender;

-(void)UpdateProfile;
-(NSData*)testData;
-(void)UserProfileAgain;
@property (weak, nonatomic) IBOutlet UIView *editLoaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *editLoader;
@property (weak, nonatomic) IBOutlet UIView *editSubview;

@end
