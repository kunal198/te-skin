//
//  EditProfileViewController.m
//  Talent Edge
//
//  Created by brst on 12/19/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController
@synthesize imgPicker;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _email.enabled = NO;
    _mobile.enabled = NO;
    
    self.imgPicker = [[UIImagePickerController alloc] init];
    //self.imgPicker.allowsImageEditing = YES;
    self.imgPicker.delegate = self;
    self.imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    // Do any additional setup after loading the view.
    
    _profileImage.layer.cornerRadius =  _profileImage.frame.size.width/2;
    _profileImage.layer.borderWidth = 2.0;
    _profileImage.layer.masksToBounds = YES;
    _profileImage.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    self.about.layer.borderWidth = 2.0f;
    self.about.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.editLoaderView.hidden = false;
    
    [self.editLoader startAnimating];

      [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(editViewIsLoading) userInfo:NULL repeats:NO];
}

-(void)editViewIsLoading
{
    if (User_About == (id)[NSNull null])
    {
        User_About = @"";
        
        if ([User_About isEqualToString:@""])
        {
            User_About = @"Please let us know something about you";
        }
    }
    _fName.text = User_FirstName;
    _lName.text = User_LastName;
    _location.text = userLocation;
    _about.text = User_About;
    _email.text = saveEmail;
    _mobile.text = saveMobile;
    
    if(profileImageStr == (id)[NSNull null])
    {
        self.profileImage.image = [UIImage imageNamed:@"user_placeholder.png"];
    }
    else
    {
        [self.profileImage sd_setImageWithURL:[NSURL URLWithString:profileImageStr] placeholderImage:[UIImage imageNamed:@"user_placeholder.png"]];
    }
    
    self.editLoaderView.hidden = true;
    
    [self.editLoader stopAnimating];
}

- (void)doSomeFunkyStuffUpdateProfile
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}

-(void)UpdateProfile
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);

    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffUpdateProfile) onTarget:self withObject:nil animated:YES];
    
    updateProfileData = [[NSMutableArray alloc]init];
    
    NSData *imageBase64Data = [imageDataOfImage base64EncodedDataWithOptions:0];
    
    NSString *imageBase64String = [[NSString alloc] initWithData:imageBase64Data encoding:NSUTF8StringEncoding];

//    NSLog(@"imageBase64String is %@",imageBase64String);

    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[imageBase64String length]];
    NSLog(@"postLength is %@",postLength);
    
    imageBase64String = [imageBase64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSString *post;
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
         post = [NSString stringWithFormat:@"user_id=%@&Location=%@&FirstName=%@&LastName=%@&ProfilePic=%@&ContentLength=%@&FileName=%@&ContentType=%@&About=%@",LinkedIn_ID, _location.text,self.fName.text,_lName.text,imageBase64String,postLength,@"test.jpg",@"image/jpeg",_about.text];
    }
    else
    {
        post = [NSString stringWithFormat:@"user_id=%@&Location=%@&FirstName=%@&LastName=%@&ProfilePic=%@&ContentLength=%@&FileName=%@&ContentType=%@&About=%@",Creater_User_ID, _location.text,self.fName.text,_lName.text,imageBase64String,postLength,@"test.jpg",@"image/jpeg",_about.text];
    }

    NSLog(@"post is %@",post);

//    NSString *headerStr = [NSString stringWithFormat:@"Basic %@",base64String];
//    
//    NSLog(@"headerStr is %@",headerStr);
//    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:Update_ProfileAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (data)
         {
             id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             NSLog(@"result updateProfileData is %@",result);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert in updateProfileData is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data(updateProfileData) is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strEditAlert = [items objectAtIndex:1];
                 }

                 
                 if (success)
                 {
                     NSLog(@"UserProfile loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     updateProfileData = [data1 valueForKey:@"details"];
                     
                     NSLog(@"updateProfileData is %@",updateProfileData);
                     
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strEditAlert delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strEditAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
             });
         }
         else if (error)
             
             NSLog(@"%@",error);
         
     }];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        self.editLoaderView.hidden = false;
        
        [_editLoader startAnimating];
        [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(loadingUSerProfileAgain) userInfo:NULL repeats:NO];
    }
    else
    {
        NSLog(@"sfhsgcdshc");
    }
}

-(void)loadingUSerProfileAgain
{
    [self UserProfileAgain];
    
    self.editLoaderView.hidden = false;
    [_editLoader stopAnimating];
}

- (void) keyboardWillShow:(NSNotification *)note
{
    __currentKeyboardHeight = 0.0f;
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
    
    __currentKeyboardHeight = kbSize.height;
    
    // move the view up by 30 pts
    CGRect frame = self.view.frame;
    frame.origin.y = -30;
    
    //    [UIView animateWithDuration:0.3 animations:^{
    //        self.view.frame = frame;
    //    }];
}

- (void) keyboardDidHide:(NSNotification *)note
{
    
    __currentKeyboardHeight = 0.0f;
    // move the view back to the origin
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    //    [UIView animateWithDuration:0.3 animations:^{
    //        self.view.frame = frame;
    //    }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
     _profileImage.layer.cornerRadius = _profileImage.frame.size.width/2;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardDidHideNotification"
                                               object:nil];
    
    if (textField == self.fName)
    {
        _fNameHighlightedView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
    else if(textField == self.lName)
    {
        _lNameHighlightedView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
    else if(textField == self.mobile)
    {
        _mobileHighlightedView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x, self.editSubview.frame.origin.y - (_email.frame.size.height + _location.frame.size.height + _mobile.frame.size.height + _mobile.frame.size.height + _mobile.frame.size.height), self.editSubview.frame.size.width, self.editSubview.frame.size.height);
        
    }
    else if(textField == self.location)
    {
        _locationHighlightedView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
        
//        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x, self.editSubview.frame.origin.y - (_email.frame.size.height * 4), self.editSubview.frame.size.width, self.editSubview.frame.size.height);
//        
    }
    else
    {
        _emailHighlightedView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
        
        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x, self.editSubview.frame.origin.y - (_email.frame.size.height + _email.frame.size.height), self.editSubview.frame.size.width, self.editSubview.frame.size.height);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _fName)
    {
        _fNameHighlightedView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
    else if(textField == _lName)
    {
        _lNameHighlightedView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
    else if(textField == _mobile)
    {
        _mobileHighlightedView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x, 0, self.editSubview.frame.size.width, self.editSubview.frame.size.height);
        
    }
    
    else if(textField == _location)
    {
        _locationHighlightedView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        
//        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x, 0, self.editSubview.frame.size.width, self.editSubview.frame.size.height);
    }
    else
    {
        _emailHighlightedView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x, 0, self.editSubview.frame.size.width, self.editSubview.frame.size.height);
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if(textView == _about)
    {
        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x,self.editSubview.frame.origin.y - 200, self.editSubview.frame.size.width, self.editSubview.frame.size.height);
        NSLog(@"editSubview x is %f and editSubview y is %f",self.editSubview.frame.origin.x,self.editSubview.frame.origin.y);
    }
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if(textView == _about)
    {
        self.editSubview.frame = CGRectMake(self.editSubview.frame.origin.x,self.editSubview.frame.origin.y + 200, self.editSubview.frame.size.width, self.editSubview.frame.size.height);
    }
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_fName resignFirstResponder ];
    [_lName resignFirstResponder ];
    [_about resignFirstResponder];
    [_location resignFirstResponder];
    [_email resignFirstResponder ];
    [_mobile resignFirstResponder];
}



- (IBAction)backButton:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)changeProfileImage:(id)sender
{
//    UIImagePickerController *pickerController = [[UIImagePickerController alloc]init];
//    pickerController.delegate = self;
//    
//    [self presentViewController:pickerController animated:YES completion:nil];

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil otherButtonTitles: @"Take a new photo",@"Choose from existing", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        return;
    };
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    imagePicker.allowsEditing = YES;
    
    if (buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if (buttonIndex == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo
{
    _profileImage.image = img;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileimage.png"];
    
    NSLog(@"savedImagePath%@",savedImagePath);
    image_updateProfilePic = img;
    imageDataOfImage = UIImageJPEGRepresentation(image_updateProfilePic,0.0f);
    [imageDataOfImage writeToFile:savedImagePath atomically:NO];
  
   [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)saveBtn:(id)sender
{
    if([_fName.text isEqualToString:@""] || [_lName.text isEqualToString:@""] || [_email.text isEqualToString:@""]||[_about.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"All text fields are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
//    else if([_fName.text isEqualToString:@""])
//    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter first name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
//    }
//    else if([_lName.text isEqualToString:@""])
//    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter last name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
//    }
//    else if([_email.text isEqualToString:@""])
//    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter your location" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
//    }
//    else if([_about.text isEqualToString:@""])
//    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter your information" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
//    }
    else
    {
        [self UpdateProfile];
    }
    [self.view endEditing:YES];
}


-(void)UserProfileAgain
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffUpdateProfile) onTarget:self withObject:nil animated:YES];
    
    UserDataAgain = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post;
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"user_id=%@",LinkedIn_ID];
    }
    else
    {
        post = [NSString stringWithFormat:@"user_id=%@",Creater_User_ID];
    }
    
    NSLog(@"post in UserProfile is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:User_Profile_API]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (data)
         {
             id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             NSLog(@"result UserProfile is %@",result);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert in UserProfile is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data(UserProfile) is %@",data1);
                 
                 
                 if (success)
                 {
                     NSLog(@"UserProfile loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     UserDataAgain = [data1 valueForKey:@"details"];
                     
                     NSLog(@"userProfileData is %@",UserDataAgain);
                     
                     profileImageStr = [[UserDataAgain objectAtIndex:0] valueForKey:@"ProfileImageThumbnailPath"];
                     
                     NSLog(@"userProfileData is %@",[UserDataAgain valueForKey:@"ProfileImageThumbnailPath"]);
                     
                     if (profileImageStr == (id)[NSNull null])
                     {
                         _profileImage.image = [UIImage imageNamed:@"user_placeholder.png"];
                     }
                     else
                     {
                         [_profileImage sd_setImageWithURL:[NSURL URLWithString:profileImageStr] placeholderImage:[UIImage imageNamed:@"user_placeholder.png"]];
                     }
                     
                     //                     self.blurImage.image = [self blurredImageWithImage:img];
                     
                     User_FirstName = [[UserDataAgain objectAtIndex:0] valueForKey:@"FirstName"];
                     
                     User_LastName = [[UserDataAgain objectAtIndex:0] valueForKey:@"LastName"];
                     
//                     self.UserNameLbl.text = [NSString stringWithFormat:@"%@ %@",User_FirstName,User_LastName];
                     
                     fullName = [NSString stringWithFormat:@"%@ %@",User_FirstName,User_LastName];
                     
//                     self.UserAddressLbl.text = [[UserDataAgain objectAtIndex:0] valueForKey:@"Location"];
                     
                     userLocation = [[UserDataAgain objectAtIndex:0] valueForKey:@"Location"];
                     
                     User_About = [[UserDataAgain objectAtIndex:0] valueForKey:@"About"];
                     
                     saveEmail = [[UserDataAgain objectAtIndex:0] valueForKey:@"Email"];
                     
                     NSLog(@"saveEmail is %@",saveEmail);
                     
                     [self dismissViewControllerAnimated:true completion:nil];
                     
                     HUD.hidden = YES;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
             });
         }
         else if (error)
             
             NSLog(@"%@",error);
         
     }];
}


@end
