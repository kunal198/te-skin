//
//  FilterTableViewCell.h
//  
//
//  Created by Mrinal Khullar on 1/21/16.
//
//

#import <UIKit/UIKit.h>

@interface FilterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@property (weak, nonatomic) IBOutlet UIButton *checkBox;

/////////////////////////////

@property (weak, nonatomic) IBOutlet UIImageView *myQuestionsA3ImageView;
@property (weak, nonatomic) IBOutlet UILabel *myquesTitle;
@property (weak, nonatomic) IBOutlet UILabel *muquesName;
@property (weak, nonatomic) IBOutlet UILabel *myquesTimeLbl;

@property (weak, nonatomic) IBOutlet UIImageView *checkboxImageView;
@property (weak, nonatomic) IBOutlet UIButton *shareBtnOutlet;

@end
