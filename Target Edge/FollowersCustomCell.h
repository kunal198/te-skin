//
//  FollowersCustomCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowersCustomCell : UITableViewCell


/////////////////profile Follower Tab////////

@property (weak, nonatomic) IBOutlet UIImageView *followerImageView;
@property (weak, nonatomic) IBOutlet UILabel *FollowerAddressLbl;

@property (weak, nonatomic) IBOutlet UILabel *followerNameLbl;



@end
