 //
//  FollowersCustomCell.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "FollowersCustomCell.h"

@implementation FollowersCustomCell

- (void)awakeFromNib
{
    _followerImageView.layer.cornerRadius = _followerImageView.frame.size.width/2;
    
   
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    
    [super setSelected:selected animated:animated];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        /////////////follower Tab////////////
        
        
        _followerImageView.frame = CGRectMake(_followerImageView.frame.origin.x , _followerImageView.frame.origin.y, _followerImageView.frame.size.height, _followerImageView.frame.size.height);
        _FollowerAddressLbl.frame = CGRectMake(_followerImageView.frame.origin.x + _followerImageView.frame.size.width + 10 , _FollowerAddressLbl.frame.origin.y, _FollowerAddressLbl.frame.size.width, _FollowerAddressLbl.frame.size.height);
        _followerNameLbl.frame = CGRectMake(_followerImageView.frame.origin.x + _followerImageView.frame.size.width + 10 , _followerNameLbl.frame.origin.y, _followerNameLbl.frame.size.width, _followerNameLbl.frame.size.height);
        [_FollowerAddressLbl setFont:[UIFont systemFontOfSize:18]];
        [_followerNameLbl setFont:[UIFont systemFontOfSize:16]];
        
    }
    else
    {
        NSLog(@"IN ELSE-CONDITION");
    }

    // Configure the view for the selected state
}

@end
