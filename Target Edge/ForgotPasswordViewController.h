//
//  ForgotPasswordViewController.h
//  Talent Edge
//
//  Created by brst on 12/3/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogInWithEmailViewController.h"
#define ForgotPasswordAPI @"http://mobileapi.talentedge.hub1.co/Api/Activity/ForgotPassword"
#import "MBProgressHUD.h"

@interface ForgotPasswordViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIAlertViewDelegate>
{
    NSMutableData *ForgotMutableData;
    NSMutableArray *_ForgotArray;
    MBProgressHUD *HUD;
    NSString *strForgotAlert;
}

@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UILabel *headerLbl;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIImageView *emailHighlightedview;
@property (weak, nonatomic) IBOutlet UIButton *verificationButtonOutlet;
- (IBAction)verificationbutton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *verificationCodeView;
@property (weak, nonatomic) IBOutlet UIImageView *codehighlightedView;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UILabel *codeHeaderLbl;
@property (weak, nonatomic) IBOutlet UIButton *enterButton;
- (IBAction)enterButtonAction:(id)sender;
- (IBAction)crossWhiteBtn:(id)sender;

@property(strong,nonatomic) NSString *RecevingToken;

-(void)ForgotPassword;

// This method is used to receive the data which we get using post method.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data;

// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;

// This method is used to process the data after connection has made successfully.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;


@end
