//
//  ForgotPasswordViewController.m
//  Talent Edge
//
//  Created by brst on 12/3/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()
{
    NSMutableData *forgotPasswordData;
}
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self placeholderColor];
    [self.verificationCodeView setHidden:YES];
    
  
    
    // Do any additional setup after loading the view.
    
  
}
- (void)doSomeFunkyStuffForgot
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(5000000);
    }
}

-(void)ForgotPassword
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffForgot) onTarget:self withObject:nil animated:YES];
//    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
//    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post = [NSString stringWithFormat:@"Email=%@&RoleName=%@",self.emailTextField.text,@"User"];
    NSLog(@"post is %@",post);
   
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSLog(@"postData is %@",postData);
    
//    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:ForgotPasswordAPI]];
    
    [request setHTTPMethod:@"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
     //[request setValue:@"User" forHTTPHeaderField:@"RoleName"];
//    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setHTTPBody:postData];
    
    NSURLConnection *theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    if( theConnection )
    {
        NSLog(@"Got Connection");
        
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"response is %@", response);
    
    ForgotMutableData = [[NSMutableData alloc]init];
    
    //    NSMutableArray *headerArray = [[NSMutableArray alloc]init];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
    ForgotMutableData = [[NSMutableData alloc]initWithData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    id result = [NSJSONSerialization JSONObjectWithData:ForgotMutableData options:kNilOptions error:nil];
    NSLog(@"result is %@",result);
    
    BOOL success = [[result objectForKey:@"result"] boolValue];
    
    NSString *msgAlert = [result objectForKey:@"message"];
    
//    NSArray *Successdata = [result valueForKey:@"Object"];
    
//    NSString *errorAlert = [Successdata valueForKey:@"message"];
    
//    NSLog(@"data is %@",Successdata);
    
    NSLog(@"msgAlert is %@",msgAlert);
    
    NSArray *items = [msgAlert componentsSeparatedByString:@"~"];
    
    if(items.count>1)
    {
        strForgotAlert = [items objectAtIndex:1];
    }

    
        if (success)
        {
            NSLog(@"login in else part");
            
            //        [_ForgotArray removeAllObjects];
            //
            //        NSLog(@"for loop %@", Successdata);
            //
            //        [_ForgotArray addObject:Successdata];
            //
            //        NSLog(@" _objArray is %lu",(unsigned long)_ForgotArray);
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Password has been successfully sent. Please check your email." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            HUD.hidden = true;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter your registered email id." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            HUD.hidden = true;
            
        }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _emailTextField)
    {
        _emailHighlightedview.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField == _emailTextField)
    {
        _emailHighlightedview.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
}

-(void)placeholderColor
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        [_headerLbl setFont:[UIFont systemFontOfSize:25]];
        [_codeHeaderLbl setFont:[UIFont systemFontOfSize:25]];
        [_verificationButtonOutlet.titleLabel setFont:[UIFont systemFontOfSize:22]];
        [_enterButton.titleLabel setFont:[UIFont systemFontOfSize:22]];
        [_emailTextField setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
        [_codeTextField setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
}

   // _enterButton.layer.borderWidth=2.0f;
  //  _enterButton.layer.borderColor= [[UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0]CGColor];
    _verificationButtonOutlet.layer.borderWidth=2.0f;
    _verificationButtonOutlet.layer.borderColor= [[UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0]CGColor];

 //   _verificationCodeView.hidden = YES;
    _emailView.hidden = NO;
    [_emailTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
  //  [_codeTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
}





-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_emailTextField resignFirstResponder ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}

-(BOOL)checkingEmail:(NSString *)checkEmail
{
    NSLog(@"checking Email");
    BOOL var1 = true;
    
    
    if ([checkEmail containsString:@"@"])                                          // if string contains @
    {
        NSArray *myWords = [checkEmail componentsSeparatedByString:@"@"];
        
        //myWords is an array,separate strings by @ and keeping string into array
        
        for (int i = 0; i<[myWords count]; i++)                                    //checks string one by one
        {
            if ([myWords[i] isEqual: @""])  // if string is empty than return false and myWords[i] is a string
                
            {
                var1 = false;
            }
        }
        
        if (var1)
        {
            
            if ([myWords[[myWords count] - 1] containsString:@"."])
            {
                NSLog(@"working %@",myWords[[myWords count] - 1]);
                
                NSArray *myDotWords = [myWords[[myWords count] - 1] componentsSeparatedByString:@"."];
                
                for (int i = 0; i<[myDotWords count]; i++)
                {
                    if ([myDotWords[i] isEqual: @""])
                    {
                        var1 = false;
                    }
                }
            }
            else
            {
                var1 = false;
            }
        }
    }
    else
    {
        var1 = false;
    }
    
    
    return var1;
}


- (IBAction)verificationbutton:(id)sender
{
    [self ForgotPassword];
    [self.view endEditing:YES];
}

//- (IBAction)enterButtonAction:(id)sender
//{
//    [self ForgotPassword];
////    [self.navigationController popViewControllerAnimated:YES];
//}

- (IBAction)crossWhiteBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
