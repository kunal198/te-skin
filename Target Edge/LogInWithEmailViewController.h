//
//  LogInWithEmailViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/2/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForgotPasswordViewController.h"
#define LoginAPI @"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Authenticate"
#define signUpWithLinkedINAPI @"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Register"
#define PushAPI @"http://mobileapi.talentedge.hub1.co/Api/profile/AddPushDetails"
#import "MBProgressHUD.h"
#import "NewViewController.h"
#import "AppDelegate.h"
#import "AskProViewController.h"

extern NSString *userID;
extern  NSString *role_Name;
extern NSString *AdvisorProfilePic;
extern NSString *saveEmail;
extern NSString *savedfirst_name;
extern  NSString *savedlast_name;
extern NSString *savedlocation;

@interface LogInWithEmailViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate,UIAlertViewDelegate>
{
    NSMutableArray *_objArray;
    NSMutableData *loginMutableData;
    MBProgressHUD *HUD;
    NSString *strLoginAlert;
    NSMutableArray *_objAdvisorArray;
    BOOL isTickSelected;
    BOOL isLoginUser;
    NSString *strErrorAlert;
    NSMutableArray *linkedInArray;
    NSMutableArray *UserpushObj;
    
}

@property (weak, nonatomic) IBOutlet UITextField *emailtextField;
@property (weak, nonatomic) IBOutlet UITextField *passowrdTextfield;
@property (weak, nonatomic) IBOutlet UIImageView *loginHiglightedView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordHighLightedView;
@property (weak, nonatomic) IBOutlet UILabel *loginVCHeaderLbl;
@property (weak, nonatomic) IBOutlet UIButton *logInButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *loginFooterOutlet;

- (IBAction)alreadyHaveAccountBtn:(id)sender;

- (IBAction)rememberMeBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *outletOfRememberMe;


- (IBAction)forgotPasswordButton:(id)sender;

- (IBAction)loginButtonClicked:(id)sender;

-(void)Login;
-(void)pushMethodForUser;
-(void)AdvisorLogin;

// This method is used to receive the data which we get using post method.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data;

// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;

// This method is used to process the data after connection has made successfully.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
@end
