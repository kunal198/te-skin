//
//  LogInWithEmailViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/2/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "LogInWithEmailViewController.h"
#import "SSViewController.h"
#import "ForgotPasswordViewController.h"
NSString *userID;
NSString *role_Name;
NSString *AdvisorProfilePic;
NSString *saveEmail;
NSString *savedfirst_name;
NSString *savedlast_name;
NSString *savedlocation;

@interface LogInWithEmailViewController ()

@end

@implementation LogInWithEmailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ self placeholderColor];
    isTickSelected = YES;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
         [_loginVCHeaderLbl setFont:[UIFont systemFontOfSize:25]];
         [_logInButtonOutlet.titleLabel setFont:[UIFont systemFontOfSize:22]];
         [_loginFooterOutlet.titleLabel setFont:[UIFont systemFontOfSize:22]];
         [_emailtextField setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
         [_passowrdTextfield setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
    }
}
- (void)doSomeFunkyStuffLogin
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                               (CFStringRef)self,
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                               CFStringConvertNSStringEncodingToEncoding(encoding)));
}

//-(void)SignUpWithLinkedIn
//{
//    HUD = [[MBProgressHUD alloc] initWithView:self.view];
//    HUD.labelText = @"Loading...";
//    //    HUD.detailsLabelText = @"Just relax";
//    // HUD.mode = MBProgressHUDModeDeterminate;
//    [self.view addSubview:HUD];
//    
//    linkedInArray = [[NSMutableArray alloc] init];
//    
////    NSData *imageBase64DataReg = [imageData base64EncodedDataWithOptions:0];
////    
////    NSString *imageBase64StringReg = [[NSString alloc] initWithData:imageBase64DataReg encoding:NSUTF8StringEncoding];
////    
////    NSString *postLengthReg = [NSString stringWithFormat:@"%lu", (unsigned long)[imageBase64StringReg length]];
////    NSLog(@"postLengthReg is %@",postLengthReg);
////    
////    imageBase64StringReg = [imageBase64StringReg stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
//    
//    [HUD showWhileExecuting:@selector(doSomeFunkyStuffLogin) onTarget:self withObject:nil animated:YES];
//    NSString *post = [NSString stringWithFormat:@"FirstName=%@&LastName=%@&Email=%@&AuthenticateType=%@&Mobile=%@&Location=%@&ProfilePic=%@&ContentLength=%@&FileName=%@&ContentType=%@",_firstName.text ,_lastName.text,_emailId.text,@"1",_location.text,_password.text,imageBase64StringReg,postLengthReg,@"new.png",@"image/png"];
//    NSLog(@"post is %@",post);
//    
//    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Register"]];
//    
//    [request setHTTPMethod:@"POST"];
//    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
//    //    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request setHTTPBody:postData];
//    
//    
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
//     {
//         if (data)
//         {
//             id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//             NSLog(@"result updateProfileData is %@",result);
//             
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 
//                 BOOL success = [[result objectForKey:@"result"] boolValue];
//                 
//                 //        NSString *errorAlert = [result objectForKey:@"false"];
//                 
//                 NSString *msgAlert = [result objectForKey:@"message"];
//                 
//                 NSLog(@"msgAlert is %@",msgAlert);
//                 
//                 NSDictionary *dataReg = [result valueForKey:@"Object"];
//                 
//                 NSLog(@"dataReg is %@",dataReg);
//                 
//                 NSArray *items = [msgAlert componentsSeparatedByString:@"~"];
//                 
//                 if(items.count>1)
//                 {
//                     str = [items objectAtIndex:1];
//                 }
//                 
//                 if (success)
//                 {
//                     NSLog(@"sign up in else part");
//                     
//                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:str delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
//                     [alert show];
//                     
//                     signUpData = [dataReg valueForKey:@"details"];
//                     
//                     NSLog(@"signUpData is %@",signUpData);
//                     
//                     HUD.hidden = true;
//                     
//                 }
//                 
//                 else
//                 {
//                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:str delegate:nil cancelButtonTitle:@"Ok"otherButtonTitles: nil];
//                     [alert show];
//                     HUD.hidden = true;
//                     
//                 }
//             });
//         }
//         else if (error)
//             
//             NSLog(@"%@",error);
//         
//     }];
//}

-(void)Login
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
   // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffLogin) onTarget:self withObject:nil animated:YES];
    
    _objArray = [[NSMutableArray alloc]init];
    
    
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",self.emailtextField.text ,self.passowrdTextfield.text];
    
    post = [post stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"post is %@",post);
    
    saveEmail = self.emailtextField.text;
    NSLog(@"saveEmail is %@",saveEmail);
    
    NSUserDefaults *SaveEmailNS = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    [SaveEmailNS setObject:saveEmail forKey:@"saveEmail"];

    
//    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];

    NSString *plainString = [NSString stringWithFormat:@"%@:%@",self.emailtextField.text,self.passowrdTextfield.text];
    NSData *plainData = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainData base64EncodedStringWithOptions:0];
    NSLog(@"%@", base64String); // Zm9v
    
    NSString *headerStr = [NSString stringWithFormat:@"Basic %@",base64String];
    
    NSLog(@"headerStr is %@",headerStr);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:LoginAPI]];
    
    [request setHTTPMethod:@"POST"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    [request addValue:headerStr forHTTPHeaderField:@"Authorization"];
//    [request setValue:@"Advisor" forHTTPHeaderField:@"RoleName"];
    [request setHTTPBody:postData];
    
    NSURLConnection *theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    if( theConnection )
    {
        NSLog(@"theConnection is  %@",theConnection);
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"response is %@", response);
    
    loginMutableData = [[NSMutableData alloc]init];
        
        NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
        
        NSLog(@"headers is %@",headers);
        
        NSString *tokenValue = [headers valueForKey:@"Token"];
        
        NSLog(@"tokenValue is %@",tokenValue);
        
        [[NSUserDefaults standardUserDefaults] setObject:tokenValue forKey:@"preferenceName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
    
    loginMutableData = [[NSMutableData alloc]initWithData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    id result = [NSJSONSerialization JSONObjectWithData:loginMutableData options:kNilOptions error:nil];
    
    NSLog(@"result is %@",result);
    
    BOOL success = [[result objectForKey:@"result"] boolValue];
    
    NSString *msgAlert = [result objectForKey:@"message"];
    
    NSArray *Successdata = [result valueForKey:@"Object"];
    
    userID = [Successdata valueForKey:@"id"];
    
    NSLog(@"userID is %@",userID);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    [prefs setObject:userID forKey:@"saveUserID"];
    
    NSArray *responeData = [result valueForKey:@"response"];
    
    NSString *errorAlert = [responeData valueForKey:@"message"];
    
    NSLog(@"data is %@",Successdata);
    
    NSLog(@"msgAlert is %@",msgAlert);
    
    NSArray *items = [msgAlert componentsSeparatedByString:@"~"];
    
    if(items.count>1)
    {
        strLoginAlert = [items objectAtIndex:1];
    }
    
    NSArray *itemz = [errorAlert componentsSeparatedByString:@"~"];
    
    if(itemz.count>1)
    {
        strErrorAlert = [itemz objectAtIndex:1];
    }
    
    if (success)
    {
        AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     
        [appDelegate registerDeviceTokenOnPushBots];
        
        NSLog(@"login in else part");
        
        [_objArray removeAllObjects];

        NSLog(@"for loop %@", Successdata);
        
        [_objArray addObject:Successdata];
        
        NSLog(@" _objArray is %lu",(unsigned long)_objArray);
        
        NSDictionary *data1 = [result valueForKey:@"Object"];
        
        NSLog(@"data1 is %@",data1);
        
        role_Name = [data1 valueForKey:@"role_name"];
        
        savedfirst_name = [data1 valueForKey:@"first_name"];
        savedlast_name = [data1 valueForKey:@"last_name"];
        savedlocation = [data1 valueForKey:@"location"];
        
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:savedfirst_name forKey:@"savedfirst_name"];
        [defaults setObject:savedlast_name forKey:@"savedlast_name"];
        [defaults setObject:savedlocation forKey:@"savedlocation"];
        [defaults setObject:saveEmail forKey:@"savedEmail"];
        
        [defaults synchronize];
        
        if([data1 valueForKey:@"profile_pic_small"] == (id)[NSNull null])
        {
             AdvisorProfilePic = @"";
        }
        else
        {
            AdvisorProfilePic = [data1 valueForKey:@"profile_pic_small"];

        }
        
//        NSData *dataImage = [AdvisorProfilePic dataUsingEncoding:NSUTF8StringEncoding];
        
        UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:AdvisorProfilePic]]];
        
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(img) forKey:@"SaveImage"];
        
        NSLog(@"role_Name is %@",role_Name);
        
//         [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainUser"];
//        
//        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainAdvisor"];

        
        if ([role_Name isEqual:@"User"])
        {
//            if (isTickSelected == NO)
//            {
//                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"loginAgainUser"];
//                
//            }
            
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"loginAgainUser"];

            SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
            [self.navigationController pushViewController:ss animated:YES];

            HUD.hidden = true;
        }
        else
            //if([role_Name isEqual:@"Advisor"])
        {
            
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"loginAgainAdvisor"];
            
            NSLog(@"Advisor Login");
            NewViewController *new = [self.storyboard instantiateViewControllerWithIdentifier:@"NewViewController"];
            [self.navigationController pushViewController:new animated:YES];
            HUD.hidden = true;
        }
        
        [self pushMethodForUser];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Invalid email/password." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        HUD.hidden = true;
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
//    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
//    {
//        [_emailtextField setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
//        [_passowrdTextfield setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
//        
//    }
    
    if (textField == _emailtextField)
    {
        _loginHiglightedView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];

    }
    else
    {
        _passwordHighLightedView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];

    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField == _emailtextField)
    {
        _loginHiglightedView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        
    }
    else
    {
        _passwordHighLightedView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        
    }
}

-(void)placeholderColor
{
    //if (UItextField)
    
    [_emailtextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_passowrdTextfield setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    _logInButtonOutlet.layer.borderWidth = 2.0f;
//    _loginFooterOutlet.layer.borderColor= [[UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0]CGColor];
//    _loginFooterOutlet.layer.borderWidth=2.0f;
    _logInButtonOutlet.layer.borderColor= [[UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0]CGColor];
}

- (IBAction)alreadyHaveAccountBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rememberMeBtn:(id)sender
{
    
    if(isTickSelected == YES)
    {
        [self.outletOfRememberMe setImage:[UIImage imageNamed:@"AdvisorTick.png"] forState:UIControlStateNormal];

        isTickSelected = NO;
    }
    else
    {
        isTickSelected = YES;
        [self.outletOfRememberMe setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
           [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainUser"];
    }
}


- (IBAction)forgotPasswordButton:(id)sender
{
    ForgotPasswordViewController *forgotVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPassword"];
    
    [self.navigationController pushViewController:forgotVC animated:YES];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_emailtextField resignFirstResponder ];
    [_passowrdTextfield resignFirstResponder ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}

-(BOOL)checkingEmail:(NSString *)checkEmail
{
    NSLog(@"checking Email");
    BOOL var1 = true;
    
    
    if ([checkEmail containsString:@"@"])                                          // if string contains @
    {
        NSArray *myWords = [checkEmail componentsSeparatedByString:@"@"];
        
        //myWords is an array,separate strings by @ and keeping string into array
        
        for (int i = 0; i<[myWords count]; i++)                                    //checks string one by one
        {
            if ([myWords[i] isEqual: @""])  // if string is empty than return false and myWords[i] is a string
                
            {
                var1 = false;
            }
        }
        
        if (var1)
        {
            
            if ([myWords[[myWords count] - 1] containsString:@"."])
            {
                NSLog(@"working %@",myWords[[myWords count] - 1]);
                
                NSArray *myDotWords = [myWords[[myWords count] - 1] componentsSeparatedByString:@"."];
                
                for (int i = 0; i<[myDotWords count]; i++)
                {
                    if ([myDotWords[i] isEqual: @""])
                    {
                        var1 = false;
                    }
                }
            }
            else
            {
                var1 = false;
            }
        }
    }
    else
    {
        var1 = false;
    }
    
    
    return var1;
}

- (BOOL)strongPassword:(NSString *)yourText
{
    BOOL strongPwd = YES;
    
    if([yourText length] > 4)
        strongPwd = YES;
    
    return strongPwd;
}

- (IBAction)loginButtonClicked:(id)sender
{
        NSMutableDictionary *DicForLogin = [[NSUserDefaults standardUserDefaults] valueForKey:self.emailtextField.text];
    
        NSString *keyToEmailIdName = [DicForLogin valueForKey:@"keyToEmailIdName"];
        NSString *keyToPasswordName = [DicForLogin valueForKey:@"keyToPasswordName"];
        NSString *keyToUserOrAdvisor = [DicForLogin valueForKey:@"keyToUserOrAdvisor"];
    
    if ([keyToEmailIdName isEqualToString:self.emailtextField.text] && [keyToPasswordName isEqualToString:self.passowrdTextfield.text])
        {
            
            if([keyToUserOrAdvisor containsString:@"User"] || [keyToUserOrAdvisor containsString:@"user"])
            {
                AskProViewController *AskProViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AskProViewController"];
                [self.navigationController pushViewController:AskProViewController animated:YES];
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"loginAgainUser"];

            }
            else
            {
                NewViewController *new = [self.storyboard instantiateViewControllerWithIdentifier:@"NewViewController"];
                [self.navigationController pushViewController:new animated:YES];
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"loginAgainAdvisor"];
              
            }
        }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Invalid Emailid/Password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.passowrdTextfield resignFirstResponder];
    if (buttonIndex == 0)
    {
        SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
        [self.navigationController pushViewController:ss animated:YES];
    }
}

-(void)pushMethodForUser
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    UserpushObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post = [NSString stringWithFormat:@"ReceiverID=%@&DeviceToken=%@&Platform=%@&Alias=%@",Creater_User_ID,deviceTokenString,@"0",Creater_User_ID];
    NSLog(@"post is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:PushAPI]];
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result push notification is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@"push notification loaded successfully");
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"push notification API not working" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

@end
