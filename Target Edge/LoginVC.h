//
//  LoginVC.h
//  Target Edge
//
//  Created by Mrinal Khullar on 11/19/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrationVC.h"
#import "SSViewController.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import <Pushbots/Pushbots.h>

extern id valueForLinkedIn;
extern NSString *linkedInUserID;

@interface LoginVC : UIViewController<UIAlertViewDelegate,MBProgressHUDDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate>
{
    NSString *responeSessionLastErrorStr;
    MBProgressHUD *HUD;
    NSMutableArray *signUpDataForLinkedIn;
    NSString *firstNameForLInkedIN;
    NSString *emailidForLinkedIn;
    NSString *lastnameForLinkedIn;
    NSString *locationForLinkedIn;
    NSMutableArray *loginWithLinkedInObj;
    NSMutableData *loginLinkedInMutableData;
    NSString *sessinValue;
    NSString *tokenNewMethod;
    NSMutableArray *advisorPushObj;
    
}

- (IBAction)btnLinkedIN:(id)sender;
- (IBAction)btnEmail:(id)sender;
- (IBAction)btnSignUp:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *OutletBuutonNewUser;
@property (weak,nonatomic) UIAlertAction *saveAction;
@property (nonatomic,strong) NSError *lastError;
-(void)newMethod;
-(void)SignUpWithLinkedIn;
-(void)pushMethodForLinkedIn;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
- (IBAction)okBtn:(id)sender;
- (IBAction)cancelBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *txtAlertView;

@property (weak, nonatomic) IBOutlet UIView *linkedInBlackViewForAlertBox;

@end
