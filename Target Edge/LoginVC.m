//
//  LoginVC.m
//  Target Edge
//
//  Created by Mrinal Khullar on 11/19/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "LoginVC.h"
//#import "OAuthLoginView.h"
#import <UIKit/UIKit.h>
#import "LogInWithEmailViewController.h"

NSString *linkedInUserID;
id valueForLinkedIn;

@interface LoginVC ()
{
    //OAuthLoginView *linkedInLoginView;
    NSString *mobileStr;
    
}

@end

@implementation LoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     [[Pushbots sharedInstance] unregister];
    // Do any additional setup after loading the view.
    
  //  [self ipadCompatability];
    
    self.linkedInBlackViewForAlertBox.hidden = true;
    self.txtAlertView.layer.cornerRadius = 6;
    self.txtAlertView.layer.masksToBounds = YES;
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(pushMethod:) userInfo:nil repeats:YES];
    
}

-(void)pushMethod:(NSTimer *)timer
{
    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"ViewStillToCome"] == YES)
    {
        SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
        
        [self.navigationController pushViewController:ss animated:YES];

        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ViewStillToCome"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ViewIsAlreadyAppear"];
    }
}

-(void)ipadCompatability
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        UIImageView *loginImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
        
        loginImage.frame = CGRectMake(0, 0, width, height);
        [self.view addSubview:loginImage];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"login"] == NO)
    {
        NSLog(@"Login View");
    }
    else
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"login"] == YES)
        {
            
            LogInWithEmailViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
            [self.navigationController pushViewController:login animated:YES];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"login"];
        }
        
    }
    
    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Relogin"] == YES)
//    {
//        
//        SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
//        [self.navigationController pushViewController:ss animated:YES];
//
//    }
//    else
//    {
//        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"Relogin"];
//    }
    
    
    
    
  
  
//    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"emailLInkedIn"])
//    {
//        NSLog(@"login ");
//        NSUserDefaults *ps = [NSUserDefaults standardUserDefaults];
//        // getting an NSString
//        NSString *email = [ps stringForKey:@"emailLInkedIn"];
//        NSLog(@"email is %@",email);
//        SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
//        [self.navigationController pushViewController:ss animated:YES];
//        
//    }
//    


  
  //  [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"ViewWillCome"];
}

- (IBAction)btnLinkedIN:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"LinkedInAccessTokenKey"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SavedLinkedInUserID"];
    
    
    [self newMethod];
//    [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION, nil] state:@"some state" showGoToAppStoreDialog:YES successBlock:^(NSString *returnState)
//    {
//        NSLog(@"%s","success called!");
//        LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
//        NSLog(@"value=%@ isvalid=%@",[session value],[session isValid] ? @"YES" : @"NO");
//        sessinValue = [session value];
//        NSMutableString *text = [[NSMutableString alloc] initWithString:[session.accessToken description]];
//        [text appendString:[NSString stringWithFormat:@",state=\"%@\"",returnState]];
//        NSLog(@"Response label text %@",text);
//        
//        [self newMethod];
//                                      
//    }
//    errorBlock:^(NSError *error)
//     {
//         NSLog(@"%s %@","error called! ", [error description]);
//     }
//     ];
}


-(void)newMethod
{
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffLoginVC) onTarget:self withObject:nil animated:YES];
    void (^PerformDataFetch)() = ^()
    {
        if ([LISDKSessionManager hasValidSession])
        {
            NSString *urlString = [NSString stringWithFormat:@"%@/people/~:(id,first-name,last-name,maiden-name,email-address,picture-url,location:(name),headline)", LINKEDIN_API_URL];
            [[LISDKAPIHelper sharedInstance] getRequest:urlString success:^(LISDKAPIResponse *response)
            {
//                tokenNewMethod = [[LISDKSessionManager sharedInstance].session.accessToken serializedString];
//                LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
//                NSLog(@"value=%@ isvalid=%@",[session value],[session isValid] ? @"YES" : @"NO");
                tokenNewMethod = sessinValue;
                NSLog(@"tokenNewMethod is %@",tokenNewMethod);
                
                [[NSUserDefaults standardUserDefaults] setValue:tokenNewMethod forKey:@"LinkedInAccessTokenKey"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSData *objectData = [response.data dataUsingEncoding:NSUTF8StringEncoding];
                valueForLinkedIn = [NSJSONSerialization JSONObjectWithData:objectData options:kNilOptions error:nil];
                NSLog(@"%@",valueForLinkedIn);
                
                
//                 self.linkedInBlackViewForAlertBox.hidden = false;
                firstNameForLInkedIN = [valueForLinkedIn objectForKey:@"firstName"];
                
                NSLog(@"firstNameForLInkedIN is %@",firstNameForLInkedIN);
                
                emailidForLinkedIn = [valueForLinkedIn valueForKey:@"emailAddress"];
                        
                NSLog(@"emailidForLinkedIn is %@",emailidForLinkedIn);
                
                lastnameForLinkedIn = [valueForLinkedIn objectForKey:@"lastName"];
                
                NSLog(@"lastnameForLinkedIn is %@",lastnameForLinkedIn);
                
                NSDictionary  *dic = [valueForLinkedIn valueForKey:@"location"];
                locationForLinkedIn = [dic valueForKey:@"name"];
                NSLog(@"locationForLinkedIn is %@",locationForLinkedIn);
                
                HUD.hidden = true;
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isPopUpVisible"] == false)
                    {
                        self.linkedInBlackViewForAlertBox.hidden = false;
                        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isPopUpVisible"];
                    }
                    else
                    {
                        NSLog(@"linkedIn in else part");
                        isPushBool =  false;
                        [self LoginWithLinkedIn];
                    }

                });
                
                
            } error:^(LISDKAPIError *error)
             {
                 NSLog(@"%@",error);
            }];
        }
    };
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"LinkedInAccessTokenKey"];
    
    if (token.length)
    {
//        LISDKAccessToken *accessToken = [LISDKAccessToken LISDKAccessTokenWithSerializedString:token];
//        if (accessToken.expiration < [NSDate date])
//        {
//            [LISDKSessionManager createSessionWithAccessToken:accessToken];
        sessinValue = token;
            PerformDataFetch();
//        }
    }
    else
    {
        [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION, nil] state:nil showGoToAppStoreDialog:YES successBlock:^(NSString *returnState)
         {
           

                     NSLog(@"%s","success called!");
                     LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
                     NSLog(@"value=%@ isvalid=%@",[session value],[session isValid] ? @"YES" : @"NO");
                     sessinValue = [session value];
                     NSMutableString *text = [[NSMutableString alloc] initWithString:[session.accessToken description]];
                     [text appendString:[NSString stringWithFormat:@",state=\"%@\"",returnState]];
                     NSLog(@"Response label text in else pary %@",text);
             
              PerformDataFetch();
             
        }
      errorBlock:^(NSError *error)
         {
             NSLog(@"%@",error);
        }];
    }
    
 
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
    }
}

-(void)checkIt
{
    NSString *name ;

    if([name isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Textfield is empty!!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
    }
}

-(void)SignUpWithLinkedIn
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    signUpDataForLinkedIn = [[NSMutableArray alloc] init];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffLoginVC) onTarget:self withObject:nil animated:YES];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    mobileStr = [prefs stringForKey:@"mobileNosaved"];

    
    NSString *post = [NSString stringWithFormat:@"FirstName=%@&LastName=%@&Email=%@&AuthenticateType=%@&Mobile=%@&Location=%@",firstNameForLInkedIN ,lastnameForLinkedIn,emailidForLinkedIn,@"2",mobileStr,locationForLinkedIn];
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Register"]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (data)
         {
             id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             NSLog(@"result updateProfileData is %@",result);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];

                 NSString *msgAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"msgAlert is %@",msgAlert);
                 
                 NSDictionary *dataReg = [result valueForKey:@"Object"];
                 
                 NSLog(@"dataReg is %@",dataReg);
                 
                 if (success)
                 {
                     NSLog(@"sign up in else part");
                     
                     isPushBool = false;
                     [self LoginWithLinkedIn];
//                     
//                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"done" delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
//                     [alert show];
//                     
//                     signUpDataForLinkedIn = [dataReg valueForKey:@"details"];
//                     
//                     NSLog(@"signUpDataForLinkedIn is %@",signUpDataForLinkedIn);
                     
                     HUD.hidden = true;
                     
                 }
                 
                 else
                 {
                     isPushBool = false;
                     [self LoginWithLinkedIn];
                     HUD.hidden = true;
                     
                 }
             });
         }
         else if (error)
             
             NSLog(@"%@",error);
         
     }];
}

-(void)pushMethodForLinkedIn
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    advisorPushObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post = [NSString stringWithFormat:@"ReceiverID=%@&DeviceToken=%@&Platform=%@&Alias=%@",LinkedIn_ID,deviceTokenString,@"0",LinkedIn_ID];
    NSLog(@"post is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:PushAPI]];
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result push notification is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@"push notification loaded successfully");
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"push notification API not working" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)LoginWithLinkedIn
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffLoginVC) onTarget:self withObject:nil animated:YES];
    
    loginWithLinkedInObj = [[NSMutableArray alloc]init];
    
    NSString *post = [NSString stringWithFormat:@"email=%@&password=%@",emailidForLinkedIn ,tokenNewMethod];
    NSLog(@"post is %@",post);
    
    NSString *plainString = [NSString stringWithFormat:@"%@:%@",emailidForLinkedIn,tokenNewMethod];
    NSData *plainData = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [plainData base64EncodedStringWithOptions:0];
    NSLog(@"%@", base64String); // Zm9v
    
    NSString *headerStr = [NSString stringWithFormat:@"OpenID %@",base64String];
    
    NSLog(@"headerStr is %@",headerStr);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:LoginAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    [request addValue:headerStr forHTTPHeaderField:@"Authorization"];
    //    [request setValue:@"Advisor" forHTTPHeaderField:@"RoleName"];
    [request setHTTPBody:postData];
    
    NSURLConnection *theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    if( theConnection )
    {
        NSLog(@"theConnection is  %@",theConnection);
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"response is %@", response);
    
    loginLinkedInMutableData = [[NSMutableData alloc]init];
    
    NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
    
    NSLog(@"headers is %@",headers);
    
    NSString *tokenValue = [headers valueForKey:@"Token"];
    
    NSLog(@"tokenValue is %@",tokenValue);
    
    [[NSUserDefaults standardUserDefaults] setObject:tokenValue forKey:@"preferenceName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
    
    loginLinkedInMutableData = [[NSMutableData alloc]initWithData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    id result = [NSJSONSerialization JSONObjectWithData:loginLinkedInMutableData options:kNilOptions error:nil];
    
    NSLog(@"result is %@",result);
    
    BOOL success = [[result objectForKey:@"result"] boolValue];
    
    NSString *msgAlert = [result objectForKey:@"message"];
    
    NSArray *Successdata = [result valueForKey:@"Object"];
    
    linkedInUserID = [Successdata valueForKey:@"id"];
    
    NSLog(@"linkedInUserID is %@",linkedInUserID);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    [prefs setObject:linkedInUserID forKey:@"SavedLinkedInUserID"];
    
    NSArray *responeData = [result valueForKey:@"response"];
    
    NSString *errorAlert = [responeData valueForKey:@"message"];
    
    NSLog(@"data is %@",Successdata);
    
    NSLog(@"msgAlert is %@",msgAlert);
    
    if (success)
    {
        AppDelegate* dd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        
        [dd registerDeviceTokenOnPushBots];
        
        NSLog(@"login in else part");
        
        [loginWithLinkedInObj removeAllObjects];
        
        NSLog(@"for loop %@", Successdata);
        
        [loginWithLinkedInObj addObject:Successdata];
        
        NSLog(@" loginWithLinkedInObj is %lu",(unsigned long)loginWithLinkedInObj);
        
        NSDictionary *data1 = [result valueForKey:@"Object"];
        
        NSLog(@"data is %@",data1);
        
        NSString *linkedIn1 = [data1 valueForKey:@"first_name"];
        NSString *linkedIn2 = [data1 valueForKey:@"last_name"];
        
        
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:linkedIn1 forKey:@"linkedInFirstName"];
        [defaults setObject:linkedIn2 forKey:@"linkedInLastName"];
        [defaults setObject:emailidForLinkedIn forKey:@"linkedInEmailId"];
        [defaults synchronize];
        
        [self pushMethodForLinkedIn];
        
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"loginAgainLinkedIn"];
        
        SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
        [self.navigationController pushViewController:ss animated:YES];
        
        HUD.hidden = true;
    }
    else
    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This email id is already registered" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
        [self SignUpWithLinkedIn];
        
        _linkedInBlackViewForAlertBox.hidden = true;
        [self.view endEditing:YES];
        HUD.hidden = true;
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_mobileTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}


- (void)doSomeFunkyStuffLoginVC
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}


//- (void)updateControlsWithResponseLabel:(BOOL)updateResponseLabel
//{
//    [LISDKSessionManager
//     createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, nil]
//     state:nil
//     showGoToAppStoreDialog:YES
//     successBlock:^(NSString *returnState)
//    {
//         NSLog(@"%s","success called!");
//         LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
//        NSLog(@"session is %@",session);
//     }
//     errorBlock:^(NSError *error)
//    {
//         NSLog(@"%@",error);
//     }
//     ];
//}


//-(void)getProfileDetailsFromLinkedIn
//{
//    linkedInLoginView =[[OAuthLoginView alloc] init];
//    
//    linkedInLoginView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDetailsOfProfile:) name:@"loginViewDidFinish" object:nil];
//    
//    [self presentViewController:linkedInLoginView animated:YES completion:nil];
//}

-(void)showDetailsOfProfile:(NSNotification *)notification
{
    
    NSDictionary *jsonResponse = notification.userInfo;
    
    NSLog(@"notification  : %@", notification.userInfo);
    
    NSString *position;
    NSString *company;
    
    if([jsonResponse objectForKey:@"positions"])
    {
        if ([[jsonResponse objectForKey:@"positions"] objectForKey:@"values"])
        {
            if([[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0])
            {
                if ([[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"title"])
                {
                    position=[[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"title"];
                }
                
                
                if ([[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"company"])
                {
                    
                    if ([[[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"company"]objectForKey:@"name"])
                    {
                        company=[[[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"company"]objectForKey:@"name"];
                    }
                }
            }
        }
    }
    
    NSString *_id;
    
    if ([jsonResponse objectForKey:@"id"])
    {
        _id=[jsonResponse objectForKey:@"id"];
    }
    
    
    NSString *profileUrl;
    
    if ([jsonResponse objectForKey:@"publicProfileUrl"])
    {
        profileUrl=[jsonResponse objectForKey:@"publicProfileUrl"];
    }
    
    NSString *email;
    if ([jsonResponse objectForKey:@"emailAddress"])
    {
        email = [jsonResponse objectForKey:@"emailAddress"];
        NSLog(@"email is %@",email);
        
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"emailLInkedIn"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    
    NSString *industry;
    if ([jsonResponse objectForKey:@"industry"])
    {
        industry=[jsonResponse objectForKey:@"industry"];
    }
    
    NSString *pictureUrl;
    if ([jsonResponse objectForKey:@"pictureUrl"])
    {
        pictureUrl=[jsonResponse objectForKey:@"pictureUrl"];
    }
    
    
    NSString *biography;
    if ([jsonResponse objectForKey:@"summary"])
    {
        biography=[jsonResponse objectForKey:@"summary"];
    }
    
    
    NSString *location;
    if ([jsonResponse objectForKey:@"location"])
    {
        NSDictionary *locationDictionary = [jsonResponse objectForKey:@"location"];
        location = [locationDictionary objectForKey:@"name"];
        //location = [location substringToIndex:[location rangeOfString:@","].location];
    }
    
    NSString *twitterAccountName;
    if ([jsonResponse objectForKey:@"primaryTwitterAccount"])
    {
        if ([[jsonResponse objectForKey:@"primaryTwitterAccount"] objectForKey:@"providerAccountName"])
        {
            twitterAccountName=[[jsonResponse objectForKey:@"primaryTwitterAccount"] objectForKey:@"providerAccountName"];
        }
    }
    
    NSString *day;
    NSString *month;
    NSString *year;
    NSDate *dob ;
    if ([jsonResponse objectForKey:@"dateOfBirth"] )
    {
        
        day = [[jsonResponse objectForKey:@"dateOfBirth"] objectForKey:@"day"];
        month = [[jsonResponse objectForKey:@"dateOfBirth"] objectForKey:@"month"];
        year = [[jsonResponse objectForKey:@"dateOfBirth"] objectForKey:@"year"];
        
        NSString *str ;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [formatter setTimeZone:gmt];
        str = [NSString stringWithFormat:@"%@/%@/%@ 00:00 AM",month,day,year];
        dob = [formatter dateFromString:str];
        
        
    }
    
    NSString *contactNumber;
    if ([jsonResponse objectForKey:@"phoneNumbers"])
    {
        if ([[jsonResponse objectForKey:@"phoneNumbers"] objectForKey:@"values"])
        {
            NSArray *arr = [[jsonResponse objectForKey:@"phoneNumbers"] objectForKey:@"values"];
            if ([arr count]>0) {
                NSDictionary *con = [arr objectAtIndex:0];
                contactNumber = [con objectForKey:@"phoneNumber"];
            }
        }
    }
}



- (IBAction)btnEmail:(id)sender
{
    LogInWithEmailViewController *LoginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
    [self.navigationController pushViewController:LoginVC animated:NO];
}

- (IBAction)btnSignUp:(id)sender
{
    RegistrationVC *Registration = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationVC"];
    [self.navigationController pushViewController:Registration animated:NO];
    
}

- (IBAction)rememberMeVtn:(id)sender
{
      
}
- (IBAction)okBtn:(id)sender
{
    if ([_mobileTextField.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Enter your mobile no" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else if ( _mobileTextField.text.length != 10 )
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Mobile Number should be 10 digit" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:_mobileTextField.text forKey:@"mobileNosaved"];
         mobileStr = _mobileTextField.text ;
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self SignUpWithLinkedIn];

    }
    
    [self.view endEditing:YES];
}

- (IBAction)cancelBtn:(id)sender
{
    self.linkedInBlackViewForAlertBox.hidden = true;
    [self.view endEditing:YES];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"[235689][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    
    return matches;
}


@end
