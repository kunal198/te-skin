//
//  MOOCViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "SelectedMOOCViewController.h"
#import "A3ViewController.h"
#import "SettingViewController.h"
#import "SelectedProTalk.h"
#define MOOC_Category_Listing @"http://mobileapi.talentedge.hub1.co/api/MOOC/MOOC_Category_Listing"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

extern NSString *passCategoryName;

@interface MOOCViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>
{
    NSMutableArray *moocItemList;
    MBProgressHUD *HUD;
    NSMutableArray *Mooc_Obj;
    NSString *strMoocAlert;
  
}

@property (weak, nonatomic) IBOutlet UITableView *objOfMoocTableView;

- (IBAction)btnMOOC:(id)sender;
- (IBAction)btnSetting:(id)sender;
- (IBAction)btnA3:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *MOOCHighlightedView;
@property (weak, nonatomic) IBOutlet UILabel *MOOCLbl;
@property (weak, nonatomic) IBOutlet UIImageView *MOOCimageOutlet;
@property (weak, nonatomic) IBOutlet UIButton *MoocbuttonOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *settingImageOutlet;
@property (weak, nonatomic) IBOutlet UIButton *settingButtonOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *A3ImageOutlet;
@property (weak, nonatomic) IBOutlet UIButton *A3ButtonOutlet;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property(assign)   BOOL moocPush;
- (IBAction)infoBtn:(id)sender;

-(void)MoocCategoryListing;

@end
