//
//  MOOCViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "MOOCViewController.h"

NSString *passCategoryName;

@interface MOOCViewController ()

@end

@implementation MOOCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
//    [self MoocCategoryListing];
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [_MOOCLbl setFont:[UIFont systemFontOfSize:16]];
        _settingImageOutlet.frame = CGRectMake(_settingImageOutlet.frame.origin.x, 43, 50, 50);
        _MOOCimageOutlet.frame = CGRectMake(_MOOCimageOutlet.frame.origin.x, 43, _MOOCimageOutlet.frame.size.width, 50);
        
        _A3ImageOutlet.frame = CGRectMake(_A3ImageOutlet.frame.origin.x, 43, _A3ImageOutlet.frame.size.width, 50);

          NSLog(@"self.view.Frame=%@", NSStringFromCGRect(_navigationView.frame));
        //  NSLog(@"navigation height%@",_navigationView.frame);
        
    }

    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"guide"] == NO)
    {
        [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(pushMethod:) userInfo:nil repeats:NO];
        
        self.MOOCHighlightedView.hidden = NO;
        
        [[NSUserDefaults standardUserDefaults ] setBool:YES forKey:@"guide"];
        
        [[NSUserDefaults standardUserDefaults ] setBool:YES forKey:@"pop"];

    }

    // Do any additional setup after loading the view.
    
//    self.navigationController.navigationBar.hidden = NO;
    
    _objOfMoocTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    
    // Do any additional setup after loading the view.
    moocItemList = [[NSMutableArray alloc]init];
    [moocItemList addObject:@"Career Planning"];
    [moocItemList addObject:@"Skill & Competencies"];
    [moocItemList addObject:@" Interview Tips"];
//    [moocItemList addObject:@"It"];
//    [moocItemList addObject:@"Finance"];
//    [moocItemList addObject:@"Hr"];
//    [moocItemList addObject:@"Starups"];
//    [moocItemList addObject:@"Business Ops"];
//    [moocItemList addObject:@"Product"];
//    
    self.navigationItem.hidesBackButton = YES;

}

-(void)viewDidAppear:(BOOL)animated
{
    if(_moocPush == true)
    {
        _moocPush = false;
        SettingViewController *setting = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
        setting.isSettingPush = true;
        [self.navigationController pushViewController:setting animated:NO];
    }
}

- (void)doSomeFunkyStuffMOOC
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}

-(void)MoocCategoryListing
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMOOC) onTarget:self withObject:nil animated:YES];
    
    Mooc_Obj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
//    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:MOOC_Category_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strMoocAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     Mooc_Obj = [data1 valueForKey:@"Categories"];
                     
                     NSLog(@"Mooc_Obj is %@",Mooc_Obj);
                     HUD.hidden = true;
                     
                     //                     NSString *Answer_Video_Title = [[PlusQuestionObj objectAtIndex:0] valueForKey:@"Answer_Video_Title"];
                     //
                     //                     NSLog(@"Answer_Video_Title is %@",Answer_Video_Title);
                     //
                     //                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     //                     [alert show];
                     
                     [self.objOfMoocTableView reloadData];
                     
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}


-(void)pushMethod:(NSTimer *)timer
{
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton = YES;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [moocItemList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.moocLbl.text = [moocItemList objectAtIndex:indexPath.row];

    Cell.moocLbl.textAlignment = NSTextAlignmentCenter;
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    tableView.separatorInset = UIEdgeInsetsZero;

    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectedProTalk *selectedPro = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedProTalk"];
//    selectedPro.selectedProTalk = [moocItemList objectAtIndex:indexPath.row];
    passCategoryName = [moocItemList objectAtIndex:indexPath.row];
    NSLog(@"passCategoryName is %@",passCategoryName);
    [self.navigationController pushViewController:selectedPro animated:YES];

////    if (indexPath.row == 0)
////    {
////        SelectedMOOCViewController *selectedMOOC = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedMOOCViewController"];
////        selectedMOOC.titleLbl = moocItemList[indexPath.row];
////        selectedMOOC.isSelectedEquityOne = true;
////        [self.navigationController pushViewController:selectedMOOC animated:YES];
////    }
////    else if (indexPath.row == 1)
////    {
////        SelectedMOOCViewController *selectedMOOC = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedMOOCViewController"];
////        selectedMOOC.titleLbl = moocItemList[indexPath.row];
////        selectedMOOC.isSelectedEquityTwo = true;
////        [self.navigationController pushViewController:selectedMOOC animated:YES];
////
////    }
////    else if (indexPath.row == 2)
////    {
////        SelectedMOOCViewController *selectedMOOC = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedMOOCViewController"];
////        selectedMOOC.titleLbl = moocItemList[indexPath.row];
////        selectedMOOC.isSelectedEquityThree = true;
////        [self.navigationController pushViewController:selectedMOOC animated:YES];
////        
////    }
////    else
////    {
//        SelectedMOOCViewController *selectedMOOC = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectedMOOCViewController"];
//    if (indexPath.row == 0)
//    {
//        selectedMOOC.isSelectedEquityOne = true;
//        selectedMOOC.titleLbl = moocItemList[indexPath.row];
//        [self.navigationController pushViewController:selectedMOOC animated:YES];
//    }
//    else if (indexPath.row == 1)
//    {
//        selectedMOOC.isSelectedEquityTwo = true;
//        selectedMOOC.titleLbl = moocItemList[indexPath.row];
//        [self.navigationController pushViewController:selectedMOOC animated:YES];
//    }
//    else if (indexPath.row == 2)
//    {
//        selectedMOOC.isSelectedEquityThree = true;
//        selectedMOOC.titleLbl = moocItemList[indexPath.row];
//        [self.navigationController pushViewController:selectedMOOC animated:YES];
//    }

   }

- (IBAction)btnMOOC:(id)sender
{
    
}

- (IBAction)btnSetting:(id)sender
{
    SettingViewController *setting = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [self.navigationController pushViewController:setting animated:YES];
    
}

- (IBAction)btnA3:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)infoBtn:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}
@end
