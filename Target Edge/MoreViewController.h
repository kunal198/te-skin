//
//  MoreViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/27/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "CustomCell.h"
#import "AnswersViewController.h"
#import "A3ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define MoreQuestion_Listing @"http://mobileapi.talentedge.hub1.co/api/Advisor/Questions_listing"


@interface MoreViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *colletionArray;
    UIImage *thumbnail;
    NSMutableArray *PlusQuestionObj;
    MPMoviePlayerController *videoPlayerViewMore;
    BOOL isPlayAgnMore;
    BOOL isDoneBtnClicked;
    NSString *strPlusAlert;
    id fetchIndex;
    NSString *answrVideoLinkMore;
    MBProgressHUD *HUD;
    NSString *video_Title;
    NSString *video_Link;
    NSMutableArray *reverseMoreArray;
}
@property (weak, nonatomic) IBOutlet UITableView *moreTableView;
@property (weak, nonatomic) IBOutlet UIImageView *A3ImageOutlet;
@property (weak, nonatomic) IBOutlet UILabel *answerLabelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *A3LabelOutlet;
- (IBAction)shareBtnMore:(id)sender;

- (IBAction)a3PopOut:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *moreProfileImg;
@property (weak, nonatomic) IBOutlet UILabel *moreProfileName;
@property (weak, nonatomic) IBOutlet UILabel *moreProfileAddress;
@property(nonatomic,strong) NSString *setImage;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *address;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property(strong,nonatomic) NSString *moreImage;

@property(nonatomic, assign) UIImage *profImg;
@property(strong,nonatomic) NSString *moreName;
@property(strong,nonatomic) NSString *moreAddress;
@property(strong,nonatomic) NSString *GetAdvisorIDMore;

-(void)MoreQuestionListing;

@end
