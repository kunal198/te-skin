//
//  MoreViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/27/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "MoreViewController.h"

@interface MoreViewController ()
{
    int m;
}

@end

@implementation MoreViewController
@synthesize moreProfileAddress,moreProfileName,moreProfileImg;
@synthesize name,address;

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
    m = 0;
    
//    [self MoreQuestionListing];
    [self FillArray];
    [self headerContent];
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        _A3LabelOutlet.hidden = NO;
        
        _A3ImageOutlet.hidden = YES;
        [_answerLabelOutlet setFont:[UIFont systemFontOfSize:25]];
       [_A3LabelOutlet setFont:[UIFont systemFontOfSize:30]];
        _A3LabelOutlet.textColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        
        //  NSLog(@"navigation height%@",_navigationView.frame);
    }
    self.headerView.layer.borderColor = [UIColor grayColor].CGColor;
    self.headerView.layer.borderWidth = 0.5f;
    self.moreTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
//    [self headerContent];
}
- (void)doSomeFunkyStuffMore
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}
-(void)MoreQuestionListing
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMore) onTarget:self withObject:nil animated:YES];
    PlusQuestionObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post = [NSString stringWithFormat:@"AdvisorID=%@",self.GetAdvisorIDMore];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:MoreQuestion_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
    
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strPlusAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     PlusQuestionObj = [data1 valueForKey:@"questions"];
                     
                     if (PlusQuestionObj.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                     NSLog(@"ProAdviceObj is %@",PlusQuestionObj);
                     HUD.hidden = true;
                     
                    //
//                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                     [alert show];
                     
                     [self.moreTableView reloadData];
                     
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{

    NSLog(@"self.setImage is %@",self.setImage);
    
    NSURL *url = [NSURL URLWithString:self.setImage];
    
    NSLog(@"url is %@",url);
    
//    [moreProfileImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"NoUsername.png"]];
    
    moreProfileImg.image = [UIImage imageNamed:_setImage];
    moreProfileImg.contentMode = UIViewContentModeScaleAspectFit;
    [videoPlayerViewMore.view removeFromSuperview];
    
}

-(void)FillArray
{
    colletionArray = [[NSMutableArray alloc]init];
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"ABHIJIT BHADURI",@"FooterTitle",
                               @"The Chief Learning Officer of Wipro Ltd",@"FooterDescription",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                             @"bhaduri.png",@"ProfileImage",
                               @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd. Has been part of companies like Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. Abhijit writes regularly for the Economic Times, People Matters and blogs for Times of India.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"PRABIR JHA",@"FooterTitle",
                               @"Global Chief People Officer of Cipla",@"FooterDescription",
                               @"Ford Lump Sum Buyout- White Paper Explained (Pile vs. Stream) (06).mp4",@"VideoLink",
                               @"Prabir.png",@"ProfileImage",
                               @"Prabir Jha, an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. Prabir brings with him an impeccable academic and professional record.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"Dr AQUIL BUSRAI",@"FooterTitle",
                               @"Chief Executive Officer at aquil busrai consulting",@"FooterDescription",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Aquil.png",@"ProfileImage",
                               @"Dr Aquil Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. Currently runs his own consulting firm.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"SRIPADA CHANDRASEKHAR ",@"FooterTitle",
                               @"President and global head of HR at Dr. Reddy’s Laboratoires",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Sripada.png",@"ProfileImage",
                               @"Dr. Sripada Chandrarsekhar is the global head of HR at Dr. Reddy’s Laboratoires, Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"VIKRAM BECTOR",@"FooterTitle",
                               @"Chief Talent Officer at Reliance Industries",@"FooterDescription",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Vikram.png",@"ProfileImage",@"Vikram Bector - Chief Talent Officer at Reliance Industries Limited. Over two decades of experience with some leading Indian conglomerates.",@"info",nil]];
    
}


-(void)headerContent
{
    colletionArray = [[NSMutableArray alloc]init];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];

    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4.mp4",@"video",
                               @"25 m",@"time",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
}

#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    return [colletionArray count];
    return [colletionArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

//    Cell.moreCellImage.image = [UIImage imageNamed:@"no_image_icon.png"];

    Cell.moreTitleName.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];

    
    Cell.moreAuthorName.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"names"];
    Cell.moreTimeLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"time"];
    
    
    ///////////////
    NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
    
    NSLog(@"videoPath is %@",videoPath);
    NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
    UIImage *profileImageStatic = [self generateThumbImage:streamURL];
    Cell.moreCellImage.image = profileImageStatic;
    ///////////////
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;

    // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    isDoneBtnClicked = false;
    NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        videoPlayerViewMore = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
        //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
        [self.view addSubview:videoPlayerViewMore.view];
        videoPlayerViewMore.fullscreen = YES;
        videoPlayerViewMore.controlStyle = MPMovieControlStyleEmbedded;
        videoPlayerViewMore.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        moviePlayer.view.layer.zPosition = 1;
        [videoPlayerViewMore prepareToPlay];
        [videoPlayerViewMore play];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteMore:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewMore];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickMore:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    }
}

- (void)aMoviePlaybackCompleteMore:(NSNotification*)notification
{
    NSLog(@"isPlayAgn is %hhd",isPlayAgnMore);
    if (isPlayAgnMore == false)
    {
        isDoneBtnClicked = true;
        if (videoPlayerViewMore.view)
        {
            [videoPlayerViewMore.view removeFromSuperview];
            [videoPlayerViewMore stop];
        }
            if (!isDoneBtnClicked)
            {
                
                m = 1;
                
                [videoPlayerViewMore.view removeFromSuperview];
                [videoPlayerViewMore stop];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice" ofType:@"mp4"];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerViewMore = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerViewMore.view];
                videoPlayerViewMore.fullscreen = YES;
                videoPlayerViewMore.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerViewMore.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerViewMore prepareToPlay];
                [videoPlayerViewMore play];

            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteMore:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewMore];
            
            isPlayAgnMore = true;
        }
    }
    else
    {
        
        [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             videoPlayerViewMore.view.alpha = 0;
                         }completion:^(BOOL finished)
         {
             [videoPlayerViewMore.view removeFromSuperview];
              [videoPlayerViewMore stop];
         }];
        isPlayAgnMore = false;
    }
}

-(void)doneButtonClickMore:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        [videoPlayerViewMore.view removeFromSuperview];
         [videoPlayerViewMore stop];
    }
    else
    {
//        answrVideoLinkMore = @"";
        [videoPlayerViewMore.view removeFromSuperview];
         [videoPlayerViewMore stop];
         isPlayAgnMore = false;
        isDoneBtnClicked = false;
        videoPlayerViewMore = nil;
    }
    
}

- (IBAction)shareBtnMore:(id)sender
{
    NSLog(@"ShareBtn pressed fro sharing on social media %@ and %@",video_Title,video_Link);
    
    NSString *message_Title = video_Title;
    NSString *message_Link = video_Link;
    NSString *text_msg = @"To view the full answer please download the talentedge app";
    NSString *talentedge_description = @"Welcome to the world of anytime anywhere learning and be the part of the learners community who are changing the way India learns.We ensure a journey full of knowledge and advancement. You can choose from the array of offering we have for our learners via out Direct to Device platform.";
    
    NSArray * shareItems = @[message_Title,message_Link,text_msg,talentedge_description];
    
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    
    [self presentViewController:avc animated:YES completion:nil];
}

- (IBAction)a3PopOut:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
//    NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

@end
