//
//  MyProfileProAdviceCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileProAdviceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ProdviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *ProAdvicetime;
@property (weak, nonatomic) IBOutlet UILabel *ProAdviceTitle;
@property (weak, nonatomic) IBOutlet UILabel *AdvisorName;
@property (weak, nonatomic) IBOutlet UIButton *proadviceReplyBtn;

@end
