//
//  MyProfileViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "MyProfileProAdviceCell.h"
#import "AdvidorFollowerCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "MBProgressHUD.h"

#import "ELCImagePickerController.h"
#import "Constants.h"
#import <AWSS3/AWSS3.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "My_quesTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define Advisor_ProAdvice_Listing @"http://mobileapi.talentedge.hub1.co/api/Advisor/GetAdvisorDetails"
#define Advisor_GiveAnswerAPI @"http://mobileapi.talentedge.hub1.co/api/Advisor/GiveAnswer"
#define StitchQuestion_Listing @"http://mobileapi.talentedge.hub1.co/api/Advisor/Questions_listing"
#define Advisor_Follow_Listing @"http://mobileapi.talentedge.hub1.co/api/Activity/FollowList"
#define DisplayImage_AskProAPI @"http://mobileapi.talentedge.hub1.co/api/Advisor/Advisors_Listing"

extern BOOL isPushNotificationForAdvisor;

@interface MyProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate,UIAlertViewDelegate,MPMediaPickerControllerDelegate>
{
    int replyIndex;
    
    NSMutableArray *AdvisorArray;
    NSString *questionLink;
    MBProgressHUD *HUD;
    MPMoviePlayerController *videoPlayerViewMyProfile;
    
    NSArray *videoImages;
    NSArray *selectedNameArray;
    NSArray *headingArray;
    NSArray *selectedTimeArray;
    NSArray *selectedAddressArray;
    NSArray *selectedUpvotesArray;
    NSMutableArray *followerCollectionArray;
    BOOL isPlayAgnAndAgn;
    UIImage *thumbnail;
    NSMutableArray *MyProfileProAdviceObj;
    NSArray *mediaTypes;
    NSURL *chosenMovieMyProfile;
    double secondsMyProfile;
    NSString *secDisplayMyProfile;
    NSData *movieDataMyProfile;
    NSArray *saveVideoMyProfile;
    NSString *keyStrMyProfile;
    NSArray *arrMyProfile;
    NSString *strForAlertMyProfile;
    NSMutableArray *giveAnswerArrayMyProfile;
    NSString *answrVideoLinkMyProfile;
    NSMutableArray *stitchVideoArray;
    NSString *strStitch;
    NSString *stitch_answrVideoLinkMore;
    BOOL isPlayAgnStitch;
    NSMutableArray *FollowerObjMyProfile;
    NSString *strForAlertFollower;
    NSMutableArray *GetImageData;
    UILabel *headerLbl1;
    NSString *Question_Video_Title;
    BOOL isFollowerSelected;
    NSMutableArray *dummyarray;
    NSTimer *timer;
    BOOL isDoneMyProfileUnreplied;
    BOOL isDoneMyProfileProadvice;
    NSMutableArray *logoutDataForAdvisor;
    NSMutableArray *reverseUnrepliedArray;
    NSMutableArray *reverseProadviceArray;
    

}
@property(strong,nonatomic) NSString *GiveVideoNameMyProfile;
@property(strong,nonatomic) NSURL *fileURLMyProfile;

@property (weak, nonatomic) IBOutlet UITableView *AdvisorTableView;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

- (IBAction)backBtn:(id)sender;
- (IBAction)ReplyBtn:(id)sender;
- (IBAction)addBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *followerAdvisorTableView;

-(void)Advisor_Listing;
-(void)saveVideoMyProfile;
-(void)showAlert;

- (IBAction)profileBtn:(id)sender;
- (IBAction)proAdvice:(id)sender;
- (IBAction)followBtn:(id)sender;
- (IBAction)logoutBtnAdv:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *advisorAcrollView;
@property (weak, nonatomic) IBOutlet UIImageView *blurMyProfileImage;
@property (weak, nonatomic) IBOutlet UIImageView *roundProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *AdvisorName;
@property (weak, nonatomic) IBOutlet UILabel *AdvisorAddress;
@property (weak, nonatomic) IBOutlet UIView *profileHighightViewAdv;
@property (weak, nonatomic) IBOutlet UIView *proadviceViewAdv;
@property (weak, nonatomic) IBOutlet UIView *followViewAdv;

@property(strong,nonatomic) NSString *MyProfileAdvID;
@property (weak, nonatomic) IBOutlet UIView *loaderViewMyProfile;
@property (weak, nonatomic) IBOutlet UIProgressView *progressViewMyProfile;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageViewMyprofile;
//@property(strong,nonatomic) NSString *GetAdvisorIDStitch;
@property (weak, nonatomic) IBOutlet UIButton *my_questionOutlet;


-(void)stitchVideos;
-(void)FollowListingMyProfile;
-(void)FetchProgressValueMyProfile;
-(void)logoutMethodForAdvisor;


@property (weak, nonatomic) IBOutlet UITableView *my_questionsTableView;

- (IBAction)my_quesBtn:(id)sender;
- (IBAction)replyMy_ques:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *my_quesHighLightView;
@property(strong,nonatomic) NSString *passImageMyProfile;
@property (weak, nonatomic) IBOutlet UITextView *infoViewMyProfile;
@property (weak, nonatomic) IBOutlet UILabel *My_questionsCount;
@property (weak, nonatomic) IBOutlet UILabel *Proadvice_Count;
@property (weak, nonatomic) IBOutlet UILabel *follower_Count;


//////////////

- (IBAction)new_FollwerBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *alertView;
@property(assign) BOOL isMyProfile;





@end

