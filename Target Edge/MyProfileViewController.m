//
//  MyProfileViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "MyProfileViewController.h"
#import "UploadViewController.h"
#import "VideoDiscussionsHeader.h"
#import "AppDelegate.h"


static int flag_for_alert = 0;

extern NSString *answerVideo;
BOOL isPushNotificationForAdvisor;

@interface MyProfileViewController ()
{
    NSString *MyProfileQuestionID;
    AppDelegate *appDele;
}
@end

@implementation MyProfileViewController
@synthesize MyProfileAdvID;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self headerContent];
    
    isPushBool = false;

    self.my_questionsTableView.delegate = self;
    self.my_questionsTableView.dataSource = self;
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FetchProgressValueMyProfile)  name:@"PROGRESSVALUEMyProfile" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AlertBoxMyProfile)  name:@"ALERTBOXMyProfile" object:nil];
    
    appDele = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.loaderView.hidden = false;
    
    [self.ActivityIndicator startAnimating];
    
//    [self profileBtn:self];
    
   [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadViewDatazz) userInfo:NULL repeats:NO];
}

-(void)FollowListingMyProfile
{
    isFollowerSelected = true;
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMyProfile) onTarget:self withObject:nil animated:YES];
    
    FollowerObjMyProfile = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];

    
    //    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
    NSString *post;
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
         post = [NSString stringWithFormat:@"AdvisorID=%@",LinkedIn_ID];
    }
    else
    {
        post = [NSString stringWithFormat:@"AdvisorID=%@",Creater_User_ID];
    }
    
    NSLog(@"post follower is %@",post );
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:Advisor_Follow_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strForAlertFollower = [items objectAtIndex:1];
                 }
                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     FollowerObjMyProfile = [[data1 valueForKey:@"FollowList"] mutableCopy];
                     
//                     NSLog(@"FollowerObj is %@",FollowerObjMyProfile);
                     
                     if (FollowerObjMyProfile.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                     
                     self.Proadvice_Count.text = [NSString stringWithFormat:@"%d",FollowerObjMyProfile.count];
                     
                     [self.followerAdvisorTableView reloadData];
                     HUD.hidden = true;
                 }
                 else
                 {
//                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strForAlertFollower delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                     [alert show];
//                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}


- (void)doSomeFunkyStuffMyProfile
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}

-(void)stitchVideos
{
    isFollowerSelected = false;
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMyProfile) onTarget:self withObject:nil animated:YES];
    stitchVideoArray = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue in stitchVideos %@",savedValue);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    NSString *post;

    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"AdvisorID=%@",LinkedIn_ID];
    }
    else
    {
        post = [NSString stringWithFormat:@"AdvisorID=%@",Creater_User_ID];

    }
    
        NSLog(@"advisorID in stitchVideos is %@",Creater_User_ID);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:StitchQuestion_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strStitch = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     stitchVideoArray = [[data1 valueForKey:@"questions"] mutableCopy];
                     
                     if (stitchVideoArray.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                     NSLog(@"ProAdviceObj is %@",stitchVideoArray);
                     HUD.hidden = true;
                     
                     self.follower_Count.text = [NSString stringWithFormat:@"%d",stitchVideoArray.count];
                    
                     [self.followerAdvisorTableView reloadData];
                 }
                 else
                 {
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)GiveAnswerMyProfile
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    NSString *post;

    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMyProfile) onTarget:self withObject:nil animated:YES];
    
    giveAnswerArrayMyProfile = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    advisorID = [[PlusQuestionAnsObj objectAtIndex:index] valueForKey:@"AdvisorID"];
    
    NSLog(@"advisorID is %@",advisorID);
    
//    NSString *whiteSpacesMyProfile = [keyStrMyProfile stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
   questionLink = [NSString stringWithFormat:@"https://s3-us-west-1.amazonaws.com/talentedge1/%@",keyStrMyProfile];
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
       post = [NSString stringWithFormat:@"QuestionID=%@&Answer_Video_Link=%@&Answer_Video_Title=%@&playing_time=%@",MyProfileQuestionID,questionLink,keyStrMyProfile,secDisplayMyProfile];
    }
    else
    {
      post = [NSString stringWithFormat:@"QuestionID=%@&Answer_Video_Link=%@&Answer_Video_Title=%@&playing_time=%@",MyProfileQuestionID,questionLink,keyStrMyProfile,secDisplayMyProfile];
    }

    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:Advisor_GiveAnswerAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result AskPro is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strForAlertMyProfile = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     
                     //[self Advisor_Listing];
                     NSLog(@"AskPro loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     giveAnswerArrayMyProfile = [data1 valueForKey:@"QuestionID"];
                     
                     if (giveAnswerArrayMyProfile == (id)[NSNull null])
                     {
//                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                         [alert show];
//                         HUD.hidden = true;
                     }
                     
                     NSLog(@"askQuestionArray is %@",giveAnswerArrayMyProfile);
                     
                     NSMutableDictionary *dicAnswer = [[NSMutableDictionary alloc]init];
                     int i = [[MyProfileProAdviceObj valueForKey:@"QuestionID"]indexOfObject:MyProfileQuestionID];
                     dicAnswer = [[MyProfileProAdviceObj objectAtIndex:i] mutableCopy];
                     [dicAnswer setObject:questionLink forKey:@"Answer_Video_Link"];
                     [MyProfileProAdviceObj replaceObjectAtIndex:i withObject:dicAnswer];
                     
                     [MyProfileProAdviceObj removeObjectAtIndex:i];
                     [self.AdvisorTableView reloadData];
                     

                     
                     HUD.hidden = true;
                 }
                 else
                 {
//                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong. Please upload your answer again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                     [alert show];
                     HUD.hidden = true;
                 }
                 
                 //[self.AdvisorTableView reloadData];
                 
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
    
    
    
//    [self viewDidLoad];
//    
//    
//    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(reloadAdvisorProfileTable) userInfo:NULL repeats:NO];

    
}

-(void)Advisor_Listing
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    NSString *post;

    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
     MyProfileProAdviceObj = [[NSMutableArray alloc]init];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMyProfile) onTarget:self withObject:nil animated:YES];
  
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSLog(@"GetAdvisorID is %@",advisorID);
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"AdvisorID=%@",LinkedIn_ID];
    }
    else
    {
       post = [NSString stringWithFormat:@"AdvisorID=%@",Creater_User_ID];
    }

    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:Advisor_ProAdvice_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                                          
                     dummyarray = [NSMutableArray new];
                     
                     dummyarray = [data1 valueForKey:@"questions"];
                     
                     if (dummyarray.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     else
                     {
                         for(int i = 0; i < dummyarray.count; i++)
                         {
                             if([[dummyarray objectAtIndex:i] valueForKey:@"Answer_Video_Link"] == [NSNull null])
                             {
                                 [MyProfileProAdviceObj addObject:[dummyarray objectAtIndex:i]];
                             }
                         }
//                         NSMutableArray* uniqueValues = [[NSMutableArray alloc] init];
//                         for(id e in MyProfileProAdviceObj)
//                         {
//                             if(![uniqueValues containsObject:e])
//                             {
//                                 [uniqueValues addObject:e];
//                             }
//                         }
                    
//                         MyProfileProAdviceObj = [uniqueValues mutableCopy];
                         flag_for_alert = 1;
                         
//                         if (replyIndex == 0)
//                         {
//                             NSLog(@"");
//                         }
//                         else
//                         {
//                             if (MyProfileProAdviceObj.count >0)
//                             {
//                                 NSLog(@"ProAdviceObj.count is %lu",(unsigned long)MyProfileProAdviceObj.count);
//                                 
//                                 NSLog(@"replyIndex is %d",replyIndex);
//                                 
//                                 Question_Video_Title = [[MyProfileProAdviceObj objectAtIndex:replyIndex - 1]valueForKey:@"Question_Video_Title"];
//                                 
//                                  NSLog(@"replyIndex is %d",replyIndex);
//                                 
//                                 NSLog(@"Question_Video_Title is %@",Question_Video_Title);
//                                 
//                             }
//                         }
                         
                        
                         
                         if (MyProfileProAdviceObj.count == 0)
                         {
                             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                             [alert show];
                             HUD.hidden = true;
                         }
                     }
       
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                     self.My_questionsCount.text = [NSString stringWithFormat:@"%d",MyProfileProAdviceObj.count];
                     });
                     
                     [_AdvisorTableView reloadData];
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)loadViewDatazz
{

    self.AdvisorName.text = globalAdvisorName;
    self.AdvisorAddress.text = globalAdvisorAddress;
    
    NSLog(@"dummyarray.count is %lu",(unsigned long)dummyarray.count);
    self.My_questionsCount.text = @"7";
    self.Proadvice_Count.text = @"7";
    self.follower_Count.text = @"7";
    
    
    self.roundProfileImage.layer.cornerRadius =  self.roundProfileImage.frame.size.width/2;
    self.roundProfileImage.layer.borderWidth = 2.0;
    self.roundProfileImage.layer.masksToBounds = YES;
    self.roundProfileImage.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    NSMutableArray *passArray = [[NSMutableArray alloc]init];
    [passArray addObjectsFromArray:[appDele GlobalCollectionProfile]];
        
    NSLog(@"passArray is %lu",(unsigned long)passArray.count);
    
    UILabel *headerLbl;

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"profileimage.png"];
                
    NSData *imgData = [NSData dataWithContentsOfFile:filePath];
                
//    BOOL fileExists = [fileManager fileExistsAtPath:filePath];
//    if (fileExists == YES)
//    {
//        NSLog(@"file exist");
//        self.roundProfileImage.image = [UIImage imageWithData:imgData];
//    }
//        
//
//                NSURL *url = [NSURL URLWithString:globalImage];
//                
//                NSLog(@"url is %@",url);
//                
//                [self.roundProfileImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"NoUsername.png"]];
//
//                NSURL *urlBlurr = [NSURL URLWithString:globalImage];
//                UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlBlurr]];
//                //
    
                 self.roundProfileImage.image = [UIImage imageNamed:@"advisor5.jpeg"];
                self.blurMyProfileImage.image = [self blurredImageWithImage:[UIImage imageNamed:@"advisor5.jpeg"]];
                
            //    self.blurMyProfileImage.image = [self blurredImageWithImage:[UIImage imageNamed:[[passArray objectAtIndex:indexValue] valueForKey:@"ProfileImages"]]];
                
                headerLbl1 = [[UILabel alloc]init];
                [headerLbl1 setTextColor:[UIColor darkGrayColor]];
                // [answerLbl setBackgroundColor:[UIColor clearColor]];
                [headerLbl1 setFont:[UIFont boldSystemFontOfSize:15.0f]];
                CGFloat width = self.view.frame.size.width-20;
                headerLbl1.numberOfLines = 100;
                [headerLbl1 setText:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."];
             //   [headerLbl1 setText:self.info];
                headerLbl1.frame = CGRectMake(10, 20, self.view.frame.size.width-20, [self heightForText:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s." withWidth:width font:headerLbl1.font]);
                
                [self.advisorAcrollView addSubview:headerLbl1];
        
        [self.advisorAcrollView setContentSize:CGSizeMake(self.view.frame.size.width, headerLbl1.frame.size.height+headerLbl.frame.size.height+60)];

    if ([notificationMessage containsString:@"New Questions have been added for your response in the questions queue"] || [notificationMessage containsString:@"Your weekly questions queue has been updated"])
    {
         [self proAdvice:@""];
        _isMyProfile = false;
    }
    else
    {
        [self proAdvice:@""];
    }
    
    self.followerAdvisorTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.AdvisorTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    [self.advisorAcrollView setShowsHorizontalScrollIndicator:NO];

    self.loaderView.hidden = true;
    
    [self.ActivityIndicator stopAnimating];
}

+ (CGFloat)heightForText:(NSString*)text font:(UIFont*)font withinWidth:(CGFloat)width
{
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    CGFloat area = size.height * size.width;
    CGFloat height = roundf(area / width);
    return ceilf(height / font.lineHeight) * font.lineHeight;
}

-(CGFloat) heightForText:(NSString *)text withWidth:(CGFloat) textWidth font:(UIFont*)font
{
    
    CGSize constraint = CGSizeMake(textWidth, 20000.0f);
    CGRect rect = [text boundingRectWithSize:constraint
                                     options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                  attributes:@{NSFontAttributeName:font}
                                     context:nil];
    CGFloat height = rect.size.height;
    
    height = ceilf(height);
    //    NSLog(@"height %f", height);
    return height;
}

-(UIImage*)blurredImageWithImage:(UIImage *)sourceImage
{
    
    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:15.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    //  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
    // up exactly to the bounds of our original image /
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *retVal = [UIImage imageWithCGImage:cgImage];
    return retVal;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ReplyBtn:(id)sender
{
    replyIndex = [sender tag]-121;
    
//    MyProfileQuestionID = [[MyProfileProAdviceObj objectAtIndex:[sender tag]- 121] valueForKey:@"QuestionID"];
//    //    questionId = [[PlusQuestionAnsObj objectAtIndex:(int)[sender currentTitle]]  valueForKey:@"QuestionID"];
//    NSLog(@"questionId for reply is %@",MyProfileQuestionID);
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        picker.videoMaximumDuration = 180.0f;
        
        mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        
        picker.mediaTypes = mediaTypes;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"No Camera in this Device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }

}

#pragma mark - UIImagePickerController delegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Give title" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//    alert.tag = 1;
//    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    [alert show];
    
     chosenMovieMyProfile = [info objectForKey:UIImagePickerControllerMediaURL];
     [self saveVideoMyProfile];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0)
        {
//            _GiveVideoNameMyProfile = [alertView textFieldAtIndex:0].text;
//            
//            NSLog(@"_GiveVideoName is %@",_GiveVideoNameMyProfile);
//            // name contains the entered value
//            [self saveVideoMyProfile];
        }
    }
    if (alertView.tag == 3)
    {
        if (buttonIndex == 1)
        {
//            [self logoutMethodForAdvisor];
            LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            [self.navigationController pushViewController:login animated:YES];
            
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"Login"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainAdvisor"];
            
            [[UIApplication sharedApplication] unregisterForRemoteNotifications];
        
        }
    }
}

-(void)logoutMethodForAdvisor
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMyProfile) onTarget:self withObject:nil animated:YES];
    
    logoutDataForAdvisor = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue for Logout_API is %@",savedValue);
    
    NSString *post = [NSString stringWithFormat:@"Email=%@&RoleName=%@",saveEmail,@"Advisor"];

    NSLog(@"post in Logout_API is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:Logout_API]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (data)
         {
             id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             NSLog(@"result UserProfile is %@",result);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert in UserProfile is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data(logoutData) is %@",data1);
                 
                 
                 if (success)
                 {
                     NSLog(@"UserProfile loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     logoutDataForAdvisor = [data1 valueForKey:@"details"];
                     
                     NSLog(@"logoutDataForAdvisor is %@",logoutDataForAdvisor);
                     
                     
                     LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                     [self.navigationController pushViewController:login animated:YES];
                     
                     [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"Login"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainAdvisor"];
                     
                     [[UIApplication sharedApplication] unregisterForRemoteNotifications];
                     
                     HUD.hidden = YES;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
             });
         }
         else if (error)
             
             NSLog(@"%@",error);
         
     }];
}


- (NSURL*)grabFileURL:(NSString *)fileNamesMyProfile
{
//    NSString *strVideoName = [_GiveVideoNameMyProfile stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
    NSString *strVideoName = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
;
    
    NSLog(@"strVideoName is %@",strVideoName);
    
    fileNamesMyProfile = [strVideoName stringByAppendingString:@".mp4"];
    
    NSLog(@"fileNames is %@",fileNamesMyProfile);
    
    keyStrMyProfile = [NSString stringWithFormat:@"Answer%@.mp4",strVideoName];
    
    NSLog(@"keystr is %@",keyStrMyProfile);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *documentsDirectoryURL = [[fileManager URLsForDirectory: NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *fileURLs = [NSURL URLWithString:fileNamesMyProfile relativeToURL:documentsDirectoryURL];
    
    arrMyProfile = [NSArray arrayWithObjects:[fileURLs path], nil];
    
    return fileURLs;
}


-(void)saveVideoMyProfile
{
    // grab our movie URL
    
    NSLog(@"info is %@",chosenMovieMyProfile);
    
    // save it to the documents directory
    _fileURLMyProfile = [self grabFileURL:@"%@.mp4"];
    
    NSLog(@"fileURL is %@",_fileURLMyProfile);
    
    AVURLAsset *avUrl = [AVURLAsset assetWithURL:chosenMovieMyProfile];
    CMTime time = [avUrl duration];
    secondsMyProfile = ceil(time.value/time.timescale);
    
    NSLog(@"seconds is %f",secondsMyProfile);
    
    //    double newCurrentTime = objAudio.currentTime;
    int min = floor(secondsMyProfile/60);
    int sec = trunc(secondsMyProfile - min * 60);
    
    if (sec < 10)
    {
        secDisplayMyProfile = [NSString stringWithFormat:@"%i:%i" ,min,sec];
        NSLog(@"secDisplay is %@",secDisplayMyProfile);
    }
    else
    {
        secDisplayMyProfile = [NSString stringWithFormat:@"%i:%i" ,min,sec];
        NSLog(@"secDisplay is %@",secDisplayMyProfile);
    }
    
    movieDataMyProfile = [NSData dataWithContentsOfURL:chosenMovieMyProfile];
    
    [movieDataMyProfile writeToURL:_fileURLMyProfile atomically:YES];
    
    // save it to the Camera Roll
    UISaveVideoAtPathToSavedPhotosAlbum([chosenMovieMyProfile path], nil, nil, nil);
    
    saveVideoMyProfile = [NSArray arrayWithObjects:[chosenMovieMyProfile path], nil];
    
    NSLog(@"saveVideo is %@",saveVideoMyProfile);
    
    // and dismiss the picker
    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.progressViewMyProfile.hidden = NO;
    self.loaderViewMyProfile.hidden = NO;
    
    NSString *whiteSpacesMyProfile = [keyStrMyProfile stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
    UploadViewController * _obj_UploadViewController = [[UploadViewController alloc]init];
    [_obj_UploadViewController viewDidLoad];
    [_obj_UploadViewController CallAWS3ConnectionForMyProfile:chosenMovieMyProfile :whiteSpacesMyProfile];
    //    [_obj_UploadViewController CallAWS3Connection:chosenMovie :[NSString stringWithFormat:@"%@",chosenMovie]];
    
    [_obj_UploadViewController convertingDataIntoBitesForMyProfile];
}

-(void)FetchProgressValueMyProfile
{
    appDele = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.progressViewMyProfile.progress  = appDele.globle_progress_value;
}

-(void)AlertBoxMyProfile
{
//    [self GiveAnswerMyProfile];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Video has been successfully Uploaded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
      [alert show];
    self.loaderViewMyProfile.hidden = YES;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)addBtn:(id)sender
{
    
}
- (IBAction)profileBtn:(id)sender
{
    NSLog(@"ahsgdhagd");
    
//    self.alertView.hidden = true;
    
    _profileImageViewMyprofile.image = [UIImage imageNamed:@"profileUser1.png"];
    self.profileHighightViewAdv.hidden = NO;
    _proadviceViewAdv.hidden = YES;
    _followViewAdv.hidden = YES;    
    self.AdvisorTableView.hidden = YES;
    self.followerAdvisorTableView.hidden = YES;
    _my_quesHighLightView.hidden = YES;
    _my_questionsTableView.hidden = YES;
    headerLbl1.hidden = NO;
}

-(void)showAlert
{
    if(flag_for_alert == 1)
    {
        if (dummyarray.count == 0)
        {
            flag_for_alert = 0;
            
            [timer invalidate];
            timer = nil;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Good work! All Questions are replied" delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (IBAction)proAdvice:(id)sender
{
    [self headerContent];

    NSLog(@"dummyarray.count is %d",MyProfileProAdviceObj.count);
    
    _profileImageViewMyprofile.image = [UIImage imageNamed:@"profileUser.png"];
    
    _profileHighightViewAdv.hidden = YES;
    _proadviceViewAdv.hidden = NO;
    _followViewAdv.hidden = YES;

    _AdvisorTableView.hidden = NO;
    _followerAdvisorTableView.hidden = YES;
    _my_quesHighLightView.hidden = YES;
    _my_questionsTableView.hidden = YES;

     [_advisorAcrollView setContentOffset:CGPointMake(_advisorAcrollView.frame.origin.x, 0) animated:YES];
}

- (IBAction)followBtn:(id)sender
{
    isFollowerSelected = false;

    [self headerContent];
    
    _profileImageViewMyprofile.image = [UIImage imageNamed:@"profileUser.png"];
    _profileHighightViewAdv.hidden = YES;
    _proadviceViewAdv.hidden = YES;
    _followViewAdv.hidden = NO;
    _my_quesHighLightView.hidden = YES;
    _my_questionsTableView.hidden = YES;
    
    _AdvisorTableView.hidden = YES;
    _followerAdvisorTableView.hidden = NO;
    
    _infoViewMyProfile.hidden = YES;
     [self.followerAdvisorTableView reloadData];
    
}

- (IBAction)logoutBtnAdv:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to logout?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil ];
    alert.tag = 3;
    [alert show];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainAdvisor"];
}

#pragma mark - Information


-(void)headerContent
{
    stitchVideoArray = [[NSMutableArray alloc]init];
    
    [stitchVideoArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor1.jpeg",@"ProfileImage",
                               @"Goa",@"Location",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [stitchVideoArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"Vikram.png",@"ProfileImage",
                               @"Mumbai",@"Location",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [stitchVideoArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor2.jpeg",@"ProfileImage",
                               @"Ludhiana",@"Location",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    [stitchVideoArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"advisor4.jpeg",@"ProfileImage",
                               @"Pune",@"Location",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [stitchVideoArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor3.jpeg",@"ProfileImage",
                               @"Patiala",@"Location",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [stitchVideoArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4.mp4",@"video",
                               @"25 m",@"time",
                               @"advisor4.jpeg",@"ProfileImage",
                               @"Goa",@"Location",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [stitchVideoArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor5.jpeg",@"ProfileImage",
                               @"Uttrakhand",@"Location",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
}



#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.followerAdvisorTableView)
    {
        return 1;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.AdvisorTableView)
    {
        NSLog(@"questionsProfileArray count %@",stitchVideoArray);

        return [stitchVideoArray count];
    }
    else
    {
        if (isFollowerSelected == false)
        {
            NSLog(@"stitchVideoArray is %d",stitchVideoArray.count);
              return [stitchVideoArray count];
        }
        else
        {
            NSLog(@"FollowerObjMyProfile is %@",stitchVideoArray);
            return [stitchVideoArray count];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.followerAdvisorTableView)
    {
        return 90.0;
    }
    else
    {
        return 90.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.AdvisorTableView)
    {
        static NSString *cellIdentifier = @"MyProfileProAdviceCell";
            
        MyProfileProAdviceCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
         Cell.proadviceReplyBtn.hidden = false;
        
        Cell.ProAdviceTitle.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];

        Cell.AdvisorName.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"names"];
        
        Cell.ProAdvicetime.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"time"];
 
        
        ///////////////
        NSArray *dummyArray = [[[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
        
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        UIImage *profileImageStatic = [self generateThumbImage:streamURL];
        Cell.ProdviceImageView.image = profileImageStatic;
        ///////////////
        
         Cell.proadviceReplyBtn.tag = indexPath.row + 121;
        
        answrVideoLinkMyProfile = [[MyProfileProAdviceObj objectAtIndex:indexPath.row] valueForKey:@"Answer_Video_Link"];
        
        NSLog(@"answrVideoLink plus is%@",answrVideoLinkMyProfile);
        //
        //    if ([answrVideoLink isKindOfClass:NULL] || answrVideoLink.length == 6 || [answrVideoLink isEqualToString:@"null"])
        //    {
        //        NSLog(@"sdhgshdgsdugasdasdf");
        //    }
        
        
            [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            return Cell;
            
        }
        else if(tableView == self.followerAdvisorTableView)
        {
            if (isFollowerSelected == true)
            {
                static NSString *cellIdentifier = @"AdvidorFollowerCell";
                AdvidorFollowerCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                Cell.playBtnStaticImage.hidden = true;
                Cell.follwerAdvisorTime.hidden = true;
                
                NSString *follower = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"ProfileImage"];

                
                Cell.followerAdvisorImageView.image = [UIImage imageNamed:follower];
                
                Cell.followerAdvisorName.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"Location"];
                
                Cell.followerAdvisorTitle.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"names"];
                
                 [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                return Cell;

            }
            else
            {
                static NSString *cellIdentifier = @"AdvidorFollowerCell";
                AdvidorFollowerCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
                Cell.playBtnStaticImage.hidden = false;
                Cell.follwerAdvisorTime.hidden = false;
                
                ///////////////
                NSArray *dummyArray = [[[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
                
                NSLog(@"videoPath is %@",videoPath);
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                UIImage *profileImageStatic = [self generateThumbImage:streamURL];
                Cell.followerAdvisorImageView.image = profileImageStatic;
                ///////////////
                
                Cell.followerAdvisorName.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"name"];
                Cell.follwerAdvisorTime.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"time"];
                
                //        NSLog(@" Cell.FollowerAddressLbl.text is %@", Cell.FollowerAddressLbl.text);
                
                Cell.followerAdvisorTitle.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];
                
                return Cell;

            }
        }
    else
    {
        static NSString *cellIdentifier = @"My_quesTableViewCell";
        My_quesTableViewCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        //    NSString *replace = [[PlusQuestionObj objectAtIndex:indexPath.row] valueForKey:@"Question_Video_Title"];
        //
        //    Cell.moreTitleName.text = [replace stringByReplacingOccurrencesOfString:@"=-" withString:@" "];
        
        
        Cell.My_quesTitle.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"Question_Video_Title"];
        
//        NSString *video_Title = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"Question_Video_Title"];
        
        NSString *ThumnailImgStich = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"video_thumbnail"];
        
        if (ThumnailImgStich == (id)[NSNull null])
        {
            
            Cell.My_quesImageView.image = [UIImage imageNamed:@"no_image_icon.png"];
            
        }
        else
        {
            NSURL *imageURL = [NSURL URLWithString:ThumnailImgStich];
            [Cell.My_quesImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"no_image_icon.png"]];
        }
//        NSLog(@"ThumnailImgStich is %@",ThumnailImgStich);
    
//        NSString *video_Link = [stitchVideoArray[indexPath.row] valueForKey:@"Question_Video_Link"];
        
        Cell.My_queAdvisorName.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"User_name"];
        Cell.my_quesPlayTime.text = [[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"question_video_playingTime"];
        
        Cell.layoutMargins = UIEdgeInsetsZero;
        Cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        
        // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return Cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _AdvisorTableView)
    {
        isDoneMyProfileUnreplied = false;
        
        NSArray *dummyArray = [[[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
        
        if(dummyArray.count != 0)
        {
            NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
            
            NSLog(@"videoPath is %@",videoPath);
            
            NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
            
            NSLog(@"streamURL is %@",streamURL);
            
            videoPlayerViewMyProfile = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
            //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
            [self.view addSubview:videoPlayerViewMyProfile.view];
            videoPlayerViewMyProfile.fullscreen = YES;
            videoPlayerViewMyProfile.controlStyle = MPMovieControlStyleEmbedded;
            videoPlayerViewMyProfile.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            //        moviePlayer.view.layer.zPosition = 1;
            [videoPlayerViewMyProfile prepareToPlay];
            [videoPlayerViewMyProfile play];
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteMyProfile:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewMyProfile];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickMyProfile:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    }
    else if(tableView == self.followerAdvisorTableView)
    {
        if (isFollowerSelected == false)
        {
            isDoneMyProfileProadvice = false;
   
            NSArray *dummyArray = [[[stitchVideoArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            if(dummyArray.count != 0)
            {
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerViewMyProfile = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerViewMyProfile.view];
                videoPlayerViewMyProfile.fullscreen = YES;
                videoPlayerViewMyProfile.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerViewMyProfile.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerViewMyProfile prepareToPlay];
                [videoPlayerViewMyProfile play];
            }
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteStitch:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewMyProfile];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickStitch:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
            
            }

        }
        
}

- (void)aMoviePlaybackCompleteStitch:(NSNotification*)notification
{
    if (isPlayAgnStitch == false)
    {
        
        isDoneMyProfileProadvice  = true;
        [videoPlayerViewMyProfile.view removeFromSuperview];
        [videoPlayerViewMyProfile stop];
        
            if (!isDoneMyProfileProadvice)
            {
                [videoPlayerViewMyProfile.view removeFromSuperview];
                [videoPlayerViewMyProfile stop];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice" ofType:@"mp4"];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerViewMyProfile = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerViewMyProfile.view];
                videoPlayerViewMyProfile.fullscreen = YES;
                videoPlayerViewMyProfile.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerViewMyProfile.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerViewMyProfile prepareToPlay];
                [videoPlayerViewMyProfile play];
                
            }
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteStitch:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewMyProfile];
            
            isPlayAgnStitch = true;
    }
    else
    {
        isPlayAgnStitch = false;
        [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             videoPlayerViewMyProfile.view.alpha = 0;
                         }completion:^(BOOL finished)
         {
             [videoPlayerViewMyProfile.view removeFromSuperview];
             [videoPlayerViewMyProfile stop];
         }];
    }
}

-(void)doneButtonClickStitch:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        [videoPlayerViewMyProfile.view removeFromSuperview];
        [videoPlayerViewMyProfile stop];
    }
    else
    {
        [videoPlayerViewMyProfile.view removeFromSuperview];
//        [videoPlayerViewMyProfile stop];
        isPlayAgnStitch = false;
        isDoneMyProfileProadvice = false;
        videoPlayerViewMyProfile = nil;
    }
}




-(void)doneButtonClickMyProfile:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        [videoPlayerViewMyProfile.view removeFromSuperview];
        [videoPlayerViewMyProfile stop];
    }
    else
    {
        [videoPlayerViewMyProfile.view removeFromSuperview];
        [videoPlayerViewMyProfile stop];
        isPlayAgnAndAgn = false;
        isDoneMyProfileUnreplied = false;
        videoPlayerViewMyProfile = nil;
    }
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
}

- (void)aMoviePlaybackCompleteMyProfile:(NSNotification*)notification
{
    [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                     animations:^{
                         videoPlayerViewMyProfile.view.alpha = 0;
                     }completion:^(BOOL finished)
     {
         [videoPlayerViewMyProfile.view removeFromSuperview];
         [videoPlayerViewMyProfile stop];
     }];

}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
    //  NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

//
//- (IBAction)my_quesBtn:(id)sender
//{
//    [self FollowListingMyProfile];
//    
//     [_advisorAcrollView setContentOffset:CGPointMake(_advisorAcrollView.frame.origin.x, 0) animated:YES];
//    
//    _profileImageViewMyprofile.image = [UIImage imageNamed:@"profileUser.png"];
//    
//    _profileHighightViewAdv.hidden = YES;
//    _proadviceViewAdv.hidden = YES;
//    _followViewAdv.hidden = YES;
//    
//    _AdvisorTableView.hidden = YES;
//    _followerAdvisorTableView.hidden = YES;
//    _my_quesHighLightView.hidden = NO;
//    _my_questionsTableView = NO;
//    headerLbl1.hidden = YES;
//    
//    
//    //    _profileInfoTextView.hidden = YES;
//    //    _profileTagView.hidden = YES;
//    //    _profileTextView.hidden = YES;
//    //    _upvotesTableView.hidden = YES;
//    //
//    //    [self.AdvisorTableView reloadData];
//   
//
//}

- (IBAction)new_FollwerBtn:(id)sender
{
    isFollowerSelected = true;
    
    [self headerContent];
    
    [_advisorAcrollView setContentOffset:CGPointMake(_advisorAcrollView.frame.origin.x, 0) animated:YES];
    
    _profileImageViewMyprofile.image = [UIImage imageNamed:@"profileUser.png"];
    
    _profileHighightViewAdv.hidden = YES;
    _proadviceViewAdv.hidden = YES;
    _followViewAdv.hidden = YES;
    
    _AdvisorTableView.hidden = YES;
//    _followerAdvisorTableView.hidden = YES;
     _followerAdvisorTableView.hidden = NO;
    _my_quesHighLightView.hidden = NO;
    //_my_questionsTableView = NO;
    headerLbl1.hidden = YES;
    
     [self.followerAdvisorTableView reloadData];

}
@end
