//
//  MyQuestionsA3.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/5/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "A3ViewController.h"
#import "MBProgressHUD.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#define MyQuestionsA3API @"http://mobileapi.talentedge.hub1.co/Api/profile/Unansweredquestions"

@interface MyQuestionsA3 : UIViewController<MBProgressHUDDelegate,UITableViewDataSource,UITableViewDelegate>
{
    MBProgressHUD *HUD;
    NSMutableArray *MyQuestionsA3_Obj;
    MPMoviePlayerController *videoPlayerA3;
    NSString *strForMyQues_A3;
    NSString *video_TitleUnderMyQues;
    NSString *video_LinkUnderMyQues;
    NSMutableArray* reversedArray;
    int shareInt;
    NSMutableArray *colletionArray;
    UIImage *thumbnail;
}

-(void)MyQuestionsA3Method;
-(void)headerContent;
-(UIImage *)generateThumbImage:(NSURL *)filepath;

- (IBAction)backMyQuestions:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *myQuestionA3TableView;
- (IBAction)shareUnderMy_ques:(id)sender;

@end
