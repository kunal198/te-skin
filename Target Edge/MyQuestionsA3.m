//
//  MyQuestionsA3.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/5/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "MyQuestionsA3.h"

@interface MyQuestionsA3 ()

@end

@implementation MyQuestionsA3

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
     self.myQuestionA3TableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [videoPlayerA3.view removeFromSuperview];
    [videoPlayerA3 stop];
    
    [self headerContent];
    
//    [self MyQuestionsA3Method];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)doSomeFunkyStuffMyQuesA3
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}

-(void)MyQuestionsA3Method
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];

    isA3Advisor = true;
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffMyQuesA3) onTarget:self withObject:nil animated:YES];
    
    MyQuestionsA3_Obj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSLog(@"GetAdvisorID is %@",advisorID);
    NSString *post;
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"user_id=%@",LinkedIn_ID];
    }
    else
    {
      post = [NSString stringWithFormat:@"user_id=%@",Creater_User_ID];
    }
    
    NSLog(@"post user_id is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:MyQuestionsA3API]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 strForMyQues_A3 = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 
                 if(items.count>1)
                 {
                     strForMyQues_A3 = [items objectAtIndex:1];
                     //                     strForAlert = [errorAlert stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                 }

                 if (success)
                 {
                     NSLog(@"MyQuestionsA3_Obj loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     MyQuestionsA3_Obj = [data1 valueForKey:@"videos"];
                     
                     if (MyQuestionsA3_Obj.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     NSLog(@"MyQuestionsA3_Obj is %@",MyQuestionsA3_Obj);
                     
                     [self.myQuestionA3TableView reloadData];
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)headerContent
{
    colletionArray = [[NSMutableArray alloc]init];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Masters in Analytical Options",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Advisor for a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4.mp4",@"video",
                               @"25 m",@"time",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
}

#pragma mark - Image Generation From Video

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}


#pragma mark - UITableView Delegates and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [colletionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

        static NSString *cellIdentifier = @"FilterTableViewCell";
        FilterTableViewCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
   
    
        NSLog(@"urlstring %@",[colletionArray objectAtIndex:indexPath.row]);
        Cell.shareBtnOutlet.tag = indexPath.row +1122;
        NSLog(@"shareInt is %d",indexPath.row +1122);
    
        Cell.myquesTitle.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];

        Cell.myquesTimeLbl.text =  [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"time"];
    
//        Cell.myQuestionsA3ImageView.image = [UIImage imageNamed:@"no_image_icon.png"];
    
        ///////////////
        NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
    
        NSLog(@"videoPath is %@",videoPath);
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        UIImage *profileImageStatic = [self generateThumbImage:streamURL];
        Cell.myQuestionsA3ImageView.image = profileImageStatic;
        ///////////////

    
    

        Cell.layoutMargins = UIEdgeInsetsZero;
        Cell.preservesSuperviewLayoutMargins = false;
        // Cell.separatorInset = UIEdgeInsetsZero;
        tableView.separatorInset = UIEdgeInsetsZero;
        
        [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        videoPlayerA3 = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
        //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
        [self.view addSubview:videoPlayerA3.view];
        videoPlayerA3.fullscreen = YES;
        videoPlayerA3.controlStyle = MPMovieControlStyleEmbedded;
        videoPlayerA3.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        moviePlayer.view.layer.zPosition = 1;
        [videoPlayerA3 prepareToPlay];
        [videoPlayerA3 play];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteMyQuesA3:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerA3];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickProfileMyQuesA3:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    }
}

- (void)aMoviePlaybackCompleteMyQuesA3:(NSNotification*)notification
{
    [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                     animations:^{
                         videoPlayerA3.view.alpha = 0;
                     }completion:^(BOOL finished)
     {
         [videoPlayerA3.view removeFromSuperview];
         [videoPlayerA3 stop];
     }];
}

-(void)doneButtonClickProfileMyQuesA3:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        
        [videoPlayerA3.view removeFromSuperview];
        [videoPlayerA3 stop];
    }
    else
    {
        [videoPlayerA3.view removeFromSuperview];
        [videoPlayerA3 stop];
        
    }
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
}






- (IBAction)backMyQuestions:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareUnderMy_ques:(id)sender
{
//            NSLog(@"ShareBtn pressed fro sharing on social media");
//    
//        NSString *messages = @"Got a customized career guidance from an expert I always wanted to connect with.  Download Talentedge App now to get yours!";
//    
//            NSArray * shareItems = @[messages];
//    
//            UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
//    
//            [self presentViewController:avc animated:YES completion:nil];
    
   
        shareInt = [sender tag] - 1122;
        NSString *message_Title = [[MyQuestionsA3_Obj objectAtIndex:shareInt] valueForKey:@"Question_Video_Title"];
        NSString *message_Link = [[MyQuestionsA3_Obj objectAtIndex:shareInt] valueForKey:@"question_video_link"];
    
     NSLog(@"ShareBtn pressed fro sharing on social media %@ and %@",message_Title,message_Link);
    
    NSArray * shareItems = @[message_Title,message_Link];
    
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    
    [self presentViewController:avc animated:YES completion:nil];
}
@end
