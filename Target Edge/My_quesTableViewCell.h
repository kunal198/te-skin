//
//  My_quesTableViewCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/18/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface My_quesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *My_quesImageView;
@property (weak, nonatomic) IBOutlet UILabel *my_quesPlayTime;
@property (weak, nonatomic) IBOutlet UILabel *My_quesTitle;
@property (weak, nonatomic) IBOutlet UILabel *My_queAdvisorName;
@property (weak, nonatomic) IBOutlet UIButton *my_quesReplyBtnOutlet;

@end
