//
//  NewViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/11/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvisorAskPro.h"
#import "StartUpsViewController.h"
#import "CustomCell.h"
#import "MyProfileViewController.h"
#import "LoginVC.h"
#import "MBProgressHUD.h"
#define APIForDescription @"http://mobileapi.talentedge.hub1.co/api/Advisor/Advisors_Listing"

extern  NSMutableArray *GetInfoArray;
extern  NSString *globalDes;
extern  NSString *globalImage;
extern  NSString *globalAdvisorName;
extern  NSString *globalAdvisorAddress;
extern  NSString *globalNo_questions;
extern  NSString *globalNo_followers;

@interface NewViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *AdvisorListingArray;
}

@property (weak, nonatomic) IBOutlet UITableView *advisorAskProTable;
- (IBAction)advidorInfoBtn:(id)sender;
- (IBAction)MyProfileBtn:(id)sender;
@end
