//
//  NewViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/11/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "NewViewController.h"
#import "MyProfileViewController.h"


NSMutableArray *GetInfoArray;
NSString *globalDes;
NSString *globalImage;
NSString *globalAdvisorName;
NSString *globalAdvisorAddress;
NSString *globalNo_questions;
NSString *globalNo_followers;

@interface NewViewController ()

@end

@implementation NewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(isPushNotificationForAdvisor == true)
    {
        MyProfileViewController *myProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileViewController"];
        myProfile.isMyProfile = true;
        [self.navigationController pushViewController:myProfile animated:NO];
        isPushNotificationForAdvisor = false;
    }

    
    [self GetInfo];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = true;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    AdvisorListingArray = [[NSMutableArray alloc]initWithObjects:@"Career Advisory",@"Starup Advisory",@"Business Advisory", nil];
    
     self.advisorAskProTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}

-(void)GetInfo
{
    
    GetInfoArray = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post = [NSString stringWithFormat:@"AdvisorID=%@",@""];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:APIForDescription]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result AskPro is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                
                 if (success)
                 {
                     NSLog(@"GetInfoArray loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     GetInfoArray = [data1 valueForKey:@"details"];
                     
                     NSLog(@"dataObj is %@",GetInfoArray);
                     
                     
                     
//                     NSLog(@"DescriptionAbout is %@ and %@",DescriptionAbout,ImgForMyProfile);
//                     
//                     NSLog(@"Creater_User_ID is %@ and AdvisorID is %@",Creater_User_ID,[GetInfoArray valueForKey:@"AdvisorID"]);
                     
                     for (int q=0; q<GetInfoArray.count; q++)
                     {
                         NSLog(@"[Creater_User_ID intValue] :::::::: %d",[Creater_User_ID intValue]);
                         
                         NSLog(@"AdvisorID::::::: %d",[[[GetInfoArray objectAtIndex:q] valueForKey:@"AdvisorID"] intValue]);
                         
                         if ([Creater_User_ID intValue] == [[[GetInfoArray objectAtIndex:q] valueForKey:@"AdvisorID"] intValue])
                         {
                             globalDes = [[GetInfoArray objectAtIndex:q] valueForKey:@"Description"];
                             NSLog(@"globalDes is %@",globalDes);
                             
                             globalImage = [[GetInfoArray objectAtIndex:q] valueForKey:@"Profile_Pic_Small"];
                             NSLog(@"globalImage is %@",globalImage);
                             
                             globalAdvisorName = [[GetInfoArray objectAtIndex:q] valueForKey:@"FirstName"];
                              NSLog(@"globalAdvisorName is %@",globalAdvisorName);
                             
                             globalAdvisorAddress = [[GetInfoArray objectAtIndex:q] valueForKey:@"Location"];
                             
                             globalNo_questions = [[GetInfoArray objectAtIndex:q] valueForKey:@"No_Questions"];
                             
                             globalNo_followers = [[GetInfoArray objectAtIndex:q] valueForKey:@"No_Followers"];
                             break;
                         }
                     }
                 }
                 else
                 {
                     NSLog(@"No description");
                 }
                 
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [AdvisorListingArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.advisorAskProLbl.text = [AdvisorListingArray objectAtIndex:indexPath.row];
    Cell.advisorAskProLbl.textAlignment = NSTextAlignmentCenter;
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    //[Cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        AdvisorAskPro *a3 = [self.storyboard instantiateViewControllerWithIdentifier:@"AdvisorAskPro"];
        a3.titleStr = @"Career Advisory";
        [self.navigationController pushViewController:a3 animated:YES];
    }
    else  if (indexPath.row == 1)
    {
        StartUpsViewController *start = [self.storyboard instantiateViewControllerWithIdentifier:@"StartUpsViewController"];
        start.startDisplay = @"Startup Advisory";
        [self presentViewController:start animated:YES completion:nil];
    }
    else if (indexPath.row == 2)
    {
        StartUpsViewController *start = [self.storyboard instantiateViewControllerWithIdentifier:@"StartUpsViewController"];
        start.startDisplay = @"Business Advisory";
        [self presentViewController:start animated:YES completion:nil];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (buttonIndex == 1)
    {
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:login animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"Login"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }

}
- (IBAction)advidorInfoBtn:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (IBAction)MyProfileBtn:(id)sender
{
    MyProfileViewController *profile = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileViewController"];
   [self.navigationController pushViewController:profile animated:YES];
}
@end
