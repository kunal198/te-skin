//
//  PlusViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "AdvisorAskPro.h"
#import <QuartzCore/QuartzCore.h>
#import "PlusViewController.h"
#import <AWSS3/AWSS3.h>
#import <CoreMedia/CoreMedia.h>
#import "ELCImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define PlusQuestion_Listing @"http://mobileapi.talentedge.hub1.co/api/Advisor/Questions_listing"
#define GiveAnswerAPI @"http://mobileapi.talentedge.hub1.co/api/Advisor/GiveAnswer"

@interface PlusViewController : UIViewController<UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate,MBProgressHUDDelegate>

{
    NSMutableArray *PlusQuestionAnsObj;
    NSString *strPlusAlert;
    MPMoviePlayerController *videoPlayerView;
    BOOL isPlayAgn;
    UIImage *thumbnail;
    NSArray *mediaTypes;
    NSURL *chosenMovie;
    NSString *secDisplay;
    NSString *keyStr;
    NSURL *fileURL;
    double seconds;
    NSData *movieData;
    NSArray *saveVideo;
    NSArray *arr;
    NSMutableArray *giveAnswerArray;
    NSString *strForAlert;
    NSString *questionId;
    int index;
    id fetchIndexPlus;
    NSString *answrVideoLink;
    MBProgressHUD *HUD;
    BOOL isDonePlusAdvisor;
    NSMutableArray *reversePlusAdvisor;

}
- (IBAction)backBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *plusTableView;
@property (weak, nonatomic) IBOutlet UIImageView *AdvisorImg;
@property (weak, nonatomic) IBOutlet UILabel *AdvisorName;
@property (weak, nonatomic) IBOutlet UILabel *AdvisorLocation;
@property (weak, nonatomic) IBOutlet UILabel *playTime;
@property (weak, nonatomic) IBOutlet UILabel *advisorName;
@property (weak, nonatomic) IBOutlet UIImageView *advisorImage;
@property (weak, nonatomic) IBOutlet UILabel *Advisortiltle;
@property(strong,nonatomic) NSString *GetAdvisorIDMore;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *setImage;
@property(strong,nonatomic) NSString *GiveVideoName;

@property (weak, nonatomic) IBOutlet UIView *headerViewPlus;

@property (weak, nonatomic) IBOutlet UIView *loaderViewPlus;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

-(void)GiveAnswer;
-(void)PlusQuestionListing;
-(void)headerContent;
- (IBAction)replyBtn:(id)sender;

@end
