//
//  PlusViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/12/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "PlusViewController.h"
#import "UploadViewController.h"
#import "VideoDiscussionsHeader.h"

@interface PlusViewController ()
{
    AppDelegate *appDelegate;
    NSString *strQuestionID;
    int quesAnsValue;
}
@end

@implementation PlusViewController
@synthesize name,address;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self headerContent];
        // Do any additional setup after loading the view.
    
    quesAnsValue = 0;
    
    self.headerViewPlus.layer.borderColor = [UIColor grayColor].CGColor;
    self.headerViewPlus.layer.borderWidth = 0.5f;
    self.plusTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}

-(void)viewWillAppear:(BOOL)animated
{
     self.AdvisorImg.image = [UIImage imageNamed:self.setImage];
    
//    //    NSLog(@"_name is %@",moreProfileImg);
//    //    NSLog(@"_address is %@",address);
//    
//   // self.AdvisorName.text = name;
//  //  self.AdvisorLocation.text = address;
//    NSLog(@"self.setImage is %@",self.setImage);
//    
//    NSURL *url = [NSURL URLWithString:self.setImage];
//    
//    NSLog(@"url is %@",url);
//    
//    [self.AdvisorImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"NoUsername.png"]];
    
    //    moreProfileImg.image = [UIImage imageNamed:@"no-icon.png"];
    self.AdvisorImg.contentMode = UIViewContentModeScaleAspectFit;
    [videoPlayerView.view removeFromSuperview];
    [videoPlayerView stop];
    
}

- (void)doSomeFunkyStuffPLus
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}

-(void)GiveAnswer
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffPLus) onTarget:self withObject:nil animated:YES];
    
    giveAnswerArray = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
//    advisorID = [[PlusQuestionAnsObj objectAtIndex:index] valueForKey:@"AdvisorID"];
    
    NSLog(@"advisorID is %@",advisorID);
    
     NSString *whiteSpaces = [keyStr stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
    NSString *questionLink = [NSString stringWithFormat:@"https://s3-us-west-1.amazonaws.com/talentedge1/%@",whiteSpaces];
    
    NSString *post = [NSString stringWithFormat:@"QuestionID=%@&&Answer_Video_Link=%@&Answer_Video_Title=%@&playing_time=%@",strQuestionID,questionLink,whiteSpaces,secDisplay];
    
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:GiveAnswerAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result AskPro is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strForAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     NSLog(@"AskPro loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     giveAnswerArray = [data1 valueForKey:@"QuestionID"];
                     
                     NSLog(@"askQuestionArray is %@",giveAnswerArray);
                     
                //     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strForAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
               //      [alert show];
                     [self.plusTableView reloadData];
                     
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strForAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
                 
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
    
}

-(int)calculateAnswerValue:(NSMutableArray *)arrayValue
{
    for (int i; i<arrayValue.count; i++)
    {
        if (quesAnsValue != (int)Nil)
        {
            quesAnsValue = quesAnsValue+1;
        }
        else
        {
            NSLog(@"no calculate !!");
        }
    }
    return quesAnsValue;
}


-(void)PlusQuestionListing
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffPLus) onTarget:self withObject:nil animated:YES];
    
    PlusQuestionAnsObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSLog(@"self.GetAdvisorIDMore is %@",self.GetAdvisorIDMore);
    
    NSString *post = [NSString stringWithFormat:@"AdvisorID=%@",self.GetAdvisorIDMore];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:PlusQuestion_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strPlusAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     PlusQuestionAnsObj = [data1 valueForKey:@"questions"];
                     
                     NSLog(@"ProAdviceObj is %@",PlusQuestionAnsObj);
                     HUD.hidden = true;
                     
                     //                     NSString *Answer_Video_Title = [[PlusQuestionObj objectAtIndex:0] valueForKey:@"Answer_Video_Title"];
                     //
                     //                     NSLog(@"Answer_Video_Title is %@",Answer_Video_Title);
                     //
                     //                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     //                     [alert show];
                     
                     [self.plusTableView reloadData];
                     
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strPlusAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

- (IBAction)replyBtn:(id)sender
{
    strQuestionID = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
//    questionId = [[PlusQuestionAnsObj objectAtIndex:(int)[sender currentTitle]]  valueForKey:@"QuestionID"];
    NSLog(@"questionId for reply is %@",questionId);
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        picker.videoMaximumDuration = 120.0f;
        
        mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        
        picker.mediaTypes = mediaTypes;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"No Camera in this Device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }

}

#pragma mark - Information

-(void)headerContent
{
    PlusQuestionAnsObj = [[NSMutableArray alloc]init];
    
    [PlusQuestionAnsObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [PlusQuestionAnsObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [PlusQuestionAnsObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [PlusQuestionAnsObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [PlusQuestionAnsObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [PlusQuestionAnsObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4.mp4",@"video",
                               @"25 m",@"time",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [PlusQuestionAnsObj addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
}



#pragma mark - UIImagePickerController delegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Give title" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    
    chosenMovie = [info objectForKey:UIImagePickerControllerMediaURL];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        _GiveVideoName = [alertView textFieldAtIndex:0].text;
        
        NSLog(@"_GiveVideoName is %@",_GiveVideoName);
        // name contains the entered value
        [self saveVideo];
    }
    
}

-(void)saveVideo
{
    // grab our movie URL
    
    NSLog(@"info is %@",chosenMovie);
    
    // save it to the documents directory
    fileURL = [self grabFileURL:@"%@.MOV"];
    
    NSLog(@"fileURL is %@",fileURL);
    
    AVURLAsset *avUrl = [AVURLAsset assetWithURL:chosenMovie];
    CMTime time = [avUrl duration];
    seconds = ceil(time.value/time.timescale);
    
    NSLog(@"seconds is %f",seconds);
    
    //    double newCurrentTime = objAudio.currentTime;
    int min = floor(seconds/60);
    int sec = trunc(seconds - min * 60);
    
    if (sec < 10)
    {
        secDisplay = [NSString stringWithFormat:@"%i:%i" ,min,sec];
        NSLog(@"secDisplay is %@",secDisplay);
    }
    else
    {
        secDisplay = [NSString stringWithFormat:@"%i:%i" ,min,sec];
        NSLog(@"secDisplay is %@",secDisplay);
    }
    
    movieData = [NSData dataWithContentsOfURL:chosenMovie];
    
    [movieData writeToURL:fileURL atomically:YES];
    
    // save it to the Camera Roll
    UISaveVideoAtPathToSavedPhotosAlbum([chosenMovie path], nil, nil, nil);
    
    saveVideo = [NSArray arrayWithObjects:[chosenMovie path], nil];
    
    NSLog(@"saveVideo is %@",saveVideo);
    
    // and dismiss the picker
    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.progressView.hidden = NO;
    self.loaderViewPlus.hidden = NO;
    
//    NSString *whiteSpaces = [keyStr stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
//    UploadViewController * _obj_UploadViewController = [[UploadViewController alloc]init];
//    [_obj_UploadViewController viewDidLoad];
//    [_obj_UploadViewController CallAWS3Connection:chosenMovie :whiteSpaces];
//    //    [_obj_UploadViewController CallAWS3Connection:chosenMovie :[NSString stringWithFormat:@"%@",chosenMovie]];
//    
//    [_obj_UploadViewController convertingDataIntoBites];
}

- (NSURL*)grabFileURL:(NSString *)fileNames
{
    NSString *strVideoName = [_GiveVideoName stringByReplacingOccurrencesOfString:@" " withString:@"=-"];
    
    fileNames = [strVideoName stringByAppendingString:@".MOV"];
    
    NSLog(@"fileNames is %@",fileNames);
    
    keyStr = [NSString stringWithFormat:@"%@.MOV",_GiveVideoName];
    
    NSLog(@"keystr is %@",keyStr);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *documentsDirectoryURL = [[fileManager URLsForDirectory: NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *fileURLs = [NSURL URLWithString:fileNames relativeToURL:documentsDirectoryURL];
    
    arr = [NSArray arrayWithObjects:[fileURLs path], nil];
    
    return fileURLs;
}

//-(void)FetchProgressValue
//{
//    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    self.progressView.progress  = appDelegate.globle_progress_value;
//}
//
//-(void)AlertBox
//{
//    [self GiveAnswer];
//    //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Video has been successfully Uploaded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    //  [alert show];
//    self.loaderViewPlus.hidden = YES;
//}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    //    return [colletionArray count];
    return [PlusQuestionAnsObj count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *replace = [[PlusQuestionAnsObj objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];
    
    Cell.advTilte.text = [replace stringByReplacingOccurrencesOfString:@"=-" withString:@" "];
    
    
    ///////////////
    NSArray *dummyArray = [[[PlusQuestionAnsObj objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
    
//    NSLog(@"videoPath is %@",videoPath);
    NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
    UIImage *profileImageStatic = [self generateThumbImage:streamURL];
    Cell.advImage.image = profileImageStatic;
    ///////////////
    
    Cell.advNAme.text = [[PlusQuestionAnsObj objectAtIndex:indexPath.row] valueForKey:@"names"];
    Cell.advTime.text = [[PlusQuestionAnsObj objectAtIndex:indexPath.row] valueForKey:@"time"];
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    
    // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dummyArray = [[[PlusQuestionAnsObj objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
        //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
        [self.view addSubview:videoPlayerView.view];
        videoPlayerView.fullscreen = YES;
        videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
        videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        moviePlayer.view.layer.zPosition = 1;
        [videoPlayerView prepareToPlay];
        [videoPlayerView play];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteControl:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickPLus:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
}
- (void)aMoviePlaybackCompleteControl:(NSNotification*)notification
{
    if (isPlayAgn == false)
    {

        isDonePlusAdvisor = true;
        [videoPlayerView.view removeFromSuperview];
        [videoPlayerView stop];
        
            if (!isDonePlusAdvisor)
            {
                [videoPlayerView.view removeFromSuperview];
                [videoPlayerView stop];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice" ofType:@"mp4"];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerView.view];
                videoPlayerView.fullscreen = YES;
                videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerView prepareToPlay];
                [videoPlayerView play];

            }
            
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteControl:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
        
        isPlayAgn = true;
    }
    else
    {
        //isPlayBackCalled = NO;
        isPlayAgn = false;
        //        [videoPlayerView.view removeFromSuperview];
        [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             videoPlayerView.view.alpha = 0;
                         }completion:^(BOOL finished)
         {
             [videoPlayerView.view removeFromSuperview];
             [videoPlayerView stop];
         }];
    }
}

-(void)doneButtonClickPLus:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        [videoPlayerView.view removeFromSuperview];
        [videoPlayerView stop];
    }
    else
    {
        [videoPlayerView.view removeFromSuperview];
        [videoPlayerView stop];
        isPlayAgn = false;
        isDonePlusAdvisor = false;
        videoPlayerView = nil;
    }
}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
    //    NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtn:(id)sender
{
     [self dismissViewControllerAnimated:YES completion:nil];
}
@end
