//
//  PrivacyPolicy.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppSettingController.h"

@interface PrivacyPolicy : UIViewController


@property(strong,nonatomic) NSString *titleFromAppSetting;

- (IBAction)backPrivacyPolicybtn:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *txtviewOfPrivacyPolicy;

@end
