//
//  PrivacyPolicy.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/2/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "PrivacyPolicy.h"
@interface PrivacyPolicy ()


@end

@implementation PrivacyPolicy

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _txtviewOfPrivacyPolicy.editable = NO;
    _txtviewOfPrivacyPolicy.selectable = NO;
    
    
    // Do any additional setup after loading the view.
}



- (IBAction)backPrivacyPolicybtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
