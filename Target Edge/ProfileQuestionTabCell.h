//
//  ProfileQuestionTabCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/28/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileQuestionTabCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileQuestionImageView;
@property (weak, nonatomic) IBOutlet UILabel *profileQuestionTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *profileQuestionTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *profileQuestionNameLbl;

@end
