//
//  ProfileQuestionTabCell.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/28/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "ProfileQuestionTabCell.h"

@implementation ProfileQuestionTabCell
@synthesize profileQuestionImageView,profileQuestionNameLbl,profileQuestionTimeLbl,profileQuestionTitleLbl;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
