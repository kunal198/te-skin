//
//  ProfileViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/27/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "CustomCell.h"
#import "A3ViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "QuestionsCustomCell.h"
#import "AnswerCustomCell.h"
#import "FollowersCustomCell.h"
#import "UpvotesCustomCell.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define User_QuestionAPI @"http://mobileapi.talentedge.hub1.co/Api/profile/user_questions"
#define Uplvote_Listing @"http://mobileapi.talentedge.hub1.co/api/Activity/UpVoteList"
#define Follow_Listing @"http://mobileapi.talentedge.hub1.co/api/Activity/FollowList"
#define ProAdvice_Listing @"http://mobileapi.talentedge.hub1.co/api/Advisor/Questions_listing"
#define User_ProAdviceListing @"http://mobileapi.talentedge.hub1.co/Api/profile/user_questions"
#define User_FollwerAPI @"http://mobileapi.talentedge.hub1.co/Api/profile/FollowersList"
#define AddFollowAPI @"http://mobileapi.talentedge.hub1.co/api/Activity/AddFollow"
#define UnAnsweredQuestionsAPI @"http://mobileapi.talentedge.hub1.co/Api/profile/Unansweredquestions"
#import "ProfileQuestionTabCell.h"
#import "AppDelegate.h"

@class AppDelegate;

extern NSString *answerVideo;
extern NSMutableArray *ArrayOfAdvisors;

@interface ProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate,UIAlertViewDelegate,MPMediaPickerControllerDelegate>
{
    int replyIndexQues;
    NSArray *selectedNameArray;
    NSArray *selectedUpvotesArray;
    NSArray *selectedTimeArray;
    NSArray *selectedAddressArray;
    NSArray *videoImages;
    NSArray *videoNamesArray;
    NSArray *headingArray;
    NSMutableArray *colletionArray;
    NSMutableArray *followerCollectionArray;
    NSMutableArray *UpvotesCollectionArray;
    NSMutableArray *questionsProfileArray;
    NSMutableArray *answersProfileArray;
    UIImage *thumbnail;
    NSArray *mediaTypes;
    AppDelegate *appDelegate;
    BOOL isPlayAgn;
    MPMoviePlayerController *videoPlayerView;
    NSString *FollowImg;
    NSString *User_FollowImg;
    MBProgressHUD *HUD;
    NSString *answrVideoLinkProfile;
    NSString *total_Count;
//    NSArray *No_quesAnwLblArray;
    NSMutableArray *No_quesAnwLblArray;
    NSMutableArray *No_FollowerArray;
    NSMutableArray *User_Obj;
    NSString *User_IDForSetting;
    NSString *LinkedIn_setting;
    NSString *answrVideoLinkUser_Obj;
    NSMutableArray *User_FollowerObj;
    NSString *str_UserFollwer;
    NSMutableArray *UnFollowerObj;
    BOOL isUnfollow;
    NSMutableArray *dummyarrayForquestions;
    NSString *Question_Video_TitleProfile;
    NSMutableArray *UnAnsweredQuestions_Obj;

    
    
    ////////////////
    
    NSMutableArray *user_questionArray;
    NSMutableArray *UpvoteObj;
    NSMutableArray *FollowerObj;
    NSMutableArray *ProAdviceObj;
    id fetchIndexProfile;
    BOOL isUser_ObjPlayAgn;
    NSMutableArray *questionListObj;
    BOOL isfollowerTrue;
    NSString *video_TitleUnderProfileMyQues;
    NSString *video_LinkUnderProfileMyQues;
    BOOL ischanged;
    BOOL isPlayAgnThreeTabProadvice;
    BOOL isDoneUserProfile;
    BOOL isDoneAdvisor;
    NSMutableArray *reverseUserProadviceArray;

}
@property (weak, nonatomic) IBOutlet UILabel *profileTabOutlet;
@property (weak, nonatomic) IBOutlet UILabel *questionsTabOutlet;
@property (weak, nonatomic) IBOutlet UILabel *answerTabOutlet;
@property (weak, nonatomic) IBOutlet UIView *wrapperView;
@property (weak, nonatomic) IBOutlet UITableView *upvotesTableView;

@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollTabView;
@property (weak, nonatomic) IBOutlet UILabel *ollowerTabOutlet;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *upvotesTabOutlet;
@property (weak, nonatomic) IBOutlet UIButton *profileButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *questionButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *answerButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *followerButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *upvotesButtonOutlet;
@property (weak, nonatomic) IBOutlet UIView *profileHighlightedView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITableView *questionTabelView;
@property (weak, nonatomic) IBOutlet UITableView *AnswerTableView;

@property (weak, nonatomic) IBOutlet UITableView *followerTableView;
@property (weak, nonatomic) IBOutlet UILabel *questionCountsLbl;
@property (weak, nonatomic) IBOutlet UILabel *answerCountsLbl;
@property (weak, nonatomic) IBOutlet UILabel *followerCountsLbl;
@property (weak, nonatomic) IBOutlet UILabel *upvotesCountLbl;


@property (weak, nonatomic) IBOutlet UITextView *profileInfoTextView;
@property (weak, nonatomic) IBOutlet UITextView *profileTagView;
@property (weak, nonatomic) IBOutlet UITextView *profileTextView;

@property (weak, nonatomic) IBOutlet UIView *questionHighlightedView;
@property (weak, nonatomic) IBOutlet UIView *answerHighlightedView;
@property (weak, nonatomic) IBOutlet UIView *followerHighlightedView;
@property(strong,nonatomic) NSMutableDictionary *NO_data;
@property (weak, nonatomic) IBOutlet UIView *upvotesHighlightedView;
@property(strong,nonatomic) NSString *advisorFirstName;
@property(strong,nonatomic) NSString *advisorLastName;


- (IBAction)settingsButton:(id)sender;
- (IBAction)profileTabButton:(id)sender;
- (IBAction)questionButton:(id)sender;
- (IBAction)followerButton:(id)sender;
- (IBAction)followButtonAction:(id)sender;
- (IBAction)answerBtn:(id)sender;
- (IBAction)EditButtonClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollView;
@property(strong,nonatomic) NSString *passImage;
@property(assign)int indexValue;
//////////////
@property (weak, nonatomic) IBOutlet UIImageView *blurImgProfile;

@property (weak, nonatomic) IBOutlet UIImageView *roundProfileImg;
@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(strong,nonatomic) NSString *info;
@property(strong,nonatomic) NSString *GetAdvisorID;
@property (weak, nonatomic) IBOutlet UILabel *user_Location;

@property (weak, nonatomic) IBOutlet UIView *outletOfBlurI_mageProfile;


-(void)FollowListing;
-(void)ProAdviceListing;
-(void)profileData;
-(void)UserProAdviceListing;
-(void)User_FollowListing;
-(void)AddFollow;
-(void)Unfollow;
-(void)questionListingMethod;
@property (weak, nonatomic) IBOutlet UIButton *outletOfFollowBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblMyQuestions;
- (IBAction)BtnMyQuestions:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMyQuesOutlet;

@property (weak, nonatomic) IBOutlet UILabel *quesCountLbl;
@property (weak, nonatomic) IBOutlet UIView *my_questionHighlightView;

@property (weak, nonatomic) IBOutlet UITableView *profileQuestionTableView;

//////////////////////////////////
@property (weak, nonatomic) IBOutlet UIView *threeTabView;
@property (weak, nonatomic) IBOutlet UILabel *profileLblThreeTab;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageThreeTab;
- (IBAction)profileBtnThree:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *profileBtnThreeOutlet;
@property (weak, nonatomic) IBOutlet UILabel *proadviceThreeLbl;
- (IBAction)proadviceBtnThree:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *proadviceBtnOutletThree;
@property (weak, nonatomic) IBOutlet UILabel *followLblThree;
- (IBAction)followBtnThree:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *folowBtnOutletThree;
@property (weak, nonatomic) IBOutlet UILabel *proadviceCoutThree;
@property (weak, nonatomic) IBOutlet UILabel *followCountThree;
@property (weak, nonatomic) IBOutlet UIView *profileHighlightViewThree;
@property (weak, nonatomic) IBOutlet UIView *proadviceHighlightThree;
@property (weak, nonatomic) IBOutlet UIView *followHighlightThree;
@property(nonatomic) int tranferIndex;

@property(assign) BOOL showAdvisor;


@property(nonatomic,strong) NSString *follower;
- (IBAction)shareUnderProfileMyQues:(id)sender;

@end
