

//
//  ProfileViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/27/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "ProfileViewController.h"
#import "AppDelegate.h"
#import "EditProfileViewController.h"

NSString *answerVideo;
BOOL isPlayBackCalled = NO;
NSMutableArray *ArrayOfAdvisors;

static int flag_for_alertQuestion = 0;

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize passImage,indexValue;
@synthesize GetAdvisorID;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self headerContent];
    
    self.navigationController.navigationBarHidden = true;
    
    self.outletOfFollowBtn.contentMode = UIViewContentModeScaleAspectFit;
    
    NSLog(@"_advisorName is %@",_advisorLastName);
    
    ischanged = false;
    
    NSLog(@"follower is %@",_follower);
    
   isfollowerTrue = [_follower boolValue];
    
    if (isfollowerTrue == true)
    {
        [self.outletOfFollowBtn setImage:[UIImage imageNamed:@"unfollow.png"] forState:UIControlStateNormal];
    }
    else
    {
        [self.outletOfFollowBtn setImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
    }
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.loaderView.hidden = false;
    
    [self.activityIndicator startAnimating];
    
    NSLog(@"self.info is %d and %@",indexValue,self.info);
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadViewData) userInfo:NULL repeats:NO];
    
    if (isA3Advisor == false)
    {
        self.wrapperView.hidden = false;
        self.threeTabView.hidden = true;
    }
    else
    {
        self.wrapperView.hidden = true;
        self.threeTabView.hidden = false;
       

    }
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if (_showAdvisor == true)
    {
        isA3Advisor = false;
        
    }
    
    if (isA3Advisor == false)
    {
        self.wrapperView.hidden = false;
        self.threeTabView.hidden = true;
        self.outletOfFollowBtn.hidden = true;
    }
    else
    {
        self.wrapperView.hidden = true;
        self.threeTabView.hidden = false;
        self.outletOfFollowBtn.hidden = false;
        
        [videoPlayerView.view removeFromSuperview];
        [videoPlayerView stop];
    }
}

-(void)profileData
{
    if (isA3Advisor == false)
    {
        self.questionCountsLbl.text = @"0";
        self.followerCountsLbl.text = @"0";
    }
    else
    {
        No_quesAnwLblArray = [[NSMutableArray alloc]init];
        //    No_FollowerArray = [[NSMutableArray alloc]init];
        
        No_quesAnwLblArray = [_NO_data valueForKey:@"No_Questions"];
        No_FollowerArray = [_NO_data valueForKey:@"No_Followers"];
        
        self.questionCountsLbl.text = [NSString stringWithFormat:@"%@",No_quesAnwLblArray];
        self.followerCountsLbl.text = [NSString stringWithFormat:@"%@",No_FollowerArray];
        
        NSLog(@"data is %@ and %@",No_quesAnwLblArray,No_FollowerArray);

    }
}

-(void)FollowListing
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    
    FollowerObj = [[NSMutableArray alloc]init];
    
    NSLog(@"follower listing");
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
//    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
//    if(GetAdvisorID == (id)[NSNull null])
//    {
//        NSLog(@"");
//    }
//    else
//    {
//        
//    }
    NSString *post = [NSString stringWithFormat:@"AdvisorID=%@",GetAdvisorID];
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:Follow_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     FollowerObj = [data1 valueForKey:@"FollowList"];
                     
                     if (FollowerObj.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                     NSLog(@"FollowerObj is %@",FollowerObj);
                     
                     self.followerCountsLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)FollowerObj.count];
                     self.followCountThree.text = [NSString stringWithFormat:@"%lu",(unsigned long)FollowerObj.count];
                     
//                     NSString *firstName = [[FollowerObj objectAtIndex:0] valueForKey:@"FullName"];
//                     
//                     NSLog(@"FullName is %@",firstName);
                     
                    // UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                       //                   [alert show];
                     
                     [_followerTableView reloadData];
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)User_FollowListing
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    User_IDForSetting = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    
    User_FollowerObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
    NSString *post;
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"user_id=%@",LinkedIn_ID];
    }
    else
    {
        post = [NSString stringWithFormat:@"user_id=%@",User_IDForSetting];
    }
    
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:User_FollwerAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     str_UserFollwer = [items objectAtIndex:1];
                 }

                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     User_FollowerObj = [data1 valueForKey:@"advisors"];
                     
                     self.followerCountsLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)User_FollowerObj.count];
                     
                     NSLog(@"User_FollowerObj is %@",User_FollowerObj);
                     
                     if (User_FollowerObj.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                     //                     NSString *firstName = [[FollowerObj objectAtIndex:0] valueForKey:@"FullName"];
                     //
                     //                     NSLog(@"FullName is %@",firstName);
                     
                     // UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     //                   [alert show];
                     
                     [_followerTableView reloadData];
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)AddFollow
{
//    isUnfollow = true;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];

    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    
    FollowerObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
    NSString *post;
    if(!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"Follower_User_ID=%@&Advisor_User_ID=%@&Status=%@",LinkedIn_ID,GetAdvisorID,@"1"];
    }
    else
    {
        post = [NSString stringWithFormat:@"Follower_User_ID=%@&Advisor_User_ID=%@&Status=%@",Creater_User_ID,GetAdvisorID,@"1"];
    }
  
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:AddFollowAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@" AddFollow loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     NSLog(@"FollowerObj for add follow is %@",FollowerObj);
                     
                     [self.outletOfFollowBtn setImage:[UIImage imageNamed:@"unfollow.png"] forState:UIControlStateNormal];
////                     isUnfollow = true;
                     
//                     ArrayOfAdvisors = [[NSMutableArray alloc]init];
//                     
//                     [ArrayOfAdvisors addObject:GetAdvisorID];
//                     
//                     NSLog(@"ArrayOfAdvisors is %@",ArrayOfAdvisors);
//                     
//                     NSUserDefaults *pre = [NSUserDefaults standardUserDefaults];
//                     // saving an NSString
//                     [pre setObject:ArrayOfAdvisors forKey:@"ArrayOfAdvisors"];

                     HUD.hidden = true;
                 }
                 else
                 {
//                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                     [alert show];
                     
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)Unfollow
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];

    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    UnFollowerObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSString *post = [NSString stringWithFormat:@"AdvisorID=%d",4];
    NSString *post;
    if(!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        post = [NSString stringWithFormat:@"Follower_User_ID=%@&Advisor_User_ID=%@&Status=%@",LinkedIn_ID,GetAdvisorID,@"0"];
    }
    else
    {
        post = [NSString stringWithFormat:@"Follower_User_ID=%@&Advisor_User_ID=%@&Status=%@",Creater_User_ID,GetAdvisorID,@"0"];
    }
   
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:AddFollowAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@" AddFollow loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     NSLog(@"UnFollowerObj is %@",UnFollowerObj);
                     
                     [self.outletOfFollowBtn setImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];

//                     
//                     for (int l; l<ArrayOfAdvisors.count; l++)
//                     {
//                         if ([GetAdvisorID isEqual:[ArrayOfAdvisors objectAtIndex:l]])
//                         {
//                             NSLog(@"l is %d",l);
//                             [ArrayOfAdvisors removeObjectAtIndex:l];
//                         }
//                         else
//                         {
//                             NSLog(@"Not removed");
//                         }
//                     }
                     
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

- (void)doSomeFunkyStuff
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}

-(void)questionListingMethod
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    User_IDForSetting = [prefs stringForKey:@"saveUserID"];
    NSLog(@"User_IDForSetting is %@",User_IDForSetting);
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    
    UnAnsweredQuestions_Obj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSLog(@"GetAdvisorID is %@",advisorID);
    NSString *post;
    
    if (!([LinkedIn_ID  isEqualToString:@""] || LinkedIn_ID == nil))
    {
         post = [NSString stringWithFormat:@"user_id=%@",LinkedIn_ID];
    }
    else
    {
        post = [NSString stringWithFormat:@"user_id=%@",User_IDForSetting];
    }

    NSLog(@"post user_id is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:UnAnsweredQuestionsAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     UnAnsweredQuestions_Obj = [data1 valueForKey:@"videos"];
                     
                     if (UnAnsweredQuestions_Obj.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                     self.quesCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)UnAnsweredQuestions_Obj.count];
                     
                     NSLog(@"UnAnsweredQuestions_Obj is %@",UnAnsweredQuestions_Obj);
                     
                     [self.profileQuestionTableView reloadData];
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}



-(void)ProAdviceListing
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];

    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    
    ProAdviceObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
//    NSLog(@"GetAdvisorID is %@",advisorID);
    
    NSString *post = [NSString stringWithFormat:@"AdvisorID=%@",GetAdvisorID];
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:ProAdvice_Listing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data for proadvice is %@",data1);
                 
                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     ProAdviceObj = [data1 valueForKey:@"questions"];
                     
                     self.questionCountsLbl.text = [NSString stringWithFormat:@"%d",ProAdviceObj.count];
                     self.proadviceCoutThree.text = [NSString stringWithFormat:@"%d",ProAdviceObj.count];
                     
                     
                     NSLog(@"ProAdviceObj is %@",ProAdviceObj);
                     if (ProAdviceObj.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                     [_questionTabelView reloadData];
                      HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)UserProAdviceListing
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    User_IDForSetting = [prefs stringForKey:@"saveUserID"];
    NSLog(@"User_IDForSetting is %@",User_IDForSetting);
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    
    User_Obj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    //    NSLog(@"GetAdvisorID is %@",advisorID);
    
    NSString *post;
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
         post = [NSString stringWithFormat:@"user_id=%@",LinkedIn_ID];
    }
    else
    {
        post = [NSString stringWithFormat:@"user_id=%@",User_IDForSetting];
    }
    
    NSLog(@"post user_id is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:User_ProAdviceListing]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data1 is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@"loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     User_Obj = [data1 valueForKey:@"videos"];
                     
                     if (User_Obj.count == 0)
                     {
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                         [alert show];
                         HUD.hidden = true;
                     }
                     
                      self.questionCountsLbl.text = [NSString stringWithFormat:@"%d",User_Obj.count];
                     
                     NSLog(@"User_Obj is %@",User_Obj);
                     
                     [_questionTabelView reloadData];
                     HUD.hidden = true;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     HUD.hidden = true;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)loadViewData
{
    NSLog(@"Creater_User_ID is %@",Creater_User_ID);
    
    NSLog(@"passImage is %@",passImage);
    
    self.roundProfileImg.layer.backgroundColor = [[UIColor clearColor] CGColor];
    self.roundProfileImg.layer.cornerRadius = self.roundProfileImg.frame.size.height/2;
    self.roundProfileImg.layer.borderWidth = 2.0;
    self.roundProfileImg.layer.masksToBounds = YES;
    self.roundProfileImg.layer.borderColor = [[UIColor clearColor] CGColor];
    self.questionCountsLbl.text = @"7";
    self.followerCountsLbl.text = @"7";
    self.proadviceCoutThree.text = @"7";
    self.followCountThree.text = @"7";
    
    if (passImage.length != 0)
    {
        self.profileName.hidden = NO;
        self.profileInfoTextView.hidden = YES;
        self.profileTextView.hidden = YES;
        self.profileTagView.hidden = YES;
        
        NSString* combinedString = [self.advisorFirstName stringByAppendingString:[NSString stringWithFormat:@" %@",_advisorLastName]];
        self.profileName.text = combinedString;
        
        NSMutableArray *passArray = [[NSMutableArray alloc]init];
        [passArray addObjectsFromArray:[appDelegate GlobalCollectionProfile]];
        
//        NSLog(@"passArray is %lu",(unsigned long)passArray.count);
        
        UILabel *headerLbl1;
        UILabel *headerLbl;
        
        //        for (int k; k<passArray.count; k++)
        {
            NSLog(@"%@",[passImage lowercaseString]);
//            NSLog(@"%@",[[[passArray objectAtIndex:indexValue] valueForKey:@"Name"]lowercaseString]);
            
            //            if ([[passImage lowercaseString] isEqualToString:[[[passArray objectAtIndex:indexValue] valueForKey:@"Name"]lowercaseString]])
            {
                NSURL *url = [NSURL URLWithString:passImage];
                
                NSLog(@"url is %@",url);
                
               // [self.roundProfileImg sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"NoUsername.png"]];
                
                NSURL *urlBlurr = [NSURL URLWithString:passImage];
                UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlBlurr]];
//
                 self.blurImgProfile.image = [self blurredImageWithImage:img];
                
                headerLbl1 = [[UILabel alloc]init];
                [headerLbl1 setTextColor:[UIColor darkGrayColor]];
                // [answerLbl setBackgroundColor:[UIColor clearColor]];
                [headerLbl1 setFont:[UIFont boldSystemFontOfSize:15.0f]];
                CGFloat width = self.view.frame.size.width-20;
                headerLbl1.numberOfLines = 100;
//                [headerLbl1 setText:[[passArray objectAtIndex:indexValue] valueForKey:@"about"]];
                [headerLbl1 setText:self.info];
                headerLbl1.frame = CGRectMake(10, 20, self.view.frame.size.width-20, [self heightForText:self.info withWidth:width font:headerLbl1.font]);
                
                [self.profileScrollView addSubview:headerLbl1];
                
            }
        }
        [self.profileScrollView setContentSize:CGSizeMake(self.view.frame.size.width, headerLbl1.frame.size.height+headerLbl.frame.size.height+60)];
        self.roundProfileImg.image = [UIImage imageNamed:passImage];
        self.blurImgProfile.image = [self blurredImageWithImage:[UIImage imageNamed:passImage]];
        self.profileInfoTextView.text = _info;
    }
    else
    {
        self.profileScrollView.hidden = YES;
        self.blurImgProfile.hidden = NO;
        self.roundProfileImg.hidden = NO;
        self.profileName.hidden = NO;
        self.profileInfoTextView.hidden = NO;
        self.profileTextView.hidden = YES;
        self.profileTagView.hidden = YES;

        self.roundProfileImg.image = [UIImage imageNamed:@"settingProfilePic.jpeg"];
        self.blurImgProfile.image = [self blurredImageWithImage:[UIImage imageNamed:@"settingProfilePic.jpeg"]];

        if(User_About == (id)[NSNull null])
        {
            User_About = @"Please let us know something about you";
        }
        self.profileName.text = fullName;
        self.user_Location.text = userLocation;
        
        
        self.profileInfoTextView.text = @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
        
    }
    if(isA3Advisor == false)
    {
        if (_showAdvisor == true)
        {
            if ([notificationMessage containsString:@"Your Question has been approved by the Moderator"] || [notificationMessage containsString:@"Your Question has been Rejected by the moderator. Please check your profile for further information"])
            {
                [self BtnMyQuestions:@""];
                _showAdvisor = false;
            }
            else
            {
                if ([notificationMessage containsString:@"Advisor has responded to your Question"])
                {
                    [self questionButton:@""];
                    _showAdvisor = false;
                }
                else
                {
                    NSLog(@"Worng Botification called!!");
                }
            }
        }
        else
        {
              [self profileTabButton:@""];
        }
    }
    else
    {
        [self profileBtnThree:@""];
    }
   
//    self.upvotesTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.followerTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.questionTabelView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.AnswerTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.profileQuestionTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];

    
    _outletOfFollowBtn.layer.cornerRadius = 5;
    _outletOfFollowBtn.layer.masksToBounds = YES;
    
    if (self.view.frame.size.height == 480 && self.view.frame.size.width == 320)
    {
        self.profileImage.frame = CGRectMake(114, 95,80, 80);
    }
    
    [_profileScrollTabView setShowsVerticalScrollIndicator:NO];
    
    [_profileScrollTabView setShowsHorizontalScrollIndicator:NO];
    
    [self.profileScrollTabView setContentSize:CGSizeMake(_wrapperView.frame.size.width + _ollowerTabOutlet.frame.size.width, self.profileScrollTabView.frame.size.height)];
    
    // Do any additional setup after loading the view.
    
    self.loaderView.hidden = true;
    
    [self.activityIndicator stopAnimating];
    
    
}

+ (CGFloat)heightForText:(NSString*)text font:(UIFont*)font withinWidth:(CGFloat)width
{
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:font}];
    CGFloat area = size.height * size.width;
    CGFloat height = roundf(area / width);
    return ceilf(height / font.lineHeight) * font.lineHeight;
}

-(CGFloat) heightForText:(NSString *)text withWidth:(CGFloat) textWidth font:(UIFont*)font
{
    CGSize constraint = CGSizeMake(textWidth, 20000.0f);
    CGRect rect = [text boundingRectWithSize:constraint
                                     options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                  attributes:@{NSFontAttributeName:font}
                                     context:nil];
    CGFloat height = rect.size.height;
    
    height = ceilf(height);
    //    NSLog(@"height %f", height);
    return height;
}

-(UIImage*)blurredImageWithImage:(UIImage *)sourceImage
{
    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:15.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    //  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
    // up exactly to the bounds of our original image /
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *retVal = [UIImage imageWithCGImage:cgImage];
    return retVal;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    [_AnswerTableView reloadData];
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _followerTableView)
    {
        return 1;
    }
    else if (tableView == _profileQuestionTableView)
    {
        return 1;
    }
    else
    {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _followerTableView)
    {
        return 60.0;
    }
    else if (tableView == _profileQuestionTableView)
    {
        return 86.0;
    }
    else
    {
        return 90.0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _questionTabelView)
    {
        if (isA3Advisor == false)
        {
            return [colletionArray count];
        }
        else
        {
            return [colletionArray count];
        }
 
    }
    else if(tableView == _followerTableView)
    {
        if (isA3Advisor == false)
        {
            return [colletionArray count];
        }
        else
        {
            return [colletionArray count];
        }
    }
    else
    {
        return [colletionArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _questionTabelView)
    {
        if (isA3Advisor == false)
        {
            static NSString *cellIdentifier = @"QuestionsCustomCell";
            
            QuestionsCustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            
            Cell.questionHeadinglabel.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"names"];
          
            Cell.questionTimeLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"time"];
           
            
            ///////////////
            NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
            
            NSLog(@"videoPath is %@",videoPath);
            NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
            UIImage *profileImageStatic = [self generateThumbImage:streamURL];
            Cell.questionImageView.image = profileImageStatic;
            ///////////////
            
            [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            return Cell;
        }
        else
        {
            static NSString *cellIdentifier = @"QuestionsCustomCell";
        
            QuestionsCustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        Cell.questionHeadinglabel.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];
        Cell.questionNameLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"names"];
        Cell.questionTimeLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"time"];
        
        ///////////////
        NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
            
        NSLog(@"videoPath is %@",videoPath);
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        UIImage *profileImageStatic = [self generateThumbImage:streamURL];
        Cell.questionImageView.image = profileImageStatic;
        ///////////////

            
        [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        return Cell;
        
            }
        }
        else if (tableView == _followerTableView)
        {
            if (isA3Advisor == false)
            {
                static NSString *cellIdentifier = @"FollowersCustomCell";
                FollowersCustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

                NSString *FollowerStr = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"ProfileImage"];
                Cell.followerImageView.image = [UIImage imageNamed:FollowerStr];
            
                Cell.FollowerAddressLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"Location"];
            
                Cell.followerNameLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"names"];
                Cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
                return Cell;
            }
            else
            {
                static NSString *cellIdentifier = @"FollowersCustomCell";
                
                FollowersCustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

                NSString *followerImage = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"ProfileImage"];
                Cell.followerImageView.image = [UIImage imageNamed:followerImage];

                Cell.FollowerAddressLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"Location"];
                
                Cell.followerNameLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"names"];
                
                Cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
                return Cell;
            }
        }
        else
        {
            static NSString *cellIdentifier = @"ProfileQuestionTabCell";
            ProfileQuestionTabCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

            Cell.profileQuestionTitleLbl.text = [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];

            Cell.profileQuestionTimeLbl.text =  [[colletionArray objectAtIndex:indexPath.row] valueForKey:@"time"];

            ///////////////
            NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
            
            NSLog(@"videoPath is %@",videoPath);
            NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
            UIImage *profileImageStatic = [self generateThumbImage:streamURL];
            Cell.profileQuestionImageView.image = profileImageStatic;
            ///////////////

            
            Cell.layoutMargins = UIEdgeInsetsZero;
            Cell.preservesSuperviewLayoutMargins = false;
            // Cell.separatorInset = UIEdgeInsetsZero;
            tableView.separatorInset = UIEdgeInsetsZero;
        
            [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
 
            return Cell;
        }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _questionTabelView)
    {
        if (isA3Advisor == true)
        {
            isDoneAdvisor = false;
            NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            if(dummyArray.count != 0)
            {
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerView.view];
                videoPlayerView.fullscreen = YES;
                videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerView prepareToPlay];
                [videoPlayerView play];
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteProfile:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickProfile:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
            }
        }
        else
        {
            isDoneUserProfile = false;
            NSArray *dummyArray = [[[colletionArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            if(dummyArray.count != 0)
            {
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerView.view];
                videoPlayerView.fullscreen = YES;
                videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerView prepareToPlay];
                [videoPlayerView play];
            }
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteUser_Obj:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickProfileUser_Obj:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    
            }
        }
        else if (tableView == self.profileQuestionTableView)
        {
            NSURL *streamURL = [NSURL URLWithString:[UnAnsweredQuestions_Obj[indexPath.row] valueForKey:@"question_video_link"]];
            NSLog(@"else streamURL%@",streamURL);

            NSString *UnAnswered = [[UnAnsweredQuestions_Obj objectAtIndex:indexPath.row] valueForKey:@"answer_video_link"];
        
            NSLog(@"UnAnswered is %@",UnAnswered);
        
            videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
        
        //        moviePlayer.controlStyle = MPMovieControlStyleNone;
            [self.view addSubview:videoPlayerView.view];
            videoPlayerView.fullscreen = YES;
            videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
            videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            //        moviePlayer.view.layer.zPosition = 1;
        
            [videoPlayerView prepareToPlay];
            [videoPlayerView play];
        
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteUnAnswered_Obj:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickProfileUnAnswered_Obj:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    }
}

- (void)aMoviePlaybackCompleteUnAnswered_Obj:(NSNotification*)notification
{
        [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             videoPlayerView.view.alpha = 0;
                         }completion:^(BOOL finished)
         {
             [videoPlayerView.view removeFromSuperview];
             [videoPlayerView stop];
         }];
}

-(void)doneButtonClickProfileUnAnswered_Obj:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        
        [videoPlayerView.view removeFromSuperview];
        [videoPlayerView stop];
    }
    else
    {
        [videoPlayerView.view removeFromSuperview];
        [videoPlayerView stop];
        
    }
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
}
//-(void)PlayVideo:(NSString*)videoName
//{
//    NSArray *dummyArray = [videoName componentsSeparatedByString:@"."];
//
//    if(dummyArray.count != 0)
//    {
//        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
//        
//        //        NSLog(@"videoPath is %@",videoPath);
//        
//        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteProfile:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
//
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickProfile:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
//        
//        videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
//        
//        //        moviePlayer.controlStyle = MPMovieControlStyleNone;
//        videoPlayerView.view.frame = self.view.frame;
//         [self.view addSubview:videoPlayerView.view];
//         [videoPlayerView setFullscreen:YES animated:YES];
//        
//        
////        videoPlayerView.fullscreen = YES;
////        videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
////        videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
////        //        moviePlayer.view.layer.zPosition = 1;
////        
////        
////        [videoPlayerView prepareToPlay];
////        [videoPlayerView play];
////        
////        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
////        [[NSNotificationCenter defaultCenter] addObserver:self
////                                                 selector:@selector(doneButtonClick:)
////                                                     name:MPMoviePlayerWillExitFullscreenNotification
////                                                   object:nil];
//    }
//  }

-(void)doneButtonClickProfile:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        
        [videoPlayerView.view removeFromSuperview];
         [videoPlayerView stop];
    }
    else
    {
        [videoPlayerView.view removeFromSuperview];
         [videoPlayerView stop];
        isDoneAdvisor = false;
        videoPlayerView = nil;
        
        isPlayAgnThreeTabProadvice = false;
        
    }
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:MPMoviePlayerDidExitFullscreenNotification
//                                                  object:nil];
}

-(void)doneButtonClickProfileUser_Obj:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        
        [videoPlayerView.view removeFromSuperview];
         [videoPlayerView stop];
    }
    else
    {
        [videoPlayerView.view removeFromSuperview];
         [videoPlayerView stop];
        isUser_ObjPlayAgn = false;
        isDoneUserProfile = false;
        videoPlayerView = nil;
        
    }
}

- (void)aMoviePlaybackCompleteProfile:(NSNotification*)notification
{
    if (isPlayAgnThreeTabProadvice == false)
    {
        isDoneAdvisor = true;
        
        if (videoPlayerView.view)
        {
            [videoPlayerView.view removeFromSuperview];
            [videoPlayerView stop];
        }

        if (!isDoneAdvisor)
            {
                [videoPlayerView.view removeFromSuperview];
                [videoPlayerView stop];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice" ofType:@"mp4"];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerView.view];
                videoPlayerView.fullscreen = YES;
                videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerView prepareToPlay];
                [videoPlayerView play];

            
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteProfile:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
        
        isPlayAgnThreeTabProadvice = true;
        }
    }
    else
    {
        //isPlayBackCalled = NO;
        
        //        [videoPlayerView.view removeFromSuperview];
        [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             videoPlayerView.view.alpha = 0;
                         }completion:^(BOOL finished)
         {
             [videoPlayerView.view removeFromSuperview];
              [videoPlayerView stop];
         }];
        
        isPlayAgnThreeTabProadvice = false;
    }
}

- (void)aMoviePlaybackCompleteUser_Obj:(NSNotification*)notification
{
    //    NSString *resourceName = @"SunSpot_1080p_main.m4v";
    if (isUser_ObjPlayAgn == false)
    {
        isDoneUserProfile = true;
        
        [videoPlayerView stop];
        [videoPlayerView.view removeFromSuperview];

            if (!isDoneUserProfile)
            {
                [videoPlayerView.view removeFromSuperview];
                [videoPlayerView stop];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice" ofType:@"mp4"];
                
                NSLog(@"videoPath is %@",videoPath);
                
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                
                NSLog(@"streamURL is %@",streamURL);
                
                videoPlayerView = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
                //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
                [self.view addSubview:videoPlayerView.view];
                videoPlayerView.fullscreen = YES;
                videoPlayerView.controlStyle = MPMovieControlStyleEmbedded;
                videoPlayerView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                //        moviePlayer.view.layer.zPosition = 1;
                [videoPlayerView prepareToPlay];
                [videoPlayerView play];
            
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteUser_Obj:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
            
                isUser_ObjPlayAgn = true;
            }
    }
    else
    {
        //isPlayBackCalled = NO;
        isUser_ObjPlayAgn = false;
        //        [videoPlayerView.view removeFromSuperview];
        [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             videoPlayerView.view.alpha = 0;
                         }completion:^(BOOL finished)
         {
             [videoPlayerView.view removeFromSuperview];
              [videoPlayerView stop];
         }];
    }
}

#pragma mark - Information


-(void)headerContent
{
    colletionArray = [[NSMutableArray alloc]init];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor1.jpeg",@"ProfileImage",
                               @"Goa",@"Location",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"Vikram.png",@"ProfileImage",
                               @"Mumbai",@"Location",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor2.jpeg",@"ProfileImage",
                               @"Ludhiana",@"Location",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4",@"video",
                               @"25 m",@"time",
                               @"advisor4.jpeg",@"ProfileImage",
                               @"Pune",@"Location",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor3.jpeg",@"ProfileImage",
                               @"Patiala",@"Location",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Skills required for manufacturing Industry",@"HeaderTitle",
                               @"MaheshGupta.mp4.mp4",@"video",
                               @"25 m",@"time",
                               @"advisor4.jpeg",@"ProfileImage",
                               @"Goa",@"Location",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Bob Cary",@"names",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Entrepreneur as a Career Option",@"HeaderTitle",
                               @"RohitBhayana.mp4",@"video",
                               @"35 m",@"time",
                               @"advisor5.jpeg",@"ProfileImage",
                               @"Uttrakhand",@"Location",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Martin",@"names",nil]];
}


#pragma mark - UIButtons

- (IBAction)settingsButton:(id)sender
{
    if (ischanged == true)
    {
        NSMutableDictionary *Dic = [NSMutableDictionary new];
        NSLog(@"tranferIndex is %d",_tranferIndex);
        Dic = [[dataObj objectAtIndex:self.tranferIndex] mutableCopy];
        
        NSString *newStr = [NSString stringWithFormat:@"%@",[Dic valueForKey:@"IsFollowed"]];
        
        if ([newStr isEqualToString:@"0"])
        {
            [Dic removeObjectForKey:@"IsFollowed"];
            [Dic setObject:@"1" forKey:@"IsFollowed"];
        }
        else
        {
            [Dic removeObjectForKey:@"IsFollowed"];
            [Dic setObject:@"0" forKey:@"IsFollowed"];
        }

        [dataObj removeObjectAtIndex:self.tranferIndex];
        [dataObj insertObject:Dic atIndex:self.tranferIndex];
        
        NSLog(@"Dic in ununpvote is %@",Dic);
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
     
    }
    
}

- (IBAction)A3Button:(id)sender
{
    A3ViewController *a3 = [self.storyboard instantiateViewControllerWithIdentifier:@"A3ViewController"];
    a3.isFromProfile = YES;
    [self.navigationController pushViewController:a3 animated:YES];
}

- (IBAction)profileTabButton:(id)sender
{
    _profileImageView.image = [UIImage imageNamed:@"profileUser1.png"];
    _profileHighlightedView.hidden = NO;
    _questionHighlightedView.hidden = YES;
    _answerHighlightedView.hidden = YES;
    _followerHighlightedView.hidden = YES;
//    _upvotesHighlightedView.hidden = YES;
    
    _questionTabelView.hidden = YES;
    _AnswerTableView.hidden = YES;
    _followerTableView.hidden = YES;
//    _upvotesTableView.hidden = YES;
    
    _profileInfoTextView.hidden = NO;
    _profileTagView.hidden = NO;
    _profileTextView.hidden = NO;
    
    if (isA3Advisor == false)
    {
        _outletOfFollowBtn.hidden = true;
    }
    else
    {
        _outletOfFollowBtn.hidden = false;
    }
    
    self.profileQuestionTableView.hidden = true;
    self.my_questionHighlightView.hidden = true;
}

- (IBAction)questionButton:(id)sender
{
    if (isA3Advisor == false)
    {
//            [self UserProAdviceListing];
        
            _profileImageView.image = [UIImage imageNamed:@"profileUser.png"];
            _profileHighlightedView.hidden = YES;
            _questionHighlightedView.hidden = NO;
            _answerHighlightedView.hidden = YES;
            _followerHighlightedView.hidden = YES;
            //    _upvotesHighlightedView.hidden = YES;
            _questionTabelView.hidden = NO;
            _AnswerTableView.hidden = YES;
            _followerTableView.hidden = YES;
            _profileInfoTextView.hidden = YES;
            _profileTagView.hidden = YES;
            _profileTextView.hidden = YES;
            [_profileScrollTabView setContentOffset:CGPointMake(_profileScrollTabView.frame.origin.x, 0) animated:YES];
        }
    self.profileQuestionTableView.hidden = true;
    self.my_questionHighlightView.hidden = true;
}

- (IBAction)answerButton:(id)sender
{
    [_AnswerTableView reloadData];
    
    _profileImageView.image = [UIImage imageNamed:@"profileUser.png"];
    _profileHighlightedView.hidden = YES;
    _questionHighlightedView.hidden = YES;
    _answerHighlightedView.hidden = NO;
    _followerHighlightedView.hidden = YES;
    _upvotesHighlightedView.hidden = YES;
    _questionTabelView.hidden = YES;
    _AnswerTableView.hidden = NO;
    _followerTableView.hidden = YES;
    _profileInfoTextView.hidden = YES;
    _profileTagView.hidden = YES;
    _profileTextView.hidden = YES;
    _upvotesTableView.hidden = YES;
    self.profileQuestionTableView.hidden = true;
    self.questionHighlightedView.hidden = true;
    
    [_AnswerTableView reloadData];
    
    
    
    [_profileScrollTabView setContentOffset:CGPointMake(_profileScrollTabView.frame.origin.x, 0) animated:NO];
    
    // [_profileScrollTabView setContentOffset:CGPointMake(_profileTabOutlet.frame.origin.x + _profileTabOutlet.frame.size.width, 0) animated:YES];
    
    //  [_profileScrollTabView setContentOffset:CGPointMake(_profileScrollTabView.frame.origin.x, 0) animated:YES];
    
    
}

- (IBAction)followerButton:(id)sender
{
    if (isA3Advisor == false)
    {
//        [self User_FollowListing];
        
        [_profileScrollTabView setContentOffset:CGPointMake(_profileTabOutlet.frame.origin.x + _profileTabOutlet.frame.size.width, 0) animated:YES];
        _profileImageView.image = [UIImage imageNamed:@"profileUser.png"];
        _profileHighlightedView.hidden = YES;
        _questionHighlightedView.hidden = YES;
        _answerHighlightedView.hidden = YES;
        _followerHighlightedView.hidden = NO;
        //    _upvotesHighlightedView.hidden = YES;
        
        _questionTabelView.hidden = YES;
        _AnswerTableView.hidden = YES;
        _followerTableView.hidden = NO;
        
        _profileInfoTextView.hidden = YES;
        _profileTagView.hidden = YES;
        _profileTextView.hidden = YES;
        //    _upvotesTableView.hidden = YES;
        
        //    [_followerTableView reloadData];

    }
//    else
//    {
//        [self FollowListing];
//        
//        [_profileScrollTabView setContentOffset:CGPointMake(_profileTabOutlet.frame.origin.x + _profileTabOutlet.frame.size.width, 0) animated:YES];
//        _profileImageView.image = [UIImage imageNamed:@"profileUser.png"];
//        _profileHighlightedView.hidden = YES;
//        _questionHighlightedView.hidden = YES;
//        _answerHighlightedView.hidden = YES;
//        _followerHighlightedView.hidden = NO;
//        //    _upvotesHighlightedView.hidden = YES;
//        
//        _questionTabelView.hidden = YES;
//        _AnswerTableView.hidden = YES;
//        _followerTableView.hidden = NO;
//        
//        _profileInfoTextView.hidden = YES;
//        _profileTagView.hidden = YES;
//        _profileTextView.hidden = YES;
//        //    _upvotesTableView.hidden = YES;
//        
//        //    [_followerTableView reloadData];
//    }
    self.profileQuestionTableView.hidden = true;
    self.my_questionHighlightView.hidden = true;
}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

- (IBAction)followButtonAction:(id)sender
{
    if (ischanged == true)
    {
        ischanged = false;
    }
    else
    {
        ischanged = true;
    }
    
    if (isfollowerTrue == true)
    {
        isfollowerTrue = false;
         [self Unfollow];
//        [self.outletOfFollowBtn setImage:[UIImage imageNamed:@"unfollow.png"] forState:UIControlStateNormal];
    }
    else
    {
        isfollowerTrue = true;
        [self AddFollow];
//        [self.outletOfFollowBtn setImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)answerBtn:(id)sender
{
    NSLog(@"Answer btn Clicked");
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        picker.videoMaximumDuration = 30.0f;
        
        mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        
        picker.mediaTypes = mediaTypes;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"No Camera in this Device" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (IBAction)EditButtonClicked:(id)sender
{
//    EditProfileViewController *EditProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
//    
//    
//    EditProfile.blurImage = _blurImgProfile.image;
//    
//    [self presentViewController:EditProfile animated:YES completion:nil];
    
}
- (IBAction)BtnMyQuestions:(id)sender
{
//    [self questionListingMethod];
    
    if (isA3Advisor == false)
    {
        [_profileScrollTabView setContentOffset:CGPointMake(_profileTabOutlet.frame.origin.x + _profileTabOutlet.frame.size.width, 0) animated:YES];
        _profileImageView.image = [UIImage imageNamed:@"profileUser.png"];
        _profileHighlightedView.hidden = YES;
        _questionHighlightedView.hidden = YES;
        _answerHighlightedView.hidden = YES;
        _followerHighlightedView.hidden = YES;
        _questionTabelView.hidden = YES;
        _AnswerTableView.hidden = YES;
        _followerTableView.hidden = YES;
        _profileInfoTextView.hidden = YES;
        _profileTagView.hidden = YES;
        _profileTextView.hidden = YES;
    }
    
    self.profileQuestionTableView.hidden = false;
    self.my_questionHighlightView.hidden = false;
    
}
- (IBAction)profileBtnThree:(id)sender
{
    _profileImageThreeTab.image = [UIImage imageNamed:@"profileUser1.png"];
    _profileHighlightViewThree.hidden = NO;
    _proadviceHighlightThree.hidden = YES;
    _answerHighlightedView.hidden = YES;
    _followHighlightThree.hidden = YES;
    //    _upvotesHighlightedView.hidden = YES;
    
    _questionTabelView.hidden = YES;
    _AnswerTableView.hidden = YES;
    _followerTableView.hidden = YES;
    //    _upvotesTableView.hidden = YES;
    
    _profileInfoTextView.hidden = NO;
    _profileTagView.hidden = NO;
    _profileTextView.hidden = NO;
    
    if (isA3Advisor == false)
    {
        _outletOfFollowBtn.hidden = true;
    }
    else
    {
        _outletOfFollowBtn.hidden = false;
    }
    
    self.profileQuestionTableView.hidden = true;
    self.my_questionHighlightView.hidden = true;
    
}
- (IBAction)proadviceBtnThree:(id)sender
{
    if (isA3Advisor == true)
    {
//        [self ProAdviceListing];
        
        _profileImageThreeTab.image = [UIImage imageNamed:@"profileUser.png"];
        _profileHighlightViewThree.hidden = YES;
        _proadviceHighlightThree.hidden = NO;
        _answerHighlightedView.hidden = YES;
        _followHighlightThree.hidden = YES;
        //    _upvotesHighlightedView.hidden = YES;
        _questionTabelView.hidden = NO;
        _AnswerTableView.hidden = YES;
        _followerTableView.hidden = YES;
        
        _profileInfoTextView.hidden = YES;
        _profileTagView.hidden = YES;
        _profileTextView.hidden = YES;
        
        [_profileScrollTabView setContentOffset:CGPointMake(_profileScrollTabView.frame.origin.x, 0) animated:YES];
    }
    self.profileQuestionTableView.hidden = true;
    self.my_questionHighlightView.hidden = true;

}
- (IBAction)followBtnThree:(id)sender
{
    if (isA3Advisor == true)
    {
//        [self FollowListing];
        
        [_profileScrollTabView setContentOffset:CGPointMake(_profileTabOutlet.frame.origin.x + _profileTabOutlet.frame.size.width, 0) animated:YES];
        _profileImageThreeTab.image = [UIImage imageNamed:@"profileUser.png"];
        _profileHighlightViewThree.hidden = YES;
        _proadviceHighlightThree.hidden = YES;
        _answerHighlightedView.hidden = YES;
        _followHighlightThree.hidden = NO;
        //    _upvotesHighlightedView.hidden = YES;
        
        _questionTabelView.hidden = YES;
        _AnswerTableView.hidden = YES;
        _followerTableView.hidden = NO;
        
        _profileInfoTextView.hidden = YES;
        _profileTagView.hidden = YES;
        _profileTextView.hidden = YES;
        //    _upvotesTableView.hidden = YES;
        
        //    [_followerTableView reloadData];
    }
    self.profileQuestionTableView.hidden = true;
    self.my_questionHighlightView.hidden = true;
}
- (IBAction)shareUnderProfileMyQues:(id)sender
{
//    NSLog(@"ShareBtn pressed fro sharing on social media %@ and %@",video_TitleUnderProfileMyQues,video_LinkUnderProfileMyQues);
//    
//    NSString *message_Title = video_TitleUnderProfileMyQues;
//    NSString *message_Link = video_LinkUnderProfileMyQues;
    
        NSString *text_msg = @"To view the full answer please download the talentedge app";
        NSString *talentedge_description = @"Welcome to the world of anytime anywhere learning and be the part of the learners community who are changing the way India learns.We ensure a journey full of knowledge and advancement. You can choose from the array of offering we have for our learners via out Direct to Device platform.";
    
    NSArray * shareItems = @[text_msg,talentedge_description];
    
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    
    [self presentViewController:avc animated:YES completion:nil];

}
@end