//
//  QuestionsCustomCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionsCustomCell : UITableViewCell

////////////profile question Tab/////////////////

@property (weak, nonatomic) IBOutlet UIImageView *questionImageView;
@property (weak, nonatomic) IBOutlet UILabel *questionHeadinglabel;
@property (weak, nonatomic) IBOutlet UILabel *questionTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *questionNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *questionPlayImage;


@end
