//
//  QuestionsCustomCell.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "QuestionsCustomCell.h"

@implementation QuestionsCustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        /////////////////////question Tab/////////////////////
        
        _questionImageView.frame = CGRectMake(15, _questionImageView.frame.origin.y ,130,_questionImageView.frame.size.height );
        
        _questionPlayImage.frame = CGRectMake(67, _questionPlayImage.frame.origin.y,_questionPlayImage.frame.size.height  , _questionPlayImage.frame.size.height );
        
        _questionHeadinglabel.frame = CGRectMake(_questionImageView.frame.origin.x + _questionImageView.frame.size.width + 10, 10 ,_questionHeadinglabel.frame.size.width,  _questionHeadinglabel.frame.size.height);
        [_questionHeadinglabel setFont:[UIFont systemFontOfSize:18]];
        
        _questionNameLbl.frame = CGRectMake(_questionImageView.frame.origin.x + _questionImageView.frame.size.width + 10,40,_questionNameLbl.frame.size.width, _questionHeadinglabel.frame.size.height);
        [_questionNameLbl setFont:[UIFont systemFontOfSize:20]];
        
        
        _questionTimeLbl.frame = CGRectMake(_questionNameLbl.frame.origin.x + _questionNameLbl.frame.size.width + 10, _questionNameLbl.frame.origin.y ,_questionTimeLbl.frame.size.width, _questionTimeLbl.frame.size.height);
        [_questionTimeLbl setFont:[UIFont systemFontOfSize:20]];
    }
    else
    {
        NSLog(@"IN ESLE IN QUESTION CUSTOM CELL");
    }

    // Configure the view for the selected state
}

@end
