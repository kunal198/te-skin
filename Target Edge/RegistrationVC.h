//
//  RegistrationVC.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogInWithEmailViewController.h"
#import "ServerConnection.h"
#define signUpAPI @"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Register"
#import "MBProgressHUD.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface RegistrationVC : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIAlertViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UIPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *signUpData;
    MBProgressHUD *HUD;
    UIImagePickerController *imgPicker;
    NSArray *paths;
    NSData *imageData;
    NSString *str;
    UIPopoverController *popover;
}
@property (weak, nonatomic) IBOutlet UIScrollView *registerationScrollView;
@property (weak, nonatomic) IBOutlet UIView *wrapperView;
@property (weak, nonatomic) IBOutlet UILabel *LabelOutlet;
@property (weak, nonatomic) IBOutlet UIButton *outletOfSubmitBtn;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *emailId;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *location;
@property (weak, nonatomic) IBOutlet UIImageView *firstNameHighlightedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *lastNameHighlightedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *locationHighlightedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *emailIdHighlightedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordHighlightedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *confirmPasswordHighlightedImageView;
@property (nonatomic) CGFloat _currentKeyboardHeight;
@property (weak, nonatomic) IBOutlet UIImageView *registerImage;
@property (weak, nonatomic) IBOutlet UITextField *userOrAdvisorTextField;
@property (weak, nonatomic) IBOutlet UIImageView *userOrAdvisorHighlightView;
- (IBAction)registerBtnImage:(id)sender;

- (IBAction)BackButton:(id)sender;

- (IBAction)submitButton:(id)sender;

// This method is used to receive the data which we get using post method.
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data;

// This method receives the error report in case of connection is not made to server.
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;

// This method is used to process the data after connection has made successfully.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
- (IBAction)wrapperBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *UserOrAdvisorTableView;


@end
