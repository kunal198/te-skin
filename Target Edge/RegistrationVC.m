//
//  RegistrationVC.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "RegistrationVC.h"

@interface RegistrationVC ()
{
    NSArray *selectionArray;
    NSString *choosenStr;
    int selectedIndex;
    BOOL selectedRow;
}
@end

@implementation RegistrationVC

- (void)viewDidLoad
{
    selectionArray = [NSArray arrayWithObjects:@"User",@"Advisor", nil];
    _UserOrAdvisorTableView.hidden = true;
    [super viewDidLoad];
    
    imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    self.registerImage.layer.cornerRadius =  self.registerImage.frame.size.width/2;
    self.registerImage.layer.borderWidth = 2.0;
    self.registerImage.layer.masksToBounds = YES;
    self.registerImage.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    _outletOfSubmitBtn.layer.borderWidth = 1.5f;
    _outletOfSubmitBtn.layer.borderColor = [[UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0]CGColor];

    [self placeholderColor];
    
    
    // Do any additional setup after loading the view.
}
- (void)doSomeFunkyStuff
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}

-(void)SignUp
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.view addSubview:HUD];
    
    signUpData = [[NSMutableArray alloc] init];
    
    NSData *imageBase64DataReg = [imageData base64EncodedDataWithOptions:0];
    
    NSString *imageBase64StringReg = [[NSString alloc] initWithData:imageBase64DataReg encoding:NSUTF8StringEncoding];
    
    NSString *postLengthReg = [NSString stringWithFormat:@"%lu", (unsigned long)[imageBase64StringReg length]];
    NSLog(@"postLengthReg is %@",postLengthReg);
    
    imageBase64StringReg = [imageBase64StringReg stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuff) onTarget:self withObject:nil animated:YES];
    NSString *post = [NSString stringWithFormat:@"FirstName=%@&LastName=%@&Email=%@&AuthenticateType=%@&Mobile=%@&Location=%@&ProfilePic=%@&ContentLength=%@&FileName=%@&ContentType=%@",_firstName.text ,_lastName.text,_emailId.text,@"1",_location.text,_password.text,imageBase64StringReg,postLengthReg,@"new.png",@"image/png"];
    NSLog(@"post is %@",post);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Register"]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
//    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (data)
         {
             id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             NSLog(@"result updateProfileData is %@",result);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                
                BOOL success = [[result objectForKey:@"result"] boolValue];
                
                //        NSString *errorAlert = [result objectForKey:@"false"];
                
                NSString *msgAlert = [result objectForKey:@"message"];
                
                NSLog(@"msgAlert is %@",msgAlert);
                
                NSDictionary *dataReg = [result valueForKey:@"Object"];
                
                NSLog(@"dataReg is %@",dataReg);
                
                NSArray *items = [msgAlert componentsSeparatedByString:@"~"];
                
                if(items.count>1)
                {
                    str = [items objectAtIndex:1];
                }
                
                if (success)
                {
                    NSLog(@"sign up in else part");
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:str delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
                    [alert show];
                    
                    signUpData = [dataReg valueForKey:@"details"];
                    
                    NSLog(@"signUpData is %@",signUpData);
                    
                    HUD.hidden = true;
                 
                }
                
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:str delegate:nil cancelButtonTitle:@"Ok"otherButtonTitles: nil];
                    [alert show];
                    HUD.hidden = true;
                    
                }
             });
         }
         else if (error)
             
             NSLog(@"%@",error);
         
     }];
}

#pragma mark - UITableView Data Sources and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [selectionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"UserOrAdvisorCell";
    UITableViewCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.textLabel.text = selectionArray[indexPath.row];
    
//    if(indexPath.row == 0)
//    {
//        if (selectedRow == true)
//        {
//            Cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            selectedRow = false;
//        }
//    }
//    else
//    {
//        Cell.accessoryType = UITableViewCellAccessoryNone;
//    }
       return Cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    choosenStr = selectionArray[indexPath.row];
    _userOrAdvisorTextField.text = choosenStr;
    
//    selectedIndex = indexPath.row;
//    selectedRow = true;
    
    _UserOrAdvisorTableView.hidden = true;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.firstName)
    {
        _firstNameHighlightedImageView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
    else if(textField == self.lastName)
    {
        _lastNameHighlightedImageView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
    else if(textField == self.password)
    {
        _passwordHighlightedImageView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
        
//        NSLog(@"_registerationScrollView.contentOffset.x is %f",_registerationScrollView.contentOffset.x);
//        NSLog(@"%f",__currentKeyboardHeight);
//        NSLog(@"password.frame.size.height is %f",_password.frame.size.height);
//        NSLog(@"_confirmPassword.frame.size.height is %f",_confirmPassword.frame.size.height);
        
        [UIView animateWithDuration:0.3 animations:^{
            _registerationScrollView.frame = CGRectMake(_registerationScrollView.frame.origin.x, -20.0f, _registerationScrollView.frame.size.width, _registerationScrollView.frame.size.height);
            
        }];
    }
    else if(textField == self.location)
    {
        _locationHighlightedImageView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
    else if (textField == self.userOrAdvisorTextField)
    {
        [self.userOrAdvisorTextField resignFirstResponder];
        _UserOrAdvisorTableView.hidden = false;
        _userOrAdvisorHighlightView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
        
        
//        [UIView animateWithDuration:0.3 animations:^{
//            _registerationScrollView.frame = CGRectMake(_registerationScrollView.frame.origin.x, -80.0f, _registerationScrollView.frame.size.width, _registerationScrollView.frame.size.height);
//            
//        }];
    }
    else
    {
        _emailIdHighlightedImageView.backgroundColor = [UIColor colorWithRed:225.0/255 green:110.0/255 blue:67.0/255 alpha:1.0];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _UserOrAdvisorTableView.hidden = true;
    
    if (textField == self.firstName)
    {
        _firstNameHighlightedImageView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
    else if(textField == self.lastName)
    {
        _lastNameHighlightedImageView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
    else if (textField == self.userOrAdvisorTextField)
    {
         _userOrAdvisorHighlightView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        
//        [UIView animateWithDuration:0.3 animations:^{
//            _registerationScrollView.frame = CGRectMake(_registerationScrollView.frame.origin.x, +80.0f, _registerationScrollView.frame.size.width, _registerationScrollView.frame.size.height);
//            
//        }];
    }
    else if(textField == self.password)
    {
        _passwordHighlightedImageView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
        
        [UIView animateWithDuration:0.3 animations:^{
            _registerationScrollView.frame = CGRectMake(_registerationScrollView.frame.origin.x, +20.0f, _registerationScrollView.frame.size.width, _registerationScrollView.frame.size.height);
            
        }];
        
    }
    else if(textField == self.location)
    {
        _locationHighlightedImageView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }
    else
    {
        _emailIdHighlightedImageView.backgroundColor = [UIColor colorWithRed:186.0/255 green:181.0/255 blue:181.0/255 alpha:1.0];
    }

}

- (void) keyboardWillShow:(NSNotification *)note
{
    __currentKeyboardHeight = 0.0f;
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
    
    __currentKeyboardHeight = kbSize.height;
    
    // move the view up by 30 pts
    CGRect frame = self.view.frame;
    frame.origin.y = -30;
}

- (void) keyboardDidHide:(NSNotification *)note
{
    __currentKeyboardHeight = 0.0f;
    // move the view back to the origin
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
}

-(void)placeholderColor
{
    [self.firstName setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.lastName setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.emailId setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.userOrAdvisorTextField setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

    [self.password setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

    [self.confirmPassword setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];

    [self.location setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];


    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        [_LabelOutlet setFont:[UIFont systemFontOfSize:25]];
        
        [_outletOfSubmitBtn.titleLabel setFont:[UIFont systemFontOfSize:25]];
        [_location setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
        [_confirmPassword setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
        [_password setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
        [_emailId setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];

        [_lastName setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
        [_firstName setFont:[UIFont fontWithName:@"HelveticaNeue" size:22.f]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_firstName resignFirstResponder ];
    [_lastName resignFirstResponder ];
    [_emailId resignFirstResponder ];
    [_password resignFirstResponder ];
    [_confirmPassword resignFirstResponder ];
    [_location resignFirstResponder];
   // [_userOrAdvisorTextField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}

- (IBAction)registerBtnImage:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil otherButtonTitles: @"Take a new photo",@"Choose from existing", nil];
    [actionSheet showInView:self.view];
//    UIImagePickerController *pickerController = [[UIImagePickerController alloc]init];
//    pickerController.delegate = self;
//    
//    [self presentViewController:pickerController animated:YES completion:nil];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        return;
    };
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    imagePicker.allowsEditing = YES;
    
    if (buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if (buttonIndex == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo
{
    self.registerImage.image = img;

    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"profileimage.png"];
    
    NSLog(@"savedImagePath is %@",savedImagePath);
    UIImage *image = img;
    imageData = UIImageJPEGRepresentation(image,0.0f);
    [imageData writeToFile:savedImagePath atomically:NO];
    
    [self dismissViewControllerAnimated:true completion:nil];
}


- (IBAction)BackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)checkingEmail:(NSString *)checkEmail
{
    NSLog(@"checking Email");
    BOOL var1 = true;
    
    
    if ([checkEmail containsString:@"@"])                                          // if string contains @
    {
        NSArray *myWords = [checkEmail componentsSeparatedByString:@"@"];
        
        //myWords is an array,separate strings by @ and keeping string into array
        
        for (int i = 0; i<[myWords count]; i++)                                    //checks string one by one
        {
            if ([myWords[i] isEqual: @""])  // if string is empty than return false and myWords[i] is a string
                
            {
                var1 = false;
            }
        }
        
        if (var1)
        {
            
            if ([myWords[[myWords count] - 1] containsString:@"."])
            {
                NSLog(@"working %@",myWords[[myWords count] - 1]);
                
                NSArray *myDotWords = [myWords[[myWords count] - 1] componentsSeparatedByString:@"."];
                
                for (int i = 0; i<[myDotWords count]; i++)
                {
                    if ([myDotWords[i] isEqual: @""])
                    {
                        var1 = false;
                    }
                }
            }
            else
            {
                var1 = false;
            }
        }
    }
    else
    {
        var1 = false;
    }
    
    
    return var1;
}

- (BOOL)strongPassword:(NSString *)yourText
{
    BOOL strongPwd = YES;
    
    if([yourText length] > 4)
        strongPwd = YES;
    
    return strongPwd;
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}



- (IBAction)submitButton:(id)sender
{
    if([_firstName.text isEqualToString:@""] || [_lastName.text isEqualToString:@""] || [_emailId.text isEqualToString:@""]||[_location.text isEqualToString:@""]||[_password.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"All text fields are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }
    else if ([_userOrAdvisorTextField.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Enter User/Advisor." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else  if (![self checkingEmail:_emailId.text] )
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Invalid Email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSLog(@"Sign Up");
        
        
         NSMutableDictionary *dicForCompare = [[NSMutableDictionary  alloc] init];
        
        NSLog(@"%@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allValues]);
        
           // [dicForCompare valueForKey:_emailId.text];
//            dicForCompare = [CheckingLoginDefaults stringForKey:@"keyToPasswordName"];
        
        dicForCompare = [[NSUserDefaults standardUserDefaults]valueForKey:_emailId.text];
        
        
        if (dicForCompare.count >0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"This emailid is already registered." delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            NSMutableDictionary *dic = [[NSMutableDictionary  alloc] init];
            
            [dic setObject:_firstName.text forKey:@"keyToFirstName"];
            [dic setObject:_lastName.text forKey:@"keyToLastName"];
            [dic setObject:_emailId.text forKey:@"keyToEmailIdName"];
            [dic setObject:_location.text forKey:@"keyToLocationName"];
            [dic setObject:_password.text forKey:@"keyToPasswordName"];
            [dic setObject:_userOrAdvisorTextField.text forKey:@"keyToUserOrAdvisor"];
            
            [[NSUserDefaults standardUserDefaults] setObject:dic forKey:_emailId.text];
            [[NSUserDefaults standardUserDefaults] synchronize];

            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Registration successful" delegate:self cancelButtonTitle:@"Ok"otherButtonTitles:nil];
            [alert show];
        }

    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
        _firstName.text = @"";
        _lastName.text = @"";
        _emailId.text = @"";
        _password.text = @"";
        _location.text = @"";

        LogInWithEmailViewController *LogInWithEmailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
        [self.navigationController pushViewController:LogInWithEmailViewController animated:YES];
        
//    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }
    else
    {
        NSLog(@"sfhsgcdshc");
    }
}

- (NSString *)mimeTypeForPath:(NSString *)path
{
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}


- (IBAction)wrapperBtn:(id)sender
{
    [self.view endEditing:YES];
}
@end
