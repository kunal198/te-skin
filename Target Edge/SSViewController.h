//
//  SSViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CustomCell.h"
#import "SelfieCategoryVC.h"
#import "A3ViewController.h"
#import "AskProViewController.h"
#import "LogInWithEmailViewController.h"
#import "MBProgressHUD.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSData+AES.h"
#import <CommonCrypto/CommonCryptor.h>
#import "NSData+AESCrypt.h"
#import <CommonCrypto/CommonKeyDerivation.h>
#import <Security/Security.h>
#import "GM_AES128_CTR.h"
#import "ProfileViewController.h"
#import "NSData+AESCrypt.h"
#import <CommonCrypto/CommonCryptor.h>
#import "LoginVC.h"
#import "MBProgressHUD.h"

//#import "OAuthLoginView.h"


@interface SSViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>
{
    NSMutableArray *itemList;
    NSString *dataForSS;
    MBProgressHUD *HUD;
   
    NSMutableString *sbuf;
    
}

@property (weak, nonatomic) IBOutlet UILabel *SSLbl;
@property (weak, nonatomic) IBOutlet UITableView *objOfSSTableView;
@property (weak, nonatomic) IBOutlet UIView *SSHighlightedView;
@property (weak, nonatomic) IBOutlet UIImageView *SSImageOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *A3ImageOutlet;
@property (readonly) NSUInteger length;
@property (readonly) const void *bytes NS_RETURNS_INNER_POINTER;


- (IBAction)btnSS:(id)sender;

- (IBAction)btnA3:(id)sender;

- (IBAction)infoBtn:(id)sender;



- (NSData *)AES128EncryptedDataWithKey:(NSString *)key iv:(NSString *)iv;
- (NSData *)AES128EncryptedDataWithKey:(NSString *)key;


-(NSMutableData*) EncryptAES: (NSString *) key;

@end
