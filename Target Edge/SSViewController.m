//
//  SSViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "SSViewController.h"


@interface SSViewController ()
{
    NSString *hexStr;
     NSMutableData *encrypted;

}

@end

@implementation SSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(isPushBool == true)
    {
        AskProViewController *ask = [self.storyboard instantiateViewControllerWithIdentifier:@"AskProViewController"];
        ask.fromApp = true;
        [self.navigationController pushViewController:ask animated:NO];
        isPushBool = false;
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [_SSLbl setFont:[UIFont systemFontOfSize:18]];
    }

    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"guide"] == NO)
    {
        [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(pushMethod:) userInfo:nil repeats:NO];
        
        self.SSHighlightedView.hidden = NO;

        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"pop"];
    }
    
    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"SSStillToCome"] == YES)
    {
        
    }
    
    self.SSHighlightedView.hidden = NO;
    
    self.navigationController.navigationBar.hidden = YES;
    
    _objOfSSTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
 
    // Do any additional setup after loading the view.
    
    itemList = [[NSMutableArray alloc] init];
    [itemList addObject:@"Are You A Go Getter"];
    [itemList addObject:@"Are You A Leader"];
    [itemList addObject:@"Know Yourself"];
    [itemList addObject:@"Selfie Scan"];
    [itemList addObject:@"Winning Attitude"];
    
    self.navigationItem.hidesBackButton = YES;

    /////////////////////////////////////// Encription Process /////////////////////////////////////////////////////////

}

-(void)pushMethod:(NSTimer *)timer
{
    AskProViewController *a3 = [self.storyboard instantiateViewControllerWithIdentifier:@"AskProViewController"];
    [self.navigationController pushViewController:a3 animated:YES];
}

- (NSData *)AES128EncryptedDataWithKey:(NSString *)key iv:(NSString *)iv
{
    return [self AES128Operation:kCCEncrypt key:key iv:iv];
}

- (NSData *)AES128Operation:(CCOperation)operation key:(NSString *)key iv:(NSString *)iv
{
    char keyPtr[kCCKeySizeAES128 + 1];
    bzero(keyPtr, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    char ivPtr[kCCBlockSizeAES128 + 1];
    bzero(ivPtr, sizeof(ivPtr));
    if (iv)
    {
        [iv getCString:ivPtr maxLength:sizeof(ivPtr) encoding:NSUTF8StringEncoding];
    }
    
    NSUInteger dataLength = [self length];
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(operation,
                                          kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr,
                                          kCCBlockSizeAES128,
                                          ivPtr,
                                          [self bytes],
                                          dataLength,
                                          buffer,
                                          bufferSize,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess)
    {
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    free(buffer);
    return nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([[NSUserDefaults standardUserDefaults ] boolForKey:@"pop"] == NO)
    {
        NSLog(@"success");
    }
    else
    {
        self.SSHighlightedView.hidden = YES;
        self.navigationItem.hidesBackButton = YES;
    }
 
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *strValue1 =    [defaults objectForKey:@"savedfirst_name"];
//    NSString *strValue2 =    [defaults objectForKey:@"savedlast_name"];
//    NSString *strValue3 =    [defaults objectForKey:@"savedlocation"];
//    NSString *strValue4 =    [defaults objectForKey:@"savedEmail"];
    
//    NSMutableDictionary *contentDictionary = [[NSMutableDictionary alloc]init];
//    [contentDictionary setValue:@"vishal_sharma2009@gmail.com" forKey:@"loginId"];
//    [contentDictionary setValue:@"vishal111" forKey:@"firstName"];
//    [contentDictionary setValue:@"sharma" forKey:@"lastName"];
//    [contentDictionary setValue:@"Mohali" forKey:@"city"];
//    [contentDictionary setValue:@"1992-01-16" forKey:@"dob"];
//    [contentDictionary setValue:@"123 ABC" forKey:@"state"];
//    [contentDictionary setValue:@"Male" forKey:@"gender"];
//    [contentDictionary setValue:@"India" forKey:@"country"];
//    
//    NSData *data = [NSJSONSerialization dataWithJSONObject:contentDictionary options:NSJSONWritingPrettyPrinted error:nil];
//    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    
//    NSLog(@"jsonStr is %@",jsonStr);
    
//    NSString *privateKey = @"CbddmBz6lmP47467";
//    
////    NSString *privateKey = @"";
//    
//    const char *ptr = [privateKey UTF8String];
//    
//    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
//    
//    CC_MD5(ptr, strlen(ptr), md5Buffer);
//    
//    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
//    
//    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
//        
//        [output appendFormat:@"%02x",md5Buffer[i]];
//    
//    NSLog(@"MD5 is %@",output);
//    
//    NSString *str_domain = @"Interest Test";
//
////    NSData *encryptedz = [self encryptString:str_domain withKey:output];
//
//    NSData *encryptedz = [GM_AES128_CTR encryptString:str_domain withKey:output];
////
//    NSLog(@"   encrypted DATA --> %@", encryptedz);
//    
//    NSString *decrypted = [GM_AES128_CTR decryptData:encryptedz withKey:output];
//    
//    NSLog(@"   decrypted -------> %@", decrypted);
//
////
//    hexStr = [[self class] stringWithHexBytes:encryptedz];
////
//////    hexStr = [[self class] dataForHex:encryptedz];
////   
////    
//    NSLog(@"hexStr is %@",hexStr);
//    
//    NSData *InputData = [str_domain dataUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"InputData is %@",InputData);
//    NSData *encryptedData = [self encryptString:str_domain withKey:output];
//    NSLog(@"encryptedData is %@",encryptedData);
//    
//    
}


-(NSData *)dataForHex:(NSString *)hex
{
    NSString *hexNoSpaces = [[[hex stringByReplacingOccurrencesOfString:@" " withString:@""]
                              stringByReplacingOccurrencesOfString:@"<" withString:@""]
                             stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    unsigned char whole_byte = 0;
    char byte_chars[3] = {'\0','\0','\0'};
    for (NSUInteger i = 0; i < [hexNoSpaces length] / 2; i++) {
        byte_chars[0] = (unsigned char) [hexNoSpaces characterAtIndex:(NSUInteger) (i * 2)];
        byte_chars[1] = (unsigned char) [hexNoSpaces characterAtIndex:(NSUInteger) (i * 2 + 1)];
        whole_byte = (unsigned char)strtol(byte_chars, NULL, 16);
        [data appendBytes:&whole_byte length:1];
    }
    return data;
}



- (NSString*)hexStringFromData:(NSData *)data
{
    unichar* hexChars = (unichar*)malloc(sizeof(unichar) * (data.length*2));
    unsigned char* bytes = (unsigned char*)data.bytes;
    for (NSUInteger i = 0; i < data.length; i++)
    {
        unichar c = bytes[i] / 16;
        if (c < 10) c += '0';
        else c += 'a' - 10;
        hexChars[i*2] = c;
        c = bytes[i] % 16;
        if (c < 10) c += '0';
        else c += 'a' - 10;
        hexChars[i*2+1] = c;
    }
    NSString* retVal = [[NSString alloc] initWithCharactersNoCopy:hexChars length:data.length*2 freeWhenDone:YES];
    return retVal;
}


- (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key
{
    return [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKey:key];
    
}

- (void)doSomeFunkyStuffSS
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.SSLbl.text = [itemList objectAtIndex:indexPath.row];
    Cell.SSLbl.textAlignment = NSTextAlignmentCenter;
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    
    //[Cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelfieCategoryVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelfieCategoryVC"];
    vc.selectedSS = [itemList objectAtIndex:indexPath.row];
    
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)btnSS:(id)sender
{
    
}

- (IBAction)btnA3:(id)sender
{
    AskProViewController *ask = [self.storyboard instantiateViewControllerWithIdentifier:@"AskProViewController"];
    [self.navigationController pushViewController:ask animated:YES];
//    A3ViewController *a3 = [self.storyboard instantiateViewControllerWithIdentifier:@"A3ViewController"];
//    [self.navigationController pushViewController:a3 animated:YES];
}

- (IBAction)infoBtn:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@" Scan your personality, interests, skills and values. Assessment provides you a laser sharp insight regarding the career trajectory you should pursue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

+ (NSData *)AES128EncryptWithKey:(NSString *)key withData:(NSData*)_data
{
    // ‘key’ should be 16 bytes for AES128
    char keyPtr[kCCKeySizeAES128 + 1]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [_data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That’s why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCEncrypt, kCCAlgorithmAES128, kCCOptionECBMode | kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES128,
                                          NULL /* initialization vector (optional) */,
                                          [_data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesEncrypted );
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}

+ (NSString*) stringWithHexBytes:(NSData*)_data
{
    NSMutableString *stringBuffer = [NSMutableString stringWithCapacity:([_data length] * 2)];
    const unsigned char *dataBuffer = [_data bytes];
    int i;
    for (i = 0; i < [_data length]; ++i)
    {
        [stringBuffer appendFormat:@"%02lX", (unsigned long)dataBuffer[i]];
    }
    return [stringBuffer copy];
}



@end
