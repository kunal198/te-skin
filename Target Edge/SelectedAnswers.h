//
//  SelectedAnswers.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/24/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import <MediaPlayer/MediaPlayer.h>

@interface SelectedAnswers : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *selectedNameArray;
    NSArray *selectedUpvotesArray;
    NSArray *selectedTimeArray;
    NSArray *selectedAddressArray;
    NSArray *videoImages;
    NSArray *videoNamesArray;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableOfSelectedAnswerView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedMoocImages;
- (IBAction)moocDismissBtn:(id)sender;
- (IBAction)videoPlayBtn:(id)sender;

@end
