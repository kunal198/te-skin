//
//  SelectedAnswers.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/24/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "SelectedAnswers.h"

@interface SelectedAnswers ()

@end

@implementation SelectedAnswers

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableOfSelectedAnswerView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    
    selectedNameArray = [[NSArray alloc]initWithObjects:@"sheryll Mant,",@"Rick Martin,",@"James Dough,", nil];
    selectedTimeArray = [[NSArray alloc]initWithObjects:@"25 m",@"35 m",@"10 m", nil];
    selectedAddressArray = [[NSArray alloc]initWithObjects:@"VP,CoraFloats",@"CTO,Tech Mahindra",@"AVP,Infosys", nil];
    selectedUpvotesArray = [[NSArray alloc]initWithObjects:@"324",@"500",@"145", nil];
    videoImages = [[NSArray alloc]initWithObjects:@"VideoThumbnail1.png",@"VideoThumbnail1.png",@"VideoThumbnail1.png", nil];
   // videoNamesArray = [NSArray arrayWithObjects:@"", nil]
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSLog(@"videoImages %lu",(unsigned long)videoImages.count);
    return [videoImages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.hrViewName.text = [selectedNameArray objectAtIndex:indexPath.row];
    
    Cell.hrViewName.frame = CGRectMake(Cell.hrViewName.frame.origin.x, Cell.hrViewName.frame.origin.y, [Cell.hrViewName sizeThatFits:Cell.hrViewName.bounds.size].width, Cell.hrViewName.frame.size.height);
    
    Cell.viewUpvotes.text = [selectedUpvotesArray objectAtIndex:indexPath.row];
    
    Cell.viewUpvotes.frame = CGRectMake(Cell.hrViewName.frame.origin.x + [Cell.hrViewName sizeThatFits:Cell.hrViewName.bounds.size].width, Cell.viewUpvotes.frame.origin.y, Cell.viewUpvotes.frame.size.width, Cell.viewUpvotes.frame.size.height);
    Cell.upvotes.frame = CGRectMake(Cell.viewUpvotes.frame.origin.x + [Cell.viewUpvotes sizeThatFits:Cell.viewUpvotes.bounds.size].width, Cell.upvotes.frame.origin.y, Cell.upvotes.frame.size.width, Cell.upvotes.frame.size.height);
    Cell.viewTime.text = [selectedTimeArray objectAtIndex:indexPath.row];
    Cell.viewAddress.text = [selectedAddressArray objectAtIndex:indexPath.row];
    Cell.hrViewImage.image = [UIImage imageNamed:@"user.png"];
    
    NSLog(@"videoImages is %@",videoImages);
    
    Cell.videoImage.image = [UIImage imageNamed:videoImages[indexPath.row]];
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    
    [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    
    return Cell;
}




- (IBAction)moocDismissBtn:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)videoPlayBtn:(id)sender
{
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"HRServices" ofType:@"mp4"];
    
    NSLog(@"videoPath is %@",videoPath);
    
    NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
    
    NSLog(@"streamURL is %@",streamURL);
    
    MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:streamURL];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
    [videoPlayerView.moviePlayer play];
    

    
}
@end
