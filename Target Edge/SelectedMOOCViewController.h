//
//  SelectedMOOCViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "A3ViewController.h"
#import "ELCImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "AppDelegate.h"


@interface SelectedMOOCViewController : UIViewController<UITableViewDataSource,UITabBarDelegate,ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate>
{
    NSMutableArray *selectedArray;
    UIImage *thumbnail;
   }

- (IBAction)btnSelectedMoocDone:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *outletOfSelectedMoocTable;
@property (weak, nonatomic) IBOutlet UIButton *MOOCButtonOutlet;

@property(nonatomic,strong) NSString *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *headerTitle;
@property (nonatomic, assign) BOOL isSelectedEquityOne;
@property (nonatomic, assign) BOOL isSelectedEquityTwo;
@property (nonatomic, assign) BOOL isSelectedEquityThree;



@end
