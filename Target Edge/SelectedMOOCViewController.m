//
//  SelectedMOOCViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "SelectedMOOCViewController.h"

@interface SelectedMOOCViewController ()

@end

@implementation SelectedMOOCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        [_headerTitle setFont:[UIFont systemFontOfSize:27]];
        [_MOOCButtonOutlet.titleLabel setFont:[UIFont systemFontOfSize:27]];
        
        
    }
    
    self.outletOfSelectedMoocTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.headerTitle.text = self.titleLbl;
    
    [self FillArray];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)FillArray
{
    if (_isSelectedEquityOne == true)
    {
        
        
        
        selectedArray = [[NSMutableArray alloc]init];
        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  @"Career planning in private equity and Venture Capital",@"HeaderTitle",
                                  @"RohitBhayana.mp4",@"VideoLink",
                                  @"Rohit Bhayana",@"names",
                                  @"8:50 m",@"time",
                                  nil]];
        
        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  @"Career planning in private Equity",@"HeaderTitle",
                                  @"SandeepSinha.mp4",@"VideoLink",
                                  @"Sandeep Sinha",@"names",
                                  @"7:17 m",@"time",
                                  nil]];
    }
    else if (_isSelectedEquityTwo == true)
    {
        selectedArray = [[NSMutableArray alloc]init];
        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  @"Career planning in Branding and Advertisement",@"HeaderTitle",
                                  @"ShouvikRoy.mp4",@"VideoLink",
                                  @"Shouvik Roy",@"names",
                                  @"8:50 m",@"time",
                                  nil]];
//
//    }
//    else if (_isSelectedEquityThree == true)
//    {
        
        
    }
    else if (_isSelectedEquityThree == true)
    {
    
        selectedArray = [[NSMutableArray alloc]init];
        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  @"Career Option in Entrepreneurship",@"HeaderTitle",
                                  @"MaheshGupta.mp4",@"VideoLink",
                                  @"Mahesh Gupta",@"names",
                                  @"8:50 m",@"time",
                                  nil]];
        
    
//        selectedArray = [[NSMutableArray alloc]init];
//    
//    
//    
//    
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Martin",@"names",
//                                  @"25 m",@"time",
//                                  nil]];
//        
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Skills required for manufacturing Industry",@"HeaderTitle",
//                                  @"RohitBhayana.mp4",@"VideoLink",
//                                  @"Bob Cary",@"names",
//                                  @"20 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Martin",@"names",
//                                  @"25 m",@"time",
//                                  nil]];
//        
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Skills required for manufacturing Industry",@"HeaderTitle",
//                                  @"RohitBhayana.mp4",@"VideoLink",
//                                  @"Bob Cary",@"names",
//                                  @"20 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Martin",@"names",
//                                  @"25 m",@"time",
//                                  nil]];
//        
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Skills required for manufacturing Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Bob Cary",@"names",
//                                  @"20 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
//                                  @"RohitBhayana.mp4",@"VideoLink",
//                                  @"Martin",@"names",
//                                  @"25 m",@"time",
//                                  nil]];
//        
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Skills required for manufacturing Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Bob Cary",@"names",
//                                  @"20 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Skills required for manufacturing Industry",@"HeaderTitle",
//                                  @"RohitBhayana.mp4",@"VideoLink",
//                                  @"Bob Cary",@"names",
//                                  @"20 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Martin",@"names",
//                                  @"25 m",@"time",
//                                  nil]];
//        
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Skills required for manufacturing Industry",@"HeaderTitle",
//                                  @"RohitBhayana.mp4",@"VideoLink",
//                                  @"Bob Cary",@"names",
//                                  @"20 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Martin",@"names",
//                                  @"25 m",@"time",
//                                  nil]];
//        
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Skills required for manufacturing Industry",@"HeaderTitle",
//                                  @"MaheshGupta.mp4",@"VideoLink",
//                                  @"Bob Cary",@"names",
//                                  @"20 m",@"time",
//                                  nil]];
//        
        
        //    [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
        //                              @"Opportunities and Challenges in HR Industry",@"HeaderTitle",
        //                              @"HRServices.mp4",@"VideoLink",
        //                              @"Martin",@"names",
        //                              @"25 m",@"time",
        //                              nil]];
        //
        //    [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
        //                              @"Skills required for manufacturing Industry",@"HeaderTitle",
        //                              @"MarutiSuzuki.mp4",@"VideoLink",
        //                              @"Bob Cary",@"names",
        //                              @"20 m",@"time",
        //                              nil]];
        

        
        
    }
    else
    {
//        selectedArray = [[NSMutableArray alloc]init];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career Option in Entrepreneurship",@"HeaderTitle",
//                                  @"s.mp4",@"VideoLink",
//                                  @"Mahesh Gupta",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career planning in Branding and Advertisement",@"HeaderTitle",
//                                  @"tbi.mp4",@"VideoLink",
//                                  @"Shouvik Roy",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career Option in Entrepreneurship",@"HeaderTitle",
//                                  @"s.mp4",@"VideoLink",
//                                  @"Mahesh Gupta",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career planning in Branding and Advertisement",@"HeaderTitle",
//                                  @"tbi.mp4",@"VideoLink",
//                                  @"Shouvik Roy",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career Option in Entrepreneurship",@"HeaderTitle",
//                                  @"s.mp4",@"VideoLink",
//                                  @"Mahesh Gupta",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career planning in Branding and Advertisement",@"HeaderTitle",
//                                  @"tbi.mp4",@"VideoLink",
//                                  @"Shouvik Roy",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career Option in Entrepreneurship",@"HeaderTitle",
//                                  @"s.mp4",@"VideoLink",
//                                  @"Mahesh Gupta",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career planning in Branding and Advertisement",@"HeaderTitle",
//                                  @"tbi.mp4",@"VideoLink",
//                                  @"Shouvik Roy",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career Option in Entrepreneurship",@"HeaderTitle",
//                                  @"s.mp4",@"VideoLink",
//                                  @"Mahesh Gupta",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
//        [selectedArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                  @"Career planning in Branding and Advertisement",@"HeaderTitle",
//                                  @"tbi.mp4",@"VideoLink",
//                                  @"Shouvik Roy",@"names",
//                                  @"8:50 m",@"time",
//                                  nil]];
    }

    
}

-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
    NSLog(@"url is %@",url);
    
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

-(void)PlayVideo:(NSString*)videoName
{
    NSArray *dummyArray = [videoName componentsSeparatedByString:@"."];
    
    NSLog(@"dummyArray is %@",dummyArray);
    
    // NSArray *dummyArray = [[[selectedArray objectAtIndex:(int)[sender tag]-999] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:streamURL];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
        [videoPlayerView.moviePlayer play];
    }
}



#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [selectedArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.selectedMoocName.text = [[selectedArray objectAtIndex:indexPath.row] valueForKey:@"HeaderTitle"];
    
    [Cell.selectedMoocName sizeToFit];
    Cell.selectedMoocName.lineBreakMode = NSLineBreakByWordWrapping;
    Cell.answerLbl.numberOfLines = 2;
    
    Cell.selectedMoocAuthor.text = [[selectedArray objectAtIndex:indexPath.row] valueForKey:@"names"];
    Cell.selectedMoocTime.text = [[selectedArray objectAtIndex:indexPath.row] valueForKey:@"time"];
    
    /////
    NSArray *dummyArray = [[[selectedArray objectAtIndex:indexPath.row] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
    
    NSLog(@"videoPath is %@",videoPath);
    
    NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
    
    NSLog(@"streamURL is %@",streamURL);
    
    
    Cell.SelectedMoocImg.image = [self generateThumbImage:streamURL];
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    
    // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"videoPlayArray is %@",selectedArray);
    
    [self PlayVideo:[[selectedArray objectAtIndex:indexPath.row]valueForKey:@"VideoLink"]];
    
    
}


- (IBAction)btnSelectedMoocDone:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
