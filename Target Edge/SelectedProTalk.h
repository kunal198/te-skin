//
//  SelectedProTalk.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/1/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOOCViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "FilterTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <MediaPlayer/MediaPlayer.h>
#define MOOC_ListingAPI @"http://mobileapi.talentedge.hub1.co/api/MOOC/MOOCListing"
#define MOOC_IndustryAPI @"http://mobileapi.talentedge.hub1.co/api/MOOC/Industries_Listing"

@interface SelectedProTalk : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    NSMutableArray *colletionArray;
    NSMutableArray *CategoryArray;
    NSMutableArray *IndustryArray;
    NSMutableArray *staticArray;
    NSMutableArray *ThreestaticArray;
    NSMutableArray *SingleArray;
    NSMutableArray *FourArray;
    
    int i;
    UIImage *profile;
    UIButton *rightArrowBtn;
    UIButton *leftArrowBtn;
    int index;
    UIImage *thumbnail;
    BOOL isCategorySelected;
    NSMutableArray *SelectedDataObj;
    MBProgressHUD *HUD;
    NSString *selectedImg;
    MPMoviePlayerController *playBtnLinkSelectedPro;
    NSMutableArray *SelectedFilterObj;
    BOOL isSelectedRow;
    BOOL isALL;
    int selectedIndex;
    NSString *selectedIndustry;
    NSMutableArray *SelectedIndustryObj;
    NSString *strForAlert;
    BOOL isIndustrySelected;
    NSMutableArray *dummyArraySelected;
    BOOL isAllSeclected;
    BOOL isAfterSelected;
    NSMutableArray *industries_name;
    NSString *joinedString;
    BOOL isTidleSelected;
}
- (IBAction)filterBtn:(id)sender;
- (IBAction)EndFilter:(id)sender;
- (IBAction)submitBtn:(id)sender;

- (IBAction)backBtnProTalk:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *ProTalkScrollView;
@property (weak, nonatomic) IBOutlet UIView *filterView;

-(void)addContent:(NSMutableArray*)arrayObj;
-(void)addCategoryContent;
-(void)addIndustryContent;

-(UIImage *)generateThumbImage:(NSURL *)filepath;
- (IBAction)categoryBtn:(id)sender;
- (IBAction)industryBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *outletOfIndustry;
@property (weak, nonatomic) IBOutlet UIButton *outletOfCategory;

@property (weak, nonatomic) IBOutlet UIButton *outletOfSubmitBtn;
@property (weak, nonatomic) IBOutlet UIButton *outletOfFilterBtn;

@property(nonatomic,strong) NSString *selectedProTalk;
@property (weak, nonatomic) IBOutlet UILabel *selectedProTalkLbl;

-(void)SelectedPro;
-(void)FilterIndustryListing;
-(void)SelectedIndustryName;
-(void)UserInfoArray;
-(void)FillArray;
-(void)SingleArrayFillArray;
-(void)staticArrayFillArray;


- (IBAction)submitFiltrBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelFilterOutlet;
- (IBAction)CancelFilterBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submitBtnOutlet;

@property (weak, nonatomic) IBOutlet UIView *backFilterationView;
@property (weak, nonatomic) IBOutlet UITableView *filterTableView;
@property (strong,nonatomic) NSMutableArray *arrayOfIndexes;
- (IBAction)clearBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *clearBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *shareOnProTalkLbl;
- (IBAction)shareBtnProTalk:(id)sender;
- (IBAction)checkBoxBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *applyOutlet;
- (IBAction)applyBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *checkboxImageView;

@end
