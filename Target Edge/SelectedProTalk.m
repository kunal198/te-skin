//
//  SelectedProTalk.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 1/1/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "SelectedProTalk.h"

@interface SelectedProTalk ()
{
    int j;
    BOOL isWorking;
    NSMutableArray *arr;
    
}

@end

@implementation SelectedProTalk
@synthesize selectedProTalk,selectedProTalkLbl,arrayOfIndexes;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    SelectedFilterObj = [[NSMutableArray alloc] initWithObjects:@"Video 1",@"Video 2",@"Video 3",@"Video 4",@"Video 5", nil];
    
    if ([selectedProTalkLbl.text isEqualToString:@"Skills & Competencies"])
    {
        self.shareOnProTalkLbl.hidden = true;
        self.outletOfFilterBtn.hidden = true;
    }
    else if ([selectedProTalkLbl.text isEqualToString:@"Interview Tips"])
    {
        self.shareOnProTalkLbl.hidden = true;
        self.outletOfFilterBtn.hidden = true;
    }
    
    dummyArraySelected = [[NSMutableArray alloc] init];
    selectedProTalkLbl.text = passCategoryName;
    
    NSLog(@"selectedProTalkLbl.text is %@ %@",selectedProTalkLbl.text,passCategoryName);
    
 
    self.clearBtnOutlet.hidden = true;
    
    _filterView.layer.cornerRadius = 6;
    _filterView.layer.masksToBounds = YES;
    
    arrayOfIndexes = [NSMutableArray new];
//    self.filterView.layer.cornerRadius = 10;
    self.filterView.layer.masksToBounds = YES;
    //self.filterView.layer.borderColor = [[UIColor grayColor] CGColor];
   // self.filterView.layer.borderWidth = 5.0;
    
    self.backFilterationView.hidden = YES;
    
    [self.outletOfCategory setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateSelected];
    [self.outletOfIndustry setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateSelected];
    
    self.outletOfSubmitBtn.layer.cornerRadius = 8.0;
    self.outletOfSubmitBtn.layer.borderWidth = 2.0;
    self.outletOfSubmitBtn.layer.masksToBounds = YES;
    self.outletOfSubmitBtn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.outletOfIndustry.layer.cornerRadius = 6.0;
    self.outletOfIndustry.layer.borderWidth = 2.0;
    self.outletOfIndustry.layer.masksToBounds = YES;
    self.outletOfIndustry.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.outletOfCategory.layer.cornerRadius = 6.0;
    self.outletOfCategory.layer.borderWidth = 2.0;
    self.outletOfCategory.layer.masksToBounds = YES;
    self.outletOfCategory.layer.borderColor = [[UIColor lightGrayColor] CGColor];

     self.filterView.hidden = YES;
    isCategorySelected = YES;
    
    [self FillArray];
    [self FourArray];
    [self ThreeStaticCategory];
    [self staticArrayFillArray];
    [self SingleArrayFillArray];
    [self addContent:colletionArray];
    
//    [self SelectedPro];
    
    _outletOfSubmitBtn.layer.borderWidth = 1.5f;
    _outletOfSubmitBtn.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    _outletOfCategory.layer.borderWidth = 1.5f;
    _outletOfCategory.layer.borderColor = [[UIColor whiteColor]CGColor];
    
    _outletOfIndustry.layer.borderWidth = 1.5f;
    _outletOfIndustry.layer.borderColor = [[UIColor whiteColor]CGColor];

    // Do any additional setup after loading the view.
}

- (void) dealloc
{
    
    // [super dealloc]; //(provided by the compiler)
}

- (void)doSomeFunkyStuffSelected
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(50000000);
    }
}

-(void)SelectedPro
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffSelected) onTarget:self withObject:nil animated:YES];
    
    SelectedDataObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post;
    
    if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
    {
        passCategoryName = [passCategoryName stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        post = [NSString stringWithFormat:@"user_id=%@&CategoryName=%@&IndustryName=%@",LinkedIn_ID,passCategoryName,@""];
    }
    else
    {
        passCategoryName = [passCategoryName stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
       post = [NSString stringWithFormat:@"user_id=%@&CategoryName=%@&IndustryName=%@",Creater_User_ID,passCategoryName,@""];
    }

    NSLog(@"post in selectedProTalk is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:MOOC_ListingAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result SelectedPro is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data(selected_Mooc) is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strForAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     isIndustrySelected = false;
                     NSLog(@"SelectedPro loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     SelectedDataObj = [data1 valueForKey:@"videos"];
                     
                     NSLog(@"dataObj is %@",SelectedDataObj);
                     
                     DescriptionAbout = [SelectedDataObj valueForKey:@"Description"];

                     [self addContent:SelectedDataObj];
                      HUD.hidden = YES;
                 }
                 else
                 {
                     isIndustrySelected = false;
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:strForAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)SelectedIndustryName
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffSelected) onTarget:self withObject:nil animated:YES];
    
    SelectedIndustryObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *posts;
    
    if ([joinedString containsString:@"~"])
    {
        if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
        {
            joinedString = [joinedString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
             posts = [NSString stringWithFormat:@"user_id=%@&CategoryName=%@&IndustryName=%@",LinkedIn_ID,passCategoryName,joinedString];
        }
        else
        {
            joinedString = [joinedString stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
            NSLog(@"joinedString in api is %@",joinedString);
             posts = [NSString stringWithFormat:@"user_id=%@&CategoryName=%@&IndustryName=%@",Creater_User_ID,passCategoryName,joinedString];
        }
    }
    else
    {
        if (!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil))
        {
            selectedIndustry = [selectedIndustry stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
             posts = [NSString stringWithFormat:@"user_id=%@&CategoryName=%@&IndustryName=%@",LinkedIn_ID,passCategoryName,selectedIndustry];
        }
        else
        {
            selectedIndustry = [selectedIndustry stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
              posts = [NSString stringWithFormat:@"user_id=%@&CategoryName=%@&IndustryName=%@",Creater_User_ID,passCategoryName,selectedIndustry];
        }
    }

    NSLog(@"posts in selectedProTalk is %@",posts);
    NSData *postData = [posts dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:MOOC_ListingAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result SelectedPro Industry is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data(Industry) is %@",data1);
                 
                 NSArray *items = [errorAlert componentsSeparatedByString:@"~"];
                 
                 if(items.count>1)
                 {
                     strForAlert = [items objectAtIndex:1];
                 }
                 if (success)
                 {
                     isIndustrySelected = true;
                     
                     NSLog(@"SelectedPro Industry loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     SelectedIndustryObj = [data1 valueForKey:@"videos"];
                     
//                     SelectedIndustryObj = [data1 valueForKey:@"Indistries"];
                     
                    DescriptionAbout = [SelectedIndustryObj valueForKey:@"Description"];

                     [self addContent:SelectedIndustryObj];
                     HUD.hidden = YES;
                 }
                 else
                 {
                     isIndustrySelected = true;
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No videos to display." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     [self addContent:SelectedIndustryObj];
                     
                     HUD.hidden = YES;
                 }
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}


-(void)FilterIndustryListing
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffSelected) onTarget:self withObject:nil animated:YES];
    
    SelectedFilterObj = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    
    NSString *post = [NSString stringWithFormat:@"CategoryName=%@",@"Career Planning"];
    NSLog(@"post in selectedFilter is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:MOOC_IndustryAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result SelectedPro is %@",result);
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 NSLog(@"errorAlert is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 NSLog(@"data is %@",data1);
                 
                 if (success)
                 {
                     NSLog(@"SelectedFilterObj loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     SelectedFilterObj = [data1 valueForKey:@"Indistries"];
                     
                     industries_name = [SelectedFilterObj valueForKey:@"IndustryName"];
                     
                     NSLog(@"SelectedFilterObj is %@",SelectedFilterObj);
                    
                     [self.filterTableView reloadData];
                     
                     HUD.hidden = YES;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:errorAlert delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
                 
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

#pragma mark - Information

-(void)SingleArrayFillArray
{
    SingleArray = [[NSMutableArray alloc]init];
    [SingleArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                            @"Don't hire the best",@"HeaderTitle",
                            @"2",@"Answers",
                            @"ABHIJIT BHADURI",@"FooterTitle",
                            @"The Chief Learning Officer of Wipro Ltd",@"FooterDescription",
                            @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                            @"bhaduri.png",@"ProfileImage",
                            @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd. Has been part of companies like Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. Abhijit writes regularly for the Economic Times, People Matters and blogs for Times of India.",@"info",nil]];
    
}

-(void)staticArrayFillArray
{
    staticArray = [[NSMutableArray alloc]init];
    [staticArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"ABHIJIT BHADURI",@"FooterTitle",
                               @"The Chief Learning Officer of Wipro Ltd",@"FooterDescription",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               
                               @"bhaduri.png",@"ProfileImage",
                               @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd. Has been part of companies like Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. Abhijit writes regularly for the Economic Times, People Matters and blogs for Times of India.",@"info",nil]];
    
    [staticArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"PRABIR JHA",@"FooterTitle",
                               @"Global Chief People Officer of Cipla",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Prabir.png",@"ProfileImage",
                               @"Prabir Jha, an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. Prabir brings with him an impeccable academic and professional record.",@"info",nil]];
    
}

-(void)ThreeStaticCategory
{
    ThreestaticArray = [[NSMutableArray alloc]init];
    [ThreestaticArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"Problems in HR Industry",@"HeaderTitle",
                                 @"3",@"Answers",
                                 @"SRIPADA CHANDRASEKHAR ",@"FooterTitle",
                                 @"President and global head of HR at Dr. Reddy’s Laboratoires",@"FooterDescription",
                                 @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                                 @"Sripada.png",@"ProfileImage",@"Dr. Sripada Chandrarsekhar is the global head of HR at Dr. Reddy’s Laboratoires, Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors",@"info",nil]];
    
    [ThreestaticArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"Don't hire the best",@"HeaderTitle",
                                 @"2",@"Answers",
                                 @"VIKRAM BECTOR",@"FooterTitle",
                                 @"Chief Talent Officer at Reliance Industries",@"FooterDescription",
                                 @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                                 @"Vikram.png",@"ProfileImage",@"Vikram Bector - Chief Talent Officer at Reliance Industries Limited. Over two decades of experience with some leading Indian conglomerates.",@"info",nil]];
    [ThreestaticArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 @"Don't hire the best",@"HeaderTitle",
                                 @"2",@"Answers",
                                 @"Dr AQUIL BUSRAI",@"FooterTitle",
                                 @"Chief Executive Officer at aquil busrai consulting",@"FooterDescription",
                                 @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                                 @"Aquil.png",@"ProfileImage",@"Dr Aquil Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. Currently runs his own consulting firm.",@"info",nil]];
    
}

-(void)FourArray
{
    FourArray = [[NSMutableArray alloc]init];
    [FourArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"PRABIR JHA",@"FooterTitle",
                               @"Global Chief People Officer of Cipla",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Prabir.png",@"ProfileImage",
                               @"Prabir Jha, an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. Prabir brings with him an impeccable academic and professional record.",@"info",nil]];
    
    [FourArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"Dr AQUIL BUSRAI",@"FooterTitle",
                               @"Chief Executive Officer at aquil busrai consulting",@"FooterDescription",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Aquil.png",@"ProfileImage",@"Dr Aquil Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. Currently runs his own consulting firm.",@"info",nil]];
    
    [FourArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"SRIPADA CHANDRASEKHAR ",@"FooterTitle",
                               @"President and global head of HR at Dr. Reddy’s Laboratoires",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Sripada.png",@"ProfileImage",@"Dr. Sripada Chandrarsekhar is the global head of HR at Dr. Reddy’s Laboratoires, Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors",@"info",nil]];
    
    [FourArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"VIKRAM BECTOR",@"FooterTitle",
                               @"Chief Talent Officer at Reliance Industries",@"FooterDescription",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Vikram.png",@"ProfileImage",@"Vikram Bector - Chief Talent Officer at Reliance Industries Limited. Over two decades of experience with some leading Indian conglomerates.",@"info",nil]];
}

-(void)FillArray
{
    colletionArray = [[NSMutableArray alloc]init];
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"ABHIJIT BHADURI",@"FooterTitle",
                               @"The Chief Learning Officer of Wipro Ltd",@"FooterDescription",
                               @"AdvisorVoices - Janet Barr, Collaborative Financial Solutions.mp4",@"VideoLink",
                               
                               @"bhaduri.png",@"ProfileImage",
                               @"Abhijit Bhaduri works as the Chief Learning Officer of Wipro Ltd. Has been part of companies like Microsoft, PepsiCo & Colgate leading HR teams in India, Asia Pac and US. Abhijit writes regularly for the Economic Times, People Matters and blogs for Times of India.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"PRABIR JHA",@"FooterTitle",
                               @"Global Chief People Officer of Cipla",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Prabir.png",@"ProfileImage",
                               @"Prabir Jha, an alumnus of St. Stephen's College, Delhi and XLRI Jamshedpur, last assignment was with Reliance Industries Ltd. (RIL), as President & Group CHRO. Prabir brings with him an impeccable academic and professional record.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"Dr AQUIL BUSRAI",@"FooterTitle",
                               @"Chief Executive Officer at aquil busrai consulting",@"FooterDescription",
                               @"How to Get Rid of Acne - 3 Step Guide - Doctor's Advice.mp4",@"VideoLink",
                               @"Aquil.png",@"ProfileImage",@"Dr Aquil Busrai has had 42 odd years’ experience in Industry. He has worked in various HR roles with Unilever in Kenya and India and was Executive Director HR Motorola for Asia Pacific countries. Currently runs his own consulting firm.",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Problems in HR Industry",@"HeaderTitle",
                               @"3",@"Answers",
                               @"SRIPADA CHANDRASEKHAR ",@"FooterTitle",
                               @"President and global head of HR at Dr. Reddy’s Laboratoires",@"FooterDescription",
                               @"How To Take Care Of Your Hands At Home.mp4",@"VideoLink",
                               @"Sripada.png",@"ProfileImage",@"Dr. Sripada Chandrarsekhar is the global head of HR at Dr. Reddy’s Laboratoires, Chandra has over three decades of experience across India's leading firms in Public, Private and Multi-National sectors",@"info",nil]];
    
    [colletionArray addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               @"Don't hire the best",@"HeaderTitle",
                               @"2",@"Answers",
                               @"VIKRAM BECTOR",@"FooterTitle",
                               @"Chief Talent Officer at Reliance Industries",@"FooterDescription",
                               @"Regulatory inquiry - Submission process for Issues Paper - Dennis MacManus (Advisor) Sep 2013.mp4",@"VideoLink",
                               @"Vikram.png",@"ProfileImage",@"Vikram Bector - Chief Talent Officer at Reliance Industries Limited. Over two decades of experience with some leading Indian conglomerates.",@"info",nil]];
    
}


#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [SelectedFilterObj count] + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"FilterTableViewCell";
    FilterTableViewCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    Cell.checkboxImageView.tag = indexPath.row;
    
    if (indexPath.row == 0)
    {
         Cell.categoryName.text = @"All";
    
         Cell.checkboxImageView.image = [UIImage imageNamed:@"noTick.png"];
    }
    else
    {
        if (isWorking == false)
        {
            if ([dummyArraySelected containsObject:[SelectedFilterObj objectAtIndex:indexPath.row-1]])
            {
                Cell.checkboxImageView.image = [UIImage imageNamed:@"yesTick.png"];
            }
            else
            {
                Cell.checkboxImageView.image = [UIImage imageNamed:@"noTick.png"];
            }
            
            Cell.categoryName.text = [SelectedFilterObj objectAtIndex:indexPath.row-1];
            
            
            Cell.categoryName.numberOfLines = 2;
            
            Cell.layoutMargins = UIEdgeInsetsZero;
            Cell.preservesSuperviewLayoutMargins = false;
            // Cell.separatorInset = UIEdgeInsetsZero;
            tableView.separatorInset = UIEdgeInsetsZero;
            
            // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        }
    }

    if (isAllSeclected == true)
    {
        Cell.checkboxImageView.image = [UIImage imageNamed:@"yesTick.png"];
    }
    else if(isWorking == true)
    {
        Cell.checkboxImageView.image = [UIImage imageNamed:@"noTick.png"];
        [dummyArraySelected removeAllObjects];
    }
    
     return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    isSelectedRow = true;
    selectedIndex = (int)indexPath.row;
    
    FilterTableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
       if (selectedIndex == 0)
       {

           if (isAllSeclected == true)
           {
               isAllSeclected = false;
               isWorking = true;
           }
           else
           {
               isAllSeclected = true;
//               isWorking = true;
           }
           
       }
       else
       {
           if (isWorking == true)
           {
               isWorking = false;
           }
           
           if (isAllSeclected == true)
           {
               isAllSeclected = false;
               [dummyArraySelected removeAllObjects];
           }
        
           if ([dummyArraySelected containsObject:[SelectedFilterObj objectAtIndex:indexPath.row-1]])
           {
               NSLog(@"%ld",(long)indexPath.row);
               NSLog(@"dummyArraySelected %@",dummyArraySelected);
               
               [dummyArraySelected removeObject:[SelectedFilterObj objectAtIndex:indexPath.row-1]];
               selectedCell.checkboxImageView.image = [UIImage imageNamed:@"yesTick.png"];
           }
           else
           {
               [dummyArraySelected addObject:[SelectedFilterObj objectAtIndex:indexPath.row-1]];
               selectedCell.checkboxImageView.image = [UIImage imageNamed:@"noTick.png"];
               
               NSLog(@"%ld",(long)indexPath.row);
               NSLog(@"dummyArraySelected %@",dummyArraySelected);
           }
            selectedIndustry = [SelectedFilterObj objectAtIndex:indexPath.row -1];
           
           arr = [NSMutableArray arrayWithArray:dummyArraySelected];
           joinedString = [arr componentsJoinedByString:@"~"];
           
           NSLog(@"joinedString is %@ arr is %lu",joinedString,(unsigned long)arr.count);
           
       }
    
    
    [self.filterTableView reloadData];
}


#pragma Mark - Information of Users



-(void)addCategoryContent
{
        int noOfItems = (int)CategoryArray.count;
        
        NSLog(@"noOfItems is %i",noOfItems);
        
        [self.ProTalkScrollView setContentSize:(CGSizeMake(self.ProTalkScrollView.frame.size.width*(noOfItems), self.ProTalkScrollView.frame.size.height))];
        
        for(i = 0; i < noOfItems; i++)
        {
            UIView *mainViewWrapper = [[UIView alloc]initWithFrame:CGRectMake(i*self.ProTalkScrollView.frame.size.width, 0, self.ProTalkScrollView.frame.size.width, self.ProTalkScrollView.frame.size.height)];
            [mainViewWrapper setBackgroundColor:[UIColor clearColor]];
            
            [self.ProTalkScrollView addSubview:mainViewWrapper];
            
            UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, mainViewWrapper.frame.size.width - 20, mainViewWrapper.frame.size.height - 30)];
            [mainView setBackgroundColor:[UIColor clearColor]];
            mainView.layer.masksToBounds = true;
            mainView.layer.cornerRadius = 8;
            mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            mainView.layer.borderWidth = 0.8f;
            [mainViewWrapper addSubview:mainView];
            
            float headerFooterHeight = mainView.frame.size.height/4;
            
            NSLog(@"headerFooterHeight %f",headerFooterHeight);
            
            float width = mainView.frame.size.width;
            float height = mainView.frame.size.height - 150;
            
            NSLog(@"%f",height);
            
            float x = 0;      // here
            float y = 0;      // here
            float space = 10;
            
            
            UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, headerFooterHeight)];
            [headerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
            [mainView addSubview:headerView];
            
            float lblHeight = (headerView.frame.size.height - space*3)/3 ;
            
            UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(x, mainView.frame.size.height - headerFooterHeight, width, headerFooterHeight)];
            [footerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
            [mainView addSubview:footerView];
            
            profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(space,space/4,headerView.frame.size.height - (space*0.7),headerView.frame.size.height - (space*0.7))];
            [profileImage setBackgroundColor:[UIColor clearColor]];
            //[self.itemImageButton setImage:[UIImage imageNamed:stretchImage] forState:UIControlStateNormal];
            UIImage *profileImageName = [UIImage imageNamed:[[CategoryArray objectAtIndex:i] valueForKey:@"ProfileImage"]];
            profileImage.image = profileImageName;
            profileImage.layer.cornerRadius = profileImage.frame.size.width/2;
            profileImage.layer.borderWidth = 2.0;
            profileImage.layer.masksToBounds = YES;
            
            profileImage.layer.borderColor = [[UIColor whiteColor]CGColor];
            profileImage.contentMode = UIViewContentModeScaleAspectFit;
            
            
            profile = profileImage.image;
            [headerView addSubview:profileImage];
            
            ///profile image Button///
            
            
            UIButton *profileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            //            [profileBtn addTarget:self action:@selector(profileBtnClicked:)
            //                 forControlEvents:UIControlEventTouchUpInside];
            profileBtn.backgroundColor = [UIColor clearColor];
            //            profileBtn.tag = i+777;
            //[profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            //profileBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
            profileBtn.frame = CGRectMake(space,space,headerView.frame.size.height - (space*2),headerView.frame.size.height - (space*2));
            [headerView addSubview:profileBtn];
            
            UIImageView *voteImage = [[UIImageView alloc] initWithFrame:CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5)),lblHeight,footerView.frame.size.height - footerView.frame.size.height - (lblHeight+(space*1.5)),footerView.frame.size.height - (lblHeight+(space*2.5)))];
            
            UIImage *voteImageIcon = [UIImage imageNamed:@"tlt_upvotes1.png"];
            voteImage.image = voteImageIcon;
            [voteImage setBackgroundColor:[UIColor clearColor]];
            [headerView addSubview:voteImage];
            
            
            UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), space, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight + (space/2))];
            [titleLbl setTextColor:[UIColor darkGrayColor]];
            [titleLbl setText:[[CategoryArray objectAtIndex:i] valueForKey:@"FooterTitle"]];
            [titleLbl setBackgroundColor:[UIColor clearColor]];
            //[titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
            [headerView addSubview:titleLbl];
            
            
            UILabel *userNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), lblHeight+(space*1.5), width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
            userNameLbl.numberOfLines = 2;
            [userNameLbl setTextColor:[UIColor blackColor]];
            // [userNameLbl setTextColor:[UIColor colorWithRed:157.0f/255.0f green:157.0f/255.0f blue:157.0f/255.0f alpha:1]];
            
            [userNameLbl setText:[[CategoryArray objectAtIndex:i] valueForKey:@"FooterDescription"]];
            [userNameLbl setBackgroundColor:[UIColor clearColor]];
            //[userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
            [headerView addSubview:userNameLbl];
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                
                [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 20.0f]];
                [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 18.0f]];
                // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 20.0f]];
            }
            else
            {
                [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
                [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
                // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 14.0f]];
            }
            
            
            UIButton *but = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            //            [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            //[but setFrame:answerLbl.frame];
            but.tag = i +333;
            [but setExclusiveTouch:YES];
            [footerView addSubview:but];
            
            float combineHeightOfFooterHeader = mainView.frame.size.height - (headerView.frame.size.height);
            
            UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(x, headerView.frame.size.height, width, combineHeightOfFooterHeader)];
            [centerView setBackgroundColor:[UIColor clearColor]];
            [mainView addSubview:centerView];
            
            NSLog(@" main view %f and %f",centerView.frame.size.width, centerView.frame.size.height);
            
            UIImageView *centerImage = [[UIImageView alloc]initWithFrame:CGRectMake(x, 0 , width, combineHeightOfFooterHeader)];
            [centerImage setBackgroundColor:[UIColor clearColor]];
            
            
            /////////////////////
            NSArray *dummyArray = [[[CategoryArray objectAtIndex:i] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
            
            NSLog(@"videoPath is %@",videoPath);
            NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
            UIImage *staticImage = [self generateThumbImage:streamURL];
            centerImage.image = staticImage;
            
            [centerImage setContentMode: UIViewContentModeScaleAspectFit];
            [centerImage setBackgroundColor:[UIColor blackColor]];
            [centerView addSubview:centerImage];
            
            ////////////////////////
            
            NSLog(@"voteImage.frame.size.width is %f %f",voteImage.frame.size.width,centerView.frame.size.height);

            UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, centerView.frame.size.width, centerView.frame.size.height)];
            [playBtn setBackgroundColor:[UIColor clearColor]];
            playBtn.tag = i+999;
            //[playBtn addTarget:self action:@selector(playClicked:) forControlEvents:UIControlEventTouchUpInside];
            [centerView addSubview:playBtn];
            
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playbutton.png"]];
            
            img.frame = CGRectMake((centerView.frame.size.width - (centerView.frame.size.width/6)) / 2, (centerView.frame.size.height - (centerView.frame.size.width/6)) / 2, centerView.frame.size.width / 6, centerView.frame.size.width / 6);
            
            [centerView addSubview:img];

        }
}
-(void)addIndustryContent
{
            int noOfItems = (int)IndustryArray.count;
            
            NSLog(@"noOfItems is %i",noOfItems);
            
            [self.ProTalkScrollView setContentSize:(CGSizeMake(self.ProTalkScrollView.frame.size.width*(noOfItems), self.ProTalkScrollView.frame.size.height))];
            
            for(i = 0; i < noOfItems; i++)
            {
                UIView *mainViewWrapper = [[UIView alloc]initWithFrame:CGRectMake(i*self.ProTalkScrollView.frame.size.width, 0, self.ProTalkScrollView.frame.size.width, self.ProTalkScrollView.frame.size.height)];
                [mainViewWrapper setBackgroundColor:[UIColor clearColor]];
                
                [self.ProTalkScrollView addSubview:mainViewWrapper];
                
                UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, mainViewWrapper.frame.size.width - 20, mainViewWrapper.frame.size.height - 30)];
                [mainView setBackgroundColor:[UIColor clearColor]];
                mainView.layer.masksToBounds = true;
                mainView.layer.cornerRadius = 8;
                mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                mainView.layer.borderWidth = 0.8f;
                [mainViewWrapper addSubview:mainView];
                
                float headerFooterHeight = mainView.frame.size.height/4;
                
                NSLog(@"headerFooterHeight %f",headerFooterHeight);
                
                float width = mainView.frame.size.width;
                float height = mainView.frame.size.height - 150;
                
                NSLog(@"%f",height);
                
                float x = 0;      // here
                float y = 0;      // here
                float space = 10;
                
                
                UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, headerFooterHeight)];
                [headerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
                [mainView addSubview:headerView];
                
                float lblHeight = (headerView.frame.size.height - space*3)/3 ;
                
                
                UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(x, mainView.frame.size.height - headerFooterHeight, width, headerFooterHeight)];
                [footerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
                [mainView addSubview:footerView];
                
                profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(space,space/4,headerView.frame.size.height - (space*0.7),headerView.frame.size.height - (space*0.7))];
                [profileImage setBackgroundColor:[UIColor clearColor]];
                //[self.itemImageButton setImage:[UIImage imageNamed:stretchImage] forState:UIControlStateNormal];
                UIImage *profileImageName = [UIImage imageNamed:[[IndustryArray objectAtIndex:i] valueForKey:@"ProfileImage"]];
                profileImage.image = profileImageName;
                profileImage.layer.cornerRadius = profileImage.frame.size.width/2;
                profileImage.layer.borderWidth = 2.0;
                profileImage.layer.masksToBounds = YES;
                
                profileImage.layer.borderColor = [[UIColor whiteColor]CGColor];
                profileImage.contentMode = UIViewContentModeScaleAspectFit;
                
                
                profile = profileImage.image;
                [headerView addSubview:profileImage];
                
                ///profile image Button///
                
                
                UIButton *profileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                //            [profileBtn addTarget:self action:@selector(profileBtnClicked:)
                //                 forControlEvents:UIControlEventTouchUpInside];
                profileBtn.backgroundColor = [UIColor clearColor];
                //            profileBtn.tag = i+777;
                //[profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                //profileBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
                profileBtn.frame = CGRectMake(space,space,headerView.frame.size.height - (space*2),headerView.frame.size.height - (space*2));
                [headerView addSubview:profileBtn];
                
                UIImageView *voteImage = [[UIImageView alloc] initWithFrame:CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5)),lblHeight,footerView.frame.size.height - footerView.frame.size.height - (lblHeight+(space*1.5)),footerView.frame.size.height - (lblHeight+(space*2.5)))];
                
                UIImage *voteImageIcon = [UIImage imageNamed:@"tlt_upvotes1.png"];
                voteImage.image = voteImageIcon;
                [voteImage setBackgroundColor:[UIColor clearColor]];
                [headerView addSubview:voteImage];
                
                
                UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), space, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight + (space/2))];
                [titleLbl setTextColor:[UIColor darkGrayColor]];
                [titleLbl setText:[[IndustryArray objectAtIndex:i] valueForKey:@"FooterTitle"]];
                [titleLbl setBackgroundColor:[UIColor clearColor]];
                //[titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
                [headerView addSubview:titleLbl];
                
                
                UILabel *userNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(profileImage.frame.size.width + (space*2), lblHeight+(space*1.5), width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
                userNameLbl.numberOfLines = 2;
                [userNameLbl setTextColor:[UIColor blackColor]];
                // [userNameLbl setTextColor:[UIColor colorWithRed:157.0f/255.0f green:157.0f/255.0f blue:157.0f/255.0f alpha:1]];
                
                [userNameLbl setText:[[IndustryArray objectAtIndex:i] valueForKey:@"FooterDescription"]];
                [userNameLbl setBackgroundColor:[UIColor clearColor]];
                //[userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
                [headerView addSubview:userNameLbl];
                
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                {
                    
                    [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 20.0f]];
                    [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 18.0f]];
                    // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 20.0f]];
                }
                else
                {
                    [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
                    [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
                    // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 14.0f]];
                }
                
                
                UIButton *but = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                //            [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                //[but setFrame:answerLbl.frame];
                but.tag = i +333;
                [but setExclusiveTouch:YES];
                [footerView addSubview:but];
                
                float combineHeightOfFooterHeader = mainView.frame.size.height - (headerView.frame.size.height);
                
                UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(x, headerView.frame.size.height, width, combineHeightOfFooterHeader)];
                [centerView setBackgroundColor:[UIColor clearColor]];
                [mainView addSubview:centerView];
                
                NSLog(@" main view %f and %f",centerView.frame.size.width, centerView.frame.size.height);
                
                UIImageView *centerImage = [[UIImageView alloc]initWithFrame:CGRectMake(x, 0 , width, combineHeightOfFooterHeader)];
                [centerImage setBackgroundColor:[UIColor clearColor]];
                 centerImage.contentMode = UIViewContentModeScaleAspectFit;
                
                NSArray *dummyArray = [[[IndustryArray objectAtIndex:i] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
                
                NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
                
                NSLog(@"videoPath is %@",videoPath);
                NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
                UIImage *staticImage = [self generateThumbImage:streamURL];
                centerImage.image = staticImage;
                
                //                    centerImage.image = [UIImage imageNamed:[[colletionArray objectAtIndex:i] valueForKey:@"ProfileImage"]];
                [centerImage setContentMode: UIViewContentModeScaleAspectFit];
                [centerImage setBackgroundColor:[UIColor blackColor]];
                [centerView addSubview:centerImage];
                
                NSLog(@"voteImage.frame.size.width is %f %f",voteImage.frame.size.width,centerView.frame.size.height);

                UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, centerView.frame.size.width, centerView.frame.size.height)];
                //   UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x + centerView.frame.size.width / 8 + (space/2), 0 , width - ((centerView.frame.size.width / 8 + (space/2))*2), combineHeightOfFooterHeader)];
                [playBtn setBackgroundColor:[UIColor clearColor]];
                //  UIImage *playImage = [UIImage imageNamed:@"play.png"];
                playBtn.tag = i+999;
                // [playBtn setBackgroundImage:playImage forState:UIControlStateNormal];
                //[playBtn setBackgroundColor:[UIColor redColor]];
                [playBtn addTarget:self action:@selector(playClicked:) forControlEvents:UIControlEventTouchUpInside];
                [centerView addSubview:playBtn];
                
                UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playbutton.png"]];
                
                img.frame = CGRectMake((centerView.frame.size.width - (centerView.frame.size.width/6)) / 2, (centerView.frame.size.height - (centerView.frame.size.width/6)) / 2, centerView.frame.size.width / 6, centerView.frame.size.width / 6);
                
                [centerView addSubview:img];
   
            }
}


-(void)addContent:(NSMutableArray*)arrayObj
{
    
    @autoreleasepool
    {
        for(UIView * subView in self.ProTalkScrollView.subviews ) // here write Name of you ScrollView.
        {
            
            [subView removeFromSuperview];
        }
        //
        int noOfItems = (int)arrayObj.count;
        
        NSLog(@"noOfItems is %i",noOfItems);
        
        [self.ProTalkScrollView setContentSize:(CGSizeMake(self.ProTalkScrollView.frame.size.width*(noOfItems), self.ProTalkScrollView.frame.size.height))];
        
        for(i = 0; i < noOfItems; i++)
        {
            UIView *mainViewWrapper = [[UIView alloc]initWithFrame:CGRectMake(i*self.ProTalkScrollView.frame.size.width, 0, self.ProTalkScrollView.frame.size.width, self.ProTalkScrollView.frame.size.height)];
            [mainViewWrapper setBackgroundColor:[UIColor clearColor]];
            
            [self.ProTalkScrollView addSubview:mainViewWrapper];
            
            UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, mainViewWrapper.frame.size.width - 20, mainViewWrapper.frame.size.height - 30)];
            [mainView setBackgroundColor:[UIColor clearColor]];
            mainView.layer.masksToBounds = true;
            mainView.layer.cornerRadius = 8;
            mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            mainView.layer.borderWidth = 0.8f;
            [mainViewWrapper addSubview:mainView];
            
            float headerFooterHeight = mainView.frame.size.height/4;
            
            //            NSLog(@"headerFooterHeight %f",headerFooterHeight);
            
            float width = mainView.frame.size.width;
            float height = mainView.frame.size.height - 150;
            
            //            NSLog(@"%f",height);
            
            float x = 0;      // here
            float y = 0;      // here
            float space = 10;
            
            
            UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, headerFooterHeight)];
            [headerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
            [mainView addSubview:headerView];
            
            float lblHeight = (headerView.frame.size.height - space*3)/3 ;
            
            UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(x, mainView.frame.size.height - headerFooterHeight, width, headerFooterHeight)];
            [footerView setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1]];
            [mainView addSubview:footerView];

            UIButton *profileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            //            [profileBtn addTarget:self action:@selector(profileBtnClicked:)
            //                 forControlEvents:UIControlEventTouchUpInside];
            profileBtn.backgroundColor = [UIColor clearColor];
            //            profileBtn.tag = i+777;
            //[profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            //profileBtn.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
            profileBtn.frame = CGRectMake(space,space,headerView.frame.size.height - (space*2),headerView.frame.size.height - (space*2));
            [headerView addSubview:profileBtn];
            
            UIImageView *voteImage = [[UIImageView alloc] initWithFrame:CGRectMake(footerView.frame.size.width - (footerView.frame.size.height - (space*5)),lblHeight,footerView.frame.size.height - footerView.frame.size.height - (lblHeight+(space*1.5)),footerView.frame.size.height - (lblHeight+(space*2.5)))];
            
            UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(space, space, width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight + (space/2))];
            [titleLbl setTextColor:[UIColor darkGrayColor]];
            [titleLbl setText:[[arrayObj objectAtIndex:i] valueForKey:@"FooterTitle"]];
            [titleLbl setBackgroundColor:[UIColor clearColor]];
            //[titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
            [headerView addSubview:titleLbl];
            
            
            UILabel *userNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(space, lblHeight+(space*1.5), width - ((profileImage.frame.size.width + (space*2)) + (voteImage.frame.size.width + (space*2))), lblHeight*3)];
            userNameLbl.numberOfLines = 2;
            [userNameLbl setTextColor:[UIColor blackColor]];
            userNameLbl.tag = i + 562;
            // [userNameLbl setTextColor:[UIColor colorWithRed:157.0f/255.0f green:157.0f/255.0f blue:157.0f/255.0f alpha:1]];
            
            [userNameLbl setText:[[arrayObj objectAtIndex:i] valueForKey:@"HeaderTitle"]];
            [userNameLbl setBackgroundColor:[UIColor clearColor]];
            //[userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
            [headerView addSubview:userNameLbl];
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                
                [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 20.0f]];
                [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 18.0f]];
                // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 20.0f]];
            }
            else
            {
                [titleLbl setFont:[UIFont fontWithName: @"Helvetica-Bold" size: 12.0f]];
                [userNameLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 11.0f]];
                // [answerLbl setFont:[UIFont fontWithName: @"Helvetica-Light" size: 14.0f]];
            }
            
            
            UIButton *but = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            //            [but addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            //[but setFrame:answerLbl.frame];
            but.tag = i +333;
            [but setExclusiveTouch:YES];
            [footerView addSubview:but];
            
            float combineHeightOfFooterHeader = mainView.frame.size.height - (headerView.frame.size.height + footerView.frame.size.height);
            
            UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(x, headerView.frame.size.height, width, combineHeightOfFooterHeader)];
            
            [centerView setBackgroundColor:[UIColor clearColor]];
            [mainView addSubview:centerView];
            
            //            NSLog(@" main view %f and %f",centerView.frame.size.width, centerView.frame.size.height);
            
            
            UIImageView *imgView = [[UIImageView alloc]init];
            //            imgView.frame = CGRectMake(centerView.frame.origin.x, centerView.frame.origin.y, centerView.frame.size.width, centerView.frame.size.height);
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            
            UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 0, centerView.frame.size.width, centerView.frame.size.height)];
            //   UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(x + centerView.frame.size.width / 8 + (space/2), 0 , width - ((centerView.frame.size.width / 8 + (space/2))*2), combineHeightOfFooterHeader)];
            [playBtn setBackgroundColor:[UIColor clearColor]];//DefaultBackground-1
    
            NSString *imageForMooc = [[arrayObj objectAtIndex:i] valueForKey:@"ProfileImage"];
            
            
            
            /////////
            NSArray *dummyArray = [[[arrayObj objectAtIndex:i] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
            
            NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
            
            NSLog(@"videoPath is %@",videoPath);
            NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
            UIImage *staticImage = [self generateThumbImage:streamURL];
            
            //////////
            
            [playBtn setBackgroundImage:staticImage forState:UIControlStateNormal];
            playBtn.tag = i+999;
            //            [playBtn setBackgroundImage:imgView.image forState:UIControlStateNormal];
            [playBtn setBackgroundColor:[UIColor clearColor]];
            [playBtn addTarget:self action:@selector(playClickedSelectedPro:) forControlEvents:UIControlEventTouchUpInside];
            [centerView addSubview:playBtn];
            
            UIImageView *backImage = [[UIImageView alloc] initWithFrame:CGRectMake((centerView.frame.size.width - (centerView.frame.size.width/6)) / 2, (centerView.frame.size.height - (centerView.frame.size.width/6)) / 2, centerView.frame.size.width / 6, centerView.frame.size.width / 6)];
            backImage.image = [UIImage imageNamed:@"playbutton.png"];
            
            [centerView addSubview:backImage];
            

            UILabel *headerLbl = [[UILabel alloc]init];
            [headerLbl setTextColor:[UIColor darkGrayColor]];
            // [answerLbl setBackgroundColor:[UIColor clearColor]];
            [headerLbl setFont:[UIFont fontWithName: @"Helvetica Neue" size: 12.0f]];
            [headerLbl setText:[[arrayObj objectAtIndex:i] valueForKey:@"FooterDescription"]];
            CGSize answerFrame = [headerLbl sizeThatFits:headerLbl.bounds.size];
            headerLbl.numberOfLines = 4;
            headerLbl.frame = CGRectMake((footerView.frame.origin.x) + (space/2), (footerView.frame.size.height - answerFrame.height)/2 - (space*2.7), footerView.frame.size.width -(space/2),footerView.frame.size.height);

            NSLog(@"headerLbl.frame is %@", NSStringFromCGRect(headerLbl.frame));
            [footerView addSubview:headerLbl];
            
        }

    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)playClickedSelectedPro:(UIButton*)sender
{
    NSLog(@"Play clicked");
    
    NSArray *dummyArray = [[[colletionArray objectAtIndex:(int)[sender tag]-999] valueForKey:@"VideoLink"] componentsSeparatedByString:@"."];
    
    if(dummyArray.count != 0)
    {
        NSString *videoPath = [[NSBundle mainBundle] pathForResource:[dummyArray objectAtIndex:0] ofType:[dummyArray objectAtIndex:1]];
        
        NSLog(@"videoPath is %@",videoPath);
        
        NSURL *streamURL = [NSURL fileURLWithPath:videoPath];
        
        NSLog(@"streamURL is %@",streamURL);
        
        playBtnLinkSelectedPro = [[MPMoviePlayerController alloc]initWithContentURL:streamURL];
        //        moviePlayer,.controlStyle = MPMovieControlStyleNone;
        [self.view addSubview:playBtnLinkSelectedPro.view];
        playBtnLinkSelectedPro.fullscreen = YES;
        playBtnLinkSelectedPro.controlStyle = MPMovieControlStyleEmbedded;
        playBtnLinkSelectedPro.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        //        moviePlayer.view.layer.zPosition = 1;
        [playBtnLinkSelectedPro prepareToPlay];
        [playBtnLinkSelectedPro play];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackCompleteSelectedPro:) name:MPMoviePlayerPlaybackDidFinishNotification object:playBtnLinkSelectedPro];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonClickSelectedPro:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
        
    }
    else
    {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Alert!!" message:@"No video available" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
        CGFloat pageWidth = scrollView.frame.size.width; // you need to have a iVar with getter for scrollView
        float fractionalPage = scrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        index = (int)page;
        
        NSLog(@"index is %d",index);
}

- (void)aMoviePlaybackCompleteSelectedPro:(NSNotification*)notification
{
    [UIView animateWithDuration:0.7 delay:1.0 options: UIViewAnimationCurveEaseInOut
                     animations:^{
                         playBtnLinkSelectedPro.view.alpha = 0;
                     }completion:^(BOOL finished)
     {
          [playBtnLinkSelectedPro stop];
//         playBtnLinkSelectedPro = nil;
         [playBtnLinkSelectedPro.view removeFromSuperview];
        
     }];

}

-(void)doneButtonClickSelectedPro:(NSNotification*)aNotification
{
    NSNumber *reason = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    
    if ([reason intValue] == MPMovieFinishReasonUserExited)
    {
        NSLog(@"playBtnLinkSelectedPro done in if");
         [playBtnLinkSelectedPro stop];
//          playBtnLinkSelectedPro = nil;
        [playBtnLinkSelectedPro.view removeFromSuperview];
         playBtnLinkSelectedPro = nil;
       
    }
    else
    {
        NSLog(@"playBtnLinkSelectedPro done in else");
        [playBtnLinkSelectedPro stop];
//          playBtnLinkSelectedPro = nil;
        [playBtnLinkSelectedPro.view removeFromSuperview];
         playBtnLinkSelectedPro = nil;
    }
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aMoviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerView];
}


-(UIImage *)generateThumbImage:(NSURL *)filepath
{
    NSURL *url = filepath;
    
    //    NSLog(@"url is %@",url);
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    CMTime time = [asset duration];
    time.value = time.value *0.5;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    
    return thumbnail;
}

- (IBAction)categoryBtn:(id)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:false];
    }
    else
    {
        [sender setSelected:true];
    }
    [self.outletOfIndustry setSelected:false];

    
//    if(isCategorySelected == YES)
//    {
//        [self.outletOfCategory setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
//        [self.outletOfIndustry setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//        isCategorySelected = NO;
//    }
//    else
//    {
//        isCategorySelected = YES;
//        [self.outletOfCategory setImage:[UIImage imageNamed:@"no-icon.png"] forState:UIControlStateNormal];
//        [self.outletOfIndustry setImage:[UIImage imageNamed:@"1451656114_tick.png"] forState:UIControlStateNormal];
//    }
    
}

- (IBAction)industryBtn:(id)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:false];
    }
    else
    {
        [sender setSelected:true];
    }
    [self.outletOfCategory setSelected:false];
//    if(isCategorySelected == YES)
//    {
//        [self.outletOfCategory setImage:[UIImage imageNamed:@"1451656114_tick.png"] forState:UIControlStateNormal];
//        [self.outletOfIndustry setImage:[UIImage imageNamed:@"no-icon.png"] forState:UIControlStateNormal];
//        isCategorySelected = NO;
//    }
//    else
//    {
//        isCategorySelected = YES;
//        [self.outletOfCategory setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//        [self.outletOfIndustry setImage:[UIImage imageNamed:@"tick.png"] forState:UIControlStateNormal];
//    }
}

- (IBAction)filterBtn:(id)sender
{
//    [self FilterIndustryListing];

    self.filterView.hidden = NO;
    self.backFilterationView.hidden = NO;
    self.outletOfFilterBtn.hidden = YES;
    self.clearBtnOutlet.hidden = false;
}

- (IBAction)EndFilter:(id)sender
{
    self.filterView.hidden = YES;
     self.outletOfFilterBtn.hidden = NO;
}

- (IBAction)submitBtn:(id)sender
{
    
//    if ([self.outletOfIndustry isSelected])
//    {
//        [self FillIndustryArray];
//        [self addContent:IndustryArray];
//        self.filterView.hidden = YES;
//        self.outletOfFilterBtn.hidden = NO;
//    }
//    else if ([self.outletOfCategory isSelected])
//    {
//        [self FillCategoryArray];
//        [self addContent:CategoryArray];
//        self.filterView.hidden = YES;
//        self.outletOfFilterBtn.hidden = NO;
//
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select a category" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel",nil];
//        [alert show];
//        [self FillArray];
//        [self addContent:colletionArray];
//    }
//    
//  
////    [self.ProTalkScrollView setContentOffset:CGPointMake(0, self.ProTalkScrollView.contentSize.width)];

    
}

- (IBAction)backBtnProTalk:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"dcfhdsc");
    }
    else
    {
        self.filterView.hidden = YES;
        self.outletOfFilterBtn.hidden = NO;
    }
}

- (IBAction)submitFiltrBtn:(id)sender
{
    self.filterView.hidden = YES;
//    self.outletOfFilterBtn.hidden = YES;
    self.backFilterationView.hidden = YES;
}
- (IBAction)CancelFilterBtn:(id)sender
{
    self.filterView.hidden = YES;
//    self.outletOfFilterBtn.hidden = YES;
    self.backFilterationView.hidden = YES;
    
}
- (IBAction)clearBtn:(id)sender
{
    self.filterView.hidden = YES;
    self.outletOfFilterBtn.hidden = false;
    self.backFilterationView.hidden = YES;
    self.clearBtnOutlet.hidden = true;
//    self.outletOfFilterBtn.hidden = false;
}

- (IBAction)shareBtnProTalk:(id)sender
{
//    
//    if (isIndustrySelected == true)
//    {
//        if(SelectedIndustryObj.count == 0)
//        {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Nothing to share." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//            [alert show];
//
//        }
//        else
//        {
//            NSString *message_Title = [[SelectedIndustryObj objectAtIndex:index]valueForKey:@"Title"];
//            NSString *message_Link = [[SelectedIndustryObj objectAtIndex:index]valueForKey:@"video_link"];
//            
//            if ([message_Title isEqualToString:@""]||[message_Link isEqualToString:@""])
//            {
//                NSLog(@"hgdyad");
//            }
//            else
//            {                                                                                                                                                                                                     
    
                NSString *message_Title = @"Came across amazing knowledge bank on industry trends and career tips. Check it out. Download Talentedge App now!";
                NSArray * shareItems = @[message_Title];
                
                UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
                
                [self presentViewController:avc animated:YES completion:nil];
//            }
//
//        }
//    }
//    else
//    {
//
//        NSString *message_Title = [[SelectedDataObj objectAtIndex:index]valueForKey:@"Title"];
////        NSString *message_Title = @"sncjsdcn";
//        NSString *message_Link = [[SelectedDataObj objectAtIndex:index]valueForKey:@"video_link"];
//        
//        if ([message_Title isEqualToString:@""]||[message_Link isEqualToString:@""])
//        {
//            NSLog(@"hgdyad");
//        }
//        else
//        {
//            NSArray * shareItems = @[message_Title,message_Link];
//            
//            UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
//            
//            [self presentViewController:avc animated:YES completion:nil];
//        }
//    }
}

- (IBAction)checkBoxBtn:(id)sender
{
//    isSelectedRow = true;
//    selectedIndex = (int)[sender tag];
//    
//    if (selectedIndex == 0)
//    {
//        if (isSelectedRow == true)
//        {
//            [dummyArraySelected removeObject:@"0"];
//            self.checkboxImageView.image = [UIImage imageNamed:@"yes.png"];
//        }
//        else
//        {
//            [dummyArraySelected addObject:@"0"];
//           self.checkboxImageView.image = [UIImage imageNamed:@"no.png"];
//            
//        }
//    }
//    else
//    {
//        if (isSelectedRow == true)
//        {
//            [dummyArraySelected removeObject:[[SelectedFilterObj objectAtIndex:[sender tag]-1] valueForKey:@"Industry_ID"]];
//            self.checkboxImageView.image = [UIImage imageNamed:@"yes.png"];
//        }
//        else
//        {
//            [dummyArraySelected addObject:[[SelectedFilterObj objectAtIndex:[sender tag]-1] valueForKey:@"Industry_ID"]];
//            self.checkboxImageView.image = [UIImage imageNamed:@"no.png"];
//        }
//    }
//  
//
//    [self.filterTableView reloadData];
}
- (IBAction)applyBtn:(id)sender
{
    if (isAllSeclected == true || arr.count == SelectedFilterObj.count)
    {
        NSLog(@"All Done");
         [self addContent:colletionArray];
    }
    else if (arr.count == 4)
    {
        [self addContent:FourArray];
    }
    else if(arr.count == 3)
    {
        [self addContent:ThreestaticArray];
    }
    else if(arr.count == 2)
    {
        [self addContent:staticArray];
    }
    else if(arr.count == 1)
    {
        [self addContent:SingleArray];
    }

    
    self.filterView.hidden = YES;
    self.outletOfFilterBtn.hidden = false;
    self.backFilterationView.hidden = YES;
    self.clearBtnOutlet.hidden = true;

    
}
@end
