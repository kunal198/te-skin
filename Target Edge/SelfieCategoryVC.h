//
//  SelfieCategoryVC.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSViewController.h"
#import "MBProgressHUD.h"

@interface SelfieCategoryVC : UIViewController<MBProgressHUDDelegate,UIWebViewDelegate>
{
    MBProgressHUD *HUD;
     NSMutableArray *ss_Obj;
    NSString *stringWithoutSpaces;
}

-(void)loadTheSSSection;
-(void)ssMethod;
-(void)getCompanyCode;

- (IBAction)btnDone:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *selfieCategoryWebView;
@property (weak, nonatomic) IBOutlet UILabel *HeaderLbl;
@property (weak, nonatomic) IBOutlet UIButton *DoneButtonOutlet;

@property(strong,nonatomic) NSString *selectedSS;

@end
