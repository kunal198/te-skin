//
//  SelfieCategoryVC.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/20/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "SelfieCategoryVC.h"

@interface SelfieCategoryVC ()

@end

@implementation SelfieCategoryVC
@synthesize selectedSS;

- (void)viewDidLoad
{
    _selfieCategoryWebView.delegate = self;
    
    
    [super viewDidLoad];
    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        [_HeaderLbl setFont:[UIFont systemFontOfSize:25]];
        [_DoneButtonOutlet.titleLabel setFont:[UIFont systemFontOfSize:25]];
    }

    self.HeaderLbl.text = selectedSS;
    
    
//    NSString *fullURL = @"http://www.talentedge.in/#/";
//    NSURL *url = [NSURL URLWithString:fullURL];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    [_selfieCategoryWebView loadRequest:requestObj];
   
 
    // Do any additional setup after loading the view.
    
}


-(void)viewDidAppear:(BOOL)animated
{
    NSUserDefaults *p = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *newStrForchecking = [p stringForKey:@"dicLogin"];
    NSLog(@"newStrForchecking is please %@",newStrForchecking);
    
    if (newStrForchecking == NULL)
    {
        [self ssMethod];
    }
    else
    {
        [self getCompanyCode];
        
    }
}

-(void)loadTheSSSection
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Loading...";
    //    HUD.detailsLabelText = @"Just relax";
    // HUD.mode = MBProgressHUDModeDeterminate;
    [self.selfieCategoryWebView addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffSelfieScan) onTarget:self withObject:nil animated:YES];
    
    
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    // getting an NSString
//    NSString *pleaseUrl = [prefs stringForKey:@"urlForSS"];
    
    NSString *hopeStr = [stringWithoutSpaces stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSLog(@"hopeStr is %@",hopeStr);
    
    NSURL *url = [NSURL URLWithString:hopeStr];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    //    NSLog(@"%@ %@",url,urlRequest);
    [_selfieCategoryWebView loadRequest:urlRequest];
    [self.view addSubview:_selfieCategoryWebView];

}
-(void)ssMethod
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strValue1 = [defaults objectForKey:@"savedfirst_name"];
    NSString *strValue2 = [defaults objectForKey:@"savedlast_name"];
    //    NSString *strValue3 =    [defaults objectForKey:@"savedlocation"];
    NSString *strValue4 = [defaults objectForKey:@"savedEmail"];
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    
    NSUserDefaults* defaultz = [NSUserDefaults standardUserDefaults];
    NSString *linked1 = [defaultz objectForKey:@"linkedInFirstName"];
    NSString *linked2 = [defaultz objectForKey:@"linkedInLastName"];
    NSString *linked3 = [defaultz objectForKey:@"linkedInEmailId"];
    
    ss_Obj = [[NSMutableArray alloc]init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    NSString *WheelBoxStr;
    
    if ((!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil)))
    {
        WheelBoxStr = [NSString stringWithFormat:@"http://beta.brstdev.com/26RL/TalentEdge/regis.php?loginId=%@&firstName=%@&lastName=%@&dob=1936-02-16&state=haryana&gender=male&country=India&city=hisar",linked3,linked1,linked2];
    }
    else
    {
        WheelBoxStr = [NSString stringWithFormat:@"http://beta.brstdev.com/26RL/TalentEdge/regis.php?loginId=%@&firstName=%@&lastName=%@&dob=1936-02-16&state=haryana&gender=male&country=India&city=hisar",strValue4,strValue1,strValue2];
    }
    
    NSLog(@"new is %@",WheelBoxStr);
    
    NSString *newString = [WheelBoxStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSLog(@"newString is %@",newString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:newString]];
    
    [request setHTTPMethod:@"POST"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:@"2eW5fbMNJQLTtMn" forHTTPHeaderField:@"accessToken"];
    //    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result in SS section is %@",result);
                 
                 NSDictionary *dicLogin = [result valueForKey:@"login_id"];
                 NSLog(@"dicLogin is %@",dicLogin);
                 
                 NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:dicLogin forKey:@"dicLogin"];
                 [defaults synchronize];
                 
                 [self getCompanyCode];
                 
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
}

-(void)getCompanyCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //    NSString *strValue3 =    [defaults objectForKey:@"savedlocation"];
    NSString *strValue4 =    [defaults objectForKey:@"savedEmail"];
    
    NSUserDefaults *defaultzzs = [NSUserDefaults standardUserDefaults];
    
    //    NSString *strValue3 =    [defaults objectForKey:@"savedlocation"];
    NSString *linked3 =    [defaultzzs objectForKey:@"linkedInEmailId"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    
    
    NSString *getcomapnyCdoeStr;
    if ((!([LinkedIn_ID isEqualToString:@""] || LinkedIn_ID == nil)))
    {
        getcomapnyCdoeStr = [NSString stringWithFormat:@"http://beta.brstdev.com/26RL/TalentEdge/response.php?loginId=%@&domain_name=Interest Test&test_name=%@&osr=OSR",linked3,selectedSS];
    }
    else
    {
        getcomapnyCdoeStr = [NSString stringWithFormat:@"http://beta.brstdev.com/26RL/TalentEdge/response.php?loginId=%@&domain_name=Interest Test&test_name=%@&osr=OSR",strValue4,selectedSS];
    }
    
    
http://beta.brstdev.com/26RL/TalentEdge/response.php?loginId=sachin.mittal.101.delhi@wheebox.com&domain_name=Interest%20Test&test_name=Selfie%20Scan&osr=OSR
    
    
    NSLog(@"new is %@",getcomapnyCdoeStr);
    
    NSString *newString = [getcomapnyCdoeStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSLog(@"newString is %@",newString);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:newString]];
    
    [request setHTTPMethod:@"POST"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:@"2eW5fbMNJQLTtMn" forHTTPHeaderField:@"accessToken"];
    //    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 NSLog(@"result in getcompany section is %@",result);
                 
                 NSDictionary *urlForSS = [result valueForKey:@"url"];
                 NSLog(@"urlForSS get company section  %@",urlForSS);
                 
                 NSString *convertedStr = [NSString stringWithFormat:@"%@", urlForSS];
                 
                 stringWithoutSpaces = [convertedStr stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
                 NSLog(@"stringWithoutSpaces is %@",stringWithoutSpaces);
                 
                 NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:urlForSS forKey:@"urlForSS"];
                 [defaults synchronize];
                 
                 
                  [self loadTheSSSection];
                 
                 
                 //                 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                 //                 // getting an NSString
                 //                 Creater_User_ID = [prefs stringForKey:@"saveUserID"];
                 //
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         });
     }];
    
    
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
    HUD.hidden = false;

}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    HUD.hidden = true;
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)doSomeFunkyStuffSelfieScan
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000000);
    }
}


- (IBAction)btnDone:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
