//
//  ServerConnection.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/21/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Admin.h"
#define signUpAPI @"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Register"

@interface ServerConnection : NSObject

-(void)SignUp:(NSString*)FirstName LastName:(NSString*)LastName Email:(NSString*)Email Mobile:(NSString*)Mobile;

@end
