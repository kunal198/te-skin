//
//  ServerConnection.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/21/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerConnection.h"

@implementation ServerConnection

-(void)SignUp:(NSString*)FirstName LastName:(NSString*)LastName Email:(NSString*)Email Mobile:(NSString*)Mobile
{
    
    NSString *post = [NSString stringWithFormat:@"FirstName=%@&LastName=%@&Email=%@&Mobile=%@",FirstName ,LastName,Email,Mobile];
    NSLog(@"post is %@ %@ %@ %@",FirstName,LastName,Email,Mobile);
    
    NSData *body = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:signUpAPI]];
    
    [request setHTTPMethod:@"POST"];
    [request addValue:@"APIKEY" forHTTPHeaderField:@APIKey];

    
    
    [NSURLConnection connectionWithRequest:request delegate:self];
    
    NSString *boundary = @"unique-consistent-string";
    
//    // set Content-Type in HTTP header
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *postData = [NSMutableData data];
    
    [postData appendData:body];
    
    // add params (all params are strings)
    [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add image data
//    if (imageData)
//    {
//        [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"imageFormKey"] dataUsingEncoding:NSUTF8StringEncoding]];
//        [postData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//        [postData appendData:imageData];
//        [postData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
//    
    [postData appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
    // setting the body of the post to the reqeust
    [request setHTTPBody:postData];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if(data.length > 0)
        {
            NSLog(@"Success");
        }
        else
        {
            NSLog(@"false");
        }
    }];
    
}





@end
