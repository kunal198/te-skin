//
//  SettingViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/21/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "A3ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>
#import "CustomCell.h"
#import "ViewController.h"
#import "ProfileViewController.h"
#import "ChangePasswordVC.h"
#import "EditProfileViewController.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define User_Profile_API @"http://mobileapi.talentedge.hub1.co/Api/profile/User_Profile_Details"
#define Logout_API @"http://mobileapi.talentedge.hub1.co/Api/Authenticate/Logout"
#import "AppSettingController.h"
#import "ContactTalenty.h"

extern  NSString *profileImageStr;
extern  NSString *fullName;
extern  NSString *userLocation;
extern  NSString *User_FirstName;
extern NSString *User_LastName;
extern NSString *User_About;
extern NSString *isFollowBtnVisible;
extern NSString *saveMobile;
@interface SettingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,MBProgressHUDDelegate>
{
    NSMutableArray *settingArray;
    NSMutableArray *imageSettingArray;
    BOOL isLogin;
    MBProgressHUD *HUD;
    NSMutableArray *userProfileData;
    NSMutableArray *logoutData;
    int b;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *settinfImageOutlet;
@property (weak, nonatomic) IBOutlet UILabel *UserNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *UserAddressLbl;
@property (weak, nonatomic) IBOutlet UIImageView *MOOCImageOutlet;

@property (weak, nonatomic) IBOutlet UIImageView *roundImage;

@property (weak, nonatomic) IBOutlet UIImageView *blurImage;
@property (weak, nonatomic) IBOutlet UIView *outletOfViewOverBlurImage;
@property (weak, nonatomic) IBOutlet UITableView *outletOfSettingTable;
@property(assign) BOOL isSettingPush;

- (IBAction)btnOrangeSetting:(id)sender;

- (IBAction)btnGrayMOOC:(id)sender;
- (IBAction)profileButton:(id)sender;

-(UIImage*)blurredImageWithImage:(UIImage *)sourceImage;
@property (weak, nonatomic) IBOutlet UIView *settingLoaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *settingActivityindicator;
@property (weak, nonatomic) IBOutlet UIView *hiddingWhileLoading;

-(void)logoutMethod;
-(void)UserProfile;
-(void)clearNotificationsSetting;

@end
