//
//  SettingViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 11/21/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "SettingViewController.h"

NSString *profileImageStr;
NSString *fullName;
NSString *userLocation;
NSString *User_FirstName;
NSString *User_LastName;
NSString *User_About;
NSString *isFollowBtnVisible;
NSString *saveMobile;
@interface SettingViewController ()

@end

@implementation SettingViewController
@synthesize blurImage;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    b = 0;
    
    _hiddingWhileLoading.hidden = false;
    // Do any additional setup after loading the view.
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [_UserNameLbl setFont:[UIFont systemFontOfSize:20]];
        [_UserAddressLbl setFont:[UIFont systemFontOfSize:18]];
        _roundImage.frame = CGRectMake((self.view.frame.size.width/2)-(_roundImage.frame.size.height/2), _roundImage.frame.origin.y, _roundImage.frame.size.height, _roundImage.frame.size.height);
        
        _MOOCImageOutlet.frame = CGRectMake(_MOOCImageOutlet.frame.origin.x, 43, _MOOCImageOutlet.frame.size.width, 50);
        _settinfImageOutlet.frame = CGRectMake((self.view.frame.size.width/2)-(25) , 43, 50, 50);
         NSLog(@"self.view.Frame=%@", NSStringFromCGRect(_settinfImageOutlet.frame));
    }
     self.outletOfSettingTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    settingArray = [[NSMutableArray alloc]init];
    [settingArray addObject:@"Your Profile"];
//    [settingArray addObject:@"Edit Profile"];
//    [settingArray addObject:@"Discovery Preferences"];
    [settingArray addObject:@"App Settings"];
    [settingArray addObject:@"Invite Friends"];
    [settingArray addObject:@"Contact Talentedge"];
//    [settingArray addObject:@"Change Password"];
    [settingArray addObject:@"Logout"];
    
    imageSettingArray = [[NSMutableArray alloc]init];
    [imageSettingArray  addObject:[UIImage imageNamed:@"profileUser.png"]];
//     [imageSettingArray  addObject:[UIImage imageNamed:@"edit.png"]];
//    [imageSettingArray  addObject:[UIImage imageNamed:@"2_g.png"]];
    [imageSettingArray  addObject:[UIImage imageNamed:@"1_g.png"]];
    [imageSettingArray  addObject:[UIImage imageNamed:@"profile1.png"]];
    [imageSettingArray addObject:[UIImage imageNamed:@"tel.png"]];
//    [imageSettingArray addObject:[UIImage imageNamed:@"password-reset-icon.png"]];
    [imageSettingArray addObject:[UIImage imageNamed:@"logout"]];
    
   // [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadViewSetting) userInfo:NULL repeats:NO];
}

-(void)loadViewSetting
{
//    [self UserProfile];
    
     self.roundImage.image = [UIImage imageNamed:@"settingProfilePic.jpeg"];
     self.blurImage.image = [self blurredImageWithImage:[UIImage imageNamed:@"settingProfilePic.jpeg"]];
    
    self.roundImage.layer.backgroundColor = [[UIColor clearColor] CGColor];
    self.roundImage.layer.cornerRadius = self.roundImage.frame.size.height/2;
    self.roundImage.layer.borderWidth = 2.0;
    self.roundImage.layer.masksToBounds = YES;
    self.roundImage.layer.borderColor=[[UIColor clearColor] CGColor];
}
-(void)viewWillAppear:(BOOL)animated
{
    _hiddingWhileLoading.hidden = false;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadViewSetting) userInfo:NULL repeats:NO];
}


-(void)viewDidAppear:(BOOL)animated
{
    b = 0;
    _hiddingWhileLoading.hidden = true;
    
    if (_isSettingPush == true)
    {
        ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        [self presentViewController:profileVC animated:NO completion:nil];
        _isSettingPush = false;
        profileVC.showAdvisor = true;
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)doSomeFunkyStuffUserProfile
{
    float progress = 0.0;
    
    while (progress < 1.0)
    {
        progress += 0.01;
        HUD.progress = progress;
        usleep(500000);
    }
}

-(void)UserProfile
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];

    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffUserProfile) onTarget:self withObject:nil animated:YES];
    
    userProfileData = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue is %@",savedValue);
    NSString *post;
    
    if (!((LinkedIn_ID == nil)|| [LinkedIn_ID isEqualToString:@""]))
    {
        post = [NSString stringWithFormat:@"user_id=%@",LinkedIn_ID];
    }
    else
    {
        post = [NSString stringWithFormat:@"user_id=%@",Creater_User_ID];
    }
    
    NSLog(@"post in UserProfile is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:User_Profile_API]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
             if (data)
             {
                 id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                 
                 NSLog(@"result UserProfile is %@",result);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert in UserProfile is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                     
                 NSLog(@"data(UserProfile) is %@",data1);
                 
                if (success)
                 {
                     NSLog(@"UserProfile loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     userProfileData = [data1 valueForKey:@"details"];
                     
                     NSLog(@"userProfileData is %@",userProfileData);
                     
                     profileImageStr = [[userProfileData objectAtIndex:0] valueForKey:@"ProfileImageThumbnailPath"];
                     
                     NSLog(@"userProfileData is %@",[[userProfileData objectAtIndex:0] valueForKey:@"ProfileImageThumbnailPath"]);
                     
                     if (profileImageStr == (id)[NSNull null])
                     {
                         self.roundImage.image = [UIImage imageNamed:@"user_placeholder.png"];
                     }
                     else
                     {
                         [self.roundImage sd_setImageWithURL:[NSURL URLWithString:profileImageStr] placeholderImage:[UIImage imageNamed:@"user_placeholder.png"]];
                         NSURL *urlBlurr = [NSURL URLWithString:profileImageStr];
                         UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlBlurr]];
                         
                         self.blurImage.image = [self blurredImageWithImage:img];
                     }
                     
                     User_FirstName = [[userProfileData objectAtIndex:0] valueForKey:@"FirstName"];
                     
                     User_LastName = [[userProfileData objectAtIndex:0] valueForKey:@"LastName"];
                     
                     self.UserNameLbl.text = [NSString stringWithFormat:@"%@ %@",User_FirstName,User_LastName];
                     
                     fullName = [NSString stringWithFormat:@"%@ %@",User_FirstName,User_LastName];
                     
                     self.UserAddressLbl.text = [[userProfileData objectAtIndex:0] valueForKey:@"Location"];
                     
                     userLocation = [[userProfileData objectAtIndex:0] valueForKey:@"Location"];
                     
                     User_About = [[userProfileData objectAtIndex:0] valueForKey:@"About"];
                     
                     saveEmail = [[userProfileData objectAtIndex:0] valueForKey:@"Email"];
                     
                     saveMobile = [[userProfileData objectAtIndex:0] valueForKey:@"MobileNumber"];                     
                     NSLog(@"saveEmail is %@",saveEmail);
                     
                     HUD.hidden = YES;
                     _hiddingWhileLoading.hidden = true;
                    
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
                     });
             }
             else if (error)
                 
                 NSLog(@"%@",error);
         
     }];
}
-(void)logoutMethod
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    Creater_User_ID = [prefs stringForKey:@"saveUserID"];
    
    NSUserDefaults *prefz = [NSUserDefaults standardUserDefaults];
    // saving an NSString
    LinkedIn_ID = [prefz stringForKey:@"SavedLinkedInUserID"];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    //    HUD.labelText = @"Doing funky stuff...";
    HUD.detailsLabelText = @"Loading...";
    //    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    [self.view addSubview:HUD];
    
    [HUD showWhileExecuting:@selector(doSomeFunkyStuffUserProfile) onTarget:self withObject:nil animated:YES];
    
    logoutData = [[NSMutableArray alloc]init];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"preferenceName"];
    NSLog(@"savedValue for Logout_API is %@",savedValue);
    
    NSString *post;
    
    if (!((LinkedIn_ID == nil)|| [LinkedIn_ID isEqualToString:@""]))
    {
        post = [NSString stringWithFormat:@"Email=%@&RoleName=%@",saveEmail,@"User"];
    }
    else
    {
       post = [NSString stringWithFormat:@"Email=%@&RoleName=%@",saveEmail,@"User"];
    }
    
   
    NSLog(@"post in Logout_API is %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:Logout_API]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:savedValue forHTTPHeaderField:@"Token"];
    [request setValue:@"123ABC890567" forHTTPHeaderField:@"APIKEY"];
    //        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (data)
         {
             id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             NSLog(@"result UserProfile is %@",result);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 BOOL success = [[result objectForKey:@"result"] boolValue];
                 
                 NSString *errorAlert = [result objectForKey:@"message"];
                 
                 NSLog(@"errorAlert in UserProfile is %@",errorAlert);
                 
                 NSDictionary *data1 = [result valueForKey:@"Object"];
                 
                 NSLog(@"data(logoutData) is %@",data1);
                 
                 
                 if (success)
                 {
                     NSLog(@"UserProfile loaded successfully");
                     
                     success = [[result valueForKey:@"result"]boolValue];
                     
                     logoutData = [data1 valueForKey:@"details"];
                     
                     NSLog(@"logoutData is %@",logoutData);
                     
                     LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                     [self.navigationController pushViewController:login animated:YES];
                     
                     [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"Login"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainUser"];
                     [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainLinkedIn"];
                     [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"SavedLinkedInUserID"];
                     [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"saveUserID"];
                     
                     [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"urlForSS"];
                     [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"dicLogin"];
                     
                     [[UIApplication sharedApplication] unregisterForRemoteNotifications];
                     
                     HUD.hidden = YES;
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                     [alert show];
                     
                     HUD.hidden = YES;
                 }
             });
         }
         else if (error)
             
             NSLog(@"%@",error);
         
     }];
}

-(UIImage*)blurredImageWithImage:(UIImage *)sourceImage
{
    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:15.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    //  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
    // up exactly to the bounds of our original image /
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *retVal = [UIImage imageWithCGImage:cgImage];
    return retVal;
}


#pragma mark - UITableView Delegate/DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [imageSettingArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    CustomCell *Cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Cell.lblOfSettingCell.text = [settingArray objectAtIndex:indexPath.row];
    Cell.imgOfSettingCell.image = imageSettingArray[indexPath.row];
    Cell.lblOfSettingCell.textAlignment = NSTextAlignmentCenter;
    
    Cell.layoutMargins = UIEdgeInsetsZero;
    Cell.preservesSuperviewLayoutMargins = false;
    // Cell.separatorInset = UIEdgeInsetsZero;
    tableView.separatorInset = UIEdgeInsetsZero;
    
   // [Cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return Cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (b == 0)
    {
        b++;
        
        if (indexPath.row == 0)
        {
            ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            [self presentViewController:profileVC animated:YES completion:nil];
            
            isA3Advisor = false;
        }
//        else if (indexPath.row == 1)
//        {
//            EditProfileViewController *EditProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
//            
//            //        EditProfile.blurImage = _blurImgProfile.image;
//            
//            [self presentViewController:EditProfile animated:YES completion:nil];
//            
//        }
        else if(indexPath.row == 1)
        {
            AppSettingController *app = [self.storyboard instantiateViewControllerWithIdentifier:@"AppSettingController"];
            [self.navigationController pushViewController:app animated:YES];
        }
        else if (indexPath.row == 2)
        {
            NSLog(@"ShareBtn pressed fro sharing on social media");
            
            NSString *talentedge_description = @"Hi, I just tried this cool Talent Edge app and got personal advise from industry leaders. You can also try it now, click on the link below to download:  http://linktodownloadtheapp.com";
            
            NSArray * shareItems = @[talentedge_description];
            
            UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
            
            [self presentViewController:avc animated:YES completion:nil];
            b = 0;
            
        }
        else if (indexPath.row == 3)
        {
            ContactTalenty *contact = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactTalenty"];
            [self.navigationController pushViewController:contact animated:YES];
        }
//        else if (indexPath.row == 5)
//        {
//            ChangePasswordVC *change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
//            [self.navigationController pushViewController:change animated:YES];
//            
//        }
        else if (indexPath.row == 4)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Do you want to logout?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil ];
            [alert show];
            [self clearNotificationsSetting];
            [[Pushbots sharedInstance] unregister];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
     b = 0;
    _hiddingWhileLoading.hidden = true;
    if (buttonIndex == 0)
    {
          _hiddingWhileLoading.hidden = true;
          b = 0;
    }
    
    if (buttonIndex == 1)
    {
        LoginVC *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:login animated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"loginAgainUser"];
    }
}

- (IBAction)btnOrangeSetting:(id)sender
{
    
}

- (IBAction)btnGrayMOOC:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];    
}

- (IBAction)profileButton:(id)sender
{
    
//    ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//    [self presentViewController:profileVC animated:YES completion:nil];
//    
//    isA3Advisor = false;
}

- (void) clearNotificationsSetting
{
    [[Pushbots sharedInstance] setBadgeCount:@"0"];
    [[Pushbots sharedInstance] resetBadgeCount];
    [[Pushbots sharedInstance] clearBadgeCount];
}

@end
