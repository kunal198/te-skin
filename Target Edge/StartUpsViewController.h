//
//  StartUpsViewController.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/25/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AskProViewController.h"

@interface StartUpsViewController : UIViewController

- (IBAction)backStartUpBtn:(id)sender;

@property(nonatomic,strong) NSString *startDisplay;
@property (weak, nonatomic) IBOutlet UILabel *startTitle;

@end
