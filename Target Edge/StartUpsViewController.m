//
//  StartUpsViewController.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/25/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "StartUpsViewController.h"

@interface StartUpsViewController ()

@end

@implementation StartUpsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.startTitle.text = self.startDisplay;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backStartUpBtn:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
