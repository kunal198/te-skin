//
//  TermsAndConditions.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditions : UIViewController
- (IBAction)backTermsAndConditions:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *textviewTErms;

@end
