//
//  TermsAndConditions.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 2/26/16.
//  Copyright © 2016 Mrinal Khullar. All rights reserved.
//

#import "TermsAndConditions.h"

@interface TermsAndConditions ()

@end

@implementation TermsAndConditions

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _textviewTErms.editable = NO;
    _textviewTErms.selectable = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backTermsAndConditions:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}
@end
