//
//  UpvotesCustomCell.h
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpvotesCustomCell : UITableViewCell

////////////////// profile upvotes Tab//////////////

@property (weak, nonatomic) IBOutlet UIImageView *upvotesImageView;
@property (weak, nonatomic) IBOutlet UILabel *upvotesNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *upvotesAddressLbl;

@end
