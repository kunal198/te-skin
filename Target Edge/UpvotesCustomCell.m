//
//  UpvotesCustomCell.m
//  Talent Edge
//
//  Created by Mrinal Khullar on 12/9/15.
//  Copyright © 2015 Mrinal Khullar. All rights reserved.
//

#import "UpvotesCustomCell.h"

@implementation UpvotesCustomCell

- (void)awakeFromNib
{
     _upvotesImageView.layer.cornerRadius = _upvotesImageView.frame.size.width/2;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        //////////////upvotes Tab/////////////////////
        
        
        _upvotesImageView.frame = CGRectMake(_upvotesImageView.frame.origin.x , _upvotesImageView.frame.origin.y, _upvotesImageView.frame.size.height, _upvotesImageView.frame.size.height);
        _upvotesAddressLbl.frame = CGRectMake(_upvotesImageView.frame.origin.x + _upvotesImageView.frame.size.width + 10 , _upvotesAddressLbl.frame.origin.y, _upvotesAddressLbl.frame.size.width, _upvotesAddressLbl.frame.size.height);
        _upvotesNameLbl.frame = CGRectMake(_upvotesImageView.frame.origin.x + _upvotesImageView.frame.size.width + 10 , _upvotesNameLbl.frame.origin.y, _upvotesNameLbl.frame.size.width, _upvotesNameLbl.frame.size.height);
        [_upvotesAddressLbl setFont:[UIFont systemFontOfSize:18]];
        [_upvotesNameLbl setFont:[UIFont systemFontOfSize:16]];
    }
    else
    {
        NSLog(@"ELSE PART OF UPVOTE CUSTOM CELL");
    }

    // Configure the view for the selected state
}

@end
