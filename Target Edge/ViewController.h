//
//  ViewController.h
//  Target Edge
//
//  Created by Mrinal Khullar on 11/19/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginVC.h"
#import "Admin.h"

@interface ViewController : UIViewController
{
    NSMutableArray *_imagesArray;
}

@property (weak, nonatomic) IBOutlet UIScrollView *objOfScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *objOfPageControl;
- (IBAction)changeImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *OutletOfBtnSkip;
- (IBAction)btnSkip:(id)sender;

@end

