//
//  ViewController.m
//  Target Edge
//
//  Created by Mrinal Khullar on 11/19/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize objOfPageControl,objOfScrollView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"1st");
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [_OutletOfBtnSkip.titleLabel setFont:[UIFont systemFontOfSize:22]];

        
//        _MOOCimageOutlet.frame = CGRectMake(_settingImageOutlet.frame.origin.x, _settingImageOutlet.frame.origin.y, 50, 50);
//        _MOOCimageOutlet.frame = CGRectMake(_MOOCimageOutlet.frame.origin.x, _MOOCimageOutlet.frame.origin.y, _MOOCimageOutlet.frame.size.width, 50);
    }
    
        self.navigationController.navigationBar.hidden = YES;
        // Do any additional setup after loading the view, typically from a nib.
        
        _imagesArray = [[NSMutableArray alloc]initWithObjects:@"screen2.png",@"screen1.png",@"screen3.png",@"screen7.png", nil];
        
        [self addContent];
        
        [self.view bringSubviewToFront:objOfPageControl];
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self automaticallyLogin];
    
    self.navigationController.navigationBar.hidden = YES;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Relogin"] == NO)
    {
        
      
        
//        strObj = [[NSBundle mainBundle]pathForResource:@"Official Slippery Slick Productions Intro" ofType:@"mp4"];
//        objOfmoviePlayer = [[MPMoviePlayerController alloc]initWithContentURL:[NSURL fileURLWithPath:strObj]];
//        objOfmoviePlayer.view.frame = _subView.frame;
//        
//        NSLog(@"%@",NSStringFromCGRect(_subView.frame));
//        
//        objOfmoviePlayer.scalingMode = MPMovieControlStyleFullscreen;
//        [_subView addSubview:objOfmoviePlayer.view];
//        
//        [objOfmoviePlayer play];
        
        
    }
    
    else
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Relogin"] == YES)
        {
           // isToPlay = NO;
            
           // [objOfmoviePlayer stop];
            
          //  [self goToTabBar];
            
            SSViewController *ss = [self.storyboard instantiateViewControllerWithIdentifier:@"SSViewController"];
            [self.navigationController pushViewController:ss animated:YES];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"Relogin"];
        }
        
    }
}






- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addContent
{
    int x = 0;
    
    for (int i = 0; i < _imagesArray.count; i++)
    {
        UIImageView *_scrollerImage = [[UIImageView alloc]init];
        _scrollerImage.frame = CGRectMake(x, 0, self.view.frame.size.width, self.view.frame.size.height);
        _scrollerImage.contentMode = UIViewContentModeScaleToFill;
        _scrollerImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[_imagesArray objectAtIndex:i]]];
        [objOfScrollView addSubview:_scrollerImage];
        
        x = x + self.view.frame.size.width;
    }
    
    [objOfScrollView setContentSize:CGSizeMake(x, self.view.frame.size.height)];
}

#pragma mark - ScrollView Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = objOfScrollView.frame.size.width; // you need to have a iVar with getter for scrollView
    float fractionalPage = objOfScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    objOfPageControl.currentPage = page; // you need to have a iVar with getter for pageControl

    if (page == 3)
    {
        _OutletOfBtnSkip.hidden = NO;
        
    }
    else
    {
        _OutletOfBtnSkip.hidden = YES;
        
    }
}

- (IBAction)changeImage:(id)sender
{
    CGFloat x = objOfPageControl.currentPage * objOfScrollView.frame.size.width;
    [objOfScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (IBAction)btnSkip:(id)sender
{
   // [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Relogin"];
    
    NSLog(@"btn clicked");
    
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"Relogin"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    LoginVC *_objLoginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:_objLoginVC animated:YES];
    
}

@end
