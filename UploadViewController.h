/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#import <UIKit/UIKit.h>
#import "ELCImagePickerController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "A3ViewController.h"
#import "MyProfileViewController.h"
#import "AppDelegate.h"
#import "PlusViewController.h"


@interface UploadViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, ELCImagePickerControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>
{
    NSData *runningData;
    NSString *runningStr;
    NSURL *runningUrl;
    A3ViewController *_obj_A3ViewController;
    MyProfileViewController *_obj_MyProfileController;
    AppDelegate *appDelegate;

}

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

- (IBAction)showAlertController:(id)sender;
-(void)CallAWS3ConnectionForAskQuestion:(NSURL*)FilePath :(NSString*)FileName;
-(void)CallAWS3ConnectionForMyProfile:(NSURL *)FilePath :(NSString *)FileName;
-(void)convertingDataIntoBitesForAskQuestion;
-(void)convertingDataIntoBitesForMyProfile;
-(void)convertingDataIntoBitesForImage;
-(void)CallAWS3ConnectionForImage:(NSURL*)FilePath :(NSString*)FileName;

@end

@interface UploadCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIProgressView *progressView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *label;

@end
